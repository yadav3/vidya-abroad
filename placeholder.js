[{
  name: "BSC",
  fullName: "Bachelor of Science",
  status: 1,
  universityId: 10,
  specializations: [
    {
      name: "Physics",
      fullName: "Bachelor of Science in Physics",
      status: 1,
      majorId: 2
    }, {
      name: "Biology",
      fullName: "Bachelor of Science in Biology",
      status: 1,
      majorId: 3
    }
  ]
}, {
  name: "BA",
  fullName: "Bachelor of Arts",
  status: 1,
  universityId: 11,
  specializations: [
    {
      name: "English",
      fullName: "Bachelor of Arts in English",
      status: 1,
      majorId: 2
    }, {
      name: "Writing",
      fullName: "Bachelor of Arts in Writing",
      status: 1,
      majorId: 5
    }
  ]
}]