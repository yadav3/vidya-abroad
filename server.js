const express = require("express");
const fileUpload = require("express-fileupload");
const https = require("https");
const mysql = require("mysql2");
const fs = require("fs");
var cacheService = require("express-api-cache");
var cache = cacheService.cache;
var cors = require("cors");
const { default: axios } = require("axios");

// Certificate
const privateKey = fs.readFileSync("/etc/letsencrypt/live/collegevidyaabroad.com/privkey.pem", "utf8");
const certificate = fs.readFileSync("/etc/letsencrypt/live/collegevidyaabroad.com/cert.pem", "utf8");
const ca = fs.readFileSync("/etc/letsencrypt/live/collegevidyaabroad.com/chain.pem", "utf8");

const credentials = {
  key: privateKey,
  cert: certificate,
  ca: ca,
};

const app = express();
app.use(cors());
app.use(express.json({ limit: "200mb" }));
app.use(express.urlencoded({ extended: true }));
app.use(fileUpload());

// app.use((_request, response, next) => {
//   response.setHeader("Access-Control-Allow-Origin", "*");
//   response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
//   response.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE, PUT, OPTIONS");
//   next();
// });

const conn = mysql.createPool({
  host: "localhost",
  user: "cv_abroad",
  password: "Tg98_2ry5",
  database: "cv_abroad",
});

conn.getConnection((error) => {
  if (error) throw error;
  console.log("Successfully connected to the database.");
});

const httpsServer = https.createServer(credentials, app);

app.get("/", (request, response) => {
  return response.status(200).send("Server is running");
});

// POST lead
app.post("/leads", (request, response) => {
  if (!request.body) {
    return response.status(400).send("Request has invalid format");
  }

  if (
    !request.body.gender ||
    request.body.gender === "" ||
    !request.body.name ||
    request.body.name === "" ||
    !request.body.email ||
    request.body.email === "" ||
    !request.body.phone ||
    request.body.phone === "" ||
    !request.body.country ||
    request.body.country === "" ||
    !request.body.dob ||
    request.body.dob === "" ||
    !request.body.course ||
    request.body.course === "" ||
    !request.body.specialization ||
    request.body.specialization === ""
  ) {
    return response.status(400).send("Request has invalid format");
  }

  const gender = request.body.gender;
  const name = request.body.name;
  const email = request.body.email;
  const phone = request.body.phone;
  const country = request.body.country;
  const dob = request.body.dob;
  const course = request.body.course;
  const specialization = request.body.specialization;

  const source_campaign = request.body.source_campaign;
  const campaign_name = request.body.campaign_name;
  const ad_group = request.body.ad_group;
  const ad_name = request.body.ad_name;

  let LSQ_DATA = [];

  if (source_campaign) {
    LSQ_DATA = [
      {
        Attribute: "SourceCampaign",
        Value: source_campaign,
      },
      {
        Attribute: "mx_Campaign",
        Value: campaign_name,
      },
      {
        Attribute: "mx_Ad_Group",
        Value: ad_group,
      },
      {
        Attribute: "mx_Ad_Name",
        Value: ad_name,
      },
      {
        Attribute: "FirstName",
        Value: name,
      },
      {
        Attribute: "EmailAddress",
        Value: email,
      },
      {
        Attribute: "Phone",
        Value: phone,
      },
      {
        Attribute: "mx_Course",
        Value: course,
      },
      {
        Attribute: "mx_Specialization",
        Value: specialization,
      },
      {
        Attribute: "mx_Country",
        Value: country,
      },
      {
        Attribute: "Source",
        Value: "College Vidya",
      },
      {
        Attribute: "mx_Sub_Source",
        Value: "Website",
      },
    ];
  } else {
    LSQ_DATA = [
      {
        Attribute: "FirstName",
        Value: name,
      },
      {
        Attribute: "EmailAddress",
        Value: email,
      },
      {
        Attribute: "Phone",
        Value: phone,
      },
      {
        Attribute: "mx_Course",
        Value: course,
      },
      {
        Attribute: "mx_Specialization",
        Value: specialization,
      },
      {
        Attribute: "mx_Country",
        Value: country,
      },
      {
        Attribute: "Source",
        Value: "College Vidya",
      },
      {
        Attribute: "mx_Sub_Source",
        Value: "Website",
      },
    ];
  }

  // LSQ API
  axios
    .post(
      "https://api-in21.leadsquared.com/v2/LeadManagement.svc/Lead.Create?accessKey=u$rf9f46642c28229077c6efdb5c83eac4b&secretKey=3e142a31200d7b54c9273158641a2cb02ab9a5f4",
      LSQ_DATA
    )
    .then(() => {
      console.log("LSQ Lead Added");
    })
    .catch((err) => {
      console.error(err);
    });

  conn.query(
    `INSERT into leads 
    (gender, name, email, phone, country, dob, course, source_campaign, campaign_name, ad_group, ad_name ) 
    VALUES
    (${conn.escape(gender)}, ${conn.escape(name)}, ${conn.escape(email)}, ${conn.escape(phone)}, ${conn.escape(country)}, ${conn.escape(
      dob
    )}, ${conn.escape(course)}, ${conn.escape(source_campaign)}, ${conn.escape(campaign_name)}, ${conn.escape(ad_group)}, ${conn.escape(ad_name)})`,
    (err, res) => {
      if (err) {
        return response.status(500).send(err);
      } else {
        return response.status(200).send("Lead Added");
      }
    }
  );
});

// POST Lead in Country Page
app.post("/leads/landingpage/country", (request, response) => {
  if (!request.body) {
    return response.status(400).send("Request has invalid format");
  }

  if (
    !request.body.name ||
    request.body.name === "" ||
    !request.body.email ||
    request.body.email === "" ||
    !request.body.phone ||
    request.body.phone === "" ||
    !request.body.city ||
    request.body.city === "" ||
    !request.body.country ||
    request.body.country === "" ||
    !request.body.course ||
    request.body.course === "" ||
    !request.body.specialization ||
    request.body.specialization === "" ||
    !request.body.source ||
    request.body.source === "" ||
    !request.body.lsq_source ||
    request.body.lsq_source === "" ||
    !request.body.lsq_sub_source ||
    request.body.lsq_sub_source === ""
  ) {
    return response.status(400).send("Request has invalid format");
  }

  const name = request.body.name;
  const email = request.body.email;
  const phone = request.body.phone;
  const city = request.body.city;
  const country = request.body.country;
  const course = request.body.course;
  const specialization = request.body.specialization;
  const source = request.body.source;

  const lsq_source = request.body.lsq_source;
  const lsq_sub_source = request.body.lsq_sub_source;
  const source_campaign = request.body.source_campaign;
  const campaign_name = request.body.campaign_name;
  const ad_group = request.body.ad_group;
  const ad_name = request.body.ad_name;

  let LSQ_DATA = [];

  if (source_campaign) {
    LSQ_DATA = [
      {
        Attribute: "SourceCampaign",
        Value: source_campaign,
      },
      {
        Attribute: "mx_Campaign",
        Value: campaign_name,
      },
      {
        Attribute: "mx_Ad_Group",
        Value: ad_group,
      },
      {
        Attribute: "mx_Ad_Name",
        Value: ad_name,
      },
      {
        Attribute: "FirstName",
        Value: name,
      },
      {
        Attribute: "EmailAddress",
        Value: email,
      },
      {
        Attribute: "Phone",
        Value: phone,
      },
      {
        Attribute: "mx_City",
        Value: city,
      },
      {
        Attribute: "mx_Course",
        Value: course,
      },
      {
        Attribute: "mx_Specialization",
        Value: specialization,
      },
      {
        Attribute: "mx_Country",
        Value: country,
      },
      {
        Attribute: "Source",
        Value: lsq_source,
      },
      {
        Attribute: "mx_Sub_Source",
        Value: lsq_sub_source,
      },
    ];
  } else {
    LSQ_DATA = [
      {
        Attribute: "FirstName",
        Value: name,
      },
      {
        Attribute: "EmailAddress",
        Value: email,
      },
      {
        Attribute: "Phone",
        Value: phone,
      },
      {
        Attribute: "mx_City",
        Value: city,
      },
      {
        Attribute: "mx_Course",
        Value: course,
      },
      {
        Attribute: "mx_Specialization",
        Value: specialization,
      },
      {
        Attribute: "mx_Country",
        Value: country,
      },
      {
        Attribute: "Source",
        Value: lsq_source,
      },
      {
        Attribute: "mx_Sub_Source",
        Value: lsq_sub_source,
      },
    ];
  }

  // LSQ API
  axios
    .post(
      "https://api-in21.leadsquared.com/v2/LeadManagement.svc/Lead.Create?accessKey=u$rf9f46642c28229077c6efdb5c83eac4b&secretKey=3e142a31200d7b54c9273158641a2cb02ab9a5f4",
      LSQ_DATA
    )
    .then((data) => {
      console.log(data);
    })
    .catch((err) => {
      console.error(err);
    });

  conn.query(
    `INSERT into leads (name, email, phone, country, city, course, specialization, source) VALUES (${conn.escape(name)}, ${conn.escape(
      email
    )}, ${conn.escape(phone)}, ${conn.escape(country)}, ${conn.escape(city)}, ${conn.escape(course)}, ${conn.escape(specialization)}, ${conn.escape(
      source
    )})`,
    (err, res) => {
      if (err) {
        return response.status(500).send(err);
      } else {
        return response.status(200).send("Landing Page Lead Added");
      }
    }
  );
});

// POST ottaQuery
app.post("/ottaquery", (request, response) => {
  if (!request.body) {
    return response.status(400).send("Request has invalid format");
  }
  if (
    !request.body.name ||
    request.body.name === "" ||
    !request.body.email ||
    request.body.email === "" ||
    !request.body.number ||
    request.body.number === "" ||
    !request.body.country ||
    request.body.country === "" ||
    !request.body.age ||
    request.body.age === "" ||
    !request.body.course ||
    request.body.course === "" ||
    !request.body.program ||
    request.body.program === ""
  ) {
    return response.status(400).send("Request has invalid format");
  }

  const name = request.body.name;
  const email = request.body.email;
  const number = request.body.number;
  const age = request.body.age;
  const program = request.body.program;
  const course = request.body.course;
  const country = request.body.country;
  const qualification_program = request.body.qualification.program;
  const qualification_percentage = request.body.qualification.percentage;
  const language_test = request.body.languageTest.test;
  const language_test_date = request.body.languageTest.date;
  const budget = parseInt(request.body.budget);
  const profession = request.body.profession;
  const fund = request.body.fund;

  const source_campaign = request.body.source_campaign;
  const campaign_name = request.body.campaign_name;
  const ad_group = request.body.ad_group;
  const ad_name = request.body.ad_name;

  let budgetRange;

  if (budget >= 10 && budget < 20) budgetRange = "10Lakh-20Lakh";
  else if (budget >= 20 && budget < 30) budgetRange = "20Lakh-30Lakh";
  else if (budget >= 30 && budget < 40) budgetRange = "30Lakh-40Lakh";
  else if (budget >= 40 && budget < 50) budgetRange = "40Lakh-50Lakh";
  else if (budget > 50) budgetRange = "50Lakh Above";
  else budgetRange = "Not decided yet";

  let LSQ_DATA = [];

  if (source_campaign) {
    LSQ_DATA = [
      {
        Attribute: "SourceCampaign",
        Value: source_campaign,
      },
      {
        Attribute: "mx_Campaign",
        Value: campaign_name,
      },
      {
        Attribute: "mx_Ad_Group",
        Value: ad_group,
      },
      {
        Attribute: "mx_Ad_Name",
        Value: ad_name,
      },
      {
        Attribute: "FirstName",
        Value: name,
      },
      {
        Attribute: "EmailAddress",
        Value: email,
      },
      {
        Attribute: "Phone",
        Value: number,
      },
      {
        Attribute: "mx_What_is_your_Highest_Qualification_Level",
        Value: qualification_program,
      },
      {
        Attribute: "mx_What_was_your_passing_percentile",
        Value: qualification_percentage,
      },
      {
        Attribute: "mx_Student_Language_Test",
        Value: language_test,
      },
      {
        Attribute: "mx_Student_Language_Test_Year",
        Value: language_test_date,
      },
      {
        Attribute: "mx_What_is_your_Preferred_Budget",
        Value: budgetRange,
      },
      {
        Attribute: "mx_What_is_you_profession",
        Value: profession,
      },
      {
        Attribute: "mx_How_are_you_planning_to_fund_your_studies",
        Value: fund,
      },
      {
        Attribute: "mx_Date_of_Birth",
        Value: age,
      },
      {
        Attribute: "mx_Course",
        Value: program,
      },
      {
        Attribute: "mx_Specialization",
        Value: course,
      },
      {
        Attribute: "mx_Country",
        Value: country,
      },
      {
        Attribute: "Source",
        Value: "College Vidya",
      },
      {
        Attribute: "mx_Sub_Source",
        Value: "Suggest Me",
      },
    ];
  } else {
    LSQ_DATA = [
      {
        Attribute: "FirstName",
        Value: name,
      },
      {
        Attribute: "EmailAddress",
        Value: email,
      },
      {
        Attribute: "Phone",
        Value: number,
      },
      {
        Attribute: "mx_What_is_your_Highest_Qualification_Level",
        Value: qualification_program,
      },
      {
        Attribute: "mx_What_was_your_passing_percentile",
        Value: qualification_percentage,
      },
      {
        Attribute: "mx_Student_Language_Test",
        Value: language_test,
      },
      {
        Attribute: "mx_Student_Language_Test_Year",
        Value: language_test_date,
      },
      {
        Attribute: "mx_What_is_your_Preferred_Budget",
        Value: budgetRange,
      },
      {
        Attribute: "mx_What_is_you_profession",
        Value: profession,
      },
      {
        Attribute: "mx_How_are_you_planning_to_fund_your_studies",
        Value: fund,
      },
      {
        Attribute: "mx_Date_of_Birth",
        Value: age,
      },
      {
        Attribute: "mx_Course",
        Value: program,
      },
      {
        Attribute: "mx_Specialization",
        Value: course,
      },
      {
        Attribute: "mx_Country",
        Value: country,
      },
      {
        Attribute: "Source",
        Value: "College Vidya",
      },
      {
        Attribute: "mx_Sub_Source",
        Value: "Suggest Me",
      },
    ];
  }

  // LSQ API
  axios
    .post(
      "https://api-in21.leadsquared.com/v2/LeadManagement.svc/Lead.Create?accessKey=u$rf9f46642c28229077c6efdb5c83eac4b&secretKey=3e142a31200d7b54c9273158641a2cb02ab9a5f4",
      LSQ_DATA
    )
    .then(() => {
      console.log("LSQ Lead Added");
    })
    .catch((err) => {
      console.error(err);
    });

  conn.query(
    `
    INSERT INTO ottaqueries(
      name,
      email,
      number,
      age,
      program,
      course,
      country,
      qualification_program,
      qualification_percentage,
      language_test,
      language_test_date,
      budget,
      profession,
      fund,
      source_campaign, campaign_name, ad_group, ad_name 
    ) VALUES(
      ${conn.escape(name)},
      ${conn.escape(email)},
      ${conn.escape(number)},
      ${conn.escape(age)},
      ${conn.escape(program)},
      ${conn.escape(course)},
      ${conn.escape(country)},
      ${conn.escape(qualification_program)},
      ${conn.escape(qualification_percentage)},
      ${conn.escape(language_test)},
      ${conn.escape(language_test_date)},
      ${conn.escape(budget)},
      ${conn.escape(profession)},
      ${conn.escape(fund)},
       ${conn.escape(source_campaign)}, ${conn.escape(campaign_name)}, ${conn.escape(ad_group)}, ${conn.escape(ad_name)})`,
    (err, res) => {
      if (err) {
        return response.status(500).send(err);
      } else {
        return response.status(200).send("Lead Added");
      }
    }
  );
});

// Get Course By Program
app.get("/course/:id", (req, res) => {
  console.log(req.params.id);

  const id = req.params.id === "bachelors" ? 1 : req.params.id === "masters" ? 2 : 3;

  if (!id || !id === 1 || !id === 2 || !id === 3) {
    return response.status(400).send("Request has invalid params");
  }

  conn.query(`SELECT name FROM courses WHERE category_id=${conn.escape(id)}`, (err, result) => {
    if (err) {
      return res.status(500).send(err);
    } else {
      return res.status(200).send(result);
    }
  });
});

// Get Countries
app.get("/countries", (req, res) => {
  conn.query(`SELECT name FROM countries`, (err, result) => {
    if (err) {
      return res.status(500).send(err);
    } else {
      return res.status(200).send(result);
    }
  });
});

// Get Universities by Country
app.get("/universities/:country", (req, res) => {
  const country = req.params.country;
  if (
    !country ||
    !country === "USA" ||
    !country === "Canada" ||
    !country === "UK" ||
    !country === "Germany" ||
    !country === "New Zealand" ||
    !country === "Australia"
  ) {
    return res.status(400).send("Request has invalid params!");
  }
  conn.query(
    `SELECT universities.name, universities.logo FROM countries INNER JOIN universities ON countries.id=universities.country_id WHERE countries.name=${conn.escape(
      country
    )}`,
    (err, result) => {
      if (err) {
        return res.status(500).send(err);
      } else {
        return res.status(200).send(result);
      }
    }
  );
});

// Admin panel login

app.get("/cvabroadadmin/:password/:table", (req, res) => {
  const routePassword = "ityraPaNARgAisterOcA";
  const password = req.params.password;
  const table = escapeId(req.params.table);

  if (password !== routePassword) {
    return res.status(401).send("You are not authorized to perform this action");
  }

  if (table) {
    conn.query(`SELECT * FROM ${table} ORDER BY created_at DESC`, (err, result) => {
      if (err) {
        return res.status(500).send(err);
      } else {
        return res.status(200).send(result);
      }
    });
  }
});

app.post("/cvabroadadmin/login", (req, res) => {
  if (!req.body) {
    return res.status(400).send("Request has invalid format");
  }

  if (!req.body.email || req.body.email === "" || !req.body.password || req.body.password === "") {
    return res.status(400).send("Request has invalid format");
  }

  const email = req.body.email;
  const password = req.body.password;

  conn.query(`SELECT * FROM lead_panel_admin WHERE email = '${email}' AND password = '${password}'`, (err, result) => {
    if (err) {
      return res.status(500).send(err);
    }
    if (result.length === 0) {
      return res.status(401).send({ description: "You are unauthorized to perform this action", data: null });
    }

    return res.status(200).send({ description: "Successfully logged in!", data: result[0] });
  });
});

httpsServer.listen(8377, () => {
  console.log("Server is running on port " + 8377);
});
