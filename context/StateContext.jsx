import React, { createContext, useContext, useRef, useState } from "react";

const Context = createContext({
  selectedPrograms: "",
  setSelectedPrograms: () => {},
  selectedCourse: "",
  setSelectedCourse: () => {},
  selectedCourseCategory: "",
  setSelectedCourseCategory: () => {},
  selectedCountry: "",
  setSelectedCountry: () => {},
  selectedUniversity: "",
  setSelectedUniversity: () => {},
  countriesRef: null,
  universitiesRef: null,
  heroBannerSliderRef: null,
  slideBar1Ref: null,
  slideBar2Ref: null,
  formData: {
    gender: "",
    name: "",
    email: "",
    phone: "",
    country: "",
    dob: "",
    course: "",
  },
  setFormData: () => {},
  ottaData: {
    program: "",
    course: "",
    specialization: "",
    country: "",
    qualification: {
      program: "",
      specialization: "",
      percentage: "",
    },
    languageTest: {
      test: "",
      date: "",
    },
    budget: 0,
    budgetText: "",
    profession: "",
    fund: "",
    name: "",
    email: "",
    number: "",
    age: "",
  },
  setOttaData: () => {},
  ottaErrors: {
    program: "",
    course: "",
    specialization: "",
    country: "",
    qualification: {
      program: "",
      specialization: "",
      percentage: "",
    },
    languageTest: {
      test: "",
      date: "",
    },
    budget: 0,
    budgetText: "",
    profession: "",
    name: "",
    email: "",
    number: "",
    age: "",
  },
  setOttaErrors: () => {},
  showForm: false,
  setShowForm: () => {},
  showLeadModal: false,
  setShowLeadModal: () => {},
  isLoading: false,
  setIsLoading: () => {},
  showMenu: false,
  setShowMenu: () => {},
  sliderIndex: false,
  setSliderIndex: () => {},
  showSignInModal: false,
  setShowSignInModal: () => {},
  thankYouPopupShow: false,
  setThankYouPopupShow: () => {},
  services: [],
  outServiceScrollerIndex: 0,
  courseSpecializations: {
    bachelors: [],
    masters: [],
  },
});

export const StateContext = ({ children }) => {
  const [isLogin, setIsLogin] = useState(false);
  const [showMenu, setShowMenu] = useState(false);
  const [sliderIndex, setSliderIndex] = useState(1);

  const [selectedPrograms, setSelectedPrograms] = useState(null);
  const [selectedCourse, setSelectedCourse] = useState(null);
  const [selectedCourseCategory, setSelectedCourseCategory] = useState(null);
  const [selectedCountry, setSelectedCountry] = useState(null);
  const [selectedUniversity, setSelectedUniversity] = useState([]);

  const [showForm, setShowForm] = useState(false);
  const [showLeadModal, setShowLeadModal] = useState();
  const [isLoading, setIsLoading] = useState(false);

  const [formData, setFormData] = useState({
    gender: "",
    name: "",
    email: "",
    phone: "",
    country: "",
    dob: "",
    course: "",
  });
  const [ottaData, setOttaData] = useState({
    program: "",
    course: "Not decided yet",
    specialization: "",
    country: "",
    qualification: {
      program: "",
      specialization: "",
      percentage: "",
    },
    languageTest: {
      test: "Not taken yet",
      date: new Date().getFullYear(),
    },
    budget: 0.05,
    profession: "",
    fund: "Loan",
    gender: "",
    name: "",
    email: "",
    number: "",
    age: "",
  });
  const [ottaErrors, setOttaErrors] = useState({
    program: "",
    course: "",
    specialization: "",
    country: "",
    qualification: {
      program: "",
      specialization: "",
      percentage: "",
    },
    languageTest: {
      test: "",
      date: "",
    },
    budget: "",
    profession: "",
    name: "",
    email: "",
    number: "",
    age: "",
  });

  const countriesRef = useRef(null);
  const universitiesRef = useRef(null);

  const slideBar1Ref = useRef(null);
  const slideBar2Ref = useRef(null);

  const heroBannerSliderRef = useRef(null);

  const [outServiceScrollerIndex, setOurServiceScrollerIndex] = useState(0);

  const services = [
    {
      id: 0,

      name: "Easy Country & <span>University Selection</span>",
      image: "/servicesImages/compare.png",
      icon: "/servicesImages/compare-icon.svg",
      points: [
        "AI enabled assistance platform.",
        "Easy comparison between multiple universities.",
        "Highlight only key points for quick & easy decision making.",
        "Complete transparency with respect to university details.",
        "Personalized counselling with respect to programs and universities.",
      ],
    },
    {
      id: 1,
      name: "Free <span>Personalized Counseling</span>",
      image: "/servicesImages/counselling.png",
      icon: "/servicesImages/counselling-icon.svg",
      points: [
        "Personalized counseling from Senior Counselors.",
        "Unbiased approach in finding the best Course and University options.",
        "Career guidance by focusing on opportunities.",
        "Telephonic as well as Video Counseling sessions.",
        "Maintain transparency when sharing details.",
      ],
    },
    {
      id: 2,
      name: "Assistance with <span>Application & Admission</span>",
      image: "/servicesImages/application-admission.png",
      icon: "/servicesImages/application-admission-icon.svg",
      points: [
        "Flawless applications - assured admits.",
        "High-quality SOPs, LORs, and Resumes.",
        "Real-time application tracking & follow-through with universities.",
        "Telephonic as well as Video Counseling sessions.",
        "Maintain transparency when sharing details.",
      ],
    },
    {
      id: 3,

      name: "Education  <span>Loan</span>",
      image: "/servicesImages/education-loan.png",
      icon: "/servicesImages/loan-icon.svg",
      points: [
        "Study Loans through 15+ Leading Financial Institutions",
        "Secured and Unsecured Loans",
        "Pre-visa Disbursal of Loans",
        "Hassle-Free Documentation",
        "Quick Sanction of loans",
        "Zero Service Charges",
      ],
    },
    {
      id: 4,

      name: "<span>VISA</span> Processing",
      image: "/servicesImages/visa-processing.png",
      icon: "/servicesImages/visa-icon.svg",
      points: ["Guidance on visa documentation", "Excellent visa success ratio across all countries", "Mock visa interviews"],
    },
    {
      id: 5,

      name: "<span>IELTS</span> Preparation",
      image: "/servicesImages/ielts.png",
      icon: "/servicesImages/ielts-icon.svg",
      points: [
        "Free demo sessions",
        "Access top-quality test-prep material",
        "Interactive online audio/video content  ",
        "Small class sizes for personalized attention",
        "Rich database of practice questions",
        "Mock tests simulating real test environment",
      ],
    },
  ];

  const courseSpecializations = {
    "Bachelor's": [
      "Business and Administration",
      "Computer Science and Information Technology Education",
      "Engineering and Engineering Trades",
      "Medicine",
      "Nursing",
      "Mathematics and Statistics",
      "Commerce",
      "Law",
      "Arts",
      "Agriculture, Forestry and Fishery",
      "Architecture and Building",
      "Environmental Science/ Protection Health",
      "Journalism and Information",
      "Manufacturing and Processing",
      "Personal Services",
      "Security Services",
      "Transport Services",
      "Social Services",
      "Health Sciences",
      "Life Sciences",
      "Social and Behavioral Science",
      "Physical Sciences, Sciences",
      "Humanities",
      "Veterinary",
    ],
    "Master's": [
      "Business and Administration",
      "Computer Science and Information Technology Education",
      "Engineering and Engineering Trades",
      "Medicine",
      "Nursing",
      "Mathematics and Statistics",
      "Commerce",
      "Law",
      "Arts",
      "Agriculture, Forestry and Fishery",
      "Architecture and Building",
      "Environmental Science/ Protection Health",
      "Journalism and Information",
      "Manufacturing and Processing",
      "Personal Services",
      "Security Services",
      "Transport Services",
      "Social Services",
      "Health Sciences",
      "Life Sciences",
      "Social and Behavioral Science",
      "Physical Sciences, Sciences",
      "Humanities",
      "Veterinary",
    ],
  };

  const [showSignInModal, setShowSignInModal] = useState(false);
  const [thankYouPopupShow, setThankYouPopupShow] = useState(false);

  return (
    <Context.Provider
      value={{
        selectedPrograms,
        setSelectedPrograms,
        selectedCourse,
        setSelectedCourse,
        selectedCourseCategory,
        setSelectedCourseCategory,
        selectedCountry,
        setSelectedCountry,
        selectedUniversity,
        setSelectedUniversity,
        countriesRef,
        universitiesRef,
        heroBannerSliderRef,
        formData,
        setFormData,
        showForm,
        setShowForm,
        showLeadModal,
        setShowLeadModal,
        isLoading,
        setIsLoading,
        showMenu,
        setShowMenu,
        slideBar1Ref,
        slideBar2Ref,
        sliderIndex,
        setSliderIndex,
        ottaData,
        setOttaData,
        ottaErrors,
        setOttaErrors,
        showSignInModal,
        setShowSignInModal,
        services,
        outServiceScrollerIndex,
        setOurServiceScrollerIndex,
        courseSpecializations,
        thankYouPopupShow,
        setThankYouPopupShow,
      }}
    >
      {children}
    </Context.Provider>
  );
};

export const useStateContext = () => useContext(Context);
