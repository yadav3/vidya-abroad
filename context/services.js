import axios from "axios";
export const BACKEND_API_URL = process.env.NODE_ENV === "production" ? "https://collegevidyaabroad.com:8377" : "http://localhost:8377";
export const COUNSELLOR_CLIENT_URL = process.env.NODE_ENV === "production" ? "https://counsellor.collegevidyaabroad.com" : "http://localhost:3000";
export const COUNSELLOR_SERVER_URL =
  process.env.NODE_ENV === "production" ? "https://counsellor.collegevidyaabroad.com:9000" : "http://localhost:9000";

export const AWS_BUCKET_IMAGE_PREFIX = "https://d6wn6379hq47k.cloudfront.net";

export function getAWSImagePath(filepath) {
  return `${AWS_BUCKET_IMAGE_PREFIX}/${filepath}`;
}
export function getAWSImagePathBeta(filepath) {
  return String(filepath).startsWith("/") ? filepath : `${AWS_BUCKET_IMAGE_PREFIX}/${filepath}`;
}

export const createNewLead = async (data) => {
  try {
    const response = await axios.post(BACKEND_API_URL + "/lead/create/new", data);
    return response.data;
  } catch (error) {
    console.error(error);
  }
};

export const getTestimonials = async () => {
  try {
    const response = await axios.get(BACKEND_API_URL + "/testimonials");
    const testimonials = response.data;
    return testimonials;
  } catch (error) {
    console.error(error);
  }
};

export const getCountrySource = (country) => {
  switch (country) {
    case "USA":
      return "USA Abroad";
    case "UK":
      return "UK Abroad";
    case "Germany":
      return "Germany Abroad";
    case "New Zealand":
      return "New Zealand Abroad Only";
    case "Australia":
      return "Australia & NZ Abroad";
    case "Canada":
      return "Canada Abroad";
    case "Ireland":
      return "Ireland Abroad";
    case "Not Decided Yet":
      return "Not Decided Yet";
    default:
      return "Not Decided Yet";
  }
};

export function generatePassword(length) {
  const characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  let password = "";

  for (let i = 0; i < length; i++) {
    password += characters.charAt(Math.floor(Math.random() * characters.length));
  }

  return password;
}

export const getCompareUniversitiesByParams = async (courseId, specializationId, countryId, ieltsScore, budget) => {
  try {
    const data = await axios.get(
      BACKEND_API_URL +
      `/universities/compare?courseId=${courseId}&specializationId=${specializationId}&countryId=${countryId}&ielts=${ieltsScore}&budget=${budget}`
    );
    return data.data;
  } catch (error) {
    console.error(error);
  }
};

export const getToBeCompareUniversities = async (universitiesIds, specializationsId, userId = null) => {
  try {
    const data = await axios.post(BACKEND_API_URL + `/universities/compare/data`, {
      universities: [...universitiesIds],
      specializations: [...specializationsId],
      userId: userId
    });
    return data.data;
  } catch (error) {
    console.error(error);
  }
};
export const getCompareQueryByUserId = async (userId) => {
  try {
    const data = await axios.get(BACKEND_API_URL + `/user/compare/results?userId=${userId}`);
    return data.data;
  } catch (error) {
    console.error(error);
  }
};

export async function storeUserCompareResults(uuid, categoryId, courseId, specializationId, majorId, budget, countryId, ielts, opportunityId, budgetLow, budgetHigh, countries, relatedProspectId) {
  const data = JSON.stringify({
    uuid,
    categoryId,
    courseId,
    specializationId,
    majorId,
    budget,
    countryId,
    ielts,
    opportunityId,
    budgetLow,
    budgetHigh,
    countries,
    relatedProspectId
  });

  const config = {
    method: "post",
    maxBodyLength: Infinity,
    url: BACKEND_API_URL + "/user/store/compare",
    headers: {
      "Content-Type": "application/json",
    },
    data,
  };

  try {
    const response = await axios(config);
    return response.data;
  } catch (error) {
    console.error(error);
    throw error;
  }
}

// COURSES

export const getCourses = async () => {
  try {
    const data = await axios.get(BACKEND_API_URL + `/courses`);
    return data.data;
  } catch (error) {
    console.error(error);
  }
};

export const getCoursesByCategoryId = async (categoryId) => {
  try {
    const data = await axios.get(BACKEND_API_URL + `/courses/by/category?categoryId=${categoryId}`);
    return data.data;
  } catch (error) {
    console.error(error);
  }
};
export const getSpecializationsByCourseId = async (courseId) => {
  try {
    const data = await axios.get(BACKEND_API_URL + `/specialization/by/course?courseId=${courseId}`);
    return data.data;
  } catch (error) {
    console.error(error);
  }
};
export const getAllExchangeRates = async () => {
  try {
    const data = await axios.get(BACKEND_API_URL + `/currency/exchange-rate/all`);
    return data.data;
  } catch (error) {
    console.error(error);
  }
};
export const getCounsellorCustomerByUuid = async (uuid) => {
  try {
    const data = await axios.get(COUNSELLOR_SERVER_URL + `/customer/details-by-uuid?uuid=${uuid}`);
    return data.data[0];
  } catch (error) {
    console.error(error);
  }
};
