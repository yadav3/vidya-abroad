import { useState, useEffect, useRef, useCallback } from "react";

// Intersection Observer Hook
function useOnScreen(ref, rootMargin = "0px") {
  const [isIntersecting, setIntersecting] = useState(false);

  useEffect(() => {
    const target = ref.current;

    const observer = new IntersectionObserver(
      ([entry]) => {
        // Update our state when observer callback fires
        setIntersecting(entry.isIntersecting);
      },
      {
        rootMargin,
      }
    );
    if (ref.current) {
      observer.observe(ref.current);
    }
    return () => {
      observer.unobserve(target);
    };
  }, []); // Empty array ensures that effect is only run on mount and unmount
  return isIntersecting;
}

// Internationalization Hook

function CurrencyInternationalization(num, currency) {
  return new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: currency,
    maximumFractionDigits: 0,
  }).format(num);
}

const useToggle = (initialState = false) => {
  // Initialize the state
  const [state, setState] = useState(initialState);

  const toggle = useCallback(() => setState((state) => !state), []);

  return [state, toggle];
};

// create a function that take a amount and return a formatted amount in lakh, crore, etc and also take currency as a parameter to format the amount in that currency using CurrencyInternationalization function

export default function formatCurrencyAmount(amount, currency) {
  if (amount >= 10000000 && currency === "INR") {
    return CurrencyInternationalization(amount / 10000000, currency) + " Cr";
  } else if (amount >= 100000 && currency === "INR") {
    return CurrencyInternationalization(amount / 100000, currency) + " Lakh";
  }
  return CurrencyInternationalization(amount, currency);
}

function useLocalStorage(key, initialValue) {
  // State to store our value
  // Pass initial state function to useState so logic is only executed once
  const [storedValue, setStoredValue] = useState(() => {
    if (typeof window === "undefined") {
      return initialValue;
    }
    try {
      // Get from local storage by key
      const item = window.localStorage.getItem(key);
      // Parse stored json or if none return initialValue
      return item ? JSON.parse(item) : initialValue;
    } catch (error) {
      // If error also return initialValue
      console.error(error);
      return initialValue;
    }
  });
  // Return a wrapped version of useState's setter function that ...
  // ... persists the new value to localStorage.
  const setValue = (value) => {
    try {
      // Allow value to be a function so we have same API as useState
      const valueToStore =
        value instanceof Function ? value(storedValue) : value;
      // Save state
      setStoredValue(valueToStore);
      // Save to local storage
      if (typeof window !== "undefined") {
        window.localStorage.setItem(key, JSON.stringify(valueToStore));
      }
    } catch (error) {
      // A more advanced implementation would handle the error case
      console.error(error);
    }
  };
  return [storedValue, setValue];
}

const unSlugify = (str) => {
  const capitalize = (s) => {
    if (typeof s !== "string") return "";
    return s.charAt(0).toUpperCase() + s.slice(1);
  };
  str = str.replace(/-/g, " ");
  str = str.replace(/_/g, " ");
  str = str.replace(/\w\S*/g, capitalize);
  return str;

};

const useBreakpoint = () => {
  const [breakpoint, setBreakpoint] = useState(null);

  const handleResize = () => {
    const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    if (width) {
      setBreakpoint(width);
    }
  }

  useEffect(() => {
    handleResize();
    window.addEventListener('resize', handleResize);
    return () => window.removeEventListener('resize', handleResize);
  }, []);

  return breakpoint;
};

function useCursorElement() {
  const [element, setElement] = useState(null);

  useEffect(() => {
    const handleMouseMove = (event) => {
      const x = event.clientX;
      const y = event.clientY;
      const elementAtCursor = document.elementFromPoint(x, y);
      setElement(elementAtCursor);
    };

    // Add event listener to update the element on mouse move
    document.addEventListener('mousemove', handleMouseMove);

    // Cleanup function
    return () => {
      document.removeEventListener('mousemove', handleMouseMove);
    };
  }, []); // Empty dependency array means this effect will only run once on mount

  return element;
}


export { useOnScreen, formatCurrencyAmount, CurrencyInternationalization, useToggle, useLocalStorage, unSlugify, useBreakpoint, useCursorElement };
