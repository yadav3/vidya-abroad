export const convertCurrency = (amount, toCurrency, exchangeRates, isSwitch) => {
    const toCurrencyRate = exchangeRates.find((rate) => rate.target === toCurrency);
    // convert to inr
    if (isSwitch) {
        return Number(amount / toCurrencyRate.rate).toFixed(2);
    }
    return amount
}
