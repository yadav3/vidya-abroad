export const getFAQSchema = (faqs) => {
    let mainEntity = [];

    if (faqs?.length > 0) {
        mainEntity.push(
            faqs.map((faq) => {
                return {
                    "@type": "Question",
                    name: faq.question,
                    acceptedAnswer: {
                        "@type": "Answer",
                        text: faq.answer_html,
                    },
                };
            })
        );
    }

    const faqSchema = {
        "@context": "https://schema.org",
        "@type": "FAQPage",
        mainEntity: mainEntity[0] ? mainEntity[0] : mainEntity,
    };

    return faqSchema;
};

export const getSearchSchema = () => {
    return {
        "@context": "http://schema.org",
        "@type": "WebSite",
        url: "https://www.collegevidyaabroad.com/",
        potentialAction: [
            {
                "@type": "SearchAction",
                target: "https://www.collegevidyaabroad.com/blog/search?search={search_term_string}",
                "query-input": "required name=search_term_string",
            },
        ],
    };
};

export const getBudgetByStringLimit = (budget) => {
    switch (budget) {
        case "10L - 20L":
            return [1000000, 2000000];
        case "20L - 30L":
            return [2000000, 3000000];
        case "30L - 40L":
            return [3000000, 4000000];
        case "40L - 50L":
            return [4000000, 5000000];
        case "More than 50L":
            return [5000000, 50000000];
        case "Not Decided Yet":
            return [0, 0];
        default:
            return [0, 0]
    }
}


export const formatNumber = (number) => {
    if (!number) {
        return 0;
    }

    const absNumber = Math.abs(number);

    if (absNumber > 999999999) {
        return `${Math.sign(number) * (absNumber / 1000000000).toFixed(1)}B`;
    } else if (absNumber > 999999) {
        return `${Math.sign(number) * (absNumber / 1000000).toFixed(1)}M`;
    } else if (absNumber > 999) {
        return `${Math.sign(number) * (absNumber / 1000).toFixed(1)}K`;
    } else {
        return Math.sign(number) * absNumber;
    }
};

export const formateSecondsToMinutes = (seconds) => {
    const minutes = Math.floor(seconds / 60);
    const remainingSeconds = seconds % 60;
    return `${minutes} Min ${remainingSeconds} Sec`;
};



export function divideArrayIntoSubarray(arr, x) {
    const subarrays = [];
    for (let i = 0; i < arr.length; i += x) {
        subarrays.push(arr.slice(i, i + x));
    }
    return subarrays;
}
