// import React from "react";
// // import SpeechRecognition, { useSpeechRecognition } from "react-speech-recognition";
// import Modal from "react-bootstrap/Modal";
// import { Button } from "bootstrap";
// import { BsMicFill } from "react-icons/bs";

// export default function FaqSearchMic() {
// //   const { transcript, listening, resetTranscript, browserSupportsSpeechRecognition } = useSpeechRecognition();
//   const [show, setShow] = useState(false);

//   const handleClose = () => setShow(false);
//   const handleShow = () => setShow(true);

//   if (!browserSupportsSpeechRecognition) {
//     return <span>Browser doesnt support speech recognition.</span>;
//   }

//   return (
//     <>
//       <button variant="primary" onClick={handleShow}>
//         <BsMicFill />
//       </button>

//       <Modal show={show} onHide={handleClose}>
//         <Modal.Header closeButton>
//           <Modal.Title>Modal heading</Modal.Title>
//         </Modal.Header>
//         <Modal.Body>
//           {" "}
//           <div>
//             <p>Microphone: {listening ? "on" : "off"}</p>
//             <button onClick={SpeechRecognition.startListening}>Start</button>
//             <button onClick={SpeechRecognition.stopListening}>Stop</button>
//             <button onClick={resetTranscript}>Reset</button>
//             <p>{transcript}</p>
//           </div>
//         </Modal.Body>
//         <Modal.Footer>
//           <Button variant="secondary" onClick={handleClose}>
//             Close
//           </Button>
//           <Button variant="primary" onClick={handleClose}>
//             Save Changes
//           </Button>
//         </Modal.Footer>
//       </Modal>
//     </>
//   );
// }
