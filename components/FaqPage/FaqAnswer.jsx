import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import ReactTypingEffect from "react-typing-effect";
import { useState } from "react";
import { useRecoilState, useRecoilValue } from "recoil";
import { faqSelectedFaqAtom } from "../../store/faqPageStore";
import { useEffect } from "react";
import styles from "./FaqPage.module.scss";
import Link from "next/link";

export default function FaqAnswer() {
  // const [eraseDelay, setEraseDelay] = useState(24 * 60 * 60 * 1000);
  const [showAnswer, setShowAnswer] = useState(false);
  const selectedFaq = useRecoilValue(faqSelectedFaqAtom);

  const button_html = `<a  href="/suggest-me-a-university" class="border-0 text-start mx-auto p-2 bg-orange rounded-5 px-4 text-white mt-3">Connect with Expert</a>`;

  useEffect(() => {
    if (selectedFaq) {
      setShowAnswer(true);
    } else {
      setShowAnswer(false);
    }
  }, [selectedFaq]);

  return selectedFaq ? (
    <Row id="faq-answer-box">
      <Col>
        <div className={`p-4 rounded-4 text-center bg-light mx-auto ${styles.faqAnswerBox}`}>
          <h4 className="d-flex align-items-center justify-content-center gap-2 mb-2">{selectedFaq.question}</h4>
          <ReactTypingEffect
            text={selectedFaq.answer + button_html}
            displayTextRenderer={(text, i) => {
              return (
                <div
                  className="d-flex flex-column gap-2 text-start"
                  dangerouslySetInnerHTML={{
                    __html: text,
                  }}
                ></div>
              );
            }}
            speed={50}
            eraseSpeed={1000}
            eraseDelay={24 * 60 * 60 * 1000}
            typingDelay={0}
          />
        </div>
      </Col>
    </Row>
  ) : null;
}
