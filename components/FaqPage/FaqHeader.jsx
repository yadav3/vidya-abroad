import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { GiProcessor } from "react-icons/gi";
import SectionHeading from "../global/SectionHeading/SectionHeading";
import Image from "next/image";

export function FaqHeader({}) {
  return (
    <Row>
      <Col>
        <div className="d-flex flex-column align-items-center justify-content-center mt-5">
          <Image src="/illustrations/ask-me-anything-robot.png" alt="AI Robot Illustrations" width={120} height={100} />
          <span className={`d-flex align-items-center justify-content-center gap-2 bg-bright-green text-white px-2 myshine `}>
            A.I powered
            <GiProcessor />
          </span>
          <SectionHeading
            heading={`<span> Ask anything you want!</span>`}
            subHeading={`<span>
                We have answers to all your <span class="text-cvblue fw-semibold">Study Abroad</span> queries
              </span>`}
            className="text-center my-4 mt-0 px-3"
          />
        </div>
      </Col>
    </Row>
  );
}
