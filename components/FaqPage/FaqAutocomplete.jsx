import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import uuid from "react-uuid";
import { useState } from "react";
import styles from "./FaqPage.module.scss";
import { faqDebouncedSearchAtom, faqSearchAtom, faqSelectedFaqAtom } from "../../store/faqPageStore";
import { useRecoilState } from "recoil";
import { useEffect } from "react";
import { useRef } from "react";

export default function AutoCompleteContainer({ items }) {
  const [selectedFaq, setSelectedFaq] = useRecoilState(faqSelectedFaqAtom);
  const [showAutoComplete, setShowAutoComplete] = useState(true);
  const [search, setSearch] = useRecoilState(faqSearchAtom);
  const [debouncedSearch, setDebouncedSearch] = useRecoilState(faqDebouncedSearchAtom);
  const searchBarContainerRef = useRef(null);

  const handleScrollToBottom = () => {
    // scroll to bottom of the page
    window.scrollTo({
      top: document?.body.scrollHeight,
      behavior: "smooth",
    });
  };

  const handleSelectFaq = (item) => {
    setSelectedFaq(null);
    handleScrollToBottom();

    setTimeout(() => {
      setSelectedFaq(item);
      setDebouncedSearch(item.question);
      handleHideAutoComplete();
    }, 100);
  };

  const handleHideAutoComplete = () => {
    setShowAutoComplete(false);
  };
  const handleShowAutoComplete = () => {
    setShowAutoComplete(true);
  };

  // when user clicks outside the searchbarContainerRef
  const handleClickOutside = (e) => {
    if (searchBarContainerRef.current && !searchBarContainerRef.current.contains(e.target)) {
      handleHideAutoComplete();
    }
  };

  useEffect(() => {
    handleShowAutoComplete();
  }, [search]);

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);

  if (!items || items.length === 0 || !showAutoComplete) return null;

  return (
    <div
      className={`faq_autocomplete-container position-absolute  border border-1 bg-white p-4 h-max rounded-4 mt-2 ${styles.faqAutoCompleteContainer}`}
      style={{
        width: "100%",
        maxHeight: "300px",
        overflowY: "auto",
        zIndex: 9999,
      }}
      ref={searchBarContainerRef}
    >
      <RenderAutoCompleteItems items={items} handleSelectFaq={handleSelectFaq} />
    </div>
  );
}

function AutoCompleteItem({ handleSelectFaq, item }) {
  const [debouncedSearch, setDebouncedSearch] = useRecoilState(faqDebouncedSearchAtom);

  const getHighlightedQuestion = (question) => {
    const regex = new RegExp(debouncedSearch, "gi");
    const highlightedQuestion = question.replace(regex, (match) => `<span class="fw-semibold">${match}</span>`);
    return highlightedQuestion;
  };

  return (
    <li className={`w-100 py-2 ps-2 cursor-pointer ${styles.autoCompleteItem}`} onClick={() => handleSelectFaq(item)}>
      <span
        className={`text-dark text-decoration-none `}
        dangerouslySetInnerHTML={{
          __html: getHighlightedQuestion(item.question),
        }}
      ></span>
    </li>
  );
}

function RenderAutoCompleteItems({ items, handleSelectFaq }) {
  return (
    <ul className="list-unstyled m-0">
      {items.map((item) => (
        <AutoCompleteItem key={uuid()} handleSelectFaq={handleSelectFaq} item={item} />
      ))}
    </ul>
  );
}
