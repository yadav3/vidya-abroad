import React, { useState } from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import uuid from "react-uuid";
import { faqDebouncedSearchAtom, faqSearchAtom, faqSelectedFaqAtom } from "../../store/faqPageStore";
import { useRecoilState } from "recoil";
import { useEffect } from "react";

export default function FaqSearchGuide() {
  const [search, setSearch] = useRecoilState(faqSearchAtom);
  const [selectedFaq, setSelectedFaq] = useRecoilState(faqSelectedFaqAtom);
  const [debouncedSearch, setDebouncedSearch] = useRecoilState(faqDebouncedSearchAtom);
  const [questions, setQuestions] = useState(["What are", "How much", "What would", "How many"]);

  const most_common_questions = ["What are", "How much", "What would", "How many", "Can I", "How is", "How do", "How to", "What is", "Which country"];

  const getRandomQuestions = () => {
    const questions = new Set();
    while (questions.size < 4) {
      const randomQuestion = most_common_questions[Math.floor(Math.random() * most_common_questions.length)];
      questions.add(randomQuestion);
    }
    return Array.from(questions);
  };

  // if (selectedFaq) return null;

  useEffect(() => {
    if (selectedFaq && selectedFaq?.question?.length > 0) {
      setQuestions(getRandomQuestions(4));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedFaq]);

  return (
    <Row>
      <p className="text-center mt-4">
        {selectedFaq ? "Still curious?" : "Don't have anything in mind?"}
        <span className="fw-semibold"> Try these:</span>
      </p>
      <Col>
        <div className="mx-auto d-flex flex-wrap align-items-center justify-content-center gap-4">
          {questions.map((item, index) => (
            <div
              key={uuid()}
              style={{
                width: "max-content",
              }}
              onClick={() => {
                setDebouncedSearch(item);
                setSearch(item);
              }}
            >
              <span className={`text-muted rounded-4 bg-light p-2 px-3 cursor-pointer text-center`}>{item}</span>
            </div>
          ))}
        </div>
      </Col>
    </Row>
  );
}
