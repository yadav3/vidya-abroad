import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { InputText } from "primereact/inputtext";
import { useRef } from "react";
import styles from "./FaqPage.module.scss";
import { useQuery } from "react-query";
import { getFaqAutoComplete } from "../../services/faqpage.service";
import { useState } from "react";
import uuid from "react-uuid";
import AutoCompleteContainer from "./FaqAutocomplete";
import { useRecoilState } from "recoil";
import { faqDebouncedSearchAtom, faqSearchAtom, faqSelectedFaqAtom } from "../../store/faqPageStore";
import FaqSearchMic from "./FaqSearchMic";

export function FaqSearch() {
  const [search, setSearch] = useRecoilState(faqSearchAtom);

  const searchBarContainerRef = useRef(null);
  const [debouncedSearch, setDebouncedSearch] = useRecoilState(faqDebouncedSearchAtom);

  const {
    data: autoCompleteItems,
    isLoading: isAutoCompleteItemsLoading,
    isError: autoCompleteItemsError,
  } = useQuery(["autoCompleteItems", search], async ({ queryKey: [, search] }) => await getFaqAutoComplete(search), {
    enabled: search.length > 2,
  });

  const handleDebounceSearch = (e) => {
    setDebouncedSearch(e.target.value);

    setTimeout(() => {
      setSearch(debouncedSearch);
    }, 1000);
  };

  return (
    <Row>
      <Col>
        <form
          className="d-flex align-items-center justify-content-center mb-3"
          onSubmit={(e) => {
            e.preventDefault();
            setSearch(debouncedSearch);
          }}
        >
          <span className={`p-input-icon-left mx-3 mx-md-0 position-relative ${styles.searchbarContainer}`} ref={searchBarContainerRef}>
            {isAutoCompleteItemsLoading ? <i className="pi pi-spin pi-spinner" /> : <i className="pi pi-search" />}
            <InputText placeholder="Search" className="w-100 rounded-3" value={debouncedSearch} onChange={handleDebounceSearch} />
            {/* <i
              className="pi pi-microphone text-dark position-absolute end-0 top-50 me-3   "
              style={{
                height: "20px",
                width: "20px",
                zIndex: 9999,
              }}
            /> */}
            <AutoCompleteContainer items={autoCompleteItems} />
          </span>
        </form>
      </Col>
    </Row>
  );
}
