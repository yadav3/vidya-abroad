import Link from "next/link";
import { PanelMenu } from "primereact/panelmenu";
import React from "react";
import { useState } from "react";
import { Button, Offcanvas } from "react-bootstrap";
import { BsMenuAppFill, BsTelephoneFill, BsFillCaretRightFill, BsEnvelopeFill, BsPlayBtn, BsBook, BsPeople } from "react-icons/bs";
import { HiMenu } from "react-icons/hi";
import styles from "./MobileMenu.module.scss";
import menuCountryData from "./MenuCountryData.json";
import { FooterDivider } from "../global/FooterContainer/FooterDivider";
import Image from "next/image";
import { getAWSImagePathBeta } from "../../context/services";
import { useBreakpoint } from "../../context/CustomHooks";
import { useEffect } from "react";
import { useRouter } from "next/router";
import { MdWebAsset } from "react-icons/md";

export default function MobileMenu() {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const breakpoint = useBreakpoint();

  const itemsCountry = [
    {
      label: "Explore Destinations",
      items: [
        ...menuCountryData.map(({ label, icon, href, items }) => {
          return {
            label,
            template: (item) => {
              return (
                <>
                  <div
                    className="d-flex align-items-center
                 justify-content-between cursor-pointer
                py-1 ps-2 mt-2"
                    onClick={() => {
                      window.location.href = href;
                    }}
                  >
                    <div className="text-decoration-none text-dark d-flex align-items-center gap-2 ps-3 fs-sm">
                      <div>
                        <Image src={getAWSImagePathBeta(icon)} alt={label} width={15} height={15} />
                      </div>
                      Study in {label}
                    </div>
                  </div>
                  <FooterDivider className="my-1 w-100" />
                </>
              );
            },
          };
        }),
      ],
    },
  ];

  const itemsUniversity = [
    {
      label: "Discover Universities",
      items: [],
      command: () => {
        window.location.href = "/universities";
      },
    },
  ];

  useEffect(() => {
    if (breakpoint > 990) {
      handleClose();
    }
  }, [breakpoint]);
  return (
    <>
      <Button variant="light" className="shadow-sm" onClick={handleShow}>
        <HiMenu fontSize={26} />
      </Button>

      <Offcanvas show={show} onHide={handleClose}>
        <Offcanvas.Header closeButton className="shadow-sm">
          <Offcanvas.Title>
            <div className={styles.logo} onClick={() => (window.location.href = "/")}>
              <Image src={"/logos/cv-abroad-logo.png"} alt="College Vidya Abroad Logo" width={180} height={50} objectFit="contain" />
            </div>
          </Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <div className={styles.menuContainer}>
            <p className={`text-bright-green ps-3 fs-lg mb-2 `}>Explore</p>
            <PanelMenu model={itemsCountry} className={`${styles.panelMenu} mobile-menu-panel`} />
            <PanelMenu model={itemsUniversity} className={`${styles.panelMenu} mobile-menu-panel`} />
            <p className={`text-bright-green ps-3 fs-lg my-2 `}>Resources</p>
            <FooterDivider className="my-2 " />
            <Link href="/suggest-me-a-university">
              <a onClick={handleClose} className="text-dark d-flex align-items-center justify-content-between my-4 px-2 fs-md ">
                <div className="d-flex align-items-center gap-2">
                  Suggest University in 2 Mins
                  <span className="fs-xs bg-orange text-white px-2 py-1 rounded-3 myshine ms-2">A.I Powered</span>
                </div>
                <BsFillCaretRightFill className="d-none d-sm-block" />
              </a>
            </Link>
            <FooterDivider className="my-2 " />
            <Link href="/ai-powered-faqs">
              <a onClick={handleClose} className="text-dark d-flex align-items-center justify-content-between my-4 px-2 fs-md ">
                <div className="d-flex align-items-center gap-2">
                  Explore with A.I
                  <span className="fs-xs bg-cvblue text-white px-2 py-1 rounded-3 myshine ms-2">Instan t Answer</span>
                </div>
                <BsFillCaretRightFill className="d-none d-sm-block" />
              </a>
            </Link>
            <FooterDivider className="my-2 " />
            <Link href="/blog">
              <a onClick={handleClose} className="text-dark d-flex align-items-center justify-content-between my-4 px-2 fs-md ">
                <div className="d-flex align-items-center gap-2">
                  <MdWebAsset />
                  <span>Blog</span>
                </div>
                <BsFillCaretRightFill className="d-none d-sm-block" />
              </a>
            </Link>
            <FooterDivider className="my-2 " />
            <Link href="/video-library">
              <a onClick={handleClose} className="text-dark d-flex align-items-center justify-content-between my-4 px-2 fs-md ">
                <div className="d-flex align-items-center gap-2">
                  <BsPlayBtn />
                  <span>Learn in 60 Seconds</span>
                </div>
                <BsFillCaretRightFill className="d-none d-sm-block" />
              </a>
            </Link>
            <FooterDivider className="my-2 " />
            <Link href="/resources">
              <a onClick={handleClose} className="text-dark d-flex align-items-center justify-content-between my-4 px-2 fs-md ">
                <div className="d-flex align-items-center gap-2">
                  <BsBook />
                  <span>Free eBooks</span>
                </div>
                <BsFillCaretRightFill className="d-none d-sm-block" />
              </a>
            </Link>
            <FooterDivider className="my-2 " />
            <Link href="/experts">
              <a onClick={handleClose} className="text-dark d-flex align-items-center justify-content-between my-4 px-2 fs-md ">
                <div className="d-flex align-items-center gap-2">
                  <BsPeople />
                  <span>Who we are</span>
                </div>
                <BsFillCaretRightFill className="d-none d-sm-block" />
              </a>
            </Link>
            <FooterDivider className="my-2 " />
            <Link href="https://collegevidya.in/career/">
              <a onClick={handleClose} className="text-dark d-flex align-items-center justify-content-between my-4 px-2 fs-md ">
                <div>
                  Careers
                  <span className="fs-xs bg-bright-green text-white px-2 py-1 rounded-3 myshine ms-2">We Are Hiring</span>
                </div>
                <BsFillCaretRightFill className="d-none d-sm-block" />
              </a>
            </Link>
            <FooterDivider className="my-2 " />
          </div>
          <p className={`text-bright-green ps-3 fs-lg my-2 `}>Contact Us</p>
          <FooterDivider className="my-2 " />
          <div>
            <BsTelephoneFill className="me-2" />
            Call Us At :
            <Link href="tel:18003099018">
              <a className="text-dark"> 1800 309 9018</a>
            </Link>
            <span className="fs-xs bg-orange text-white px-2 py-1 rounded-3 myshine ms-2">Toll Free</span>
          </div>
          <FooterDivider className="my-2 " />
          <div>
            <BsEnvelopeFill className="me-2" />
            EMail:
            <Link href="mailto:Info@collegevidyaabroad.com">
              <a className="text-dark"> Info@collegevidyaabroad.com</a>
            </Link>
          </div>
          <FooterDivider className="my-2 " />
        </Offcanvas.Body>
      </Offcanvas>
    </>
  );
}
