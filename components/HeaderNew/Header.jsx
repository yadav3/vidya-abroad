import React, { useEffect, useState } from "react";
import styles from "./Header.module.scss";
import Image from "next/image";
import { BsBook, BsBookFill, BsChat, BsChatDots, BsLightningChargeFill, BsList, BsPeople, BsPlayBtn, BsSearch } from "react-icons/bs";
import Link from "next/link";
import { Menubar } from "primereact/menubar";
import { useRouter } from "next/router";
import menuCountryData from "./MenuCountryData.json";
import { useBreakpoint } from "../../context/CustomHooks";
import { GiProcessor } from "react-icons/gi";
import MobileMenu from "./MobileMenu";
import Lottie from "lottie-react";
import GiftIconLottie from "../../public/gif/gift_icon_gif.json";

export default function Header({ logoURL = "/logos/cv-abroad-logo.png", styleClasses }) {
  const router = useRouter();
  const breakpoint = useBreakpoint();
  const [showMobileMenu, setShowMobileMenu] = useState(false);

  const items = [
    {
      label: "Destinations",
      className: "explore-destination-menu",
      items: [
        ...menuCountryData.map(({ label, href, icon, items }) => {
          return {
            label,
            template: () => {
              return (
                <div
                  className={`d-flex align-items-center gap-2 p-2 ps-3 fw-semibold cursor-pointer ${styles.headerDropdownItem}`}
                  onClick={() => router.push(href)}
                >
                  <span className="rounded-circle">
                    <Image src={icon} alt={label} width={15} height={15} objectFit='contain' />
                  </span>
                  {label}
                </div>
              );
            },
          };
        }),
      ],
    },
    {
      label: "Universities",
      command: () => {
        router.push("/universities");
      },
    },
    {
      label: "Blog",
      command: (item) => {
        router.push("/blog");
      },
    },

    {
      label: "Resources",
      className: "resources-menu",
      items: [
        {
          label: "University Finder",
          href: "/suggest-me-a-university/",
          template: (item) => {
            return (
              <div
                className={`d-flex align-items-center gap-2 p-2 ps-3 fw-semibold cursor-pointer ${styles.headerDropdownItem}`}
                onClick={() => router.push(item.href)}
              >
                <BsSearch />
                {item.label}
              </div>
            );
          },
        },
        {
          label: "Learn in 60 Seconds",
          href: "/video-library/",
          template: (item) => {
            return (
              <div
                className={`d-flex align-items-center gap-2 p-2 ps-3 fw-semibold cursor-pointer ${styles.headerDropdownItem}`}
                onClick={() => router.push(item.href)}
              >
                <BsPlayBtn />
                {item.label}
              </div>
            );
          },
        },
        {
          label: "Who we are",
          href: "/experts",
          template: (item) => {
            return (
              <div
                className={`d-flex align-items-center gap-2 p-2 ps-3 fw-semibold cursor-pointer ${styles.headerDropdownItem}`}
                onClick={() => router.push(item.href)}
              >
                <BsPeople />
                {item.label}
              </div>
            );
          },
        },
        {
          label: "A.I Powered FAQ's",
          href: "/ai-powered-faqs",
          template: (item) => {
            return (
              <div
                className={`d-flex align-items-center gap-2 p-2 ps-3 fw-semibold cursor-pointer ${styles.headerDropdownItem}`}
                onClick={() => router.push(item.href)}
              >
                <BsChat />
                {item.label}
              </div>
            );
          },
        },
        {
          label: "Careers",
          href: "https://collegevidya.in/career/",
          template: (item) => {
            return (
              <div
                className={`d-flex align-items-center gap-2 p-2 ps-3 fw-semibold cursor-pointer ${styles.headerDropdownItem}`}
                onClick={() => router.push(item.href)}
              >
                <BsList />
                {item.label}
              </div>
            );
          },
        },
        // {
        //   label: "Free eBooks",
        //   href: "/resources",
        //   template: (item) => {
        //     return (
        //       <div
        //         className={`d-flex align-items-center gap-2 p-2 ps-3 fw-semibold cursor-pointer ${styles.headerDropdownItem}`}
        //         onClick={() => router.push(item.href)}
        //       >
        //         <BsBook />
        //         {item.label}
        //       </div>
        //     );
        //   },
        // },
      ],
    },
    {
      label: "Free eBooks",
      template: (item) => {
        return (
          <span className="d-flex align-items-center gap-2  fw-semibold cursor-pointer  cursor-pointer" onClick={() => router.push("/resources")}>
            <BsBook />
            {item.label}
          </span>
        );
      },
    },
  ];

  const [isMenuMounted, setIsMenuMounted] = useState(false);

  useEffect(() => {
    setIsMenuMounted(true);
  }, []);

  useEffect(() => {
    if (isMenuMounted) {
      const resourcesMenu = document.querySelector(".resources-menu");
      const exploreDestinationMenu = document.querySelector(".explore-destination-menu");

      const addClass = (element) => () => {
        element && element.classList.add("p-menuitem-active");
      };

      const removeClass = (element) => () => {
        element && element.classList.remove("p-menuitem-active");
      };

      if (resourcesMenu) {
        resourcesMenu.addEventListener("mouseover", addClass(resourcesMenu));
        resourcesMenu.addEventListener("mouseout", removeClass(resourcesMenu));
      }

      if (exploreDestinationMenu) {
        exploreDestinationMenu.addEventListener("mouseover", addClass(exploreDestinationMenu));
        exploreDestinationMenu.addEventListener("mouseout", removeClass(exploreDestinationMenu));
      }
    }
  }, [isMenuMounted]);

  return (
    <>
      <header className="d-flex align-items-center justify-content-between py-3 py-lg-0 px-3 shadow-sm">
        <div className="d-flex align-items-center ">
          <div className="d-flex align-items-center ">
            <span className="d-lg-none">
              <MobileMenu />
            </span>
            <div className={`${styles.logo} cursor-pointer`} onClick={() => (window.location.href = "/")}>
              <Image src={logoURL} alt="College Vidya Abroad Logo" width={180} height={50} objectFit="contain" />
            </div>
          </div>
        </div>
        <div className="d-flex align-items-center gap-2 ">
          {breakpoint > "990" ? <Menubar model={items} className={`bg-white py-4 header-global-container ${styles.headerContainer}`} /> : null}
          {/* <Link href="/suggest-me-a-university">
            <a
              className={`${styles.mainBtn} shadow-sm bg-orange  text-white border-0 rounded-5 fs-md pt-2 pt-sm-3 fw-semibold d-flex align-items-center justify-content-center gap-2 position-relative`}
            >
              Suggest University <BsSearch className={`${styles.mainBtnIcon}`} />
              <span className={`${styles.btnTag}  position-absolute bg-white text-black px-3 fs-xs rounded-5 border border-dark `}>
                <GiProcessor /> A.I Powered
              </span>
            </a>
          </Link> */}
          {/* <Link href="/resources">
            <a
              className={`${styles.mainBtn} shadow-sm bg-orange  text-white border-0 rounded-5 fs-lg fw-semibold d-flex align-items-center justify-content-center gap-2 position-relative`}
            >
              Free Study Abroad Guides <AiOutlineBook className={`${styles.mainBtnIcon}`} />
            </a>
          </Link> */}
          {/* 
          <Link href="/ai-powered-faqs">
            <a
              className={`${styles.mainBtn} d-none   bg-bright-green shadow-sm  text-white border-0 rounded-5 fs-lg fw-semibold d-md-flex align-items-center justify-content-center gap-2 position-relative `}
            >
              Chat with A.I <BsChatDots className={`${styles.mainBtnIcon}`} />
              <span className={`${styles.btnTag} myshine position-absolute bg-white text-black px-3 fs-xs rounded-5 border border-dark`}>
                <BsLightningChargeFill /> Instant Answer
              </span>
            </a>
          </Link> */}
        </div>
      </header>
    </>
  );
}
