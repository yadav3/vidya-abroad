import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "next/image";

export function JourneyCard({ journeyDetails, reversed }) {
  return (
    <Row className={`gx-5 position-relative ${reversed ? "flex-row-reverse" : "flex-row"} mb-4 mb-sm-0`}>
      {/* <div
        className={`position-absolute ${reversed ? "end-0" : "start-0"} `}
        style={{
          bottom: "-50px",
          width: "50%",
          height: "100%",
          objectFit: "contain",
          transform: `${reversed ? "scaleX(-1)" : "scaleX(1)"}`,
        }}
      >
        <Image src={"/illustrations/left-line-journey.png"} className="" alt="" layout="fill" />
      </div> */}

      <Col md={6}>
        <div className="position-relative d-flex align-items-center justify-content-center mb-4 mb-sm-0">
          <Image src={journeyDetails.image} alt={journeyDetails.title} width={500} height={300} />
        </div>
      </Col>
      <Col md={6}>
        <div
          className="mx-auto d-flex flex-column align-items-center justify-content-center h-100"
          style={{
            width: "80%",
          }}
        >
          <div className="d-flex align-items-start gap-3">
            <span className="fs-3 text-cvblue fw-semibold">#{journeyDetails.id}</span>
            <div>
              <h3 className="text-dark-gray">{journeyDetails.title}</h3>
              <p className="text-muted">{journeyDetails.description}</p>
            </div>
          </div>
        </div>
      </Col>
    </Row>
  );
}
