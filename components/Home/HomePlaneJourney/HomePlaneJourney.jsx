import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import SectionHeading from "../../global/SectionHeading/SectionHeading";
import { PlaneJourney } from "./PlaneJourney";
import { JourneyCard } from "./JourneyCard";
import uuid from "react-uuid";

const data = [
  {
    id: 1,
    title: "Personalized Counselling for your dreams!",
    description:
      "Our Experts provide Personalized Counselling by understanding your Aspirations and Goals, making sure you have the best fit opportunity.",
    image: "/illustrations/personalized_counselling.png",
  },
  {
    id: 2,
    title: "We'll build your Profile",
    description: "Based on your interest, we start with the documentation process for applying in Universities.",
    image: "/illustrations/building_profile.png",
  },

  {
    id: 3,
    title: "Compare & Shortlist University",
    description:
      "Once your profile is ready, we prepare a list of all 'Best Fit Opportunities' for you and provide you a list of shortlisted Universities to go for!",
    image: "/illustrations/shortlist_university.png",
  },
  {
    id: 4,
    title: "Apply for Universities",
    description: "Based on your interest, we start filling the Applications of your desired Universities",
    image: "/illustrations/apply_to_university.png",
  },

  {
    id: 5,
    title: "Let the Offer Letter Rain!",
    description:
      "Majority of the cases, the Offer Letter is sent to the Prospect to be accepted, and looks forward to the deposit amount to confirm the admission within the defined deadline by respective University",
    image: "/illustrations/offer_letter.png",
  },
  {
    id: 6,
    title: "Get Assured VISA Approval",
    description:
      "Majority of the cases, the Offer Letter is sent to the Prospect to be accepted, and looks forward to the deposit amount to We make sure to assist you throughout the VISA Process, right from Application to Interview!",
    image: "/illustrations/visa_approval.png",
  },
];

export default function HomePlaneJourney() {
  return (
    <Container>
      <Row>
        <Col>
          <SectionHeading
            heading={`Perks of Starting <br/><span class="text-cvblue">Your Journey with us</span>`}
            subHeading="Here how we make your journey easy and hassle-free"
          />
        </Col>
      </Row>
      <Row className="position-relative mt-5 ps-3 ps-md-0">
        <PlaneJourney />
        <Col>
          {data.map((item, index) => {
            return <JourneyCard key={uuid()} journeyDetails={item} reversed={index % 2 === 0} />;
          })}
        </Col>
      </Row>
    </Container>
  );
}
