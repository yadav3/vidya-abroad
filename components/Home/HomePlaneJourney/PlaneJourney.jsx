import React, { useEffect, useRef, useState } from "react";
import styles from "./HomePlaneJourney.module.scss";

export function PlaneJourney() {
  // State to store the scroll top percentage
  const [scrollTop, setScrollTop] = useState(0);

  // Reference to the scroll section element
  const scrollSectionRef = useRef(null);

  // Function to handle scroll events
  const handleScroll = () => {
    // Check if the scroll section is above the viewport
    if (scrollSectionRef?.current?.getBoundingClientRect().top < 0) {
      // Calculate the scroll speed based on the window width
      const newSpeed = window.innerWidth < 768 ? 0.04 : 0.1;

      // Calculate the distance from the top of the scroll section
      const distanceFromTop = Math.abs(scrollSectionRef.current.getBoundingClientRect().top) * newSpeed;

      // Set the scroll top percentage based on the distance from the top
      if (distanceFromTop >= 100) {
        setScrollTop(100);
      } else {
        setScrollTop(distanceFromTop);
      }
    }
  };

  // Add scroll event listener on component mount
  useEffect(() => {
    window.addEventListener("scroll", handleScroll);

    // Remove the scroll event listener on component unmount
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  // Render the component
  return (
    <div className={`${styles.journeyProgressBarContainer} d-sm-block`}>
      <div
        ref={scrollSectionRef}
        className={styles.activeProgressbar}
        style={{
          height: `${scrollTop}%`,
          transition: "height ease-in-out",
        }}
      ></div>
    </div>
  );
}

