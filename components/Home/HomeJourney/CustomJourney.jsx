import React from "react";
import Image from "next/image";
import styles from "./HomeJourney.module.scss";

export default function CustomJourney({ number, heading, subHeading, paragraph, image }) {
  return (
    <div className={`${styles.jorBox} row d-flex`}>
      <div className={`${styles.jorRowBox} col-md-6`}>
        <div className={`${styles.descriptionSection} journyDescription`}>
          <div className="d-flex  gap-3">
            <span className="fs-1">#1</span>
            <div>
              <h2 >
                {heading}
                {subHeading && <span className="text-primary"> {subHeading}</span>}
              </h2>
              <p className="text-muted">{paragraph}</p>
            </div>
          </div>
        </div>
      </div>
      <div className={`${styles.jorRowBox} ${styles.jorRowBoxImage} col-md-6 mt-2`}>
        <Image src={image} width={500} height={333} className={`${styles.imgJourney}`} alt="cardImage" />
      </div>
    </div>
  );
}
