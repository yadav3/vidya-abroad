import React from "react";
import styles from "./HomeJourney.module.scss";
import { useState, useRef, useEffect } from "react";
import CustomJourney from "./CustomJourney";

export default function HomeJourney() {
  const [scrollTop, setScrollTop] = useState(0);
  const scrollSectionRef = useRef(null);
  const boxRef = useRef(null);

  const OnScroll = () => {
    if (scrollSectionRef?.current?.getBoundingClientRect().top < 0) {
      var distanceFromTop = scrollSectionRef.current.getBoundingClientRect().top;
      setScrollTop(Math.abs(distanceFromTop) / 10 > 100 ? 100 : Math.abs(distanceFromTop) / 10);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", OnScroll);
  }, []);

  return (
    <>
      <section className={`${styles.journeyContainer} ${styles.sectionGlobal}`}>
        <div className={`${styles.container} `}>
          <div className={styles.slideContainer}>
            <div className={`${styles.journey_progress} d-sm-block`}>
              <div ref={scrollSectionRef} className={styles.active_progress} style={{ height: `${scrollTop}%` }}></div>
            </div>
            <div className={`${styles.slideBox01}`} ref={boxRef}>
              <CustomJourney
                heading="We Compile -"
                subHeading="You Decide!"
                paragraph="With our Easy - Customized AI-Based Search options, 
              we will ensure that without many clicks and simple inputs, 
              you get your dream Destination &amp; Course that fits you best."
                image="/journey/weCompile.svg"
              />
            </div>
            <div className={`${styles.slideBox02} `}>
              <CustomJourney
                heading="Analyze Your"
                subHeading=" Return on Investment"
                paragraph="We understand you want to ensure your future! Wherein,
              you can analyze &amp; compare the best return of investment
               for the time, money &amp; effort you are willing to commit."
                image="/journey/returnInvestment.svg"
              />
            </div>
            <div className={`${styles.slideBox03}`}>
              <CustomJourney
                heading="Go ahead &amp;"
                subHeading=" Compare it!"
                paragraph="Comparing your options is a tedious task. Unlike other platforms,
              we will assist you in comparing universities and courses as per
               the duration, curriculum, features, ROIs, and so on. 
               We have it all sorted with our comparison portal.
               "
                image="/journey/compareit.svg"
              />
            </div>
            <div className={`${styles.slideBox05}`}>
              <CustomJourney
                heading="Still confused?"
                subHeading=" Talk to Experts!"
                paragraph="Are you still skeptical and want some good advice? Guess What?! You are just a few clicks away from our 
              Experts who would love to clear any sort of confusion you may have through our Interactive Platform."
                image="/journey/talkExperts.svg"
              />
            </div>
          </div>
        </div>
      </section>
    </>
  );
}
