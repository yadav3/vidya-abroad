import React from "react";
import { Accordion } from "react-bootstrap";
import styles from "./HomeFaq.module.scss";

const contents = [
  {
    header: "Why us?",
    body: `<p>We are CollegeVidya (Study Abroad). And through our Interactive Platform,
     we provide you with a portal to compare universities of your choice and courses of 
     your interest on an international scale. </p>
     <div>Moreover, we let you analyze and compare the best return on investment for 
     the time, money, and efforts you are willing to put into your dream of studying 
     abroad. Likewise, our experts are here to help and guide you with all of your 
     doubts, just like a good friend would.</div>`,
  },
  {
    header: "Why Study Abroad?",
    body: `<p>Your study abroad journey enables you to get a great deal of exposure as
     well as experience which also stimulates you to bloom in your career, now with
      your reasonable/better perception of the world.</p>
      <div>Furthermore, you can acquire new languages, admire other cultures, withstand
       the challenges of residency in another country and acquire a vast insight
        into the world. In today's world, almost all contemporary businesses look 
        for these aspects when hiring, and such qualities are only going to become 
        more significant in the future.</div>`,
  },
  {
    header: "What documents are required to study abroad?",
    body: `
    <p>You have to be prepared with some important documents to fulfil your dream of studying abroad. And to make it easier for you we have prepared a list of all the documents required for the same, have a look.</p>
     <ul>
     <li><b>Application Form</b></li>
     <li><b>Statement of Purpose (SOP)</b></li>
     <li><b>Academic transcripts</b></li>
     <li><b>Letter of Recommendation (LOR)</b></li>
     <li><b>Curriculum Vitae (CV) or Resume</b></li>
     <li><b>Test Scores</b></li>
     <li><b>Essays</b></li>
   
     </ul>
    `,
  },
  {
    header: "How does studying abroad help your career?",
    body: `<p>Studying abroad proposes an enormous exhibition of new alternatives by pushing you out of your comfort zone. It also improves your communication skills which is an important aspect in today's world and will help you in building other elements of your communication skills, incorporating public speaking and presenting, and reconciling academic writing, and non-verbal communication. Plus, you can also demonstrate to future and current employers that you have the open mind, resourcefulness, and momentum required to adapt to a distinct setting.</p>`,
  },
];

export default function HomeFaq() {
  return (
    <div className={`${styles.faqContainer}`}>
      <h2 className={`${styles.sectionHeading}`}>FAQs</h2>
      <div className={`${styles.left}`}>
        <h2>We would love to answer!</h2>
      </div>
      <div className={`${styles.faqs}`}>
        <Accordion defaultActiveKey={[0]}>
          {contents.map((data, i) => {
            return (
              <Accordion.Item
                key={i}
                eventKey={i}
                className={`${styles.accordionHeader}`}
              >
                <Accordion.Header>{data.header}</Accordion.Header>
                <Accordion.Body>
                  <span dangerouslySetInnerHTML={{ __html: data.body }} />
                </Accordion.Body>
              </Accordion.Item>
            );
          })}
        </Accordion>
      </div>
    </div>
  );
}
