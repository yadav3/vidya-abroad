import Image from "next/image";
import { useEffect } from "react";
import { useRef } from "react";
import uuid from "react-uuid";
import { useStateContext } from "../../../context/StateContext";
import styles from "./OurServicesPage.module.scss";
import ReactPageScroller from "react-page-scroller";
import Link from "next/link";
import { useState } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

export default function OurServicesPage() {
  const { services, outServiceScrollerIndex, setOurServiceScrollerIndex, showMenu } = useStateContext();

  return (
    <Container className="p-0" fluid>
      <Row>
        <Col>
          {services.map((service, i) => (
            <ServiceCard
              service={service}
              key={uuid()}
              uClass={i % 2 ? styles.serviceColumnReverse : styles.serviceColumn}
              bg={i % 2 ? "#E7EDFD" : "#F5F5F5"}
            />
          ))}
        </Col>
      </Row>
      <Row className="py-4 pb-5">
        <Link href="suggest-me-a-university">
          <a
            className={`  
          rounded-pill bg-cvblue text-white text-center text-decoration-none px-4 py-2 mx-auto 
          `}
            style={{
              minWidth: "max-content",
              maxWidth: "max-content",
            }}
          >
            Connect with our Expert to Know More
          </a>
        </Link>
      </Row>
    </Container>
  );
}

const ServiceCard = ({ service, uClass, bg }) => {
  return (
    <div
      className={`${styles.serviceCard} ${uClass} w-100`}
      style={{
        backgroundColor: bg,
        backgroundImage: `url(${service.bg})`,
      }}
    >
      <div className={styles.left}>
        <div className={styles.imageContainer}>
          <Image src={service.image} alt={service.name} width={500} height={500} objectFit="contain" />
        </div>
      </div>
      <div className={styles.right}>
        <h2
          dangerouslySetInnerHTML={{
            __html: service.name,
          }}
        ></h2>
        <div className={styles.points}>
          {service.points.map((point) => (
            <div className={styles.point} key={uuid()}>
              <span className={styles.pointIcon}>
                <i className="pi pi-arrow-right"></i>
              </span>
              <p>{point}</p>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};
