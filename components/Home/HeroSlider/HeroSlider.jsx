import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, Pagination, EffectFade } from "swiper";

import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/autoplay";
import "swiper/css/effect-fade";

import styles from "./HeroSlider.module.scss";
import Image from "next/image";
import Link from "next/link";
import { BsFillCpuFill } from "react-icons/bs";

export default function HeroSlider() {
  return (
    <>
      <div className={styles.heroSlider}>
        <Swiper
          spaceBetween={30}
          centeredSlides={true}
          effect={"fade"}
          autoplay={{
            delay: 4000,
            disableOnInteraction: true,
          }}
          modules={[EffectFade, Autoplay]}
          className="homeHeroSlider"
        >
          <SwiperSlide>
            <Image alt="Hero Banner" src="/banners/Artboard 1.jpg" priority={true} width={1920} height={700} className={styles.heroBanner} />
          </SwiperSlide>
          <SwiperSlide>
            <Image alt="Hero Banner" src="/banners/Artboard 2.jpg" width={1920} height={700} className={styles.heroBanner} />
          </SwiperSlide>
          <SwiperSlide>
            <Image alt="Hero Banner" src="/banners/Artboard 3.jpg" width={1920} height={700} className={styles.heroBanner} />
          </SwiperSlide>
          <SwiperSlide>
            <Image alt="Hero Banner" src="/banners/Artboard 4.jpg" width={1920} height={700} className={styles.heroBanner} />
          </SwiperSlide>
          <SwiperSlide>
            <Image alt="Hero Banner" src="/banners/Artboard 5.jpg" width={1920} height={700} className={styles.heroBanner} />
          </SwiperSlide>
        </Swiper>

        <Link href="/suggest-me-a-university">
          <a className={` ${`${styles.navLink}`} ${styles.suggestMeUniversityBtn}`}>
            Connect with an Expert
            <span className={`  ${styles.navLinkLabelMobile}`}>
              <BsFillCpuFill size={15} /> A.I Powered
            </span>{" "}
            <i className={`pi pi-angle-right ${styles.navLinkArrow}`}></i>
          </a>
        </Link>
      </div>
      <div className={styles.heroSliderMobile}>
        <Swiper
          spaceBetween={30}
          centeredSlides={true}
          loop={true}
          autoplay={{
            delay: 2500,
            disableOnInteraction: true,
          }}
          pagination={{
            clickable: true,
          }}
          modules={[Autoplay, Pagination]}
          className="homeHeroSlider"
        >
          <SwiperSlide>
            <Image alt="Hero Banner Mobile" src="/banners/home-banner-small-1.jpg" priority={true} width={648} height={298} className={styles.heroBanner} />
          </SwiperSlide>
          <SwiperSlide>
            <Image alt="Hero Banner Mobile" src="/banners/home-banner-small-2.jpg" width={648} height={298} className={styles.heroBanner} />
          </SwiperSlide>
          <SwiperSlide>
            <Image alt="Hero Banner Mobile" src="/banners/home-banner-small-3.jpg" width={648} height={298} className={styles.heroBanner} />
          </SwiperSlide>
          <SwiperSlide>
            <Image alt="Hero Banner Mobile" src="/banners/home-banner-small-4.jpg" width={648} height={298} className={styles.heroBanner} />
          </SwiperSlide>
          <SwiperSlide>
            <Image alt="Hero Banner Mobile" src="/banners/home-banner-small-5.jpg" width={648} height={298} className={styles.heroBanner} />
          </SwiperSlide>
        </Swiper>
      </div>
    </>
  );
}
