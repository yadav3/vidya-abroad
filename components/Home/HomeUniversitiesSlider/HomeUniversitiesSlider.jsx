import React from "react";
import Marquee from "react-fast-marquee";
import Image from "next/image";
import Link from "next/link";
import styles from "./HomeUniversitiesSlider.module.scss";
import SectionHeading from "../../global/SectionHeading/SectionHeading";
import Card from "react-bootstrap/Card";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import uuid from "react-uuid";
import { getAWSImagePath } from "../../../context/services";

import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/effect-fade";
import "swiper/css/pagination";
import "swiper/css/navigation";
import "swiper/css/autoplay";
import "swiper/css/grid";
import { EffectFade, Navigation, Autoplay, Grid, Pagination } from "swiper";
import { Tooltip } from "react-tippy";

export default function HomeUniversitiesSlider({ universitiesLogos }) {
  return (
    <Container fluid>
      <Row>
        <Col>
          <SectionHeading
            heading={`<span>
          <span class="text-cvblue">400+</span> Universities
      </span>`}
            subHeading={"Select from the best universities in the world"}
            subHeadingClass="m-0  mb-0 fs-lg w-100 text-center"
          />
          {/* <Swiper
            slidesPerView="auto"
            // loopedSlides={9}
            grid={{
              rows: 2,
              slidesPerRow: 2,
              fill: "row",
            }}
            pagination={false}
            spaceBetween={50}
            speed={5000}
            autoplay={{
              delay: 0,
              disableOnInteraction: false,
            }}
            loop={false}
            navigation={false}
            breakpoints={{
              "@0.00": {
                slidesPerView: 2,
                spaceBetween: 30,
              },
              500: {
                slidesPerView: 3,
                spaceBetween: 30,
              },

              800: {
                slidesPerView: 5,
                spaceBetween: 40,
              },
              900: {
                slidesPerView: 6,
                spaceBetween: 40,
              },
            }}
            modules={[Grid, Navigation, EffectFade, Autoplay, Pagination]}
            className="homeUniversitiesSlider"
          >
            {universitiesLogos.map((university, index) => (
              <SwiperSlide key={index} className={`cursor-pointer ${styles.cardSlide}`}>
                <UniversityCard university={university} />
              </SwiperSlide>
            ))}
          </Swiper> */}
        </Col>
      </Row>
      <Row
      onClick={() => window.location.href = "/universities"}
      >
        <Col>
          <Marquee gradientWidth="40px" >
            <Image src={"/banners/university-logo-scroller.png"} alt="" width={4000} height={400} />
          </Marquee>
        </Col>
      </Row>
    </Container>
  );
}

const UniversityCard = ({ university }) => {
  return (
    <Card
      className={`p-0 p-sm-4 d-flex align-items-center justify-content-center ${styles.universityLogoCard}`}
      onClick={() => (window.location.href = `/universities/${university.slug}`)}
    >
      <div
        className="position-relative"
        style={{
          width: "70px",
          height: "70px",
        }}
      >
        <Image src={getAWSImagePath(university.logo_full)} alt={university.name} width={200} height={200} objectFit="contain" layout="responsive" />
      </div>
      <span className="fs-sm text-truncate w-100 text-center mt-2 px-2">
        <Tooltip title={university.name}>{university.name}</Tooltip>
      </span>
    </Card>
  );
};
