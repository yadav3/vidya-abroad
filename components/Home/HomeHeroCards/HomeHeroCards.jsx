import React from "react";
import styles from "./HomeHeroCards.module.scss";
import uuid from "react-uuid";
import Image from "next/image";
import { useStateContext } from "../../../context/StateContext";
import { useRouter } from "next/router";

import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Pagination, Navigation } from "swiper";
import SectionHeading from "../../global/SectionHeading/SectionHeading";

export default function HomeHeroCards() {
  const router = useRouter();

  const { services } = useStateContext();

  return (
    <div className={`${styles.container} mb-md-5`}>
      <SectionHeading
        heading={`<span>
          How we can <span class="text-cvblue">help</span> you!
      </span>`}
        subHeading={"You can choose from our wide range of services."}
      />
      <Swiper
        slidesPerView="auto"
        spaceBetween={30}
        loop={true}
        pagination={{
          clickable: true,
        }}
        navigation={true}
        modules={[Pagination, Navigation]}
        breakpoints={{
          1000: {
            slidesPerView: 2,
            spaceBetween: 10,
          },
          1300: {
            slidesPerView: 3,
            spaceBetween: 10,
          },
        }}
        className="homeHeroServicesSlider"
      >
        {services.map((service, i) => (
          <SwiperSlide key={uuid()}>
            <div className={styles.serviceCard} >
              <Image alt="Service Image Illustration" src={service.image} width={300} height={300} objectFit="contain" />
              <p
                dangerouslySetInnerHTML={{
                  __html: service.name,
                }}
              ></p>
            </div>
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
}
