import React, { useEffect, useState } from "react";
import styles from "./HomeHeroVideo.module.scss";
import { BsVolumeMuteFill, BsVolumeUpFill } from "react-icons/bs";
import ReactTypingEffect from "react-typing-effect";
import Link from "next/link";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "next/image";
import { ImQuotesLeft, ImQuotesRight } from "react-icons/im";
import { useRouter } from "next/router";
import { useBreakpoint } from "../../../context/CustomHooks";

export default function HomeHeroVideo() {
  const router = useRouter();
  const breakpoint = useBreakpoint();

  const handleNavigateToSuggestMeAUniversity = () => {
    window.location.href = "/suggest-me-a-university";
  };

  return (
    <Container fluid>
      <Row>
        <Col className="p-0">
          {breakpoint <= 575.98 ? (
            <div className={`${styles.heroBannerMobileContainer} w-100`}>
              <Image
                src={"/banners/mobile-herobanner-2023.jpg"}
                alt="Home Hero Banner"
                layout="responsive"
                width={600}
                height={300}
                objectFit="cover"
              />
              <div className={`${styles.contentContainer} d-flex flex-column align-items-center justify-content-center`}>
                <h1 className="m-0">
                  <span className="">
                    <ImQuotesLeft size={20} className={styles.bannerQuotes} />
                    <span className="mx-2"> Uncomplicate </span>
                    <ImQuotesRight size={20} className={styles.bannerQuotes} />
                  </span>
                  <span className={`${styles.mainBannerSubtitle}`}>
                    your <span className="fw-bold">Study Abroad</span> planning
                  </span>
                </h1>
                <button
                  className={`myshine popup-item bg-white text-cvblue  border-0 rounded-pill px-4 py-2 fw-semibold d-flex align-items-center gap-1 ${styles.getStartedBtn} `}
                  onClick={handleNavigateToSuggestMeAUniversity}
                >
                  Speed up Admissions by <span className="fs-5">10x</span>
                </button>
              </div>
            </div>
          ) : (
            <div className={`${styles.heroVideoContainer} w-100`}>
              <video className={`${styles.heroVideo} w-100`} autoPlay muted loop playsInline poster="/banners/home-herobanner-2023.jpg">
                <source src="/videos/herobanner-video.mp4" type="video/mp4" />
              </video>
              <div className={`${styles.contentContainer} d-flex flex-column align-items-center justify-content-center `}>
                <h1 className="m-0">
                  <span className="">
                    <ImQuotesLeft size={20} className={styles.bannerQuotes} />
                    <span className="mx-2"> Uncomplicate </span>
                    <ImQuotesRight size={20} className={styles.bannerQuotes} />
                  </span>
                  <span className={`${styles.mainBannerSubtitle}`}>
                    your <span className="fw-bold">Study Abroad</span> planning
                  </span>
                </h1>

                <button
                  className={`myshine popup-item bg-white text-cvblue  border-0 rounded-pill px-4 py-2 fw-semibold d-flex align-items-center gap-1  ${styles.getStartedBtn} `}
                  onClick={handleNavigateToSuggestMeAUniversity}
                >
                  Speed up Admissions by <span className="fs-5">10x</span>
                </button>
              </div>
            </div>
          )}
        </Col>
      </Row>
    </Container>
  );
}
