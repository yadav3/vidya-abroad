import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import uuid from "react-uuid";
import Image from "next/image";
import styles from "./HomeTopCTASection.module.scss";
import Link from "next/link";

export default function HomeTopCTASection() {
  const buttons = [
    {
      name: "Discover Universities",
      href: "/universities",
      icon: "/icons/university-3d-icon.png",
    },
    {
      name: "Chat with A.I",
      href: "/ai-powered-faqs",
      icon: "/icons/chat-ai-icon.png",
    },
    {
      name: "Free eBooks",
      href: "/resources",
      icon: "/icons/blog-3d-icon.png",
    },
    // {
    //   name: "Explore Blogs",
    //   href: "/blog",
    //   icon: "/icons/blog-3d-icon.png",
    // },
  ];

  return (
    <Container>
      <Row>
        <Col>
          <div className="d-flex flex-wrap align-items-center justify-content-center gap-3  pb-0">
            {buttons.map((button) => (
              <Link href={button.href} role="button" key={uuid()}>
                <a className={`${styles.mainButton} p-2  rounded-3 d-flex align-items-center justify-content-center gap-2`}>
                  <span className={`${styles.mainButtonImage}`}>
                    {button.icon?.length > 0 ? (
                      <Image src={button.icon} alt="" width={50} height={50} objectFit="contain" layout="responsive" />
                    ) : null}
                  </span>
                  <span>{button.name}</span>
                </a>
              </Link>
            ))}
          </div>
        </Col>
      </Row>
    </Container>
  );
}
