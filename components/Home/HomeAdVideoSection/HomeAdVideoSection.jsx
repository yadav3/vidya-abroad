import React, { useState } from "react";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "next/image";
import SectionHeading from "../../global/SectionHeading/SectionHeading";
import { AiOutlinePlayCircle } from "react-icons/ai";

export default function HomeAdVideoSection() {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <Container>
      <Row>
        <Col>
          <div className="d-flex flex-column align-items-center pb-2 pb-md-5">
            <SectionHeading
              heading={`<span>
          Why <span class="text-cvblue">Choose</span> College Vidya Abroad
      </span>`}
              subHeading={"Your one way stop for all study abroad queries!"}
            />
            <div
              className="position-relative "
              style={{
                width: "900px",
                maxWidth: "100%",
              }}
            >
              {!show && (
                <div
                  className="position-absolute  top-50 start-50 translate-middle rounded-circle bg-white text-cvblue p-2 circle-pulse-ring cursor-pointer"
                  style={{
                    zIndex: 10,
                  }}
                  onClick={handleShow}
                >
                  <AiOutlinePlayCircle size={50} />
                </div>
              )}
              {show ? (
                <iframe
                  onClick={handleClose}
                  width="100%"
                  height="500"
                  src="https://www.youtube.com/embed/WxU8InTHuWU?autoplay=1"
                  title="“UNCOMPLICATE” Your Study Planning Abroad | Speed Up Admissions By 10X"
                  frameBorder="0"
                  //   allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                  allowFullScreen
                ></iframe>
              ) : (
                <Image src={"/banners/cv-abroad-ad1-thumbnail.jpg"} width={900} height={500} objectFit="contain" alt="" />
              )}
            </div>
          </div>
        </Col>
      </Row>
      {/* <Modal show={show} onHide={handleClose} centered>
        <Modal.Header closeButton>
          <Modal.Title>Speed Up Admissions By 10X</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="w-100">
            <iframe
              width="100%"
              height="577"
              src="https://www.youtube.com/embed/WxU8InTHuWU"
              title="“UNCOMPLICATE” Your Study Planning Abroad | Speed Up Admissions By 10X"
              frameBorder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
              allowFullScreen
            ></iframe>
          </div>
        </Modal.Body>
      </Modal> */}
    </Container>
  );
}
