import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Tab from "react-bootstrap/Tab";
import Nav from "react-bootstrap/Nav";
import uuid from "react-uuid";
import styles from "./HomeMajorBrowser.module.scss";

export default function HomeMajorBrowser({ majors }) {

  const categories = [
    {
      id: 1,
      name: "Bachelors",
    },
    {
      id: 2,
      name: "Masters",
    },
    {
      id: 3,
      name: "Diploma",
    },
  ];

  return (
    <Container>
      <Row>
        <Col>
          <div className={`${styles.majorTabContainer}`}>
            <Tab.Container id="left-tabs-example" defaultActiveKey="1">
              <Row>
                <Col sm={3}>
                  <Nav variant="pills" className="flex-column">
                    {categories.map((category) => (
                      <Nav.Item key={uuid()}>
                        <Nav.Link eventKey={category.id}>{category.name}</Nav.Link>
                      </Nav.Item>
                    ))}
                  </Nav>
                </Col>
                <Col sm={9}>
                  <Tab.Content>
                    {categories.map((category) => (
                      <Tab.Pane eventKey={category.id} key={uuid()} className={`${styles.majorTabPanel} p-1`}>
                        <div className="d-flex flex-wrap gap-3">
                          {majors
                            .filter((major) => major.category_id === category.id)
                            .map((major) => (
                              <div key={major.id} className="p-2 bg-light rounded-3">
                                <p className="m-0">{major.name}</p>
                                <small className="text-muted fs-sm">{major._count.specializations_majors} Specializations</small>
                              </div>
                            ))}
                        </div>
                      </Tab.Pane>
                    ))}
                  </Tab.Content>
                </Col>
              </Row>
            </Tab.Container>
          </div>
        </Col>
      </Row>
    </Container>
  );
}
