import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "next/image";
import styles from "./BookAFreeCallLeadContainer.module.scss";
import LeadForm from "../../global/LeadForm/LeadForm";
import OttaPointLeadFormPoints from "../../Otta/OttaPointLeadFormPoints";
import OttaPointLeadSwiper from "../../Otta/OttaPointLeadSwiper";
import { FooterDivider } from "../../global/FooterContainer/FooterDivider";
export default function BookAFreeCallLeadContainer({ majors }) {
  return (
    <Row className="flex-column-reverse flex-md-row">
      <Col md={6}>
        <div className="d-flex align-items-center justify-content-center">
          <LeadForm majors={majors} ctaText="Arrange a Call" />
        </div>
      </Col>
      <Col md={6}>
        <div className="d-none d-md-flex h-100 align-items-center justify-content-center">
          <OttaPointLeadFormPoints />
        </div>
        <Row className="d-md-none">
          <Col>
            <OttaPointLeadSwiper />
          </Col>
        </Row>
      </Col>
    </Row>
  );
}
