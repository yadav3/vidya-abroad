import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "next/image";
import styles from "./BookAFreeCallPageBanner.module.scss";

export default function BookAFreeCallPageBanner() {
  return (
    <Row
      className="flex-column-reverse flex-md-row  align-items-center justify-content-center pb-3 pb-md-0"
      style={{
        maxWidth: "1300px",
        marginInline: "auto",
        backgroundImage: `url("/country/countrypage/herobanners/hero_pattern.png")`,
        backgroundSize: "cover",
        backgroundPosition: "top",
      }}
    >
      <Col md={6}>
        <div className={`${styles.col1} d-flex flex-column justify-content-center`}>
          <h1 className="text-center text-md-start text-cvblue fw-bold">Book a Free Call</h1>
          <p className="text-center text-md-start d-none d-md-block">
            Unlock the power of a personalized conversation. Book a free call with our experts to discover how we can assist you in making your
            admissions 10x Faster. Don&apos;t miss out on this opportunity and take the first step towards success.
          </p>
          <p className="fw-semibold text-center text-md-start">
            & Speed Up your admission by <span className="text-cvblue fs-4">10x</span>
          </p>
        </div>
      </Col>
      <Col md={6}>
        <div className={`${styles.col2} pe-0 pe-md-5 d-flex align-items-center justify-content-center`}>
          <Image src="/illustrations/book-free-call-illustration.png" alt="Book a Free Call Banner" width={500} height={300} objectFit="contain" />
        </div>
      </Col>
    </Row>
  );
}
