import React from "react";
import { useLocalStorage } from "../../../context/CustomHooks";
import { useRouter } from "next/router";
import { FreeResourceLeadForm } from "../FreeResourceLeadForm/FreeResourceLeadForm";
import { BsArrowRight } from "react-icons/bs";
import { AiFillLock } from "react-icons/ai";
import { Tooltip } from "react-tippy";
import classNames from "classnames";

export default function ResourcePageStrip({ majors, handleDownloadFile, freebie }) {
  const [isDownloaded, setIsDownloaded] = useLocalStorage("DidUserDownloadedTheFreebie", false);
  const [downloadedFreebies, setDownloadedFreebies] = useLocalStorage("DownloadedFreebies");

  const handleDownloadEbook = () => {
    setIsDownloaded(true);
    handleDownloadFile();

    if (!downloadedFreebies?.includes(freebie.slug)) {
      setDownloadedFreebies([...(downloadedFreebies || []), freebie.slug]);
    }
  };

  const containerStyle = {
    maxWidth: "1200px",
    marginInline: "auto",
  };

  const renderDownloadButton = () => {
    const isCurrentFreebieDownloaded = downloadedFreebies?.includes(freebie.slug);

    if (isDownloaded) {
      return (
        <button
          className={`border-0 text-cvblue bg-white p-2 px-5 fs-5 rounded-3 fw-semibold`}
          onClick={handleDownloadEbook}
          style={{ opacity: isDownloaded && !isCurrentFreebieDownloaded ? 0.6 : 1 }}
          disabled={isDownloaded && !isCurrentFreebieDownloaded}
        >
          {isCurrentFreebieDownloaded ? (
            "Already Downloaded"
          ) : (
            <span>
              Download <AiFillLock />
            </span>
          )}
        </button>
      );
    } else {
      return (
        <FreeResourceLeadForm majors={majors} ctaText={`Download Free ${freebie.title}`} onSubmitDone={handleDownloadEbook}>
          <button className="border-0 text-cvblue bg-white p-2 px-5 fs-5 rounded-3 fw-semibold">
            {isCurrentFreebieDownloaded ? "Already Downloaded" : "Download"}
          </button>
        </FreeResourceLeadForm>
      );
    }
  };

  return (
    <div className="w-100 bg-cvblue text-white py-4 px-4 d-none d-sm-block">
      <div className={classNames("d-flex", "align-items-center", "justify-content-between", "w-100")} style={containerStyle}>
        <span>
          Click Here <span className="fw-bold">Download</span> <BsArrowRight size={20} className="ms-2" />
        </span>
        {renderDownloadButton()}
      </div>
    </div>
  );
}
