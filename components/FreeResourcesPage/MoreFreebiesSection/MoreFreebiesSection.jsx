import React from "react";
import SectionHeading from "../../global/SectionHeading/SectionHeading";
import FreeResourceCard from "../FreeResourceCard/FreeResourceCard";
import uuid from "react-uuid";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

export default function MoreFreebiesSection({ relatedFreebies }) {
  return (
    <div className="d-flex flex-column mb-5">
      <SectionHeading heading={`<span>Recommended for you</span>`} headingClass="" className="text-center mb-3 mt-4"  />
      <Row className="justify-content-center">
        {relatedFreebies.map((freebie) => (
          <Col key={uuid()} md={4}>
            <FreeResourceCard freebie={freebie} />
          </Col>
        ))}
      </Row>
    </div>
  );
}
