import React from "react";
import FreeResourceCard from "../FreeResourceCard/FreeResourceCard";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import uuid from "react-uuid";

export default function FreeResourcesContainer({ freebies }) {

  return (
    <Row>
      {freebies.map((freebie) => (
        <Col key={uuid()} md={4}>
          <FreeResourceCard freebie={freebie} />
        </Col>
      ))}
    </Row>
  );
}
