import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "next/image";
import { useRouter } from "next/router";
import { useLocalStorage } from "../../../context/CustomHooks";
import { FreeResourceLeadForm } from "../FreeResourceLeadForm/FreeResourceLeadForm";
import { BreadCrumb } from "primereact/breadcrumb";
import { AiFillLock, AiOutlineBulb, AiOutlineHome } from "react-icons/ai";
import { Tooltip } from "react-tippy";
import { useEffect } from "react";
import { setCookie } from "cookies-next";
import {
  FacebookIcon,
  FacebookShareButton,
  LinkedinIcon,
  LinkedinShareButton,
  TwitterIcon,
  TwitterShareButton,
  WhatsappIcon,
  WhatsappShareButton,
} from "next-share";
import { getAWSImagePath } from "../../../context/services";
import Link from "next/link";
import freebieService from "../../../services/freebie.service";

export default function FreeResourceProduct({ majors, freebie, handleDownloadFile }) {
  const router = useRouter();
  const [isDownloaded, setIsDownloaded] = useLocalStorage("DidUserDownloadedTheFreebie", false);
  const [downloadedFreebies, setDownloadedFreebies] = useLocalStorage("DownloadedFreebies", []);
  const [leadFilledUserID, setLeadFilledUserID] = useLocalStorage("LeadFilledUserID", null);

  const currentFreebie = router.query.resourceSlug;

  function calculateDiscountedPrice(price, discountPercentage) {
    var discount = price * (discountPercentage / 100); // Calculate the discount amount
    var newPrice = price - discount; // Subtract the discount from the original price
    return newPrice;
  }

  const handleDownloadEbook = async () => {
    setIsDownloaded(true);
    handleDownloadFile();
    await freebieService.addDownloadCountToFreebie(freebie.id);

    if (downloadedFreebies?.length > 0) {
      if (downloadedFreebies.includes(currentFreebie)) return;
      setDownloadedFreebies([...downloadedFreebies, router.query.resourceSlug]);
    } else {
      setDownloadedFreebies([router.query.resourceSlug]);
    }
  };



  return (
    <Row className="my-5">
      <Col md={6}>
        <div className="position-relative">
          <Image src={getAWSImagePath(freebie.cover)} alt="" width={400} height={300} layout="responsive" objectFit="contain" />
        </div>
      </Col>
      <Col md={6}>
        <div className="py-3 d-flex flex-column justify-content-between h-100 px-3 px-sm-0">
          <div>
            <div className="d-flex flex-column flex-md-row align-md-items-center justify-content-between mb-2">
              <div className="fs-sm d-flex align-items-center flex-wrap  ">
                <Link href="/">
                  <a className="text-cvblue me-1">Home</a>
                </Link>{" "}
                <span className="me-1">/</span>
                <Link href="/resources">
                  <a className="me-1">All Freebies</a>
                </Link>
                <span className="me-1">/</span>
                <p className="m-0 text-truncate">{freebie.title}</p>
              </div>
              <div className="d-flex align-items-center gap-2">
                <FacebookShareButton
                  url={`https://collegevidyaabroad.com/resources/${freebie.slug}`}
                  quote={freebie.slug}
                  hashtag={"#collegevidyaabroad"}
                >
                  <FacebookIcon size={20} round />
                </FacebookShareButton>
                <WhatsappShareButton url={`https://collegevidyaabroad.com/resources/${freebie.slug}`} title={freebie.title} separator=":  ">
                  <WhatsappIcon size={20} round />
                </WhatsappShareButton>
                <LinkedinShareButton url={`https://collegevidyaabroad.com/resources/${freebie.slug}`} title={freebie.title} summary={freebie.title}>
                  <LinkedinIcon size={20} round />
                </LinkedinShareButton>
              </div>
            </div>
            <h1 className="fs-4">{freebie.title}</h1>
            <h2 className="fs-4">
              {freebie.discount > 0 ? (
                <div className="d-flex gap-2 align-items-center">
                  <strike>₹{freebie.price}</strike>
                  <p className="m-0">₹{calculateDiscountedPrice(freebie.price, freebie.discount)}</p>
                  {calculateDiscountedPrice(freebie.price, freebie.discount) === 0 && (
                    <span className="bg-cvblue text-white  px-2 py-1 fs-md">Free</span>
                  )}
                </div>
              ) : (
                <p className="m-0">₹{freebie.price}</p>
              )}
            </h2>
            <div dangerouslySetInnerHTML={{ __html: freebie.description }}></div>
          </div>

          <div>
            <div
              className="d-flex gap-2 p-1 py-2 mb-2 fs-md text-center align-items-center justify-content-center"
              style={{
                background: "#ffc10717",
              }}
            >
              <AiOutlineBulb style={{ color: "#ffc107", minHeight: "20px", minWidth: "20px" }} className="bulbHoverAnimation" />
              <small className="text-start">{freebie.downloaded}+ Students have already downloaded this free resource!</small>
            </div>
            <div className="d-flex align-items-center gap-2 flex-wrap w-100">
              {isDownloaded ? (
                <>
                  <button
                    className="w-100 bg-cvblue p-3 text-white fs-5 border-0"
                    onClick={handleDownloadEbook}
                    style={{ opacity: isDownloaded && !downloadedFreebies.includes(currentFreebie) ? 0.6 : 1, height: "70px" }}
                    disabled={isDownloaded && !downloadedFreebies.includes(currentFreebie)}
                  >
                    {downloadedFreebies.includes(currentFreebie) && isDownloaded ? (
                      "Already Downloaded"
                    ) : (
                      <Tooltip title="You already downloaded your one freebie">
                        <span>
                          Download <AiFillLock />
                        </span>
                      </Tooltip>
                    )}
                  </button>
                  {downloadedFreebies.includes(currentFreebie) && isDownloaded && leadFilledUserID?.length > 0 ? null : (
                    <button
                      className="w-100  bg-orange p-3 text-white fs-md border-0 myshine"
                      onClick={() => (window.location.href = `/compare/universities?uuid=${leadFilledUserID}`)}
                      style={{ height: "70px" }}
                    >
                      Compare and Apply to Download
                    </button>
                  )}
                </>
              ) : (
                <FreeResourceLeadForm majors={majors} ctaText={`Download Free ${freebie.title}`} onSubmitDone={handleDownloadEbook}>
                  <button className="w-100 bg-cvblue p-3 text-white fs-5 border-0">Download</button>
                </FreeResourceLeadForm>
              )}
            </div>
            {isDownloaded && !downloadedFreebies.includes(currentFreebie) && (
              <small className="text-center mx-auto mt-1 fs-sm">You already Downloaded One Free Resource. Apply to Download More</small>
            )}
          </div>
        </div>
      </Col>
    </Row>
  );
}
