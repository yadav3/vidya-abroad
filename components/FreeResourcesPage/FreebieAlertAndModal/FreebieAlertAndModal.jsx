import React from "react";
import Offcanvas from "react-bootstrap/Offcanvas";
import styles from "./FreebieAlertAndModal.module.scss";
import { useState } from "react";
import { useEffect } from "react";
import uuid from "react-uuid";
import Image from "next/image";
import { getAWSImagePath } from "../../../context/services";
import { FooterDivider } from "../../global/FooterContainer/FooterDivider";
import { HiLockClosed } from "react-icons/hi";
import { useLocalStorage } from "../../../context/CustomHooks";
import { BsFillCloudDownloadFill } from "react-icons/bs";
import { AiOutlineClose } from "react-icons/ai";
import freebieService from "../../../services/freebie.service";
import Lottie from "lottie-react";
import GifLottieAnimation from "../../../public/gif/gift_json_gif.json";

export default function FreebieAlertAndModal({ freebies }) {
  const [didUserDownload, setDidUserDownload] = useLocalStorage("DidUserDownloadedTheFreebie", false);
  const [downloadedFreebies, setDownloadFreebies] = useLocalStorage("DownloadedFreebies", []);
  const [didUserCloseAlert, setDidUserCloseAlert] = useLocalStorage("DidUserClosedFreebieAlert", false);

  const [showAlert, setShowAlert] = useState(false);
  const [showModal, setShowModal] = useState(false);

  const handleCloseModal = () => {
    setShowModal(false);
  };
  const handleShowModal = () => {
    setShowModal(true);
  };
  const handleCloseAlert = () => {
    setShowAlert(false);
    setDidUserCloseAlert(true);
  };
  const handleShowAlert = () => {
    setShowAlert(true);
  };

  const handleDownloadFile = async (freebie) => {
    const fileUrl = getAWSImagePath(freebie.fileUrl);
    const link = document.createElement("a");
    link.href = fileUrl;
    link.download = `${freebie.slug}.pdf`;
    link.click();
    await freebieService.addDownloadCountToFreebie(freebie.id);

    if (downloadedFreebies?.length > 0) {
      if (downloadedFreebies.includes(freebie.slug)) return;
      setDownloadFreebies([...downloadedFreebies, freebie.slug]);
    } else {
      setDownloadFreebies([freebie.slug]);
    }
    setDidUserDownload(true);
    setShowAlert(false);
    setShowModal(false);
  };

  useEffect(() => {
    if (!didUserCloseAlert && !didUserDownload) {
      setTimeout(() => {
        handleShowAlert();
      }, 60000);
    }
  }, [didUserCloseAlert]);

  if (!showAlert) return null;
  return (
    <>
      <Alert handleShowModal={handleShowModal} handleCloseAlert={handleCloseAlert} />

      {showAlert && <div className={`${styles.overlay}`} onClick={handleCloseAlert}></div>}
      <Offcanvas placement="top" show={showModal} onHide={handleCloseModal} className={`${styles.offcanvasContainer}`} >
        <Offcanvas.Header closeButton>
          <div className="text-center w-100 pt-3 d-flex flex-column">
            <h4>Choose your One Free Study Abroad Guide</h4>
            <hr className="w-75 mx-auto " />
          </div>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <div className="d-flex flex-column align-items-center gap-2">
            <div className="d-flex flex-wrap gap-4 align-items-center justify-content-center">
              {freebies.map((freebie) => (
                <div
                  className={`${styles.freebieBox} d-flex position-relative cursor-pointer`}
                  key={uuid()}
                  onClick={() => handleDownloadFile(freebie)}
                  style={{
                    pointerEvents: didUserDownload ? "none" : "auto",
                  }}
                >
                  {didUserDownload && (
                    <div className={`${styles.freebieBoxCover} cursor-pointer`}>
                      <HiLockClosed size={30} className={`${styles.icon}`} />
                    </div>
                  )}

                  <Image src={getAWSImagePath(freebie.thumbnail)} alt={freebie.title} width={200} height={100} />
                </div>
              ))}
            </div>
            <small className="mx-auto mt-3 text-muted">Click on one of them to download</small>
          </div>
        </Offcanvas.Body>
      </Offcanvas>
    </>
  );
}

function Alert({ handleShowModal, handleCloseAlert }) {
  return (
    <div
      className={`${styles.alertButton} position-fixed bottom-0 end-0 m-2 mb-4 rounded-3 text-white p-3 d-flex align-items-center gap-2 slideInFromBottomPopup cursor-pointer`}
      onClick={handleShowModal}
    >
      <AiOutlineClose size={15} className="position-absolute top-0 end-0 m-2 text-dark z-10" onClick={handleCloseAlert} />
      <div className={`${styles.iconContainer} d-flex align-items-center justify-content-center `}>
        <Lottie
          animationData={GifLottieAnimation}
          height={100}
          style={{
            width: "100%",
          }}
        />
      </div>
      <div className="d-flex flex-column">
        <p className="m-0 text-black">We got something for you! 🎉</p>
        <small className="text-muted fs-sm">Download free study abroad resource now!</small>
      </div>
    </div>
  );
}
