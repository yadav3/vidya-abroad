import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import LeadForm from "../../global/LeadForm/LeadForm";

export function FreeResourceLeadForm({ majors, children, onClose, ctaText = "Compare Now", onSubmitDone = () => {} }) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <div onClick={handleShow} className="w-100">{children}</div>
      <Modal show={show} onHide={handleClose} size="lg" centered>
        <Modal.Header closeButton>
          <Modal.Title className="text-center">
            <div className="d-flex flex-column align-items-start justify-content-center">
              <span>You are just one step away...</span>
            </div>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className="d-flex flex-column align-items-center justify-content-center">
          <LeadForm majors={majors} ctaText={ctaText} onSubmitDone={onSubmitDone} />
          <small className="text-muted fs-xs">*Major is the area of study you want to pursue.</small>
        </Modal.Body>
      </Modal>
    </>
  );
}
