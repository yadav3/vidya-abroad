import React from "react";
import styles from "./FreeResourceCard.module.scss";
import Image from "next/image";
import Link from "next/link";
import { useLocalStorage } from "../../../context/CustomHooks";
import { getAWSImagePath } from "../../../context/services";
import classNames from "classnames";
import { FooterDivider } from "../../global/FooterContainer/FooterDivider";
import {MdFileDownloadDone} from 'react-icons/md'

export default function FreeResourceCard({ freebie }) {
  const [didUserDownload, setDidUserDownload] = useLocalStorage("DidUserDownloadedTheFreebie", false);
  const [downloadedFreebies, setDownloadFreebies] = useLocalStorage("DownloadedFreebies", []);

  const resourceImage = getAWSImagePath(freebie.thumbnail);
  const isDownloaded = didUserDownload && downloadedFreebies.includes(freebie.slug);
  const resourceUrl = `/resources/${freebie.slug}`;

  return (
    <div className={classNames(styles.resourceCard, "position-relative", "mb-3", "h-100")}>
      <span className={`${styles.resourceCardImage}`}>
        <Image src={resourceImage} alt={freebie.name} width={369} height={221} objectFit="contain" layout="responsive" />
      </span>
      {/* {isDownloaded && <div className={`${styles.downloadedBadge} myshine`}>Already Downloaded</div>} */}
      <FooterDivider />
      {/* {isDownloaded && <span className="d-block w-max fs-sm fw-normal bg-cvblue text-white rounded-3 px-2 mb-2 ">Downloaded</span>} */}

      <h3 className="fs-5 text-truncate">{freebie.title}</h3>
      <Link href={resourceUrl}>
        <a className="fw-semibold text-cvblue d-flex align-items-center gap-2">
          {isDownloaded && (
            <span className="d-block w-max fs-sm fw-normal bg-cvblue text-white rounded-3 px-2  ">
              <MdFileDownloadDone />
            </span>
          )}
          <span>Check it out</span>
        </a>
      </Link>
    </div>
  );
}
