import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "next/image";
import styles from "./FreeResourceBanner.module.scss";
export default function FreeResourceBanner({
  heading = `<span class="text-cvblue fw-bold">Don&apos;t Miss Out </span>
            on Your <span class="d-block text-cvblue fw-bold">Free Treats</span>`,
}) {
  return (
    <Row
      className="flex-column-reverse flex-md-row  align-items-center justify-content-center"
      style={{
        maxWidth: "1300px",
        marginInline: "auto",
        backgroundImage: `url("/country/countrypage/herobanners/hero_pattern.png")`,
        backgroundSize: "cover",
        backgroundPosition: "top",
      }}
    >
      <Col md={8}>
        <div className={`${styles.col1} d-flex flex-column justify-content-center`}>
          <h2
            className="fs-1 fw-normal"
            dangerouslySetInnerHTML={{
              __html: heading,
            }}
          ></h2>
        </div>
      </Col>
      <Col md={4}>
        <div className={`${styles.col2} pe-0 pe-md-5 d-flex align-items-center justify-content-center`}>
          <Image src="/illustrations/freebies_illustration.png" alt="Free Resource Banner Banner" width={477} height={477} objectFit="contain" />
        </div>
      </Col>
    </Row>
  );
}
