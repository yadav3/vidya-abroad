import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "next/image";
import { getAWSImagePath } from "../../../context/services";
export default function SingleResourceBanner({ handleDownloadFile, freebie }) {
  return (
    <div className="position-relative">
      <Image src={getAWSImagePath(freebie?.banner)} alt={freebie?.title} width={1440} height={342} layout="responsive" priority />
    </div>
  );
}
