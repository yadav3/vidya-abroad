import Image from 'next/image';
import React from 'react'
import styles from './Login.module.scss'
import { Form, Modal } from 'react-bootstrap';
import { InputText } from "primereact/inputtext";
import { RadioButton } from "primereact/radiobutton";
import { Button } from "primereact/button";
import { useStateContext } from '../../context/StateContext';
import { useState } from 'react';
import SecurePromiseStrip from '../global/SecurePromiseStrip/SecurePromiseStrip';

import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from 'react-hook-form';
import moment from 'moment';

export default function Login({ }) {

    const [toggleForm, setToggleForm] = useState(false)
    const { showSignInModal, setShowSignInModal } = useStateContext()
    const [mobileNumber, setMobileNumber] = useState("");

    const [userData, setUserData] = useState({
        name: '',
        email: '',
        number: '',
        dob: '',
        course: '',
        gender: ''
    })

    


    const handleClose = () => {
        setShowSignInModal(false)
    }

    return (
      <Modal show={showSignInModal} onHide={handleClose} centered>
        <div className={`${styles.header}`}>
          <div className={styles.logo}>
            <Image src="/logos/cv-abroad-logo.png" alt="College Vidya Abroad Logo" width={200} height={50} objectFit="contain" />
          </div>
        </div>
        <Modal.Body className={styles.body}>
          <h4>{toggleForm ? "To sign up, please enter the following details" : "To sign in, please enter your mobile number"}</h4>
          {toggleForm ? (
            <SignIn mobileNumber={mobileNumber} setMobileNumber={setMobileNumber} />
          ) : (
            <SignUp userData={userData} setUserData={setUserData} />
          )}
          <SecurePromiseStrip />
          <div className={styles.formSwitcher} >
            {toggleForm ? (
              <p>
                Don&apos;t have an account? <Button label="Sign Up" className="p-button-text" onClick={() => setToggleForm(false)} />
              </p>
            ) : (
              <p>
                Already have an account? <Button label="Sign In" className="p-button-text" onClick={() => setToggleForm(true)} />
              </p>
            )}
          </div>
        </Modal.Body>
      </Modal>
    );
}


const SignIn = ({ mobileNumber, setMobileNumber }) => {

    const schema = yup.object().shape({
      mobileNumber: yup.string().matches(/^[6-9]\d{9}$/, "Phone number is not valid"),
    });

    const {
      register,
      handleSubmit,
      formState: { errors },
      setValue,
    } = useForm({
      resolver: yupResolver(schema),
    });

    const onSubmit = (data) => {
      console.log(data);
    };

    return (
      <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
        <span className={styles.inputContainer}>
          <label htmlFor="mobileNumber">Mobile Number</label>
          <span className="p-input-icon-left">
            <i className="pi pi-mobile" />
            <InputText
              type="tel"
              {...register("mobileNumber", { required: true })}
            />
          </span>
            {errors.mobileNumber && <Form.Text className=" text-danger mb-2">{errors.mobileNumber?.message}</Form.Text>}
        </span>
        <Button label={`Send OTP ${mobileNumber.match(/^[6-9]\d{9}$/) ? `on +${mobileNumber}` : ""}`} />
      </form>
    );
};

const SignUp = ({ userData, setUserData }) => {

  const [gender, setGender] = useState();

  const schema = yup.object().shape({
    gender: yup.string().required("This field is required!"),
    name: yup.string().required("This field is required!").min(1, "Enter a valid name!").max(20, "Should not be maximum than 20 characters"),
    email: yup.string().email("Should be a valid email.").required("This field is required"),
    mobileNumber: yup.string().required("This field is required!").min(10, "Number should be 10 numbers long!").max(10, "Number should be 10 numbers long!"),
    dob: yup
      .string()
      .required("Date of birth is required!")
      .test("DOB", "Please choose a valid date of birth.", (date) => moment().diff(moment(date), "years") >= 1),
    course: yup.string().required("This field is required!").min(1, "Select a valid course"),
  });


  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
  } = useForm({
    resolver: yupResolver(schema),
  });

  const onSubmit = (data) => {
    console.log(data);
  };

    return (
      <form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
        <span className={styles.inputContainerGender}>
          <div className="field-radiobutton  d-flex">
            <input type='radio'
              id="gender1"
              name="genderFemale"
              value='female'
              {...register("gender", { required: true })}
              onChange={(e) => {
                setGender(e.target.value);
                setValue("gender", e.target.value);
              }}
              checked={gender === "female"}
            />
            <label htmlFor="gender1">Female</label>
          </div>
          <div className="field-radiobutton d-flex">
            <input type='radio'
              id="gender2"
              name="genderMale"
              value='male'
              {...register("gender", { required: true })}
              onChange={(e) => {
                setGender(e.target.value);
                setValue("gender", e.target.value);
              }}
              checked={gender === "female"}
            />
            <label htmlFor="gender2">Male</label>
          </div>
        </span>
        {errors.gender && <Form.Text className="d-block text-center text-danger mt-2">{errors.gender?.message}</Form.Text>}
        <div className="field">
          <label htmlFor="name" className="mb-2">
            Name
          </label>
          <InputText type="text" id="name" placeholder="Enter here" {...register("name", { required: true })} />
          {errors.name && <Form.Text className="d-block text-danger mt-2">{errors.name?.message}</Form.Text>}
        </div>

        <span className={styles.inputContainer}>
          <label htmlFor="mobileNumber">Mobile Number</label>
          <span className="p-input-icon-left">
            <i className="pi pi-mobile" />
            <InputText type="tel" {...register("mobileNumber", { required: true })} />
          </span>
          {errors.mobileNumber && <Form.Text className="d-block text-danger mt-2">{errors.mobileNumber?.message}</Form.Text>}
        </span>
        <span className={styles.inputContainer}>
          <label htmlFor="email">Email</label>
          <span className="p-input-icon-left">
            <i className="pi pi-envelope" />
            <InputText type="email" {...register("email", { required: true })} />
          </span>
        </span>
        <div className="field">
          <label htmlFor="dob" className="mb-2">
            Date of Birth
          </label>
          <InputText type="date" id="dob" {...register("dob", { required: true })} placeholder="Enter here" />
        </div>
        <div className="field">
          <label htmlFor="course" className="mb-2">
            Select a Course
          </label>
          <InputText type="text" id="course" {...register("course", { required: true })} placeholder="Enter here" />
        </div>
        <Button label="Sign Up" />
      </form>
    );
};

