import React, { useCallback, useRef, useEffect } from "react";
import { useRecoilState } from "recoil";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Autoplay, Pagination, Navigation } from "swiper";
import uuid from "react-uuid";
import { IoCheckmarkDoneCircleSharp } from "react-icons/io5";
import { FooterDivider } from "../global/FooterContainer/FooterDivider";
import { ottaPointLeadAtom } from "./OttaPointLead.store";

export default function OttaPointLeadSwiper({ showDivider = true }) {
  const pointsHtml = [
    `Select from <strong class="text-cvblue">7 Popular Study Abroad</strong> Destinations`,
    `Choose from <strong class="text-cvblue">400+ Universities</strong>`,
    `Compare <strong class="text-cvblue">5,000+ Courses</strong>`,
    `Get <strong class="text-cvblue">Personalized Counselling</strong> from Study Abroad Experts`,
    `Master IELTS with <strong class="text-cvblue">Experienced Experts</strong> of 10+ Years`,
    `Help from <strong class="text-cvblue">VISA Processing</strong>`,
    `Get <strong class="text-cvblue">Financial Assistance</strong> and <strong class="text-cvblue">Scholarships</strong> Options`,
  ];

  const [ottaPointLead, setottaPointLead] = useRecoilState(ottaPointLeadAtom);

  const sliderRef = useRef(null);

  const handlePrev = useCallback(() => {
    if (!sliderRef.current) return;
    sliderRef.current.swiper.slidePrev();
  }, []);

  const handleNext = useCallback(() => {
    if (!sliderRef.current) return;
    sliderRef.current.swiper.slideNext();
  }, []);

  useEffect(() => {
    setottaPointLead({
      handleNext: handleNext,
      handlePrev: handlePrev,
    });
  }, []);

  return (
    <div className="d-flex flex-column justify-content-center gap-3 w-100">
      <Swiper
        ref={sliderRef}
        spaceBetween={30}
        centeredSlides={true}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        modules={[Autoplay, Pagination, Navigation]}
        className="w-100"
      >
        {pointsHtml.map((point) => (
          <SwiperSlide key={uuid()}>
            <div className="py-2 pt-0">
              {/* <IoCheckmarkDoneCircleSharp
                size={30}
                className="text-cvblue"
                style={{
                  minWidth: "30px",
                  minHeight: "30px",
                }}
              /> */}
              {showDivider && <FooterDivider className="mx-auto w-75" />}
              <p
                className="fs-lg m-0 text-center"
                dangerouslySetInnerHTML={{
                  __html: point,
                }}
              ></p>
              {showDivider && <FooterDivider className="mx-auto w-75" />}
            </div>
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
}
