import { atom } from "recoil";

export const ottaPointLeadAtom = atom({
    key: "ottaPointLeadAtom",
    default: {
        handleNext: () => {},
        handlePrev: () => {},
    },
});
