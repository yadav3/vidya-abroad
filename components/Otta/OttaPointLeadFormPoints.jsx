import React from "react";

// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
import { IoCheckmarkDoneCircleSharp } from "react-icons/io5";
// Import Swiper styles
import "swiper/css";
import "swiper/css/effect-creative";
// import required modules
import { EffectCreative } from "swiper";

export default function OttaPointLeadFormPoints() {
  //   const points = [
  //     "Select from 7 Popular Study Abroad Destinations",
  //     "Choose from 400+ Universities",
  //     "Compare 5,000+ Courses",
  //     "Get Personalized Counselling from Study Abroad Experts",
  //     "Prepare for IELTS from Experts with 10 Yrs and above Experience",
  //     "Help from VISA Processing",
  //     "Get Financial Assistance and Scholarships Options",
  //   ];
  const pointsHtml = [
    "Select from <strong>7 Popular Study Abroad</strong> Destinations",
    "Choose from <strong>400+ Universities</strong>",
    "Compare <strong>5,000+ Courses</strong>",
    "Get <strong>Personalized Counselling</strong> from Study Abroad Experts",
    "Master IELTS with Experienced Experts of 10+ Years",
    "Help from <strong>VISA Processing</strong>",
    "Get <strong>Financial Assistance</strong> and <strong>Scholarships</strong> Options",
  ];

  // i have four elements with above animation, so but I need to show them one by one, only after the previous one is finished. How can I do that?

  return (
    <div className="d-flex flex-column align-items-center justify-content-center gap-3 w-100 h-100">
      <h2 className={``}>Be ready to...</h2>

      {pointsHtml.map((point, index) => {
        return (
          <div
            key={index}
            className="mb-1 rounded-2 d-flex align-items-center w-max fade-item  enterTextFromBottom"
            style={{
              animationDelay: `${index * 0.4}s`,
              width: "80%",
            }}
          >
            <p className="fs-lg mb-0 d-flex">
              <IoCheckmarkDoneCircleSharp
                size={30}
                className="text-cvblue"
                style={{
                  minWidth: "30px",
                  minHeight: "30px",
                }}
              />
              <span dangerouslySetInnerHTML={{ __html: point }} className="ms-2"></span>
            </p>
          </div>
        );
      })}
    </div>
  );
}
