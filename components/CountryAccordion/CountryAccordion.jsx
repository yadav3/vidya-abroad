import Image from "next/image";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { Accordion, Col, Container, Row } from "react-bootstrap";
import { useStateContext } from "../../context/StateContext";
import styles from "./CountryAccordion.module.scss";
import { FaUniversity, FaGraduationCap } from "react-icons/fa";
import { MdOutlineWork } from "react-icons/md";
import { AiOutlineIdcard } from "react-icons/ai";
import { GiMoneyStack, GiEarthAfricaEurope } from "react-icons/gi";
import { BiMedal } from "react-icons/bi";
export default function CountryAccordion() {
  const router = useRouter();

  const { setOttaData, ottaData } = useStateContext();

  const [data, setData] = useState([
    {
      name: "USA",
      slug: "us",
      flag: "/country/flags/usa.webp",
      image: "/country/accordion/usa.webp",
      universities: 200,
      points: [
        {
          title: "Application fee waiver for 75+ US Universities",
          icon: <FaUniversity size={30} />,
        },
        {
          title: "Scholarship to eligible students",
          icon: <BiMedal size={30} />,
        },
        {
          title: "15 years of education acceptable for few courses",
          icon: <FaGraduationCap size={30} />,
        },
        {
          title: "Visa Assistance",
          icon: <AiOutlineIdcard size={30} />,
        },
      ],
    },
    {
      name: "UK",
      flag: "/country/flags/uk.webp",
      image: "/country/accordion/uk.webp",
      universities: 100,
      slug: "uk",
      points: [
        {
          title: "19 Partner UK Universities in Top 300 of World",
          icon: <FaUniversity size={30} />,
        },
        {
          title: "1 Year Post Graduate Course",
          icon: <FaGraduationCap size={30} />,
        },
        {
          title: "3 Years Undergraduate Course",
          icon: <FaGraduationCap size={30} />,
        },
        {
          title: "Courses with Internship in UG / PG Level",
          icon: <MdOutlineWork size={30} />,
        },
      ],
    },
    {
      name: "Canada",
      slug: "canada",
      flag: "/country/flags/canada.webp",
      image: "/country/accordion/canada.webp",
      universities: 90,
      points: [
        {
          title: "Post Study Work Visa up to 3 years",
          icon: <AiOutlineIdcard size={30} />,
        },
        {
          title: "Affordable Tuition Fees",
          icon: <GiMoneyStack size={30} />,
        },
        {
          title: "15 years of education acceptable for few courses",
          icon: <FaGraduationCap size={30} />,
        },
        {
          title: "Paid  Internships",
          icon: <MdOutlineWork size={30} />,
        },
      ],
    },

    {
      name: "Germany",
      slug: "germany",
      flag: "/country/flags/germany.webp",
      image: "/country/accordion/germany.webp",
      universities: 50,
      points: [
        {
          title: "Post Study Work Visa for 18 months",
          icon: <AiOutlineIdcard size={30} />,
        },
        {
          title: "Admission without GRE possible",
          icon: <FaGraduationCap size={30} />,
        },
        {
          title: "Home to many of the Fortune 500 companies",
          icon: <MdOutlineWork size={30} />,
        },
        {
          title: "Strongest Economy in Europe",
          icon: <GiEarthAfricaEurope size={30} />,
        },
      ],
    },
    {
      name: "Australia",
      slug: "australia",
      flag: "/country/flags/australia.webp",
      image: "/country/accordion/australia.webp",
      universities: 40,
      points: [
        {
          title: "Post Study Work Visa upto 5 years",
          icon: <AiOutlineIdcard size={30} />,
        },
        {
          title: "Lucrative Scholarships to eligible students",
          icon: <MdOutlineWork size={30} />,
        },
        {
          title: "15 years of education acceptable",
          icon: <FaGraduationCap size={30} />,
        },
        {
          title: "Simplified Visa rules",
          icon: <AiOutlineIdcard size={30} />,
        },
      ],
    },
    {
      name: "New Zealand",
      slug: "new-zealand",
      flag: "/country/flags/newzealand.webp",
      image: "/country/accordion/newzealand.webp",
      universities: 10,
      points: [
        {
          title: "Lenient entry requirements",
          icon: <FaGraduationCap size={30} />,
        },
        {
          title: "Affordable tuition fees",
          icon: <GiMoneyStack size={30} />,
        },
        {
          title: "No application fees",
          icon: <GiMoneyStack size={30} />,
        },
        {
          title: "Post Study work visa up to 3 years",
          icon: <AiOutlineIdcard size={30} />,
        },
      ],
    },
  ]);

  return (
    <Accordion defaultActiveKey="0" flush>
      {data.map((country, i) => {
        return (
          <Accordion.Item eventKey={i} key={`country-${i}`} className="mb-2 ">
            <Accordion.Header
              className={`${styles.accordionHeader}`}
              style={{
                backgroundImage: `url(${country.image})`,
              }}
            >
              <div className={`${styles.flag}`}>
                <Image src={country.flag} alt={country.name} width={50} height={50} />
              </div>
              <p>{country.name}</p>
            </Accordion.Header>
            <Accordion.Body className={`${styles.accordionBodyContainer}`}>
              <Container>
                <Row className="my-4 ">
                  <Col md={4} className="mb-4 mb-md-0">
                    <div className={`text-white d-flex flex-column align-items-center justify-content-center h-100 `}>
                      <p className="m-0 fs-1 fw-semibold">{country.universities}+</p>
                      <p className="m-0 fs-5">
                        Universities <br></br>& Institutes
                      </p>
                      <button
                        className={`border-0 py-2 px-4 bg-cvblue text-white rounded-5 mt-2 fs-6 myshine`}
                        onClick={() => {
                          router.push(`/countries/${country.slug}`);
                        }}
                      >
                        Apply For {country.name}
                      </button>
                    </div>
                  </Col>
                  <Col md={8}>
                    <Row className={`g-2`}>
                      {country.points.map((point, i) => {
                        return (
                          <Col md={6} className={`p-2 rounded-4`} key={`country-point-${i}`}>
                            <div className="text-white h-100 d-flex gap-3 align-items-center py-3 px-2">
                              <div className={`${styles.icon}`}>
                                {point.icon}
                                {/* <Image src={point.icon} alt={`why choose ${country.name}`} width={50} height={45} objectFit="contain" /> */}
                              </div>
                              <p className="m-0">{point.title}</p>
                            </div>
                            <div
                              className="m-0 "
                              style={{
                                height: "2px",
                                background: "linear-gradient(90deg, rgb(14 80 211 / 78%) 10%, rgb(0 52 155 / 0%) 100%)",
                              }}
                            />
                          </Col>
                        );
                      })}
                    </Row>
                  </Col>
                </Row>
              </Container>
              {/* <div className={`${styles.accordionBtnContainer}`}>
                <button
                  className={`suggestUniversityBtn myshine fs-6 mb-3 ${styles.accordionCta2Btn}`}
                  onClick={() => {
                    router.push(`/countries/${country.slug}`);
                  }}
                >
                  Apply For {country.name}
                </button>
              </div> */}
            </Accordion.Body>
          </Accordion.Item>
        );
      })}
    </Accordion>
  );
}
