import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import styles from "./BlogFilters.module.scss";
import { MdOutlineFilterList, MdOutlineSearch } from "react-icons/md";
import { useRouter } from "next/router";
import { useState } from "react";
import { toast } from "react-hot-toast";
export default function BlogFilters() {
  const router = useRouter();

  const [search, setSearch] = useState("");
  const [searchError, seSearchError] = useState(false);

  const handleNavigateToSearchPage = (search) => {
    if (search.length === 0) return seSearchError(true);
    window.location.href = `/blog/search?search=${search}`;
  };

  return (
    <Row className="px-3 p-2 py-3 flex-column flex-md-row gap-2 gap-md-0 align-items-center ">
      {/* <Col md={2}>
        <span className={`${styles.filterBtn} d-none d-md-flex align-items-center justify-content-center gap-2 p-2  cursor-pointer shadow-sm`}>
          <MdOutlineFilterList className={styles.filterIcon} />
          Filter
        </span>
      </Col> */}
      <Col className="px-md-0">
        <div className="position-relative ">
          <span
            className={`${styles.searchIcon}
           position-absolute top-50 translate-middle-y end-0 me-2
          d-flex align-items-center justify-content-center gap-2 p-2 cursor-pointer bg-cvblue text-white rounded`}
            role="button"
          >
            <MdOutlineSearch onClick={() => handleNavigateToSearchPage(search)} className={styles.searchIcon} />
          </span>

          <input
            type="text"
            placeholder="Search blogs"
            className={`${styles.searchInput} shadow-sm rounded ${searchError ? "border border-danger" : null}`}
            value={search}
            onChange={(e) => {
              seSearchError(false);
              setSearch(e.target.value);
            }}
            onKeyPress={(e) => {
              if (e.key === "Enter") {
                handleNavigateToSearchPage(search);
              }
            }}
          />
        </div>
      </Col>
    </Row>
  );
}
