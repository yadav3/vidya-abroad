import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import BlogPostCard from "./BlogPostCard/BlogPostCard";

export default function BlogPostContainer({ posts }) {


  return (
    <Row className="px-3">
      {posts && posts.length > 0 ? (
        posts.map((post, index) => (
          <Col md={6} lg={4} key={index}>
            <BlogPostCard post={post} />
          </Col>
        ))
      ) : (
        <Col>
          <div className="text-center">
            <h3>No posts found</h3>
          </div>
        </Col>
      )}
    </Row>
  );
}
