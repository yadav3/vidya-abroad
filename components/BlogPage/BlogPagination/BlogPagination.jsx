import React, { useState } from "react";
import ReactPaginate from "react-paginate";
import styles from "./BlogPagination.module.scss";

export default function BlogPagination({ page, setPage, totalPage, itemsPerPage, posts }) {
  const handlePageChange = (data) => {
    setPage(data.selected + 1);
    //   scroll to top
    window.scrollTo(0, 0);
  };

  return (
    <div className="d-flex align-items-center justify-content-center mt-3">
      <ReactPaginate
        breakLabel="..."
        nextLabel=">"
        onPageChange={handlePageChange}
        pageRangeDisplayed={5}
        pageCount={totalPage}
        previousLabel="<"
        renderOnZeroPageCount={null}
        className="d-flex align-items-center justify-content-center gap-3 "
        nextClassName={styles.nextBtn}
        previousClassName={styles.prevBtn}
        pageClassName={styles.pageBtn}
        activeClassName={styles.activeBtn}
      />
    </div>
  );
}
