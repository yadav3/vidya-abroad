import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "next/image";
import styles from "./BlogBanner.module.scss";
export default function BlogBanner() {
  return (
    <Row
      className="flex-column-reverse flex-md-row  align-items-center justify-content-center"
      style={{
        maxWidth: "1300px",
        marginInline: "auto",
        backgroundImage: `url("/country/countrypage/herobanners/hero_pattern.png")`,
        backgroundSize: "cover",
        backgroundPosition: "top",
      }}
    >
      <Col md={8}>
        <div className={`${styles.col1} "d-flex flex-column justify-content-center "`}>
          <h2 className="fw-bold fs-1">Blogs</h2>
          <p>Looking to study abroad but feeling overwhelmed by the process? Our blog is here to guide you every step of the way!</p>
        </div>
      </Col>
      <Col md={4}>
        <div className={`${styles.col2} pe-0 pe-md-5 d-flex align-items-center justify-content-center`}>
          <Image src="/banners/blog_banner.svg" alt="Blog Banner" width={400} height={400} />
        </div>
      </Col>
    </Row>
  );
}
