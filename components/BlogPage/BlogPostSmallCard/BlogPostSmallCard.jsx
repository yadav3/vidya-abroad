import React from "react";
import styles from "./BlogPostSmallCard.module.scss";
import Image from "next/image";
import { AiOutlineCalendar, AiOutlineClockCircle } from "react-icons/ai";
import { useRouter } from "next/router";
import { getAWSImagePath } from "../../../context/services";
import moment from "moment/moment";

export default function BlogPostSmallCard({ post }) {
  const router = useRouter();

  const getPostReadTime = (post) => {
    const wordsPerMinute = 200;
    const minutes = post.contentLength / wordsPerMinute;
    const readTime = Math.ceil(minutes);
    return readTime;
  };

  const formatPublishedDate = (date) => {
    return moment(date).format("Do MMMM, YYYY");
  };

  const handleNavigateToPost = (slug) => {
    router.push(`/blog/${slug}`);
  };

  const handleNavigateToCategory = (categorySlug) => {
    router.push(`/blog/categories/${categorySlug}`);
  };

  return (
    <div
      className={`${styles.postCard} d-flex flex-column align-items-center justify-content-center position-relative mb-4
    `}
    >
      <div className={`${styles.thumbnailContainer} cursor-pointer w-100 position-relative mb-3`} onClick={() => handleNavigateToPost(post.slug)}>
        <Image alt="Blog Thumbnail" src={getAWSImagePath(post.thumbnail)} layout="responsive" width={1280} height={720} objectFit="cover" />
      </div>
      <div className={`${styles.content} `}>
        <div className={`${styles.contentHeader} d-flex align-items-center justify-content-between fs-sm`}>
          <div className="d-flex align-items-center gap-2 mb-2">
            {post?.blog_categories?.name ? (
              <div
                className={`${styles.postCategoryTag}
                rounded-3 px-2 py-1 cursor-pointer
              `}
                onClick={() => handleNavigateToCategory(post.blog_categories.category_slug)}
              >
                <span>{post.blog_categories.name}</span>
              </div>
            ) : null}
          </div>
          <div className={styles.postActions}>
            {post?.published_at ? (
              <span className="d-flex align-items-center gap-2">
                {<AiOutlineCalendar />}
                {formatPublishedDate(post.published_at)}
              </span>
            ) : null}
          </div>
        </div>
        <div className="cursor-pointer" onClick={() => handleNavigateToPost(post.slug)}>
          <div
            className={`${styles.title}
        fs-6 fw-semibold
        `}
          >
            {post?.title ? post.title : null}
          </div>
          <div
            className={`${styles.description}
      fs-sm fw-medium text-muted 
        `}
          >
            {post?.short_description ? post.short_description.slice(0, 100).concat("...") : null}
          </div>
        </div>
      </div>
    </div>
  );
}
