import React from "react";
import styles from "./BlogPostCard.module.scss";
import Image from "next/image";
import { AiOutlineClockCircle } from "react-icons/ai";
import { useRouter } from "next/router";
import { getAWSImagePath } from "../../../context/services";
import uuid from "react-uuid";
import moment from "moment";

export default function BlogPostCard({ post }) {
  const router = useRouter();

  const getPostReadTime = (post) => {
    const wordsPerMinute = 200;
    const minutes = post.contentLength / wordsPerMinute;
    const readTime = Math.ceil(minutes);
    return readTime;
  };

  const handleNavigateToPost = (slug) => {
    router.push(`/blog/${slug}`);
  };

  const handleNavigateToCategory = (categorySlug) => {
    router.push(`/blog/categories/${categorySlug}`);
  };

  const truncateLongTitle = (title) => {
    if (title.length > 50) {
      return title.substring(0, 50) + "...";
    }
    return title;
  };

  const getPublishedDate = (date) => {
    const publishedDate = new Date(date);
    // formate in english date like "7th march, 2021"
    const publishedDateInEnglish = moment(publishedDate).format("Do MMMM, YYYY");
    return publishedDateInEnglish;
  };

  return (
    <div
      className={`${styles.postCard} blog_post_card d-flex flex-column align-items-center justify-content-center position-relative
    `}
    >
      <div className={`${styles.thumbnailContainer} w-100 position-relative cursor-pointer`} onClick={() => handleNavigateToPost(post.slug)}>
        <Image alt="Blog Thumbnail" src={getAWSImagePath(post.thumbnail)} layout="responsive" width={500} height={300} />
      </div>
      <div className={`${styles.content}`}>
        <div className={`${styles.contentHeader} d-flex align-items-center justify-content-between fs-sm mb-2`}>
          <div className="cursor-pointer d-flex align-items-center gap-2 " onClick={() => handleNavigateToCategory(post.category.category_slug)}>
            {post?.category?.name ? (
              <div
                className={`${styles.postCategoryTag}
             rounded-3 px-2 py-1 
            `}
              >
                <span>{post.category.name}</span>
              </div>
            ) : null}
          </div>
          <div className={styles.postActions}>
            {post?.contentLength ? (
              <span className="d-flex align-items-center gap-2 fs-xs">
                <div className="d-none d-lg-block">{post?.published_at ? <span>{getPublishedDate(post.published_at)}</span> : null}</div>
                {<AiOutlineClockCircle />}
                {getPostReadTime(post)} min read
              </span>
            ) : null}
          </div>
        </div>
        <div
          className={`${styles.title}
        fs-4 fw-semibold cursor-pointer
        `}
          onClick={() => handleNavigateToPost(post.slug)}
        >
          {truncateLongTitle(post.title)}
        </div>
        <div
          className={`${styles.description}
      fs-6 fw-medium mb-2 text-gray cursor-pointer
        `}
          onClick={() => handleNavigateToPost(post.slug)}
        >
          {post?.short_description.substring(0, 100) + "..." ? post?.short_description.substring(0, 100) + "..." : null}
        </div>
        {/* <div className={styles.tags}>
          {post.keywords.map((keyword) => (
            <span
              key={uuid()}
              className={`${styles.keywordTag}
            rounded-pill px-3 py-1 me-2 bg-grey
              fs-sm `}
            >
              {keyword.keyword}
            </span>
          ))}
        </div> */}
      </div>
    </div>
  );
}

// {
//     "id": 1,
//     "title": "Step into the Fascinating world of WebSockets with Socket.io",
//     "short_description": "WebSocket is a technology that enables us to exchange data between the web server and a client in real time.  WebSockets is a fascinating technology; every web developer should have at least a basic understanding of it.  Today we will learn about WebSockets. We will also learn how to implement a simple WebSocket server using Socket.io in a NodeJS application.  Let’s get started!",
//     "slug": "socket-io",
//     "thumbnail": "blog/1-visual-representation-of-how-http-works.png",
//     "blog_categories": {
//         "id": 2,
//         "name": "Coding",
//         "icons_library": {
//             "icon": "icons/2-food.png"
//         }
//     },
//     "blog_post_keywords": [
//         {
//             "blog_keywords": {
//                 "keyword": "websockets"
//             }
//         },
//         {
//             "blog_keywords": {
//                 "keyword": "learn"
//             }
//         },
//         {
//             "blog_keywords": {
//                 "keyword": "coding"
//             }
//         }
//     ]
// }
