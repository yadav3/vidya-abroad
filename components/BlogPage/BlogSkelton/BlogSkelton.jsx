import React from "react";
import { Skeleton } from "primereact/skeleton";

export default function BlogSkelton() {
  return (
    <div className="p-2">
      <Skeleton width="100%" height="200px" />
      <div >
        <Skeleton width="100%" height="20px" />
        <Skeleton width="100%" height="20px" />
        <Skeleton width="100%" height="20px" />
        <Skeleton width="100%" height="20px" />
        <Skeleton width="100%" height="20px" />
      </div>
    </div>
  );
}
