import React, { useEffect, useRef, useState } from "react";
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { InputText } from "primereact/inputtext";
import { InputMask } from "primereact/inputmask";
import { Dropdown } from "primereact/dropdown";
import { Calendar } from "primereact/calendar";
import { useRouter } from "next/router";
import { toast } from "react-hot-toast";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { useRecoilState } from "recoil";
import { useLocalStorage } from "../../../context/CustomHooks";
import { COUNSELLOR_CLIENT_URL, COUNSELLOR_SERVER_URL, createNewLead, generatePassword, storeUserCompareResults } from "../../../context/services";
import { compareQueryParamsState } from "../../../store/compare/compareStore";
import { cvCounsellorDetailsAtom } from "../../../store/journeyStore";
import { yupResolver } from "@hookform/resolvers/yup";
import styles from "./BlogLeadForm.module.scss";
import moment from "moment";
import SectionHeading from "../../global/SectionHeading/SectionHeading";
import { FormControl } from "react-bootstrap";
import uuid from "react-uuid";
import { getCookie, hasCookie, setCookie } from "cookies-next";
import { createLsqOpportunity, updatedLsqLeadDetails } from "../../../services/lead.service";
import axios from "axios";

export default function BlogLeadForm({ majors }) {
  const router = useRouter();
  const schema = yup.object().shape({
    name: yup.string().required("This field is required!").min(1, "Enter a valid name!").max(20, "Should not be maxi mum than 20 characters"),
    email: yup.string().email("Should be a valid email.").required("This field is required"),
    phone: yup.string().required("This field is required!").min(10, "Number should be 10 numbers long!").max(10, "Number should be 10 numbers long!"),
    dob: yup
      .string()
      .nullable()
      .required("Date of birth is required!")
      .test("DOB", "Please choose a valid date of birth.", (date) => moment().diff(moment(date), "years") >= 1),
    country: yup.string().required("This field is required!"),
    major: yup.string().required("This field is required!"),
  });

  const [filteredMajors, setFilteredMajors] = useState(majors);
  const [leadData, setLeadData] = useLocalStorage("leadData", {});
  const [leadFilledUserID, setLeadFilledUserID] = useLocalStorage("LeadFilledUserID", null);

  const [isLoading, setIsLoading] = useState(false);
  const [userCompareQuery, setUserCompareQuery] = useRecoilState(compareQueryParamsState);
  const [cvCounsellorDetails, setCvCounsellorDetails] = useRecoilState(cvCounsellorDetailsAtom);

  const [majorCategory, setMajorCategory] = useState(null);
  const [blogHeaderHeight, setBlogHeaderHeight] = useState(0);

  const gtmButtonRef = useRef(null);

  const countries = [
    { id: 0, name: "Not Decided Yet", value: "Not Decided Yet" },
    { id: 1, name: "USA", value: "USA" },
    { id: 2, name: "Canada", value: "Canada" },
    { id: 3, name: "UK", value: "UK" },
    { id: 4, name: "Germany", value: "Germany" },
    { id: 5, name: "Australia", value: "Australia" },
    { id: 6, name: "New Zealand", value: "New Zealand" },
    { id: 7, name: "Ireland", value: "Ireland" },
  ];

  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
    getValues,
    getFieldState,
  } = useForm({
    defaultValues: {
      major: leadData?.major ? parseInt(leadData.major) : 1,
      dob: leadData?.dob ? new Date(leadData.dob) : null,
    },
    resolver: yupResolver(schema),
  });

  const createCVCounsellorCustomer = (userDetails, leadData, cv_id) => {
    axios
      .post(
        `${COUNSELLOR_SERVER_URL}/collegevidyaabroad/customer/create`,
        {
          uuid: userDetails.user_id,
          name: leadData.name,
          phone: leadData.phone,
          email: leadData.email,
          cv_id: cv_id,
          dob: new Date(leadData.age).toISOString().split("T")[0],
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      )
      .then(() => {
        setCvCounsellorDetails({
          ...cvCounsellorDetails,
          cv_id: cv_id,
          redirectLink: `${COUNSELLOR_CLIENT_URL}/collegevidya/redirect/login?cv_id=${cv_id}&country=${leadData.country}`,
        });
      })
      .catch((err) => {
        if (err?.request?.status == "409") {
          setCvCounsellorDetails({
            ...cvCounsellorDetails,
            redirectLink: `${COUNSELLOR_CLIENT_URL}/collegevidya/redirect/login?exist=1`,
          });
        } else {
          console.error(err);
          toast.error(err?.message ? err?.message : "Something went wrong, please try again!", { duration: 2000 });
        }
      });
  };

  const onSubmit = async (data) => {
    setLeadData(data);
    const cv_id = generatePassword(8);
    const user_id = generatePassword(8);
    const lsq_source = "College Vidya";
    const sub_source = getCookie("source_campaign") === "collegevidya_online_website" ? "Online Website" : "Abroad Website";
    const source_campaign = getCookie("source_campaign");
    const campaign_name = getCookie("campaign_name");
    const ad_group = getCookie("ad_group_name");
    const ad_name = getCookie("ads_name");

    const leadData = {
      userId: user_id,
      gender: data.gender,
      name: data.name,
      phone: data.phone,
      email: data.email,
      age: data.dob,
      country: data.country,
      majorId: data.major,
      source: lsq_source,
      sub_source: sub_source,
      source_campaign: source_campaign,
      campaign_name: campaign_name,
      ad_group: ad_group,
      ad_name: ad_name,
    };

    setIsLoading(true);

    try {
      await createNewLead(leadData);
      toast.success("Thank you for your interest! We will get back to you soon.");

      if (!hasCookie("lead-filled")) {
        gtmButtonRef.current.click();
      }

      setCookie("lead-filled", "true");

      const major = majors.find((major) => major.id === parseInt(leadData.majorId));
      const program = major.category_id === 1 ? "Bachelors" : major.category_id === 2 ? "Masters" : major.category_id === 3 ? "Diploma" : null;
      const selectedCountry = countries.find((country) => country.name === leadData.country);
      const budget = "Not Decided Yet";

      const lsqLeadData = {
        data: [
          {
            Attribute: "mx_Study_Destination",
            Value: selectedCountry.name,
          },
          {
            Attribute: "mx_Programme",
            Value: program,
          },
          {
            Attribute: "mx_Specilazation",
            Value: major.name,
          },
        ],
      };

      const opportunityRes = await createLsqOpportunity(
        leadData.name,
        leadData.email,
        leadData.phone,
        budget,
        selectedCountry.name,
        program,
        major.name,
        lsq_source,
        sub_source,
        source_campaign,
        campaign_name,
        ad_group,
        ad_name
      );

      const userDetails = await storeUserCompareResults(
        user_id,
        major.category_id,
        0,
        0,
        major.id,
        0,
        selectedCountry.id,
        0,
        opportunityRes.opportunityId,
        0,
        0,
        [selectedCountry.id],
        opportunityRes.relatedProspectId
      );

      setUserCompareQuery({
        uuid: userDetails.user_id,
        categoryId: userDetails.category_id,
        courseId: userDetails.course_id,
        majorId: userDetails.majorId,
        specializationId: userDetails.specialization_id,
        countryId: userDetails.country_id,
        budgetLimit: userDetails.budget,
        ieltsScore: userDetails.ielts,
        opportunityId: opportunityRes.opportunityId,
        countries: userCompareQuery?.countries ? [...userCompareQuery.countries] : [],
        relatedProspectId: opportunityRes.relatedProspectId,
      });

      setLeadFilledUserID(userDetails.user_id);
      const res = await updatedLsqLeadDetails(userDetails.user_id, lsqLeadData);

      createCVCounsellorCustomer(userDetails, leadData, cv_id);
      window.location.href = `/compare/universities?uuid=${userDetails.user_id}`;
    } catch (error) {
      console.error(error);
      toast.dismiss();
      toast.error("That's strange! Something went wrong. Don't worry you can try again.");
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    Object.keys(leadData).forEach((key) => {
      if (key === "dob") setValue(key, new Date(leadData[key]));
      else if (key === "major") setValue(key, parseInt(leadData[key]));
      else setValue(key, leadData[key]);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    // on scroll,
    window.addEventListener("scroll", () => {
      const blogReadingHeader = document?.querySelector("#blog-post-header");
      const headerHeight = blogReadingHeader?.offsetHeight + 20;
      setBlogHeaderHeight(headerHeight);
    });

    return () => {
      window.removeEventListener("scroll", () => {});
    };
  }, []);

  return (
    <Form
      onSubmit={handleSubmit(onSubmit)}
      className={`${styles.leadForm} w-100 mt-4 p-3 rounded-3 position-sticky bg-white`}
      style={{
        top: `${blogHeaderHeight}px`,
      }}
    >
      <Row className="mb-3">
        <Col>
          <SectionHeading
            heading={`<span>
            Connect with an
          <span class="text-cvblue">Expert</span>
      </span>`}
            subHeading="Please fill in your personal details"
            headingClass="fs-5 fw-semibold text-dark-gray"
            subHeadingClass="fs-md fw-normal text-dark-gray"
            className="my-3 text-center"
          />
          <hr className="my-3" />
          <div className="p-0 pe-lg-2 d-flex flex-column gap-3">
            <Form.Group className="d-flex flex-column fs-md">
              <Form.Label className=" d-lg-none" htmlFor="name-input">
                Name
              </Form.Label>
              <InputText
                type="text"
                name="name"
                id="name-input"
                placeholder="Enter your name"
                {...register("name")}
                className="bg-white rounded-3 fs-sm "
              />
              <Form.Text className="text-danger">{errors.name?.message}</Form.Text>
            </Form.Group>

            <Form.Group className="d-flex flex-column fs-md">
              <Form.Label className=" d-lg-none" htmlFor="email-input">
                Email
              </Form.Label>
              <InputText
                type="email"
                name="email"
                id="email-input"
                placeholder="Enter your email"
                {...register("email")}
                className="bg-white rounded-3 fs-sm "
              />
              <Form.Text className="text-danger">{errors.email?.message}</Form.Text>
            </Form.Group>

            <Form.Group className="d-flex flex-column fs-md">
              <Form.Label className=" d-lg-none" htmlFor="phone-input">
                Phone
              </Form.Label>
              <InputMask
                mask="9999999999"
                inputMode="numeric"
                name="phone"
                id="phone-input"
                placeholder="999-999-9999"
                {...register("phone")}
                className="bg-white rounded-3 fs-sm "
              />
              <Form.Text className="text-danger">{errors.phone?.message}</Form.Text>
            </Form.Group>

            <Form.Group className="d-flex flex-column fs-md mt-2 mt-lg-0 ">
              <Form.Label className=" d-lg-none" htmlFor="dob-input">
                Date of Birth
              </Form.Label>
              <Calendar
                className={`bg-white rounded-3 fs-sm  ${styles.inputCalendar}`}
                showOnFocus={false}
                value={getValues("dob")}
                visible={false}
                placeholder="DD/MM/YYYY"
                inputMode={"numeric"}
                name="dob"
                onChange={(e) =>
                  setValue("dob", e.value, {
                    shouldValidate: true,
                  })
                }
                dateFormat="dd/mm/yy"
                mask="99/99/9999"
                id="dob-input"
              />
              <Form.Text className="text-danger">{errors.dob?.message}</Form.Text>
            </Form.Group>

            <Form.Group className="d-flex flex-column fs-md">
              <Form.Label className=" d-lg-none">Country</Form.Label>
              <Dropdown
                value={getValues("country")}
                options={countries}
                onChange={(e) => {
                  setValue("country", e.value, {
                    shouldValidate: true,
                  });
                }}
                optionLabel="name"
                optionValue="name"
                placeholder="Select a Country"
                className={`rounded-3 `}
              />

              <Form.Text className="text-danger">{errors.dob?.message}</Form.Text>
            </Form.Group>

            <Form.Group className="d-flex flex-column fs-md">
              <Form.Label>Major</Form.Label>
              <div className="d-flex gap-3 align-items-center my-2 mb-3 fs-sm">
                <span>Filter by: </span>
                {[
                  {
                    name: "UG",
                    value: 1,
                  },

                  {
                    name: "PG",
                    value: 2,
                  },

                  {
                    name: "Diploma",
                    value: 3,
                  },
                ].map((category) => (
                  <Form.Group key={uuid()} className="d-flex flex-column align-items-center ">
                    <Form.Check
                      type="checkbox"
                      label={category.name}
                      name={category.name}
                      value={category.value}
                      className="m-0"
                      checked={parseInt(category.value) === parseInt(majorCategory)}
                      onChange={(e) => {
                        setMajorCategory(e.currentTarget.value);
                        setFilteredMajors(majors.filter((major) => parseInt(major.category_id) === parseInt(e.currentTarget.value)));
                        setValue("major", "", {
                          shouldValidate: true,
                        });
                      }}
                    />
                  </Form.Group>
                ))}
              </div>
              <Dropdown
                value={getValues("major")}
                options={filteredMajors}
                onChange={(e) => {
                  setValue("major", e.value, {
                    shouldValidate: true,
                  });
                }}
                optionLabel="identifier"
                optionValue="id"
                placeholder="Select a Major"
                className={`bg-white rounded-3 fs-sm `}
                filter={true}
                filterBy="identifier"
              />

              <Form.Text className="text-danger">{errors.major?.message}</Form.Text>
            </Form.Group>

            <button
              className={` bg-cvblue rounded-3 text-white fs-md mt-2 d-flex align-items-center justify-content-center py-2 border-0 `}
              type="submit"
              disabled={isLoading}
            >
              {isLoading ? (
                <>
                  <span className="spinner-border spinner-border-sm me-2 fs-xs" role="status" aria-hidden="true"></span>
                  Searching Universities
                </>
              ) : (
                `Apply Now`
              )}
              <input ref={gtmButtonRef} id="SUGGEST-ME" type={"hidden"} />
            </button>
          </div>
        </Col>
      </Row>
      {/* <Row>
        <Col></Col>
      </Row> */}
    </Form>
  );
}
