import Image from "next/image";
import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useState } from "react";
import { useEffect } from "react";
import {
  FacebookIcon,
  FacebookShareButton,
  WhatsappShareButton,
  WhatsappIcon,
  TwitterShareButton,
  TwitterIcon,
  LinkedinShareButton,
  LinkedinIcon,
} from "next-share";

export default function BlogProgressBar({ post }) {
  const [width, setWidth] = useState(0);
  const [show, setShow] = useState(false);

  const isInViewport = () => {
    const postEl = document.getElementById("blog-post-body");
    // if postEl is in view
    if (postEl.getBoundingClientRect().top < window.innerHeight) {
      setShow(true);
    }
    // if postEl is not in view
    else {
      setShow(false);
    }
  };

  const scrollHeight = () => {
    isInViewport();
    var el = document.documentElement,
      ScrollTop = el.scrollTop || document.body.scrollTop,
      ScrollHeight = el.scrollHeight || document.body.scrollHeight;
    var percent = (ScrollTop / (ScrollHeight - el.clientHeight)) * 100;
    // store percentage in state
    setWidth(percent);
  };

  useEffect(() => {
    window.addEventListener("scroll", scrollHeight);
    const element = document.getElementById("blog-post-body");

    return () => {
      window.removeEventListener("scroll", scrollHeight);
    };
  }, []);

  if (show)
    return (
      <div
        className="bg-white position-fixed top-0 start-0 w-100 shadow-sm"
        id="blog-post-header"
        style={{
          zIndex: 100,
        }}
      >
        <Row>
          <Col>
            <div className="px-2 py-3 d-flex align-items-center justify-content-center justify-content-md-start gap-3">
              <div>
                <Image src="/logos/cv-abroad-logo.png" alt="College Vidya Abroad Logo" width={100} height={50} objectFit="contain" />
              </div>
              <span className="fs-5 fw-medium">|</span>
              <p className="fs-md fs-md-5 m-0">{post.title}</p>
            </div>
          </Col>
          {post?.slug && post?.title ? (
            <Col className="d-none d-lg-block">
              <div className="d-flex align-items-center justify-content-end gap-3 h-100 pe-5">
                <p className="m-0">Share this post</p>
                <FacebookShareButton
                  url={`https://collegevidyaabroad.com/blog/${post.slug}`}
                  quote={post.title}
                  hashtag={post?.category?.name ? `#${post.category.name}` : "#collegevidyaabroad"}
                >
                  <FacebookIcon size={32} round />
                </FacebookShareButton>
                <WhatsappShareButton url={`https://collegevidyaabroad.com/blog/${post.slug}`} title={post.title} separator=":  ">
                  <WhatsappIcon size={32} round />
                </WhatsappShareButton>
                <TwitterShareButton
                  url={`https://collegevidyaabroad.com/blog/${post.slug}`}
                  title={post.title}
                  hashtags={post?.category?.name ? [post.category.name] : ["collegevidyaabroad"]}
                >
                  <TwitterIcon size={32} round />
                </TwitterShareButton>
                <LinkedinShareButton url={`https://collegevidyaabroad.com/blog/${post.slug}`} title={post.title}>
                  <LinkedinIcon size={32} round />
                </LinkedinShareButton>
              </div>
            </Col>
          ) : null}
        </Row>
        <span
          className="d-block"
          style={{
            height: "3px",
            minHeight: "3px",
            width: `${width}%`,
            backgroundColor: "#0f58e5",
          }}
        ></span>
      </div>
    );
  else return null;
}
