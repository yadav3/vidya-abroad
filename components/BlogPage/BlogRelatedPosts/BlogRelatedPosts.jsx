import React from "react";
import uuid from "react-uuid";
import BlogPostCard from "../BlogPostCard/BlogPostCard";
import styles from "./BlogRelatedPosts.module.scss";

import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination } from "swiper";
import "swiper/css";
import "swiper/css/pagination";
import Image from "next/image";
import { getAWSImagePath } from "../../../context/services";
import { useRouter } from "next/router";

export default function BlogRelatedPosts({ relatedLatestPosts, currentPost }) {
  const router = useRouter();
  const handleNavigateToPost = (slug) => {
    router.push(`/blog/${slug}`);
  };

  return (
    <div className="mt-4">
      <p className="fs-4 fw-semibold">Recommend for you</p>
      <div>
        <Swiper
          slidesPerView={1}
          spaceBetween={30}
          pagination={{
            clickable: true,
          }}
          breakpoints={{
            500: {
              slidesPerView: 2,
              spaceBetween: 30,
            },

            800: {
              slidesPerView: 3,
              spaceBetween: 40,
            },
            900: {
              slidesPerView: 4,
              spaceBetween: 40,
            },
          }}
          modules={[Pagination]}
          className="py-3"
        >
          {relatedLatestPosts
            .filter((post) => parseInt(post.id) !== parseInt(currentPost.id))
            .map((post) => (
              <SwiperSlide key={uuid()} className="mb-5">
                {/* <BlogPostCard post={post} /> */}
                <div className={`${styles.postCard} position-relative `}>
                  <div
                    className={`${styles.thumbnailContainer} position-relative cursor-pointer rounded-4 overflow-hidden`}
                    onClick={() => handleNavigateToPost(post.slug)}
                  >
                    <Image alt="Blog Thumbnail" src={getAWSImagePath(post.thumbnail)} layout="responsive" width={500} height={300} />
                  </div>
                  <div className={`${styles.content} mt-4`}>
                    <div
                      className={`${styles.title}
        fs-lg fw-semibold cursor-pointer mt-3 
        `}
                      onClick={() => handleNavigateToPost(post.slug)}
                    >
                      {post.title}
                    </div>
                  </div>
                </div>
              </SwiperSlide>
            ))}
        </Swiper>
      </div>
    </div>
  );
}
