import React from "react";
import { Tree } from "primereact/tree";
import { useEffect } from "react";
import { useState } from "react";
import Image from "next/image";
import uuid from "react-uuid";
import { IoIosArrowDown } from "react-icons/io";

export default function BlogTableContents(show) {
  const [showContents, setShowContents] = useState(show);
  const [nodes, setNodes] = useState([
    {
      key: "0",
      label: "Table of Contents",
      data: "",
      icon: "pi pi-fw pi-book",
      children: [],
    },
  ]);
  //   const nodes = [
  //     {
  //       key: "0",
  //       label: "Table of Contents",
  //       data: "",
  //       icon: "pi pi-fw pi-book",
  //       children: [
  //         {
  //           key: "0-0",
  //           label: "Work",
  //           data: "#work",
  //           icon: "pi pi-fw pi-link",
  //         },
  //         {
  //           key: "0-1",
  //           label: "Home",
  //           data: "#home",
  //           icon: "pi pi-fw pi-link",
  //         },
  //       ],
  //     },
  //   ];

  useEffect(() => {
    const headings = document.querySelector(".blog-post-body").querySelectorAll("h2");
    const children = [];
    //   set an id to each heading

    headings.forEach((heading) => {
      heading.id = heading.innerText.replace(/\s+/g, "-").toLowerCase();
    });

    headings.forEach((heading, index) => {
      children.push({
        key: `0-${index}`,
        label: heading.innerText,
        data: `#${heading.innerText.replace(/\s+/g, "-").toLowerCase()}`,
        icon: "pi pi-fw pi-link",
      });
    });
    setNodes([
      {
        key: "0",
        label: "Table of Contents",
        data: "",
        icon: "pi pi-fw pi-book",
        children: children,
      },
    ]);
  }, []);

  return (
    <div className="my-3 border border-1 rounded-3 py-2">
      {/* <h3>Table Contents: </h3> */}
      <div className="flex justify-content-center my-2">
        <button className="bg-white none border-0 d-flex align-items-center" onClick={() => setShowContents((prev) => !prev)}>
          <IoIosArrowDown className="ms-2" size={20} />
          <Image src="/icons/3d-book-icon.svg" alt="College Vidya Abroad" width={50} height={20} objectFit="contain" />
          <span className="fs-5 fw-medium">Table of Contents:</span>
        </button>
        {showContents && (
          <div className="w-100 ps-4 mt-2">
            {nodes[0].children.map((node) => {
              return (
                <a key={uuid()} href={node.data} className="d-flex align-items-center  p-2 cursor-pointer">
                  <span
                    className="bg-cvblue me-3"
                    style={{
                      width: "10px",
                      height: "10px",
                      minWidth: "10px",
                      minHeight: "10px",
                      borderRadius: "50%",
                    }}
                  ></span>
                  <span>{node.label}</span>
                </a>
              );
            })}
          </div>
        )}
        {/* <Tree
          value={nodes}
          className="w-full"
          onNodeClick={(e) => {
            if (e.node.data.length > 0) {
              window.location.hash = e.node.data;
            }
          }}
        /> */}
      </div>
    </div>
  );
}
