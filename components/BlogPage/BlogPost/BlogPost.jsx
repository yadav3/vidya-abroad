import React from "react";
import styles from "./BlogPost.module.scss";
import Image from "next/image";
import { AiOutlineClockCircle, AiOutlineEye } from "react-icons/ai";
import moment from "moment/moment";
import uuid from "react-uuid";
import { getAWSImagePath } from "../../../context/services";
import { useRouter } from "next/router";
import { Accordion, Breadcrumb } from "react-bootstrap";
import { MdCalendarToday } from "react-icons/md";
import DynamicAccordion from "../../global/DynamicAccordion/DynamicAccordion";
import BlogTableContents from "../BlogTableContents/BlogTableContents";
import { useEffect } from "react";
import Blog from "../../../services/blog.service";
import { formatNumber } from "../../../utils/misc";
import {
  FacebookIcon,
  FacebookShareButton,
  LinkedinIcon,
  LinkedinShareButton,
  TwitterIcon,
  TwitterShareButton,
  WhatsappIcon,
  WhatsappShareButton,
} from "next-share";

export default function BlogPost({ post }) {
  const router = useRouter();
  const getPostReadTime = (post) => {
    const wordsPerMinute = 200;
    const minutes = post.contentLength / wordsPerMinute;
    const readTime = Math.ceil(minutes);
    return readTime;
  };
  const getPublishedDate = (date) => {
    const publishedDate = new Date(date);
    // formate in english date like "7th march, 2021"
    const publishedDateInEnglish = moment(publishedDate).format("Do MMMM, YYYY");
    return publishedDateInEnglish;
  };

  const handleNavigateToCategory = (categorySlug) => {
    router.push(`/blog/categories/${categorySlug}`);
  };

  const blogService = new Blog();

  useEffect(() => {
    const incrementBlogPostViews = async () => {
      try {
        await blogService.addBlogPostView(post.slug);
      } catch (error) {
        console.error(error);
      }
    };

    if (post?.slug) {
      incrementBlogPostViews();
    }
  }, [post?.slug]);

  return (
    <div
      className={`${styles.postCard} d-flex flex-column align-items-center justify-content-center position-relative mb-3 mb-lg-0
    `}
    >
      <div className={`${styles.thumbnailContainer} w-100 position-relative`}>
        <Image alt="Blog Thumbnail" src={getAWSImagePath(post?.thumbnail)} layout="responsive" priority={true} width={1280} height={720} />
      </div>
      <div className={`${styles.content} w-100`}>
        <div className={`${styles.contentHeader} d-flex align-items-center justify-content-between fs-md`}>
          <div className="d-flex flex-column flex-md-row align-items-md-center gap-2">
            {post?.category?.name ? (
              <div
                className={`${styles.postCategoryTag}
             rounded-3 px-2 py-1 cursor-pointer
            `}
                onClick={() => handleNavigateToCategory(post.category.category_slug)}
              >
                <span>{post.category.name}</span>
              </div>
            ) : null}
          </div>
          <div className={`d-flex align-items-center gap-2`}>
            <span className="d-flex align-items-center gap-2">
              {<AiOutlineEye size={18} />}
              {!isNaN(post?.views) ? formatNumber(1000 + parseInt(post.views)) : formatNumber(1000)} reads
            </span>
            {post?.contentLength ? (
              <span className="d-flex align-items-center gap-2">
                {<AiOutlineClockCircle />}
                {getPostReadTime(post)} min read
              </span>
            ) : null}
          </div>
        </div>
        <Breadcrumb className="mt-3">
          {/* dynamic breadcrumb based on url */}
          <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
          <Breadcrumb.Item href="/blog">Blog</Breadcrumb.Item>
          <Breadcrumb.Item active>{post?.title ? post.title : null}</Breadcrumb.Item>
        </Breadcrumb>
        <div
          className={`${styles.title}
        fs-1 fw-semibold
        `}
        >
          <p>{post?.title ? post.title : null}</p>
        </div>
        <div className="d-flex align-items-center gap-3 h-100 mb-4">
          {post?.slug && post?.title ? (
            <>
              <FacebookShareButton
                url={`https://collegevidyaabroad.com/blog/${post.slug}`}
                quote={post.title}
                hashtag={post?.category?.name ? `#${post.category.name}` : "#collegevidyaabroad"}
              >
                <FacebookIcon size={32} round />
              </FacebookShareButton>
              <WhatsappShareButton url={`https://collegevidyaabroad.com/blog/${post.slug}`} title={post.title} separator=":  ">
                <WhatsappIcon size={32} round />
              </WhatsappShareButton>
              <TwitterShareButton
                url={`https://collegevidyaabroad.com/blog/${post.slug}`}
                title={post.title}
                hashtags={post?.category?.name ? [post.category.name] : ["collegevidyaabroad"]}
              >
                <TwitterIcon size={32} round />
              </TwitterShareButton>
              <LinkedinShareButton url={`https://collegevidyaabroad.com/blog/${post.slug}`} title={post.title}>
                <LinkedinIcon size={32} round />
              </LinkedinShareButton>
            </>
          ) : null}
        </div>
        {post?.published_at ? (
          <span>
            <MdCalendarToday className="me-2" />
            {getPublishedDate(post.published_at)}
          </span>
        ) : null}

        <div className={`${styles.tags} my-3 `}>
          {post?.keywords?.length > 0
            ? post.keywords.map((keyword) => (
                <span
                  key={uuid()}
                  className={`${styles.keywordTag}
          rounded-pill px-3 py-1 me-2 bg-grey
            fs-sm `}
                >
                  {keyword.keyword}
                </span>
              ))
            : null}
        </div>

        <BlogTableContents />

        <div
          className={`${styles.description} blog-post-body
      fs-5 fw-medium my-2
        `}
          id="blog-post-body"
        >
          <span dangerouslySetInnerHTML={{ __html: post?.content ? post.content.replaceAll("&nbsp;", "") : "<br/>" }} />
        </div>

        {post?.blog_faq?.length > 0 ? (
          <div key={uuid()} className="my-4">
            <h3 className="fs-2 fw-semibold mb-3">FAQs ( Frequently Asked Questions )</h3>
            <Accordion defaultActiveKey="0">
              {post.blog_faq.map((faq, i) => {
                return (
                  <Accordion.Item key={i} eventKey={i} className={`${styles.accordionHeader}`}>
                    <Accordion.Header>{faq.question}</Accordion.Header>
                    <Accordion.Body>
                      {faq?.answer_html?.length > 0 ? (
                        <span
                          dangerouslySetInnerHTML={{
                            __html: faq.answer_html,
                          }}
                        ></span>
                      ) : faq?.answer?.length > 0 ? (
                        <span>{faq.answer}</span>
                      ) : null}
                    </Accordion.Body>
                  </Accordion.Item>
                );
              })}
            </Accordion>
          </div>
        ) : null}

        <div className={`d-flex align-items-center justify-content-center gap-2`}>
          <button
            className={`
           bg-cvblue text-white px-4 py-3 rounded-pill fs-lg fw-semibold border-0 myshine  
          `}
            onClick={() => (window.location.href = "/suggest-me-a-university")}
          >
            Suggest me a University
          </button>
        </div>
      </div>
    </div>
  );
}
