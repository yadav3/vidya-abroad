import React from "react";
import styles from "./CountrySelectedStrip.module.scss";

export default function CountrySelectedStrip({ text, style, undo }) {
  return (
    <div className={styles.stripContainer} style={style}>
      <span>{text}</span>
      <button onClick={undo}>UNDO</button>
    </div>
  );
}
