import React from "react";
import { useRouter } from "next/router";

export default function Layout({ children }) {
  const router = useRouter();

  return (
    <div
      style={{
        maxWidth: "1900px",
        margin: "auto",
        overflow:
          router.pathname === "/thankyou" ||
          router.pathname.includes("compare") ||
          router.pathname.includes("blog") ||
          router.pathname.includes("/universities")
            ? "unset"
            : "hidden",
        position: router.pathname.includes("compare/overview") ? "static" : "relative",
        backgroundColor: router.pathname === "/video-library" ? "#F5F5F5" : "unset",
      }}
    >
      {children}
    </div>
  );
}
