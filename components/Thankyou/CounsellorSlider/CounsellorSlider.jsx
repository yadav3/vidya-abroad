import React, { useCallback, useRef, useState } from "react";
import styles from "./CounsellorSlider.module.scss";
import { Swiper, SwiperSlide } from "swiper/react";
// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Pagination, Navigation } from "swiper";

import { BsArrowRightCircle, BsArrowLeftCircle } from "react-icons/bs";
import BigTextHeading from "../../global/BigTextHeading/BigTextHeading";
import Image from "next/image";

export default function CounsellorSlider({ link }) {
  const [counsellors, setCounsellors] = useState([
    {
      name: "Abhishek",
      image: "/counsellors/abhishek.png",
      slug: "https://counsellor.collegevidyaabroad.com/counsellor/abhishek-sharma",
    },
    {
      name: "Akash",
      image: "/counsellors/akash.png",
      slug: "https://counsellor.collegevidyaabroad.com/counsellors",
    },
    {
      name: "Ashutosh",
      image: "/counsellors/ashutosh.png",
      slug: "https://counsellor.collegevidyaabroad.com/counsellors",
    },
    {
      name: "Dhirendra",
      image: "/counsellors/dhirendra.png",
      slug: "https://counsellor.collegevidyaabroad.com/counsellor/dhirendra-singh",
    },
    {
      name: "Esha",
      image: "/counsellors/esha.png",
      slug: "https://counsellor.collegevidyaabroad.com/counsellors",
    },
    {
      name: "Manish",
      image: "/counsellors/manish.png",
      slug: "https://counsellor.collegevidyaabroad.com/counsellor/manish-thapliyal",
    },
    {
      name: "Priyanka",
      image: "/counsellors/priyanka.png",
      slug: "https://counsellor.collegevidyaabroad.com/counsellors",
    },
    {
      name: "Ramiz",
      image: "/counsellors/ramiz.png",
      slug: "https://counsellor.collegevidyaabroad.com/counsellor/ramiz-khan",
    },
    {
      name: "Sankalp",
      image: "/counsellors/sankalp.png",
      slug: "https://counsellor.collegevidyaabroad.com/counsellors",
    },
    {
      name: "Supriya",
      image: "/counsellors/supriya.png",
      slug: "https://counsellor.collegevidyaabroad.com/counsellors",
    },
    {
      name: "Tanya",
      image: "/counsellors/tanya.png",
      slug: "https://counsellor.collegevidyaabroad.com/counsellors",
    },
    {
      name: "Udita",
      image: "/counsellors/udita.png",
      slug: "https://counsellor.collegevidyaabroad.com/counsellors",
    },
  ]);

  const counsellorSliderRef = useRef(null);

  const handlePrev = useCallback(() => {
    if (!counsellorSliderRef.current) return;
    counsellorSliderRef.current.swiper.slidePrev();
  }, []);

  const handleNext = useCallback(() => {
    if (!counsellorSliderRef.current) return;
    counsellorSliderRef.current.swiper.slideNext();
  }, []);

  return (
    <>
      {counsellors && counsellors.length > 0 && (
        <div className={`${styles.counsellorSliderContainer}`}>
          <BigTextHeading text="Talk to our " highlight="Counsellor" />

          <div className={`${styles.counsellorSlider}`}>
            <Swiper
              ref={counsellorSliderRef}
              slidesPerView={1}
              spaceBetween={0}
              slidesPerGroup={1}
              loop={true}
              loopFillGroupWithBlank={true}
              navigation={{
                enabled: true,
                nextEl: ".counsellorSlider-button-next",
                prevEl: ".counsellorSlider-button-prev",
              }}
              modules={[Pagination, Navigation]}
              breakpoints={{
                800: {
                  slidesPerView: 2,
                  spaceBetween: 0,
                },
                1000: {
                  slidesPerView: 3,
                  spaceBetween: 0,
                },
                1500: {
                  slidesPerView: 5,
                  spaceBetween: 0,
                },
              }}
              className="counsellorSlider"
            >
              {counsellors &&
                counsellors.length > 0 &&
                counsellors.map((data, i) => (
                  <SwiperSlide key={`${data.name}-${i}`}>
                    <a href={link} target="_blank" rel="noreferrer">
                      <CounsellorCard data={data} />
                    </a>
                  </SwiperSlide>
                ))}
            </Swiper>
            <button className="counsellorSlider-button counsellorSlider-button-prev" onClick={handlePrev}>
              <BsArrowLeftCircle />
            </button>
            <button className="counsellorSlider-button counsellorSlider-button-next" onClick={handleNext}>
              <BsArrowRightCircle />
            </button>
          </div>
        </div>
      )}
    </>
  );
}

export function CounsellorCard({ data }) {
  return (
    <div className={`${styles.counsellorCard}`}>
      <div className={`${styles.counsellorImage}`}>
        <Image src={data.image} alt={data.name} width={300} height={300} objectFit="cover" />
      </div>

      <div className={`${styles.counsellorDetails}`}>
        <h2>{data.name}</h2>
      </div>
    </div>
  );
}
