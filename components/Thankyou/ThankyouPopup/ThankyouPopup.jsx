import Image from "next/image";
import React from "react";
import { useStateContext } from "../../../context/StateContext";
import styles from "./ThankyouPopup.module.scss";
import { AiOutlineCloseCircle } from "react-icons/ai";
import { useRouter } from "next/router";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { ModalHeader } from "react-bootstrap";

export default function ThankyouPopup({ data, link }) {
  const { thankYouPopupShow, setThankYouPopupShow, ottaData, formData } = useStateContext();

  const handleRedirectToCounsellor = () => {
    window.open(link, "_ blank");
  };

  return (
    <Modal size="lg" show={thankYouPopupShow} className={`${styles.container}`} centered onHide={() => setThankYouPopupShow(false)}>
      <ModalHeader closeButton></ModalHeader>
      <Modal.Body className={`${styles.body}`}>
        {/* <AiOutlineCloseCircle onClick={() => setThankYouPopupShow(false)} size={20} className={`${styles.closeBtn}`} /> */}
        <Container>
          <Row>
            <Col>
              <div className={`${styles.vidyaImageContainer}`}>
                <video muted autoPlay loop src="/gif/thankyou-popup-vidya-video.m4v"></video>
              </div>
              <h4>Thank you!</h4>
              <p>
                Get Free Video Counselling with Abroad Experts | A special offer just for aspiring individuals like{" "}
                {ottaData?.name ? `you, ${ottaData.name}` : formData?.name ? `you, ${formData.name}` : "yourself"}!
              </p>
              <div className={`${styles.footer}`}>
                <Button className={`${styles.exploreBtn}`} onClick={() => setThankYouPopupShow(false)}>
                  Explore More
                </Button>
                <Button className={`${styles.freeCounsellingBtn}`} onClick={() => handleRedirectToCounsellor()}>
                  Free Counselling
                </Button>
              </div>
            </Col>
          </Row>
        </Container>
      </Modal.Body>
    </Modal>
  );
}
