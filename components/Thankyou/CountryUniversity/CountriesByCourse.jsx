import React, { useState, useEffect } from "react";
import styles from "./CountryUniversity.module.scss";
import { FaCheck } from "react-icons/fa";
import { useStateContext } from "../../../context/StateContext";
import axios from "axios";

export default function CountriesByCourse() {
  const { setSelectedCountry, selectedCountry, selectedCourse } =
    useStateContext();

  const [countries, setCountries] = useState([]);

  useEffect(() => {
    if (selectedCourse && countries.length === 0) {
      axios
        .get(
          `${
            process.env.NODE_ENV === "production"
              ? "https://collegevidyaabroad.com"
              : "http://localhost"
          }:8377/countries`
        )
        .then((data) => {
          setCountries(data.data);
        });
    }
  }, [selectedCourse]);

  return (
    <div className={` ${styles.countryContainer}`}>
      <h3>Select a Country</h3>
      <div className={` ${styles.countryButtonContainer}`}>
        {countries &&
          countries.length > 0 &&
          countries.map((country, i) => (
            <button
              key={`${country.name}-${i}`}
              onClick={() => setSelectedCountry(country.name)}
              className={` ${styles.countryButton}  ${
                selectedCountry === country.name
                  ? styles.countryButtonActive
                  : ""
              }`}
            >
              {country.name}
              {selectedCountry === country.name && <FaCheck />}
            </button>
          ))}
      </div>
    </div>
  );
}
