import React, { useState, useEffect } from "react";
import styles from "./CountryUniversity.module.scss";
import { useStateContext } from "../../../context/StateContext";
import axios from "axios";
import Image from "next/image";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import { Pagination } from "swiper";
import { getAWSImagePathBeta } from "../../../context/services";

export default function UniversityByCountry({ link }) {
  const { selectedCountry, selectedCourse } = useStateContext();
  const [universities, setUniversities] = useState([]);

  useEffect(() => {
    if (selectedCountry) {
      axios
        .get(
          `${
            process.env.NODE_ENV === "production" ? "https://collegevidyaabroad.com" : "http://localhost"
          }:8377/universities/${selectedCountry.toLowerCase()}`
        )
        .then((data) => {
          setUniversities(data.data);
        });
    }
  }, [selectedCountry]);

  return (
    <>
      <div className={` ${styles.universitiesContainer}`}>
        <h3 className="text-center">Recommended Universities for you</h3>
        <div className={` ${styles.universitiesButtonContainer}`}>
          {universities &&
            universities.length > 0 &&
            universities.map((universities, i) => (
              <div className="flip-card" key={`${universities.name}-${i}`}>
                <div className="flip-card-inner">
                  <div className="flip-card-front">
                    <button className={` ${styles.universityButton} `}>
                      {universities?.logo_full && (
                        <Image
                          src={getAWSImagePathBeta(universities.logo_full)}
                          alt="University Logo"
                          width={"100%"}
                          height={"100%"}
                          objectFit="contain"
                        />
                      )}
                      {universities.name}
                    </button>
                  </div>
                  <div className="flip-card-back">
                    {universities.name}
                    <a href={link} target="_blank" rel="noreferrer">
                      <button>Talk to an Expert</button>
                    </a>
                  </div>
                </div>
              </div>
            ))}
        </div>
        <div className={` ${styles.universitiesMobileContainer}`}>
          <Swiper
            slidesPerView={1}
            loop={true}
            spaceBetween={0}
            pagination={{
              clickable: true,
            }}
            modules={[Pagination]}
            className="universitiesByCountrySlider"
          >
            {universities &&
              universities.length > 0 &&
              universities.map((universities, i) => (
                <SwiperSlide key={`${universities.name}-${i}`}>
                  <div className="flip-card">
                    <div className="flip-card-inner">
                      <div className="flip-card-front">
                        <button className={` ${styles.universityButton} `}>
                          {universities?.logo_full && (
                            <Image src={universities.logo_full} alt="University Logo" width={"100%"} height={"100%"} objectFit="contain" />
                          )}
                          {universities.name}
                        </button>
                      </div>
                      <div className="flip-card-back">
                        <span>{universities.name}</span>
                        <a href={link} target="_blank" rel="noreferrer">
                          <button>Talk to an Expert</button>
                        </a>
                      </div>
                    </div>
                  </div>
                </SwiperSlide>
              ))}
          </Swiper>
        </div>
      </div>
    </>
  );
}
