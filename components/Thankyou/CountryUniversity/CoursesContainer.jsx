import React, { useState, useEffect } from "react";
import styles from "./CountryUniversity.module.scss";
import { FaCheck } from "react-icons/fa";
import { useStateContext } from "../../../context/StateContext";
import uuid from "react-uuid";

export default function CoursesContainer() {
  const { selectedPrograms, setSelectedCourse, selectedCourse, courseSpecializations } = useStateContext();

  const [specializations, setSpecializations] = useState([]);

  const [courses, setCourses] = useState({
    "Bachelor's": [],
    Diploma: [],
    "Master's": [],
  });

  useEffect(() => {
    if (selectedPrograms && courses[selectedPrograms]?.length === 0) {
      setSpecializations(courseSpecializations[selectedPrograms]);
    }
  }, [courseSpecializations, courses, selectedPrograms]);

  return (
    <div className={` ${styles.courseContainer}`}>
      <h3>Select a Course</h3>
      <div className={` ${styles.courseButtonContainer}`}>
        {specializations &&
          specializations?.length > 0 &&
          specializations?.map((specialization, i) => (
            <button
              key={uuid()}
              onClick={() => setSelectedCourse(specialization)}
              className={` ${styles.courseButton}  ${selectedCourse === specialization ? styles.courseButtonActive : ""}`}
            >
              {specialization}
              {selectedCourse === specialization && <FaCheck />}
            </button>
          ))}
      </div>
    </div>
  );
}
