import React from "react";
import styles from "./CountryUniversity.module.scss";
import { useStateContext } from "../../../context/StateContext";
import CountrySelectedStrip from "../../CountriesAndUniversities/CountrySelectedStrip/CountrySelectedStrip";
import CoursesContainer from "./CoursesContainer";
import ProgramsContainer from "./ProgramsContainer";
import CountriesByCourse from "./CountriesByCourse";
import UniversityByCountry from "./UniversityByCountry";

export default function CountryUniversity({link}) {
  const { selectedPrograms, setSelectedPrograms, selectedCourse, setSelectedCourse, selectedCountry, setSelectedCountry, ottaData } =
    useStateContext();

  return (
    <div className={`countryContainer ${styles.countryUniversityContainer}`}>
      <ProgramsContainer />

      {selectedPrograms && <CoursesContainer />}

      {selectedCourse && (
        <CountrySelectedStrip
          text={`You selected ${selectedPrograms === "diploma" ? "" : selectedPrograms + " in"} ${selectedCourse}`}
          undo={() => {
            setSelectedPrograms(null);
            setSelectedCourse(null);
            setSelectedCountry(null);
            window.scrollTo({
              top: "500px",
            });
          }}
        />
      )}

      {selectedCourse && <CountriesByCourse />}

      {selectedCountry && selectedCountry !== "Not decided yet" && (
        <CountrySelectedStrip
          text={`You selected ${selectedCountry}`}
          undo={() => {
            setSelectedCountry(null);
            window.scrollTo({
              top: "100px",
            });
          }}
          style={{ top: "60px" }}
        />
      )}

      {selectedCountry && <UniversityByCountry link={link} />}
    </div>
  );
}
