import React from "react";
import { useStateContext } from "../../../context/StateContext";
import styles from "./CountryUniversity.module.scss";

export default function ProgramsContainer() {
  const { selectedPrograms, setSelectedPrograms } = useStateContext();

  return (
    <div className={` ${styles.programContainer}`}>
      <h3>Select a Program</h3>
      <div className={` ${styles.programButtonContainer}`}>
        {["Bachelor's", "Master's"].map((program, i) => (
          <button
            key={`${program}-${i}`}
            onClick={() => setSelectedPrograms(program)}
            className={` ${styles.programButton}  ${selectedPrograms === program ? styles.programButtonActive : ""}`}
          >
            {program}
          </button>
        ))}
      </div>
    </div>
  );
}
