import Image from "next/image";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import { useRef } from "react";
import { AiOutlineCheck } from "react-icons/ai";
import { BiChevronsDown } from "react-icons/bi";
import { useOnScreen } from "../../../context/CustomHooks";
import { useStateContext } from "../../../context/StateContext";
import styles from "./ThankYouBanner.module.scss";

export default function ThankYouBanner({ isLoading, loginLink }) {
  const { ottaData } = useStateContext();

  const stickyCta = useRef();
  const containerRef = useRef();
  const onScreen = useOnScreen(containerRef, "");

  const [timer, setTimer] = useState(60);
  const [rotation, setRotation] = useState(0);

  useEffect(() => {
    if (containerRef.current) {
      if (!onScreen) {
        stickyCta.current.classList.add("stickCtaActive");
        stickyCta.current.style.position = "fixed";
      } else {
        stickyCta.current.style.position = "static";
      }
    }
  }, [onScreen]);

  useEffect(() => {
    if (timer === 1) setTimer(60);

    const timerID = setInterval(() => {
      setTimer(timer - 1);
    }, 1000);

    return () => {
      clearInterval(timerID);
    };
  }, [timer]);

  useEffect(() => {
    if (rotation === 360) setRotation(0);

    const rotationTimerID = setInterval(() => {
      setRotation((rotation) => rotation + 3);
    }, 10);

    return () => {
      clearInterval(rotationTimerID);
    };
  }, [rotation]);

  return (
    <div className={`${styles.container}`} ref={containerRef}>
      <div className={`${styles.left}`}>
        <div className={`${styles.introText}`}>
          <h1>
            Awesome<span>!</span>
          </h1>
          <h3>
            Based on your details, I was able to compile the best universities{" "}
            {ottaData.country && ottaData.country !== "Not decided yet" && (
              <>
                in <span className="mx-2"> {ottaData.country}</span>
              </>
            )}{" "}
            for you. Also, one of our experts will be connecting with you shortly!
          </h3>
          <div className={`${styles.stickyCta}`} ref={stickyCta}>
            <div
              style={{
                background: "none",
              }}
              className={`${styles.timerGif}`}
            >
              <Image alt="Thank you timer radar" src="/gif/thankyou-timer-gif.gif" width={200} height={200} objectFit="contain" />
              <p>{isLoading ? timer : <AiOutlineCheck />}</p>
            </div>
            <div className={`${styles.cta}`}>
              <Link href={loginLink ? loginLink : "https://counsellor.collegevidyaabroad.com/counsellors"}>
                <a target="_blank">Talk to Expert</a>
              </Link>
            </div>
          </div>
          <div className={`${styles.mouseIcon}`}>
            <BiChevronsDown size={30} />
          </div>
        </div>
      </div>

      <div className={`${styles.right}`}>
        <div className={`${styles.imageContainer}`}>
          <Image alt="Intro Vidya" src="/gif/vidya (6).gif" width={450} height={450} objectFit="contain" objectPosition="top" />
        </div>
      </div>
    </div>
  );
}
