import Image from "next/image";
import styles from "./JourneyHeader.module.scss";
import React from "react";
import { AiOutlineClockCircle } from "react-icons/ai";
import { journeyFormStaticQuestionAtom, journeyFormStepAtom } from "../../../store/journeyFormStore";
import { useRecoilState } from "recoil";
import { ProgressBar } from "react-bootstrap";
import Link from "next/link";

export default function JourneyHeader() {
  const [step, setStep] = useRecoilState(journeyFormStepAtom);
  const [questions, setQuestions] = useRecoilState(journeyFormStaticQuestionAtom);

  return (
    <div className="d-flex flex-column justify-content-center align-items-center bg-white" id="journey-form-header">
      <div className="d-flex align-items-center p-3">
        <div className="d-flex align-items-center">
          <div className="position-relative">
            <Link href="/">
              <a>
                <Image src={"/logos/cv-abroad-logo.png"} alt="College Vidya Abroad Logo" width={150} height={50} />
              </a>
            </Link>
          </div>
        </div>
      </div>
      {step === 1 ? (
        <div
          className="d-flex justify-content-center align-items-center gap-2 w-100  fs-md text-center px-3"
          style={{
            backgroundColor: "#00d383",
          }}
        >
          <AiOutlineClockCircle className="text-white" />
          <p className="text-white m-0 py-1">Find the right Study Abroad University</p>
        </div>
      ) : null}
      <ProgressBar
        style={{
          height: step === 1 ? "0px" : "5px",
          opacity: step === 1 ? 0 : 1,
        }}
        className={`w-100 rounded-0 ${styles.progressBar} `}
        now={((step - 1) / questions.length) * 100}
      />
    </div>
  );
}
