import React, { useEffect, useState } from "react";
import styles from "./JourneyForm.module.scss";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { useCallback } from "react";
import { useRecoilState } from "recoil";
import { journeyFormAtom, journeyFormStaticQuestionAtom, journeyFormStepAtom } from "../../../store/journeyFormStore";
import { Dropdown } from "primereact/dropdown";
import Image from "next/image";
import {
  COUNSELLOR_CLIENT_URL,
  COUNSELLOR_SERVER_URL,
  createNewLead,
  generatePassword,
  getAWSImagePath,
  getAWSImagePathBeta,
  storeUserCompareResults,
} from "../../../context/services";
import { Calendar } from "primereact/calendar";
import { Col, FormText, Row } from "react-bootstrap";
import OttaPointLeadFormPoints from "../../Otta/OttaPointLeadFormPoints";
import moment from "moment";
import { compareQueryParamsState } from "../../../store/compare/compareStore";
import axios from "axios";
import { cvCounsellorDetailsAtom } from "../../../store/journeyStore";
import { useRouter } from "next/router";
import { useLocalStorage } from "../../../context/CustomHooks";
import { createLsqOpportunity, updatedLsqLeadDetails } from "../../../services/lead.service";
import { getCookie } from "cookies-next";
import CounsellorStripRating from "../../global/CounsellorStripRating/CounsellorStripRating";
import { getBudgetByStringLimit } from "../../../utils/misc";

export default function JourneyForm({ majors, countries }) {
  const router = useRouter();

  const [questions, setQuestions] = useRecoilState(journeyFormStaticQuestionAtom);
  const [dynamicSchema, setDynamicSchema] = useState(yup.object().shape({}));
  const [step, setStep] = useRecoilState(journeyFormStepAtom);
  const [currentQuestion, setCurrentQuestion] = useState(questions[0]);
  const [isLoading, setIsLoading] = useState(false);

  const [journey, setJourney] = useState(journeyFormAtom);
  const [userCompareQuery, setUserCompareQuery] = useRecoilState(compareQueryParamsState);
  const [cvCounsellorDetails, setCvCounsellorDetails] = useRecoilState(cvCounsellorDetailsAtom);
  const [localStorageLeadData, setLocalStorageLeadData] = useLocalStorage("leadData", {});
  const [leadFilledUserID, setLeadFilledUserID] = useLocalStorage("LeadFilledUserID", null);

  const [headerHeight, setHeaderHeight] = useState(0);

  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
    getValues,
    getFieldState,
  } = useForm({
    resolver: yupResolver(dynamicSchema),
  });

  const onSubmit = async (data) => {
    try {
      // setLocalStorageLeadData({
      //   data,
      // });
      handleNext();

      // if last step
      if (step === questions.length) {
        setIsLoading(true);
        const program = data.degree;
        const category_id = program === "Bachelors" ? 1 : program === "Masters" ? 2 : program === "Diploma" ? 3 : null;
        const country_id = parseInt(data.countryId);
        const country =
          parseInt(data.countryId) === 0 ? "Not Decided Yet" : countries.find((country) => parseInt(country.id) === parseInt(data.countryId)).name;
        const major_id = parseInt(data.majorId);
        const major = majors.find((major) => major.id === major_id).name;
        const budget = "Not Decided Yet";
        const budgetLow = getBudgetByStringLimit("Not Decided Yet")[0];
        const budgetHigh = getBudgetByStringLimit("Not Decided Yet")[1];
        const cv_id = generatePassword(8);
        const user_id = cv_id.toString();
        const lsq_source = "College Vidya";
        const sub_source = getCookie("source_campaign") === "collegevidya_online_website" ? "Online Website" : "Abroad Website";
        const source_campaign = getCookie("source_campaign");
        const campaign_name = getCookie("campaign_name");
        const ad_group = getCookie("ad_group_name");
        const ad_name = getCookie("ads_name");

        await createNewLead({
          userId: user_id,
          name: data.fullName,
          email: data.email,
          phone: data.phone,
          country: country,
          majorId: major_id,
          budget: budget,
          program: program,
          source: lsq_source,
          sub_source: sub_source,
          source_campaign: source_campaign,
          campaign_name: campaign_name,
          ad_group: ad_group,
          ad_name: ad_name,
        });

        const opportunityRes = await createLsqOpportunity(
          data.fullName,
          data.email,
          data.phone,
          budget,
          country,
          program,
          major,
          lsq_source,
          sub_source,
          source_campaign,
          campaign_name,
          ad_group,
          ad_name
        );

        const lsqLeadData = {
          data: [
            {
              Attribute: "mx_Study_Destination",
              Value: country,
            },
            {
              Attribute: "mx_Programme",
              Value: program,
            },
            {
              Attribute: "mx_Specilazation",
              Value: major,
            },
            {
              Attribute: "mx_Budget_Options",
              Value: budget,
            },
          ],
        };

        const userDetails = await storeUserCompareResults(
          user_id,
          category_id,
          null,
          null,
          major_id,
          null,
          country_id,
          0,
          opportunityRes.opportunityId,
          budgetLow,
          budgetHigh,
          [country_id],
          opportunityRes.relatedProspectId
        );

        setUserCompareQuery({
          uuid: userDetails.user_id,
          categoryId: userDetails.category_id,
          courseId: userDetails.course_id,
          majorId: userDetails.majorId,
          specializationId: userDetails.specialization_id,
          countryId: userDetails.country_id,
          budgetLimit: userDetails.budget,
          ieltsScore: userDetails.ielts,
          opportunityId: opportunityRes.opportunityId,
          budgetLow: userDetails.budgetLow,
          budgetHigh: userDetails.budgetHigh,
          countries: userDetails?.countries ? [...userDetails.countries] : [],
        });

        setLeadFilledUserID(userDetails.user_id);
        const res = await updatedLsqLeadDetails(userDetails.user_id, lsqLeadData);

        axios
          .post(`${COUNSELLOR_SERVER_URL}/collegevidyaabroad/customer/create`, {
            uuid: userDetails.user_id,
            cv_id: user_id,
            name: data.fullName,
            phone: data.phone,
            email: data.email,
            dob: data.dob,
          })
          .then(() => {
            setCvCounsellorDetails({
              ...cvCounsellorDetails,
              cv_id: user_id,
              redirectLink: `${COUNSELLOR_CLIENT_URL}/collegevidya/redirect/login?cv_id=${user_id}&country=${country}`,
            });
          })
          .catch((err) => {
            console.error(err);
          });
        window.location.href = `/compare/universities?uuid=${userDetails.user_id}`;
      }
    } catch (error) {
      console.error(error);
    } finally {
      setIsLoading(false);
    }
  };

  const handleQuestionChange = (e) => {
    const { name, value } = e.target;
    setValue(name, value, {
      shouldValidate: true,
    });

    setJourney((prev) => {
      return {
        ...prev,
        [name]: value,
      };
    });

    // if step is not last
    if (step < questions.length) {
      handleSubmit(onSubmit)();
    }
  };

  const handleBack = () => {
    if (step > 1) {
      setStep(step - 1);
      setCurrentQuestion(questions[step - 2]);
    }
  };

  const handleNext = () => {
    if (step < questions.length) {
      setStep(step + 1);
      setCurrentQuestion(questions[step]);
    }
  };

  const generateSchema = useCallback(() => {
    const schema = {};
    if (questions[step - 1].error.required && questions[step - 1].key === "major") {
      schema[currentQuestion.name] = yup.number().required(currentQuestion.error.message).typeError(currentQuestion.error.message);
    } else if (questions[step - 1].error.required && questions[step - 1].key === "country") {
      schema[currentQuestion.name] = yup.number().required(currentQuestion.error.message).typeError(currentQuestion.error.message);
    } else if (questions[step - 1].key === "lead-form") {
      schema = {
        fullName: yup.string().required("Please enter your full name").typeError("Please enter your full name"),
        email: yup.string().required("Please enter your email").typeError("Please enter your email").email("Please enter a valid email"),
        phone: yup
          .string()
          .required("Please enter your phone number")
          .typeError("Please enter your phone number")
          .matches(/^\d{10}$/, "Please enter a valid phone number"),
        // dob: yup.string().required("Please enter your date of birth").typeError("Please enter your date of birth"),
        // date of birth validation with required and min age 15
        dob: yup
          .date()
          .typeError("Invalid date")
          .required("Please enter your date of birth")
          .min(moment().subtract(100, "years"), "Age can't be more than 100 years")
          .max(moment().subtract(15, "years"), "Age can't be less than 15 years"),
      };
    } else if (questions[step - 1].error.required) {
      schema[currentQuestion.key] = yup.string().required(currentQuestion.error.message).typeError(currentQuestion.error.message);
    } else {
      schema[currentQuestion.key] = yup.string();
    }
    setDynamicSchema(yup.object().shape(schema));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [questions, step]);

  const renderQuestion = (question) => {
    switch (question.type) {
      case "radio":
        if (question.key === "country") {
          return (
            <div className="w-75 row gap-3 align-items-center justify-content-center">
              {question.options.map((option) => (
                <label
                  key={option.id}
                  className={`col-lg-4 ${styles.optionButton}
             ${parseInt(getValues(question.name)) === parseInt(option.value) ? styles.optionButtonSelected : ""}
            bg-white rounded-4  text-black d-flex align-items-center gap-3`}
                  htmlFor={option.name}
                >
                  {countries.find((country) => country.id === option.id)?.flag.length > 0 ? (
                    <span
                      className="m-0 d-flex align-items-center justify-content-center 
                rounded-circle"
                    >
                      <Image
                        src={getAWSImagePathBeta(countries.find((country) => country.id === option.id).flag)}
                        alt={option.label}
                        width={30}
                        height={30}
                        objectFit="contain"
                      />
                    </span>
                  ) : null}

                  <span className="d-flex flex-column">
                    {option.label}
                    {option.id !== 0 ? (
                      <small className="text-muted fs-sm">{option.courseCount}+ Courses Available</small>
                    ) : (
                      <small className="text-muted fs-sm">All Courses</small>
                    )}
                    <input
                      className="form-check-input"
                      type="radio"
                      name={question.name}
                      id={option.name}
                      value={option.value}
                      {...register(question.name)}
                      checked={getValues(question.name) === option.value}
                      onChange={handleQuestionChange}
                      onClick={handleQuestionChange}
                      style={{ display: "none" }}
                    />
                  </span>
                </label>
              ))}
            </div>
          );
        } else {
          return (
            <div className="w-75 row gap-3 align-items-center justify-content-center">
              {question.options.map((option) => (
                <label
                  key={option.id}
                  className={`col-lg-4 ${styles.optionButton}
             ${getValues(question.key) === option.value ? styles.optionButtonSelected : ""}
            bg-white rounded-4  text-black d-flex align-items-center justify-content-center gap-3 `}
                  htmlFor={option.key}
                >
                  {option.label}
                  <input
                    className="form-check-input"
                    type="radio"
                    name={question.name}
                    id={option.key}
                    value={option.value}
                    {...register(question.key)}
                    checked={getValues(question.key) === option.value}
                    onChange={(e) => {
                      if (question.key === "degree") {
                        setJourney((prev) => {
                          return {
                            ...prev,
                            categoryId: option.id,
                          };
                        });
                      }
                      handleQuestionChange(e);
                    }}
                    onClick={(e) => {
                      if (question.key === "degree") {
                        setJourney((prev) => {
                          return {
                            ...prev,
                            categoryId: option.id,
                          };
                        });
                      }
                      handleQuestionChange(e);
                    }}
                    style={{ display: "none" }}
                  />
                </label>
              ))}
            </div>
          );
        }
      case "checkbox":
        return question.options.map((option) => (
          <div key={option.id} className="form-check">
            <input
              className="form-check-input"
              type="checkbox"
              name={question.name}
              id={option.key}
              value={option.value}
              {...register(question.key)}
              onChange={handleQuestionChange}
              onClick={handleQuestionChange}
            />
            <label className="form-check-label" htmlFor={option.key}>
              {option.label}
            </label>
          </div>
        ));
      case "text":
        return (
          <div>
            <input
              type="text"
              className="form-control"
              id={question.key}
              name={question.name}
              placeholder={question.placeholder}
              {...register(question.key)}
              required={question.error.required ? question.error.required : false}
              onChange={handleQuestionChange}
              onClick={handleQuestionChange}
            />
            <label htmlFor={question.key}>{question.key}</label>
          </div>
        );
      case "textarea":
        return (
          <div>
            <textarea
              className="form-control"
              placeholder={question.placeholder}
              name={question.name}
              id={question.key}
              style={{ height: "100px" }}
              {...register(question.key)}
              onChange={handleQuestionChange}
              onClick={handleQuestionChange}
            ></textarea>
            <label htmlFor={question.key}>{question.key}</label>
          </div>
        );
      case "select":
        return (
          <div className="w-75 d-flex justify-content-center">
            {
              <Dropdown
                className={styles.selectDropdown}
                name={question.name}
                id={question.key}
                options={question.options}
                value={getValues(question.name)}
                onChange={handleQuestionChange}
                onClick={handleQuestionChange}
                optionLabel="label"
                optionValue="value"
                placeholder={question.placeholder}
                // {...register(question.name)}
                itemTemplate={(option) => {
                  return (
                    <div className="flex align-items-center">
                      <div dangerouslySetInnerHTML={{ __html: option.label }}></div>
                      <small className="text-muted fs-sm">{option.courseCount}+ Courses Offered</small>
                    </div>
                  );
                }}
                filter
                filterBy="label"
              />
            }
          </div>
        );
      case "lead-form":
        return (
          <Row
            style={{
              width: "90%",
            }}
          >
            <Col lg={5}>
              {" "}
              <div className="d-flex flex-column justify-content-center h-100">
                <div>
                  <label className="mb-2" htmlFor="fullName">
                    Full Name
                  </label>
                  <input
                    type="text"
                    className={`form-control mb-2 ${styles.formInput}`}
                    id="fullName"
                    name="fullName"
                    placeholder="Full Name"
                    {...register("fullName")}
                    required
                    onChange={handleQuestionChange}
                    onClick={handleQuestionChange}
                  />
                  <FormText className="text-danger">{errors.fullName && errors.fullName.message}</FormText>
                </div>
                <div className="mt-3">
                  <label className="mb-2" htmlFor="email">
                    Email
                  </label>
                  <input
                    type="email"
                    className={`form-control mb-2 ${styles.formInput}`}
                    id="email"
                    name="email"
                    placeholder="Email"
                    {...register("email")}
                    required
                    onChange={handleQuestionChange}
                    onClick={handleQuestionChange}
                  />
                  <FormText className="text-danger">{errors.email && errors.email.message}</FormText>
                </div>
                <div className="mt-3">
                  <label className="mb-2" htmlFor="phone">
                    Phone
                  </label>
                  <span className="position-relative d-block">
                    <input
                      type="number"
                      className={`form-control mb-2 ${styles.formInput}`}
                      id="phone"
                      name="phone"
                      placeholder="Phone"
                      {...register("phone")}
                      required
                      onChange={handleQuestionChange}
                      onClick={handleQuestionChange}
                    />
                    <span
                      className="position-absolute end-0 me-3 translate-middle-y d-flex align-items-center gap-2 fs-sm bg-white"
                      style={{
                        bottom: "7%",
                      }}
                    >
                      {getValues("phone")?.length > 0 ? (
                        getValues("phone")?.length === 10 ? (
                          <span className="text-success  d-flex align-items-center gap-2">
                            Valid
                            <i className={`pi pi-check-circle`}></i>
                          </span>
                        ) : (
                          <span className="text-danger d-flex align-items-center gap-2">
                            Invalid
                            <i className={`pi pi-question-circle  `}></i>
                          </span>
                        )
                      ) : null}
                    </span>
                  </span>
                  <FormText className="text-danger">{errors.phone && errors.phone.message}</FormText>
                </div>
                <div className="mt-3 d-flex flex-column">
                  <label className="mb-2 " htmlFor="dob">
                    Date of Birth
                  </label>
                  <span className="position-relative ">
                    <Calendar
                      id="calendar"
                      name="dob"
                      className={`mb-2 w-100  ${styles.formInput} ${styles.formInputCalendar}`}
                      visible={false}
                      value={getValues("dob")}
                      onChange={handleQuestionChange}
                      showOnFocus={false}
                      placeholder="DD/MM/YYYY"
                      inputMode={"numeric"}
                      dateFormat="dd/mm/yy"
                      mask="99/99/9999"
                    />
                    <span className="position-absolute top-50 end-0 me-3 translate-middle-y d-flex align-items-center gap-2 fs-sm">
                      {getValues("dob") && moment().diff(moment(getValues("dob"), "DD/MM/YYYY"), "years") + " years old"}
                      <i className={`pi pi-calendar `}></i>
                    </span>
                  </span>
                  <FormText className="text-danger">{errors.dob && errors.dob.message}</FormText>
                </div>
                <div className="mt-3">
                  <CounsellorStripRating />
                </div>
              </div>
            </Col>
            <Col lg={7} className="d-none d-lg-block">
              <OttaPointLeadFormPoints />
            </Col>
          </Row>
        );
      default:
        return null;
    }
  };

  const renderQuestionError = (question) => {
    switch (question.key) {
      case "country":
        return errors["countryId"] && errors["countryId"].message;
      case "major":
        return errors["majorId"] && errors["majorId"].message;
      default:
        return errors[question.key] && errors[question.key].message;
        break;
    }
  };

  function getFormHeightByHeader() {
    const headerElement = document.getElementById("journey-form-header");
    if (headerElement) {
      setHeaderHeight(headerElement.offsetHeight);
    }
  }

  const setExtraQuestionsFromApi = () => {
    if (currentQuestion.key === "major") {
      if (majors.length > 0) {
        //  replace the second question with majors
        const newQuestions = questions.filter((question) => question.id !== 2);
        newQuestions.splice(1, 0, {
          id: 2,
          question: "What is your major?",
          key: "major",
          type: "select",
          name: "majorId",
          placeholder: "Select your major",
          options: majors
            .filter((major) => major.category_id === journey.categoryId)
            .map((major) => ({
              id: major.id,
              label: major.identifier,
              courseCount: major._count.specializations_majors,
              value: major.id,
            }))
            .sort((a, b) => b.courseCount - a.courseCount),
          error: {
            required: true,
            message: "Please select your major",
            pattern: null,
          },
        });

        setQuestions(newQuestions);
      }
    } else if (currentQuestion.key === "country") {
      if (countries.length > 0) {
        const newQuestions = questions.filter((question) => question.id !== 3);
        newQuestions.splice(2, 0, {
          id: 3,
          question: "What is your Study Abroad Dream Country?",
          key: "country",
          type: "radio",
          name: "countryId",
          placeholder: "Choose a country",
          options: countries
            .map((country) => ({
              id: country.id,
              label: country.name,
              value: country.id,
              courseCount: country.courseCount,
            }))
            .concat({
              id: 0,
              label: "Not Decided Yet",
              value: "0",
            })
            .sort((a, b) => b.id - a.id),
          error: {
            required: true,
            message: "Please select your country",
            pattern: null,
          },
        });

        setQuestions(newQuestions);
      }
    }
  };

  useEffect(() => {
    setExtraQuestionsFromApi();
    getFormHeightByHeader();
    generateSchema();
  }, [step, majors]);

  return (
    <form
      onSubmit={handleSubmit(onSubmit)}
      className={`d-flex flex-column align-items-center pb-5 ${styles.formContainer}`}
      style={{
        minHeight: `calc(100vh - ${headerHeight}px)`,
      }}
    >
      {questions.length > 0
        ? questions.map((question, index) => {
            if (index + 1 === step) {
              return (
                <div key={question.id} className="d-flex flex-column justify-content-between align-items-center gap-4 h-100 w-100">
                  <div className=" p-2 p-3 p-lg-3 pt-lg-5 pb-lg-2 mt-5 mt-lg-0 w-100">
                    <h2 className="text-center fs-4 fw-bold">{question.question}</h2>
                  </div>
                  <div className={styles.questionContainer}>
                    <div className="d-flex flex-wrap justify-content-center align-items-center gap-4">{renderQuestion(question)}</div>
                    <div className="text-danger text-center mt-3">{renderQuestionError(question)}</div>
                  </div>
                  <div
                    className="d-flex justify-content-center align-items-center gap-4 py-3 position-fixed bottom-0 bg-white w-100"
                    style={{
                      zIndex: 10,
                    }}
                  >
                    {step > 1 && (
                      <button type="button" onClick={handleBack} className={`${styles.btnOutline}`}>
                        Back
                      </button>
                    )}
                    <button type="submit" className={`${styles.btn}`} disabled={isLoading}>
                      {isLoading ? "Compiling..." : step === questions.length ? "Compare Universities" : "Next"}
                    </button>
                  </div>
                </div>
              );
            }
          })
        : null}
    </form>
  );
}
