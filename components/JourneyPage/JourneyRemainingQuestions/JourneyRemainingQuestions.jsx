import React, { useState } from "react";
import { yupResolver } from "@hookform/resolvers/yup";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import { useCallback } from "react";
import { useEffect } from "react";
import styles from "./JourneyRemainingQuestions.module.scss";
import { JourneyRemainingQuestionModalAtom, JourneyRemainingQuestionModalStateAtom } from "../../../store/journeyFormStore";
import { useRecoilState } from "recoil";
import { updateLeadFields, updatedLsqLeadDetails, updatedLsqOpportunity } from "../../../services/lead.service";
import { compareQueryParamsState } from "../../../store/compare/compareStore";
import { updateUserCompareQueryFields } from "../../../services/user.serivce";
import Image from "next/image";

import Lottie from "lottie-react";
import flyingPlaneAnimation from "../../../public/animations/flying-paper-plane.json";
import OttaPointLeadSwiper from "../../Otta/OttaPointLeadSwiper";
import { ottaPointLeadAtom } from "../../Otta/OttaPointLead.store";

export default function JourneyRemainingQuestions({ onClose }) {
  const [questions, setQuestions] = useState([
    {
      id: 1,
      key: "qualification",
      question: "What is your highest qualification?",
      type: "radio",
      name: "qualification",
      placeholder: "Select a option",
      error: {
        required: true,
        message: "Please select a option",
        pattern: "",
      },
      options: [
        {
          id: 1,
          key: "12th-pass",
          value: "12th-pass",
          label: "12th Pass",
        },
        {
          id: 2,
          key: "Graduate",
          value: "Graduate",
          label: "Graduate",
        },
        {
          id: 3,
          key: "post-graduate",
          value: "Post Graduate",
          label: "Post Graduate",
        },
      ],
    },
    {
      id: 2,
      key: "qualification-score",
      question: "How much did you score in your last qualification?",
      type: "radio",
      name: "qualificationScore",
      placeholder: "Select a option",
      error: {
        required: true,
        message: "Please select a option",
        pattern: "",
      },
      options: [
        {
          id: 1,
          key: "below-50",
          value: "Below 50%",
          label: "Below 50%",
        },
        {
          id: 2,
          key: "50-60",
          value: "50-60%",
          label: "50%-60%",
        },
        {
          id: 3,
          key: "60-70",
          value: "60-70%",
          label: "60%-70%",
        },
        {
          id: 4,
          key: "70-80",
          value: "70-80%",
          label: "70%-80%",
        },
        {
          id: 5,
          key: "above-80",
          value: "Above 80%",
          label: "Above 80%",
        },
      ],
    },
    {
      id: 3,
      key: "language-test",
      question: "Have you taken an Language Proficiency Test?",
      type: "radio",
      name: "languageTest",
      placeholder: "Select a option",
      error: {
        required: true,
        message: "Please select a option",
        pattern: "",
      },
      options: [
        {
          id: 1,
          key: "ielts",
          value: "IELTS",
          label: "IELTS",
        },
        {
          id: 2,
          key: "toefl",
          value: "TOEFL",
          label: "TOEFL",
        },
        {
          id: 3,
          key: "pte",
          value: "PTE",
          label: "PTE",
        },
        {
          id: 4,
          key: "duolingo",
          value: "Duolingo",
          label: "Duolingo",
        },
        {
          id: 5,
          key: "none",
          value: "None",
          label: "None",
        },
      ],
    },
    {
      id: 4,
      key: "language-test-year",
      question: "In which year did you take the test?",
      type: "radio",
      name: "languageTestYear",
      placeholder: "Select a option",
      error: {
        required: true,
        message: "Please select a option",
        pattern: "",
      },
      options: [
        {
          id: 1,
          key: "year1",
          value: new Date().getFullYear(),
          label: new Date().getFullYear(),
        },
        {
          id: 2,
          key: "year2",
          value: new Date().getFullYear() - 1,
          label: new Date().getFullYear() - 1,
        },
        {
          id: 3,
          key: "year3",
          value: "It's been a while",
          label: "It's been a while",
        },
      ],
    },
    {
      id: 5,
      key: "budget-options",
      question: "What is your overall budget?",
      type: "radio",
      name: "budgetOptions",
      placeholder: "Select a option",
      error: {
        required: true,
        message: "Please select a option",
        pattern: "",
      },
      options: [
        {
          id: 1,
          key: "10L-20L",
          value: "10L - 20L",
          label: "10 - 20 Lakh",
        },
        {
          id: 2,
          key: "20L-30L",
          value: "20L - 30L",
          label: "20 - 30 Lakh",
        },
        {
          id: 3,
          key: "30L-40L",
          value: "30L - 40L",
          label: "30 - 40 Lakh",
        },
        {
          id: 4,
          key: "40L-50L",
          value: "40L - 50L",
          label: "40 - 50 Lakh",
        },
        {
          id: 5,
          key: "more-than-50l",
          value: "More than 50L",
          label: "Above 50L",
        },
        {
          id: 6,
          key: "not-decided-yet",
          value: "Not Decided Yet",
          label: "Not Decided",
        },
      ],
    },
    {
      id: 6,
      key: "work-status",
      question: "Are you a student or working professional?",
      type: "radio",
      name: "currentlyWorking",
      placeholder: "Select a option",
      error: {
        required: true,
        message: "Please select a option",
        pattern: "",
      },
      options: [
        {
          id: 1,
          key: "student",
          value: "Student",
          label: "Student",
        },
        {
          id: 2,
          key: "working-professional",
          value: "Working Professional",
          label: "Working Professional",
        },
      ],
    },
    {
      id: 7,
      key: "study-fund",
      question: "How are you planning to fund your studies?",
      type: "radio",
      name: "studyFund",
      placeholder: "Select a option",
      error: {
        required: true,
        message: "Please select a option",
        pattern: "",
      },
      options: [
        {
          id: 1,
          key: "self-funded",
          value: "Self-Funded",
          label: "Self Funded",
        },
        {
          id: 2,
          key: "loan",
          value: "Loan",
          label: "Loan",
        },
        {
          id: 3,
          key: "sponsored-by-parents",
          value: "Sponsored by Parents",
          label: "Sponsored by Parents",
        },
        {
          id: 4,
          key: "scholarship",
          value: "Scholarship",
          label: "Scholarship",
        },
      ],
    },
    {
      id: 7,
      key: "services-looking-for",
      question: "What are you looking for?",
      type: "radio",
      name: "servicesLookingFor",
      placeholder: "Select a option",
      error: {
        required: true,
        message: "Please select a option",
        pattern: "",
      },
      options: [
        {
          id: 1,
          key: "test-preparation",
          value: "Test Preparations",
          label: "Test Preparations",
        },
        {
          id: 2,
          key: "visa-assistance",
          value: "VISA Assistance",
          label: "VISA Assistance",
        },
        {
          id: 3,
          key: "loan-and-scholarship",
          value: "Loan & Scholarships",
          label: "Loan & Scholarships",
        },
        {
          id: 4,
          key: "apply-to-university",
          value: "Apply to University",
          label: "Apply to University",
        },
        {
          id: 5,
          key: "all-of-the-above",
          value: "All of the Above",
          label: "All of the Above",
        },
      ],
    },
  ]);

  const [show, setShow] = useRecoilState(JourneyRemainingQuestionModalAtom);
  const [questionsModal, setQuestionsModal] = useRecoilState(JourneyRemainingQuestionModalStateAtom);
  const [dynamicSchema, setDynamicSchema] = useState(yup.object().shape({}));

  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
    getValues,
    getFieldState,
  } = useForm({
    resolver: yupResolver(dynamicSchema),
  });

  const [step, setStep] = useState(1);
  const [isLoading, setIsLoading] = useState(false);
  const [currentQuestion, setCurrentQuestion] = useState(questions[0]);
  const [userCompareQuery, setUserCompareQuery] = useRecoilState(compareQueryParamsState);

  const onSubmit = async (data) => {
    handleNext();
    const qualification = data.qualification;
    const qualificationScore = data.qualificationScore;
    const languageTest = data.languageTest;
    const languageTestYear = data.languageTestYear;
    const currentlyWorking = data.currentlyWorking
    const studyFund = data.studyFund;
    const budget = data.budgetOptions;
    const servicesLookingFor = data.servicesLookingFor;

    if (step === questions.length) {
      setIsLoading(true);
      try {
        await updateLeadFields(userCompareQuery.uuid, {
          qualification_program: qualification,
          qualification_percentage: qualificationScore,
          language_test: languageTest,
          language_test_date: languageTestYear,
          profession: currentlyWorking,
          fund: studyFund,
          budget_text: budget,
        });
        await updatedLsqOpportunity(currentlyWorking, languageTest, qualification, qualificationScore, studyFund, userCompareQuery.opportunityId);
        await updateUserCompareQueryFields(userCompareQuery.uuid, {
          answered_questions: 1,
        });

        const lsqLeadData = {
          data: [
            {
              Attribute: "mx_Currently_Working",
              Value: currentlyWorking,
            },
            {
              Attribute: "mx_language_proficiency_test",
              Value: languageTest,
            },
            {
              Attribute: "mx_language_test_year",
              Value: languageTestYear,
            },
            {
              Attribute: "mx_Qualification",
              Value: qualification,
            },
            {
              Attribute: "mx_Last_Qualification_Score",
              Value: qualificationScore,
            },
            {
              Attribute: "mx_How_are_you_planning_to_fund_your_studies",
              Value: studyFund,
            },
            {
              Attribute: "mx_Budget_Options",
              Value: budget,
            },
            {
              Attribute: "mx_Student_Motive",
              Value: servicesLookingFor,
            },
            
          ],
        };
        const res = await updatedLsqLeadDetails(userCompareQuery.uuid, lsqLeadData);
        setUserCompareQuery({
          ...userCompareQuery,
          answered_questions: 1,
        });
      } catch (err) {
        console.error(err);
      } finally {
        setIsLoading(false);
        handleClose();
      }
    }
  };

  const [ottaPointLead, setOttaPointLead] = useRecoilState(ottaPointLeadAtom);

  const handleNext = () => {
    if (step < questions.length) {
      setStep(step + 1);
      setCurrentQuestion(questions[step]);
      ottaPointLead.handleNext();
    }
  };

  const handlePrevious = () => {
    if (step > 1) {
      setStep(step - 1);
      setCurrentQuestion(questions[step - 2]);
      ottaPointLead.handlePrev();
    }
  };

  const handleQuestionChange = (e) => {
    const { name, value } = e.target;
    setValue(name, value);

    // if step is not last
    if (step <= questions.length) {
      handleSubmit(onSubmit)();
      ottaPointLead.handleNext();
    }
  };

  const handleClose = () => {
    setShow(false);
  };

  const generateSchema = useCallback(() => {
    const schema = {};
    if (questions[step - 1] === "currentlyWorking") {
      schema[questions[step - 1].name] = yup
        .string()
        .required(questions[step - 1].error.message)
        .typeError(questions[step - 1].error.message);
    } else if (questions[step - 1] === "qualificationScore") {
      schema[questions[step - 1].name] = yup
        .string()
        .required(questions[step - 1].error.message)
        .typeError(questions[step - 1].error.message);
    } else if (questions[step - 1].error.required) {
      schema[questions[step - 1].name] = yup
        .string()
        .required(questions[step - 1].error.message)
        .typeError(questions[step - 1].error.message);
    } else {
      schema[questions[step - 1].name] = yup.string();
    }
    setDynamicSchema(yup.object().shape(schema));
  }, [questions, step]);

  useEffect(() => {
    generateSchema();
  }, [step]);

  return (
    <Modal
      show={show}
      onHide={handleClose}
      className={`extra-questions-modal`}
      centered
      backdrop="static"
      keyboard={false}
      backdropClassName="journeyRemainingQuestionModalBackdrop"
    >
      <Modal.Header className="border-0 p-0">
        <div className="w-100">
          <Lottie
            animationData={flyingPlaneAnimation}
            height={200}
            style={{
              width: "100%",
            }}
          />
        </div>
      </Modal.Header>
      <Modal.Body className="position-relative">
        <form className="d-flex flex-column" onSubmit={handleSubmit(onSubmit)}>
          {questions.map((question, index) => {
            if (index + 1 === step) {
              return question.key === "qualification-score" ? (
                <div key={question.id}>
                  <p className="text-center fs-4 fw-semibold mt-4 mb-5">{question.question}</p>
                  <div className="row gap-4 justify-content-center px-4 px-md-0">
                    {question.options.map((option) => (
                      <label
                        key={option.id}
                        className={`
                         col-lg-4 col-md-6 col-sm-12 d-flex align-items-center justify-content-center text-center position-relative
            bg-white rounded-4  text-black d-flex align-items-center gap-2  ${styles.optionButton}
             ${getValues(question.name) === option.value ? styles.optionButtonSelected : ""}`}
                        htmlFor={option.key}
                      >
                        {(option.value === "70-80%" || option.value === "Above 80%") && (
                          <span
                            className="fs-xs 
                           position-absolute  translate-x-middle badge rounded-pill bg-bright-green
                          "
                            style={{
                              bottom: "-10px",
                            }}
                          >
                            Scholarship Available
                          </span>
                        )}
                        <input
                          id={option.key}
                          type={question.type}
                          name={question.name}
                          value={option.value}
                          onChange={handleQuestionChange}
                          checked={getValues(question.name) === option.value}
                          // {...register(question.name)}
                          className="d-none"
                        />
                        <span>{option.label}</span>
                      </label>
                    ))}
                  </div>
                  <div className="text-danger text-center mt-4">{errors[question.name]?.message}</div>
                </div>
              ) : (
                <div key={question.id}>
                  <p className="text-center fs-4 fw-semibold mt-4 mb-5">{question.question}</p>
                  <div className="row gap-3 justify-content-center">
                    {question.options.map((option) => (
                      <label
                        key={option.id}
                        className={`
                         col-lg-4 col-md-6 col-sm-12 d-flex align-items-center justify-content-center text-center
                        ${styles.optionButton}
             ${getValues(question.name) === option.value ? styles.optionButtonSelected : ""}
            bg-white rounded-4  text-black d-flex align-items-center gap-3`}
                        htmlFor={option.key}
                      >
                        <input
                          id={option.key}
                          type={question.type}
                          name={question.name}
                          value={option.value}
                          onChange={handleQuestionChange}
                          checked={getValues(question.name) === option.value}
                          // {...register(question.name)}
                          className="d-none"
                        />
                        <span>{option.label}</span>
                      </label>
                    ))}
                  </div>
                  <div className="text-danger text-center mt-4">{errors[question.name]?.message}</div>
                </div>
              );
            }
          })}
        </form>
      </Modal.Body>
      <Modal.Footer className="border-0">
        <div className="w-100 d-flex align-items-center justify-content-center gap-3">
          <Button variant="secondary" type="button" disabled={isLoading || step === 1} className={`${styles.btnOutline}`} onClick={handlePrevious}>
            Back
          </Button>
          <Button
            variant="primary"
            type="submit"
            className={`${styles.btn}`}
            onClick={() => {
              handleSubmit(onSubmit)();
            }}
          >
            {isLoading ? (
              <span className="d-flex gap-2 align-items-center">
                <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                <span>Compiling...</span>
              </span>
            ) : step === questions.length ? (
              <span>{questionsModal.submitButtonText}</span>
            ) : (
              "Next"
            )}
          </Button>
        </div>
      </Modal.Footer>
      <div className="px-2 my-3">
        <OttaPointLeadSwiper showDivider={false} />
      </div>
    </Modal>
  );
}
