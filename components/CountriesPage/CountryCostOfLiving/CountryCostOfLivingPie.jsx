import React from "react";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import { Doughnut } from "react-chartjs-2";
import ChartDataLabels from "chartjs-plugin-datalabels";
import { useState } from "react";

ChartJS.register(ArcElement, Tooltip, Legend, ChartDataLabels);

export default function CountryCostOfLivingPie({ costOfLivingData, currency }) {
  const [meter, setMeter] = useState();


  const costOfLivingTitles = costOfLivingData.map((budget) => budget.title);
  const costOfLivingBudgets = costOfLivingData.map((budget) => budget.budget);

  return (
    <Doughnut
      data={{
        labels: [...costOfLivingTitles],
        datasets: [
          {
            label: "# of Votes",
            data: [...costOfLivingBudgets],
            backgroundColor: ["#1043E9", "#3F6BFC", "#6D90FF", "#AEC1FF"],
            hoverBackgroundColor: ["#1043E9", "#3F6BFC", "#6D90FF", "#AEC1FF"],
            borderColor: ["#1043E9", "#3F6BFC", "#6D90FF", "#AEC1FF"],
            hoverBorderColor: ["#f1f1f1", "#f1f1f1", "#f1f1f1", "#f1f1f1"],
            borderWidth: 0,
            hoverBorderWidth: 10,
            borderJoinStyle: "round",
            borderAlign: "center",
            spacing: 5,
            borderRadius: 10,
          },
        ],
      }}
      options={{
        layout: {
          padding: 20,
        },
        // cutout: 90,
        plugins: {
          datalabels: {
            formatter: (value, e) => {
              return `${costOfLivingData[e.dataIndex].title} \n ${currency}${value}`;
            },
            font: (context) => {
              var width = context.chart.width;
              return {
                size: width > 500 ? 20 : 12,
              };
            },
            textAlign: "center",
            color: "#FFFFFF",
          },
          legend: {
            position: "top",
            display: false,
            labels: {
              font: {
                weight: "bold",
              },
            },
          },
          tooltip: {
            callbacks: {
              label: function (tooltipItem, data) {
                return `${tooltipItem.label} - ${currency}${tooltipItem.parsed}`;
              },
            },
          },
        },
      }}
    />
  );
}
