import React, { useRef } from "react";
import Image from "next/image";
import styles from "./CountryCostOfLiving.module.scss";
import GaugeChart from "react-gauge-chart";
import { useState } from "react";
import MonthlyExpensesSlider from "./MonthlyExpensesSlider";
import BigTextHeading from "../../global/BigTextHeading/BigTextHeading";
import uuid from "react-uuid";
import { useEffect } from "react";
import { ProgressBar } from "primereact/progressbar";
import MeterImage from "../../../public/country/countrypage/meter.png";
import NeedleImage from "../../../public/country/countrypage/needle.svg";
import CountryCostOfLivingPie from "./CountryCostOfLivingPie";
import SectionHeading from "../../global/SectionHeading/SectionHeading";
import Container from "react-bootstrap/Container";
import { CurrencyInternationalization, useOnScreen } from "../../../context/CustomHooks";
import { useIntersectionObserver } from "@uidotdev/usehooks";


export default function CountryCostOfLiving({ costOfLivingData, countryData }) {
  const [meterPercent, setMeterPercent] = useState(0.3);
  const [categories, setCategories] = useState([
    { name: "Transport", index: 0, value: 0, background: "#2B8AFA", backgroundLight: "#2B8AFA4D" },
    { name: "Rent", index: 1, value: 0, background: "#2DDD71", backgroundLight: "#2DDD714D" },
    { name: "Food", index: 2, value: 0, background: "#7C84E8", backgroundLight: "#7C84E84D" },
    { name: "Miscellaneous", index: 3, value: 0, background: "#FF7D47", backgroundLight: "#FF7D474D" },
  ]);
  const [ref, entry] = useIntersectionObserver({
    threshold: 0,
    root: null,
    rootMargin: "0px",
  });

  useEffect(() => {
    const budget_avg_low = costOfLivingData.reduce((acc, curr) => acc + curr.budget_low, 0) / costOfLivingData.length;
    const budget_avg_high = costOfLivingData.reduce((acc, curr) => acc + curr.budget_high, 0) / costOfLivingData.length;
    const avg_budget = budget_avg_high + budget_avg_low / costOfLivingData.length;
    const meterValue = (avg_budget - 270) / (80 - 270);

    setMeterPercent(meterValue);
  }, [costOfLivingData]);

  const calculateBudgetPercentOfBudgetHigh = (budgetLow, budgetHigh) => {
    return (budgetLow * 100) / budgetHigh;
  };

  const valueTemplate = (value) => {
    return (
      <React.Fragment>
        {value}/<b>100</b>
      </React.Fragment>
    );
  };


  useEffect(() => {
    if (entry?.isIntersecting) {
      setCategories([
        {
          name: "Transport",
          index: 0,
          value: calculateBudgetPercentOfBudgetHigh(costOfLivingData[0].budget_low, costOfLivingData[0].budget_high),
          background: "#2B8AFA",
          backgroundLight: "#2B8AFA4D",
        },
        {
          name: "Rent",
          index: 1,
          value: calculateBudgetPercentOfBudgetHigh(costOfLivingData[1].budget_low, costOfLivingData[1].budget_high),
          background: "#2DDD71",
          backgroundLight: "#2DDD714D",
        },
        {
          name: "Food",
          index: 2,
          value: calculateBudgetPercentOfBudgetHigh(costOfLivingData[2].budget_low, costOfLivingData[2].budget_high),
          background: "#7C84E8",
          backgroundLight: "#7C84E84D",
        },
        {
          name: "Miscellaneous",
          index: 3,
          value: calculateBudgetPercentOfBudgetHigh(costOfLivingData[3].budget_low, costOfLivingData[3].budget_high),
          background: "#FF7D47",
          backgroundLight: "#FF7D474D",
        },
      ]);
    }

  }, [costOfLivingData, entry?.isIntersecting]);

  return (
    <Container className={`${styles.container}`} id="costofliving">
      {/* <BigTextHeading text="Post Admission " highlight="Experience" className={styles.sectionHeading} /> */}
      <SectionHeading
        heading={`<span>Budget <span class="text-cvblue">Forecast</span></span>`}
        subHeading={`Here is a breakdown of the average monthly living expenses in ${countryData.name} for international students:`}
      />

      <div className={`${styles.costOfLivingContainer}`} ref={ref}>
        <div className={`${styles.left}`}>
          <p>Monthly Living Expenses</p>
          <div className={styles.meterContainer}>
            <span className={styles.meter}>
              <Image alt="Meter Image" src={MeterImage} />
            </span>
            <span
              className={styles.needle}
              style={{
                transform: ` translateX(-50%) rotate(${meterPercent}deg)`,
              }}
            >
              <Image alt="Meter Image Needle" src={NeedleImage} />
            </span>
            <span className={styles.meterBounds}>
              <p>Low</p>
              <p>High</p>
            </span>
          </div>
          <p>
            Approx. {countryData.currency_symbol}
            {costOfLivingData ? costOfLivingData.reduce((acc, curr) => acc + curr.budget, 0) : "Calculating..."}
          </p>
        </div>
        <div className={`${styles.right}`}>
          <div className="w-100 h-100 d-flex flex-column gap-3 align-items-center justify-content-between p-2">
            {categories.map((category) => (
              <div className="d-flex flex-column gap-1 w-100" key={category.index}>
                <span className="mb-1">{category.name}</span>
                <div
                  className={`${styles.progressBar} rounded-5`}
                  style={{
                    background: category.backgroundLight,
                  }}
                >
                  <div
                    className={`${styles.progress} h-100 d-flex align-items-center ps-2 rounded-5`}
                    style={{
                      width: `${category.value}%`,
                      background: category.background,
                    }}
                  >
                    <span className="text-white text-nowrap">
                      {CurrencyInternationalization(costOfLivingData[category.index].budget_low, countryData.currency)} -{" "}
                      {CurrencyInternationalization(costOfLivingData[category.index].budget_high, countryData.currency)}
                    </span>
                  </div>
                </div>
              </div>
            ))}
            <small className="text-muted fs-xs">*Please note that the prices provided are approximate and may vary!</small>
          </div>
          {/* {costOfLivingData.length > 0 ? (
              <CountryCostOfLivingPie currency={countryData.currency_symbol} costOfLivingData={costOfLivingData} />
            ) : null} */}
        </div>
      </div>
    </Container>
  );
}
