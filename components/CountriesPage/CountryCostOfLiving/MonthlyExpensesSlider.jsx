import React from "react";
import styles from "./CountryCostOfLiving.module.scss";

// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";

// import required modules
import { Pagination } from "swiper";
import Image from "next/image";
import uuid from "react-uuid";

export default function MonthlyExpensesSlider({ countryData, options, addMeterItemStyle, removeMeterItemStyle }) {
  return (
    <Swiper slidesPerView={1.5} spaceBetween={10} modules={[Pagination]} className="countryMonthlyExpenseSlider">
      {options.map((item, i) => (
        <SwiperSlide key={uuid()}>
          <div
            className={styles.gridItem}
            onMouseOver={() => {
              addMeterItemStyle(i);
            }}
            onMouseOut={() => removeMeterItemStyle(i)}
            style={{
              backgroundColor: i == 0 ? "#2B8AFAE5" : i == 1 ? "#B4E0C5" : i == 2 ? "#7C84E8E5" : "#FF7D47E5",
              boxShadow:
                i == 0
                  ? `box-shadow: 3px 8px 15px rgba(64, 150, 251, 0.2)`
                  : i == 1
                  ? `box-shadow: 3px 8px 15px rgba(180, 224, 197, 0.2)`
                  : i == 2
                  ? `box-shadow: 3px 8px 15px #D9DBF8`
                  : `box-shadow: 3px 8px 15px rgba(255, 168, 132, 0.2)`,
            }}
          >
            <Image src={item.icon} alt={item.name} width={30} height={30} objectFit="contain" />
            <h4>{item.name}</h4>
            <p>
              {countryData.currency_symbol}
              {item.budget_low} - {countryData.currency_symbol}
              {item.budget_high}
            </p>
          </div>
        </SwiperSlide>
      ))}
    </Swiper>
  );
}
