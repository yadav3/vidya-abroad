import React, { useState } from "react";
import styles from "./CountryVisaOption.module.scss";

// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
// import required modules
import { Navigation, Pagination } from "swiper";
import uuid from "react-uuid";
import BigTextHeading from "../../global/BigTextHeading/BigTextHeading";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import SectionHeading from "../../global/SectionHeading/SectionHeading";
import { BsFillArrowDownCircleFill, BsFillArrowUpCircleFill } from "react-icons/bs";
import { CurrencyInternationalization } from "../../../context/CustomHooks";

export default function CountryVisaOption({ visaOptions, countryData }) {
  const colors = ["#3C69E6", "#0D266A", "#7227EC"];

  return (
    <Container className="pb-5" id="visa" fluid>
      {/* <BigTextHeading text="" highlight="VISA " end="Options" className={styles.sectionHeading} /> */}
      <SectionHeading
        heading={`<span>VISA <span class="text-cvblue">Options</span></span>`}
        subHeading={`Since you will be traveling to ${countryData.name} on a Student VISA, checkout your VISA Options before applying for one`}
      />

      <Row>
        <Col>
          <div className="d-flex gap-3 align-items-center justify-content-center flex-wrap">
            {visaOptions.map((visa, index) => (
              <VisaCard key={uuid()} visa={visa} countryData={countryData} background={colors[index % colors.length]} />
            ))}
          </div>
        </Col>
      </Row>

      {/* <div className={`${styles.CountryVisaOption}`}>
        <Swiper
          slidesPerView={1}
          spaceBetween={30}
          centeredSlides={true}
          pagination={{
            clickable: true,
          }}
          modules={[Pagination, Navigation]}
          breakpoints={{
            600: {
              slidesPerView: 2,
            },
            1000: {
              slidesPerView: 3,
            },
            1300: {
              slidesPerView: 4,
            },
          }}
          className={`countryVisaOptionSlider ${styles.countryVisaOptionSlider}`}
        >
          {visaOptions.map((visa, index) => (
            <SwiperSlide key={uuid()}>
              <VisaCard visa={visa} countryData={countryData} background={colors[index % colors.length]} />
            </SwiperSlide>
          ))}
        </Swiper>
      </div> */}
    </Container>
  );
}

function VisaCard({ visa, countryData, background }) {
  const [showLength, setShowLength] = useState(false);

  const toggleReadMore = () => {
    setShowLength(!showLength);
  };

  return (
    <div
      className={`${styles.visaCard} text-white p-3 d-flex flex-column gap-1 justify-content-between rounded-4`}
      style={{
        background: background,
      }}
    >
      <div>
        <h6 className={`${styles.title} m-0 fs-5`}>{visa.title}</h6>
        <small className="text-white">
          Type - <span className="text-capitalize">{visa.type}</span>
        </small>
        <p
          className={`${styles.description} fs-md mt-2`}
          style={{
            maxHeight: "90px",
            overflowY: showLength ? "scroll" : "hidden",
          }}
        >
          {showLength ? visa.description : visa.description.slice(0, 100).concat("...")}
        </p>
      </div>
      <div className="d-flex align-items-center justify-content-between">
        <p className="m-0 fw-bold">{CurrencyInternationalization(visa.cost, countryData.currency)}</p>
        {showLength ? (
          <div className="d-flex align-items-center gap-2" role="button" onClick={() => toggleReadMore()}>
            <span className="fs-sm">Show Less</span>
            <BsFillArrowUpCircleFill size={25} />
          </div>
        ) : (
          <div className="d-flex align-items-center gap-2" role="button" onClick={() => toggleReadMore()}>
            <span className="fs-sm">Read More</span>
            <BsFillArrowDownCircleFill size={25} />
          </div>
        )}
      </div>
    </div>
  );
}

// const VisaCard = ({ visa, countryData }) => (
//   <div className={`${styles.visaCard} country-visa-card `}>
//     <div className="d-flex flex-row align-items-center justify-content-between ps-3">
//       <p className={`${styles.title}`}>{visa.title}</p>
//       <p className={`${styles.cost} `}>
//         {countryData.currency_symbol}
//         {visa.cost}
//       </p>
//     </div>
// <small className="ps-3">
//   Type - <span className="text-capitalize">{visa.type}</span>
// </small>
//     <p className={`${styles.description} country-visa-card-description `}>{visa.description}</p>
//   </div>
// );
