import styles from "./CountryExpertBanner.module.scss";
import React from "react";
import { AiOutlineArrowRight } from "react-icons/ai";
import Link from "next/link";
import { useStateContext } from "../../../context/StateContext";
import { useRouter } from "next/router";

export default function CountryExpertBanner({ countryData }) {
  const { setOttaData, ottaData } = useStateContext();
  const router = useRouter();

  return (
    <div className={`${styles.countryExpertBanner}`}>
      <div>
        <p>Make Your Dream To Study in The {countryData.name}</p>
        <h5>A Reality With Our Experts</h5>
      </div>

      <Link href="/suggest-me-a-university">
        <a>
          <button
            onClick={() => {
              setOttaData({ ...ottaData, country: countryData.name });
              router.replace("/suggest-me-a-university/?source=country-page");
            }}
          >
            Book Your Free 30 Minute Session Now <AiOutlineArrowRight />
          </button>
        </a>
      </Link>
    </div>
  );
}
