import Image from "next/image";
import React from "react";
import styles from "./CountryHeroBanner.module.scss";
import { getAWSImagePath, getAWSImagePathBeta } from "../../../context/services";

export default function CountryHeroBanner({ countryData }) {
  return (
    <div className={`position-relative w-100 `}>
      <Image
        src={getAWSImagePath(countryData.hero_banner_image)}
        alt={`Study in ${countryData.name}`}
        width={1440}
        height={626}
        quality={100}
        layout="responsive"
        className="w-100 h-100"
        priority={true}
      />
    </div>
  );
}
