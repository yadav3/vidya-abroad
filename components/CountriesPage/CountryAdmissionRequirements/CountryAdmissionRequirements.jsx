import Image from "next/image";
import React from "react";
import styles from "./CountryAdmissionRequirements.module.scss";
import { ListGroup } from "react-bootstrap";
import BigTextHeading from "../../global/BigTextHeading/BigTextHeading";
import uuid from "react-uuid";
import SectionHeading from "../../global/SectionHeading/SectionHeading";
import { AiFillCheckCircle } from "react-icons/ai";

export default function CountryAdmissionRequirements({ countryData, admissionRequirements }) {
  return (
    <div className={`${styles.container}`} id="admissions">
      {/* <BigTextHeading text="Admission " highlight="Requirements" className={styles.sectionHeading} />
      <p className={`${styles.sectionPara} text-center`}>
        Here are the major requirements to study in the {countryData.name}, which you need to ensure while applying to a {countryData.name}{" "}
        university:
      </p> */}

      <SectionHeading
        heading={`<span>Admission <span class="text-cvblue">Checklist</span></span>`}
        subHeading={`Your Essential ${countryData.name}  University Checklist!`}
        className="mb-4 mt-5 text-center"
      />

      <div className={`${styles.content}`}>
        <div
          className={`${styles.gridContainer}`}
          style={{
            maxHeight: "60vh",
            overflowY: "auto",
          }}
        >
          {admissionRequirements.map((req) => (
            <div className={`${styles.card}`} key={uuid()}>
              
              <span
                className="text-cvblue"
                styles={{
                  minWidth: "50px",
                  minHeight: "50px",
                }}
              >
                <AiFillCheckCircle />
              </span>
              <span className={`${styles.cardContent} `}>{req.content}</span>
            </div>
          ))}
        </div>
        <div className={`${styles.imageContainer}`}>
          <Image
            alt="Admission Requirement Illustration"
            src="/country/countrypage/admission_req_illustration.png"
            width={396}
            height={395}
            objectFit="contain"
          />
        </div>
      </div>
    </div>
  );
}
