import Image from "next/image";
import React from "react";
import styles from "./CountryTopUniversities.module.scss";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import { Navigation, Pagination, Autoplay } from "swiper";
import BigTextHeading from "../../global/BigTextHeading/BigTextHeading";
import uuid from "react-uuid";
import { useRouter } from "next/router";
import { getAWSImagePathBeta } from "../../../context/services";
import SectionHeading from "../../global/SectionHeading/SectionHeading";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

import { GrScorecard, GrStar } from "react-icons/gr";

export default function CountryTopUniversities({ universities, countryData }) {
  return (
    <div className={`${styles.container}`} id="topuniversities">
      <SectionHeading
        className="text-center my-0 mt-md-5"
        heading={`<span>Top <span class="text-cvblue">Universities</span></span>`}
        subHeading={"Discover the Best Universities in the Country!"}
      />
      <div className={`${styles.universityContainer}`}>
        <Swiper
          slidesPerView={1}
          spaceBetween={10}
          loop={true}
          pagination={{
            clickable: true,
          }}
          autoplay={{
            delay: 2500,
            disableOnInteraction: false,
            pauseOnMouseEnter: true,
          }}
          modules={[Pagination, Navigation, Autoplay]}
          breakpoints={{
            600: {
              slidesPerView: 2,
              spaceBetween: 10,
            },
            700: {
              slidesPerView: 3,
              spaceBetween: 10,
            },
            1300: {
              slidesPerView: 4,
              spaceBetween: 10,
            },
            1500: {
              slidesPerView: 4,
              spaceBetween: 10,
            },
          }}
        >
          {universities.map((university) => (
            <SwiperSlide key={uuid()}>
              <UniversityCard university={university} />
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </div>
  );
}

function UniversityCard({ university }) {
  const router = useRouter();

  function getValidRanking(data) {
    const validRanking = data.find((item) => {
      const ranking = parseInt(item.ranking);
      return !isNaN(ranking);
    });

    if (validRanking) {
      return {
        name: validRanking.name,
        ranking: parseInt(validRanking.ranking),
      };
    }

    return null;
  }

  return (
    <div
      className="d-flex flex-column gap-3 p-3 rounded-4 w-100 mb-5 position-relative cursor-pointer"
      style={{
        maxWidth: "500px",
      }}
      onClick={() => router.push(`/universities/${university.slug}`)}
    >
      <div className="rounded-4 overflow-hidden position-relative">
        {getValidRanking(university.universities_rankings) !== null ? (
          <div
            className={`${styles.badge} position-absolute top-0 start-0 m-2 rounded-2 bg-orange text-white fs-sm px-2 py-1 d-flex align-items-center gap-1`}
            style={{
              zIndex: 100,
            }}
          >
            {getValidRanking(university.universities_rankings).name}: {getValidRanking(university.universities_rankings).ranking}
          </div>
        ) : null}

        <Image
          alt={university.name}
          src={getAWSImagePathBeta(university?.universities_images[0]?.image_path)}
          objectFit="cover"
          width={500}
          height={400}
          layout="responsive"
        />
        <div
          className="position-absolute bottom-0 left-0 w-100 bg-cvblue text-white mb-2 d-flex align-items-center justify-content-center gap-1"
          style={{
            paddingBlock: "2px",
          }}
        >
          <span
            className="bg-white rounded-circle circle-pulse-ring"
            style={{
              minWidth: "7px",
              minHeight: "7px",
            }}
          ></span>
          <p className="fs-xs m-0 text-center">Compare {university._count.UniversitySpecializations}+ Courses</p>
        </div>
      </div>
      <div className="">
        <div className="d-flex align-items-center justify-content-between gap-3 fs-md">
          <small className="text-muted">{university.location}</small>
          <span className="d-flex align-items-center">
            <span>({university.ielts_required} IELTS)</span>
          </span>
        </div>
        <div>
          <h3 className="fs-lg m-0">{university.name}</h3>
        </div>
      </div>
    </div>
  );
}
