import React from "react";
import styles from "./CountryFormContainer.module.scss";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import LeadForm from "../../global/LeadForm/LeadForm";
import Image from "next/image";
import CountryLeadForm from "./CountryLeadForm";
import { MdArrowForward } from "react-icons/md";

export default function CountryFormContainer({ countrySlug, countryData, majors }) {
  return (
    <Container fluid className="p-0 my-4">
      <div className={`${styles.formRow}`}>
        <div
          className={` ${styles.illustrationColumn} d-none align-items-center justify-content-center d-md-block  position-relative overflow-hidden
           
          `}
          style={{
            backgroundColor: "#EEF1FF",
          }}
        >
          <span
            className={`d-none d-md-block position-absolute `}
            style={{
              right: "-2px",
              bottom: "-35px",
            }}
          >
            <Image src={`/bg/country-form-illustration.svg`} alt={"Contact Form Illustration"} width={320} height={320} />
          </span>
          <h4
            className=" text-center text-md-start fw-normal fs-3 m-4 position-relative mt-md-5"
            style={{
              zIndex: 1,
            }}
          >
            <span className="d-block fw-semibold">
              Expert planning
              <MdArrowForward />
            </span>{" "}
            makes all the <span className="d-block fw-semibold">difference!</span>
          </h4>
        </div>
        <div className="d-flex align-items-center justify-content-center w-100 h-100 py-0 pt-md-5 p-4 py-5 px-5">
          {/* <LeadForm ctaText="Plan Your Journey with Abroad Experts" selectedCountry={countrySlug} /> */}
          <CountryLeadForm majors={majors} countryData={countryData} countrySlug={countrySlug} />
        </div>
      </div>
    </Container>
  );
}
