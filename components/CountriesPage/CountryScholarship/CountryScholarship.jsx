import React from "react";
import Image from "next/image";
import styles from "./CountryScholarship.module.scss";

const scholarships = [
  "Fulbright Foreign Student Program",
  "#YouAreWelcomeHere Scholarship",
  "Hubert Humphrey Fellowship Program",
  "Rotary International Ambassadorial Scholarships",
  "Civil Society Leadership Awards",
  "List of MBA Scholarships in the USA",
  "Surfshark Privacy and Security Scholarship",
  "#YouAreWelcomeHere Scholarship",
  "Tata Scholarship for Cornell University",
  "Stanford Reliance Dhirubhai Fellowships for Indian Students",
  "AAUW International Fellowships",
  "List of Scholarships for MS in USA",
];

export default function CountryScholarship() {
  return (
    <div className={`${styles.container}`} id="scholarship">
      <h5>Scholarships</h5>
      <p>
        There are various scholarships offered by the USA government,
        universities and private organizations to support the education of
        international students. Here is a list of the most popular scholarships
        to study in USA:
      </p>
      <div className={`${styles.scholarshipContainer}`}>
        <div className={`${styles.left}`}>
          {scholarships.map((item, i) => {
            return (
              <div
                key={`scholarshipitem-${i}`}
                className={`${styles.listItem}`}
              >
                <p>{item}</p>
              </div>
            );
          })}
        </div>
        <div className={`${styles.right}`}>
          <Image
            src={"/country/countrypage/scholarship_girl.png"}
            alt="Scholarship Girl Illustration"
            width={295.73}
            height={379}
            objectFit="contain"
          />
        </div>
      </div>
    </div>
  );
}
