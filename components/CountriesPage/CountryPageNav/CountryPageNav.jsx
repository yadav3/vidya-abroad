import React from "react";
import { useState } from "react";
import styles from "./CountryPageNav.module.scss";

const navData = [
  {
    name: "Quick Facts",
    id: "quick-facts",
  },
  {
    name: "Top Universities",
    id: "topuniversities",
  },
  {
    name: "Admissions",
    id: "admissions",
  },
  {
    name: "Visa",
    id: "visa",
  },
  {
    name: "Cost of Living",
    id: "costofliving",
  },
  {
    name: "Work Opportunities",
    id: "workopportunities",
  },
  {
    name: "FAQ",
    id: "faq",
  },
];

export default function CountryPageNav() {
  const [hash, setHash] = useState();

  return (
    <div className={`${styles.countryPageNav} countryPageNav d-none d-md-flex`}>
      {navData.map((item) => (
        <a
          key={item.id}
          href={`#${item.id}`}
          onClick={() => setHash(item.id)}
          style={{
            color: hash === item.id ? "white" : "black",
            backgroundColor: hash === item.id ? "#0056D2" : "#ECECEC",
          }}
        >
          {item.name}
        </a>
      ))}
    </div>
  );
}
