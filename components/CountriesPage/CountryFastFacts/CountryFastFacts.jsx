import Image from "next/image";
import React from "react";
import styles from "./CountryFastFacts.module.scss";

// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";

// import required modules
import { Pagination } from "swiper";
import uuid from "react-uuid";
import CountryFastFactsSlider from "./CountryFastFactsSlider";
import BigTextHeading from "../../global/BigTextHeading/BigTextHeading";
import { useEffect } from "react";
import { getAWSImagePath, getAWSImagePathBeta } from "../../../context/services";
import SectionHeading from "../../global/SectionHeading/SectionHeading";

export default function CountryFastFacts({ funFactData, countryData }) {
  return (
    <div className="w-100 mt-5" id="quick-facts">
      {/* <h1 className="text-center fs-md">Study Abroad in USA: Uncomplicate your study abroad journey!</h1> */}
      <SectionHeading
        heading={`<span>Discover <span class="text-cvblue">${countryData.name}</span> in <span class="text-cvblue">2 Minutes</span></span>`}
        subHeading={`<span class="fs-lg fw-normal">Study Abroad in ${countryData.name}: Uncomplicate your study abroad journey!</span>`}
      />
      <div className={`${styles.factContainer}`}>
        {funFactData.map((fact) => (
          <div className={`${styles.factCard}`} key={uuid()}>
            <Image src={getAWSImagePathBeta(fact.icon)} alt={fact.title} width={45} height={45} objectFit="contain" />
            <div className={`${styles.contentContainer}`}>
              <span className={`${styles.title}`}>{fact.title}</span>
              <span className={`${styles.content}`}>{fact.value}</span>
            </div>
          </div>
        ))}
      </div>
      <div className={`${styles.factSlider}`}>
        <CountryFastFactsSlider facts={funFactData} />
      </div>
    </div>
  );
}
