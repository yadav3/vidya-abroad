import Image from "next/image";
import React from "react";
import uuid from "react-uuid";
import styles from "./CountryFastFacts.module.scss";

import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination, Navigation } from "swiper";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

export default function CountryFastFactsSlider({ facts }) {
  return (
    <Swiper
      slidesPerView={1}
      spaceBetween={30}
      slidesPerGroup={2}
      centeredSlides={true}
      pagination={true}
      modules={[Pagination, Navigation]}
      breakpoints={{
        500: {
          slidesPerView: 2,
        },
      }}
      className="countryFunFactsSlider"
    >
      {facts
        .map((_, i) => facts.slice(i, i + 2))
        .map((facts) => (
          <SwiperSlide key={uuid()}>
            <div className="d-flex flex-column gap-3 pb-5">
              {facts.map((fact) => (
                <div className={`${styles.factCard}`} key={uuid()}>
                  <Image src={fact.icon} objectFit="contain" alt={fact.title} width={60} height={60} />
                  <div className={`${styles.contentContainer}`}>
                    <span className={`${styles.title}`}>{fact.title}</span>
                    <span className={`${styles.content}`}>{fact.value}</span>
                  </div>
                </div>
              ))}
            </div>
          </SwiperSlide>
        ))}
    </Swiper>
  );
}
