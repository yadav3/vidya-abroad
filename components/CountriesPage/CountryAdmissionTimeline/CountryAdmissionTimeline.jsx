import axios from "axios";
import Image from "next/image";
import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import Container from "react-bootstrap/Container";
import Dropdown from "react-bootstrap/Dropdown";
import DropdownButton from "react-bootstrap/DropdownButton";
import uuid from "react-uuid";
import { BACKEND_API_URL, getAWSImagePathBeta } from "../../../context/services";
import BigTextHeading from "../../global/BigTextHeading/BigTextHeading";
import styles from "./CountryAdmissionTimeline.module.scss";
import PopularProgramSlider from "./PopularProgramSlider";
import PopularUniversitySlider from "./PopularUniversitySlider";
import SectionHeading from "../../global/SectionHeading/SectionHeading";
import Link from "next/link";

const tabs = {
  1: [
    {
      question: "How to apply?",
      content: "UG Applications are submitted directly through the websites of each university.",
      icon: "/icons/applyicon.png",
    },
    {
      question: "Cost Estimate",
      content: "$24,000- $32,000 (INR 17,84,745 -23,79,660) per year",
      icon: "/icons/costicon.png",
    },
  ],
  2: [
    {
      question: "How to apply?",
      content: "PG applications are directly sent to the universities via online or offline modes.",
      icon: "/icons/applyicon.png",
    },
    {
      question: "Cost Estimate",
      content: "$30,000 - $35,000 (INR 22,30,932- 26,02,754) per year",
      icon: "/icons/costicon.png",
    },
  ],
  3: [
    {
      question: "How to apply?",
      content: "MBA applications are submitted directly to the universities.",
      icon: "/icons/applyicon.png",
    },
    {
      question: "Cost Estimate",
      content: "$60,000- $48,000 (INR 44,61,864 - 35,69,491) per year",
      icon: "/icons/costicon.png",
    },
  ],
};

export default function CountryAdmissionTimeline({ slug, countryData }) {
  const [selectedProgram, setSelectedProgram] = useState(1);
  const [programs, setPrograms] = useState({
    1: [],
    2: [],
  });

  useEffect(() => {
    setPrograms(
      countryData.country_popular_program.filter((program) => program.category_id === selectedProgram).map((program) => program.PopularPrograms)
    );
  }, [selectedProgram, slug]);

  return (
    <Container className={`${styles.container}`}>
      {/* <BigTextHeading text="Admission " highlight="Timeline" className={styles.sectionHeading} /> */}
      <SectionHeading
        heading={`<span>Things to know in <span class="text-cvblue">${countryData.name}</span></span>`}
        subHeading={`Get a closer look at the essential information about university admission.`}
        className="my-3 mt-0 text-center"
      />

      <div className={`${styles.timelineContainer}`}>
        <div className={`${styles.top}`}>
          <div className={`${styles.topLeft}`}>
            {["12th / UG", "masters"].map((value, i) => {
              return (
                <button key={uuid()} onClick={() => setSelectedProgram(i + 1)} className={`${selectedProgram === i + 1 ? styles.activeBtn : ""}`}>
                  {value.toUpperCase()}
                </button>
              );
            })}
          </div>
          <div className={`${styles.topRight}`}>
            {tabs[selectedProgram].map((tab) => (
              <TabCard key={uuid()} question={tab.question} content={tab.content} icon={tab?.icon} />
            ))}
          </div>
        </div>

        <div className={`${styles.bottom}`}>
          <div className={`${styles.left}`}>
            <p className="text-sm-center">Popular 6 Programs</p>

            <div className={`${styles.bottomLeft}`}>
              {programs.length > 0
                ? programs.slice(0, 6).map((item) => (
                    <div key={uuid()} className={`${styles.gridItem} py-3`}>
                      <span className={`${styles.icon}`}>
                        <Image src={getAWSImagePathBeta(item.icon)} alt={item.name} width={60} height={60} />
                      </span>
                      <p className="fw-bold m-0 ">{item.name}</p>
                    </div>
                  ))
                : "Loading"}
            </div>
          </div>

          <div className={`${styles.right}`}>
            <p className="text-sm-center">Popular 6 Universities</p>
            <div className={`${styles.bottomRight}`}>
              {countryData.Universities?.length > 0
                ? countryData.Universities.slice(0, 6).map((item, i) => (
                    <Link key={uuid()} href={`/universities/${item.slug}`}>
                      <a>
                        <span className={`${styles.gridItem}`}>
                          <Image
                            alt="Popular Universities "
                            src={getAWSImagePathBeta(item.logo_full)}
                            width={200}
                            height={100}
                            objectFit="contain"
                            className={`${styles.logo}`}
                          />
                        </span>
                      </a>
                    </Link>
                  ))
                : "Loading"}
            </div>
          </div>
        </div>
      </div>
      <div className="d-block d-sm-none">
        <PopularProgramSlider options={programs} />
        <PopularUniversitySlider universities={countryData.Universities} />
      </div>
    </Container>
  );
}

function TabCard({ question, content, icon }) {
  return (
    <div className="d-flex flex-column ">
      <p>
        <strong>{question}</strong>
      </p>
      <div className={`${styles.tab} `}>
        <Image src={icon} alt="How to apply Icon" width={50} height={50} />
        <p>{content}</p>
      </div>
    </div>
  );
}
