import React from "react";
import styles from "./CountryAdmissionTimeline.module.scss";
import Marquee from "react-fast-marquee";
import Image from "next/image";
import uuid from "react-uuid";
import { getAWSImagePathBeta } from "../../../context/services";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
import { FreeMode, Pagination } from "swiper";

// Import Swiper styles
import "swiper/css";
import "swiper/css/free-mode";
import "swiper/css/pagination";
import { Tooltip } from "react-tippy";
import Link from "next/link";

export default function PopularUniversitySlider({ universities }) {
  const handleGotoUniversityPage = () => {
    window.location.href = "/universities";
  };

  return (
    <div>
      <div className="d-flex align-items-center justify-content-center gap-2  mb-4">
        <h5 className="m-0 text-center">Popular Universities</h5>
        <Link href={"/universities"}>
          <a className="fs-sm m-0 bg-cvblue text-white p-1 rounded-3 cursor-pointer">Show All</a>
        </Link>
      </div>
      <Swiper
        slidesPerView={2}
        spaceBetween={0}
        freeMode={true}
        pagination={{
          clickable: true,
        }}
        modules={[FreeMode, Pagination]}
        className="popularUniversitySliderCountryPage"
      >
        {universities &&
          universities.length > 0 &&
          universities.slice(0, 6).map((item, i) => (
            <SwiperSlide key={uuid()}>
              <Link href={`/universities/${item.slug}`}>
                <a>
                  <div
                    className={`bg-grey mb-5 text-center`}
                    style={{
                      width: "150px",
                      height: "100px",
                    }}
                  >
                    <div key={uuid()} className="p-2">
                      <Image
                        alt="Popular Universities "
                        src={getAWSImagePathBeta(item.logo_full)}
                        width={130}
                        height={50}
                        objectFit="contain"
                        className={`${styles.gridItem}`}
                      />
                    </div>
                    <Tooltip title={item.name} position="bottom" trigger="click">
                      <div className="bg-cvblue w-100 m-0 d-flex align-items-center justify-content-center p-1 ">
                        <small className="m-0 text-center fs-xs text-white text-truncate">{item.name}</small>
                      </div>
                    </Tooltip>
                  </div>
                </a>
              </Link>
            </SwiperSlide>
          ))}
      </Swiper>
    </div>
  );
}
