import React from "react";
import DynamicAccordion from "../../global/DynamicAccordion/DynamicAccordion";

export default function CountryFaq({ faqs }) {
  return (
    <div id="faq">
      <DynamicAccordion data={faqs} heading="Frequently asked questions" bg="/bg/faq_bg.svg" />
    </div>
  );
}
