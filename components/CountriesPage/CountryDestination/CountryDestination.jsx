import React from "react";
import Image from "next/image";
import styles from "./CountryDestination.module.scss";
import Link from "next/link";

// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
// import required modules
import { Pagination } from "swiper";
import SectionHeading from "../../global/SectionHeading/SectionHeading";

const countries = [
  { name: "USA", image: "/country/usa.png", slug: "us" },
  { name: "New Zealand", image: "/country/irland.png", slug: "new-zealand" },
  { name: "Australia", image: "/country/austrliya.png", slug: "australia" },
  { name: "UK", image: "/country/uk.png", slug: "uk" },
  { name: "Canada", image: "/country/canada.png", slug: "canada" },
  { name: "Germany", image: "/country/germany.png", slug: "germany" },
  { name: "Ireland", image: "/country/irland.png", slug: "ireland" },
];

export default function CountryDestination() {
  return (
    <div className={`${styles.container}`} id="scholarship">
      {/* <div className={`${styles.header}`}>
        <h5 className="text-center">Explore More Destination</h5>
      </div> */}
      <SectionHeading heading={`<span>Explore <span class="text-cvblue">More</span> Destination</span>`} />
      <div className={`${styles.countryContainer}`}>
        <Swiper
          slidesPerView={1}
          spaceBetween={30}
          pagination={{
            clickable: true,
          }}
          modules={[Pagination]}
          breakpoints={{
            600: {
              slidesPerView: 2,
              spaceBetween: 10,
            },
            800: {
              slidesPerView: 3,
              spaceBetween: 10,
            },
            1300: {
              slidesPerView: 4,
              spaceBetween: 10,
            },
            1500: {
              slidesPerView: 5,
              spaceBetween: 10,
            },
          }}
          className="topUniversitiesSlider"
        >
          {countries.map((country, i) => {
            return (
              <SwiperSlide key={`${country.link}${i}`}>
                <CountryCard country={country.name} link={country.slug} image={country.image} />
              </SwiperSlide>
            );
          })}
        </Swiper>
      </div>
    </div>
  );
}

function CountryCard({ country, link, image }) {
  return (
    <div className={`${styles.countryCard}`}>
      <div className={`${styles.top}`}>
        <Image alt="Explore more countries" src={image} width={300} height={300} objectFit="cover" objectPosition="center" />
        <div className={`${styles.countryTitle}`}>{country}</div>
      </div>
      <div className={`${styles.bottom}`}>
        <Link href={link}>
          <a>Explore Now {">"}</a>
        </Link>
      </div>
    </div>
  );
}
