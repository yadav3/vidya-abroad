import React from "react";
import CountryAdmissionRequirements from "./CountryAdmissionRequirements/CountryAdmissionRequirements";
import CountryAdmissionTimeline from "./CountryAdmissionTimeline/CountryAdmissionTimeline";
import CountryCareer from "./CountryCareer/CountryCareer";
import CountryCostOfLiving from "./CountryCostOfLiving/CountryCostOfLiving";
import CountryCounsellorBanner from "./CountryCounsellorBanner/CountryCounsellorBanner";
import CountryDestination from "./CountryDestination/CountryDestination";
import CountryExpertBanner from "./CountryExpertBanner/CountryExpertBanner";
import CountryFaq from "./CountryFaq/CountryFaq";
import CountryFastFacts from "./CountryFastFacts/CountryFastFacts";
import CountryHeroBanner from "./CountryHeroBanner/CountryHeroBanner";
import CountryTopUniversities from "./CountryTopUniversities/CountryTopUniversities";
import CountryVisaOption from "./CountryVisaOption/CountryVisaOption";
import QuickCompareStrip from "../global/QuickCompareStrip/QuickCompareStrip";
import QuickCompareCountryStyles from '../../components/global/QuickCompareStrip/QuickCompareStripCountryPage.module.scss'

export default function CountriesPage({ countryData, countrySlug, majors, courseCategories, countries }) {
  return (
    <>
      <CountryHeroBanner countryData={countryData} />
      <QuickCompareStrip
        countryData={countryData}
        majors={majors}
        countries={countries}
        courseCategories={courseCategories}
        customStyles={QuickCompareCountryStyles}
      />
      <CountryFastFacts funFactData={countryData.CountriesFunFacts} countryData={countryData} />
      <CountryTopUniversities countryData={countryData} universities={countryData.Universities} />
      <CountryAdmissionRequirements countryData={countryData} admissionRequirements={countryData.AdmissionRequirements} />
      {/* <CountryCounsellorBanner /> */}
      <CountryAdmissionTimeline slug={countrySlug} countryData={countryData} />
      <CountryCareer data={countryData.country_career_oppurtunities[0]} />
      <CountryCostOfLiving countryData={countryData} costOfLivingData={countryData.CountryCostOfLiving} />
      <CountryVisaOption visaOptions={countryData.CountryVisaOptions} countryData={countryData} />
      <CountryFaq faqs={countryData.CountryFaq} />
      <CountryDestination />
      <CountryExpertBanner countryData={countryData} />
    </>
  );
}
