import styles from "./CountryCounsellorBanner.module.scss";
import React from "react";
import { AiOutlineArrowRight } from "react-icons/ai";
import Link from "next/link";

export default function CountryCounsellorBanner() {

  return (
    <div className={`${styles.countryCounsellorBanner}`}>
      <div>
        <p>Get your Dream PTE or IELTS Score with College Vidya Live Classes</p>
        <h5>Learn from the Best Tutors</h5>
      </div>
      <Link href="/suggest-me-a-university">
        <a>
          <button>
            Talk to an Expert Counsellor for FREE <AiOutlineArrowRight />
          </button>
        </a>
      </Link>
    </div>
  );
}
