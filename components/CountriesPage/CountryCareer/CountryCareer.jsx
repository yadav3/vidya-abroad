import React from "react";
import Image from "next/image";
import styles from "./CountryCareer.module.scss";
import BigTextHeading from "../../global/BigTextHeading/BigTextHeading";
import uuid from "react-uuid";

import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination, Navigation } from "swiper";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import SectionHeading from "../../global/SectionHeading/SectionHeading";

export default function CountryCareer({ data }) {
  const opportunities = JSON.parse(JSON.parse(data.opputunites));
  return (
    <div className={`${styles.container}`} id="workopportunities">
      <SectionHeading
        heading={`<span>Career <span class="text-cvblue">Opportunities</span></span>`}
        subHeading={"Some Trending programs with the best career opportunities for part-time and full-time work after studies!"}
      />

      <div className={`${styles.contentContainer}`}>
        <div className={`${styles.left}`}>
          {data.country_career_oppurtunities_programs
            .map((data) => data.PopularPrograms)
            .map((option) => (
              <div className={`${styles.card}`} key={uuid()}>
                <span className={`${styles.icon}`}>
                  <Image alt="" src={option.icon} objectFit="contain" width={50} height={50} />
                </span>
                <span className="fw-semibold">{option.name}</span>
              </div>
            ))}
        </div>
        <div className={`${styles.right}`}>
          {opportunities.map((opportunity) => (
            <div className={`${styles.card}`} key={uuid()}>
              <h5>{opportunity.heading}</h5>
              <p>{opportunity.content}</p>
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
