import Link from "next/link";
import React from "react";
import styles from "./BottomNav.module.scss";
import {
  AiOutlineHome,
  AiFillHome,
  AiOutlineBook,
  AiFillBook,
  AiOutlineRobot,
  AiFillRobot,
} from "react-icons/ai";
import {  BsCameraVideo, BsCameraVideoFill, BsShieldFillCheck } from "react-icons/bs";
import uuid from "react-uuid";
import { useRouter } from "next/router";

export default function BottomNav() {
  const router = useRouter();

  const navItems = [
    { name: "Home", icon: <AiOutlineHome />, activeIcon: <AiFillHome />, link: "/" },
    {
      name: "Ask",
      icon: <AiOutlineRobot />,
      activeIcon: <AiFillRobot />,
      label: "ASK A.I",
      labelColor: "#00d8a1",
      link: "/ai-powered-faqs",
    },
    // { name: "About", icon: <AiOutlineQuestionCircle />, activeIcon: <AiFillQuestionCircle />, link: "/experts" },
    {
      name: "University",
      icon: <BsShieldFillCheck color="#1043e9" />,
      label: "A.I Powered",
      labelColor: "#fd4605",
      activeIcon: <BsShieldFillCheck color="#1043e9" />,
      link: "/suggest-me-a-university",
    },
    { name: "Blogs", icon: <AiOutlineBook />, activeIcon: <AiFillBook />, link: "/blog" },
    {
      name: "Counselling",
      icon: <BsCameraVideo />,
      label: "Experts",
      labelColor: "#1043e9",
      activeIcon: <BsCameraVideoFill />,
      link: "https://counsellor.collegevidyaabroad.com/",
    },
  ];
  return (
    <div className={`${styles.container}`}>
      {navItems.map((nav) => (
        <Link href={nav.link} key={uuid()}>
          <a className={`${styles.bottomIcon}`}>
            {nav?.label ? (
              <span className={`${styles.bottomIconLabel}`} style={{ backgroundColor: nav.labelColor }}>
                {nav.label}
              </span>
            ) : null}
            {router.pathname === nav.link ? nav.activeIcon : nav.icon}
            <span>{nav.name}</span>
          </a>
        </Link>
      ))}
    </div>
  );
}
