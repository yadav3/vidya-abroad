import Image from "next/image";
import React from "react";
import styles from "./NoResultFound.module.scss";
import { Button } from "react-bootstrap";
import { cvCounsellorDetailsAtom } from "../../../store/journeyStore";
import { useRecoilState } from "recoil";
import { useRouter } from "next/router";

export default function NoResultFound({
  heading = "Now that's a Unique Choice!",
  subheading = "We have noted and shared your Unique Choice with our Study Abroad Team.",
  showCtaText = true,
}) {
  const [cvCounsellorDetails, setCvCounsellorDetails] = useRecoilState(cvCounsellorDetailsAtom);

  const router = useRouter();

  const handleRedirectToCvCounsellor = () => {
    router.push(cvCounsellorDetails.redirectLink);
  };

  return (
    <div className="w-100 h-100 d-flex flex-column align-items-center justify-content-center p-2 text-center">
      <Image src="/gif/not found.gif" alt="No Result Found Illustration" width={200} height={200} />
      <h2>{heading}</h2>
      <p className={`${styles.noFoundPara} px-3 mx-auto`}>
        <span className="fs-md">
          <span className="d-block ">{subheading}</span>
          {showCtaText ? (
            <span>
              But feel free to click on the button below to connect with our Study Abroad Expert, who will make sure to search the{" "}
              <strong className={`${styles.highlight}`}>&apos;Best Fit Opportunity&apos;</strong> for you.
            </span>
          ) : null}
        </span>
      </p>
      {cvCounsellorDetails.redirectLink ? (
        <button className={styles.btn} onClick={handleRedirectToCvCounsellor}>
          Connect with Study Abroad Expert
        </button>
      ) : null}
    </div>
  );
}
