import React, { useState } from "react";
import styles from "./Testimonials.module.scss";
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Pagination, Navigation } from "swiper";
import { BsArrowLeftCircle, BsArrowRightCircle, BsPlayCircle } from "react-icons/bs";
import { FiPlayCircle, FiPauseCircle } from "react-icons/fi";
import { useRef } from "react";
import { useEffect } from "react";
import { useToggle } from "../../../context/CustomHooks";
import { useCallback } from "react";
import Image from "next/image";
import { getAWSImagePath, getTestimonials } from "../../../context/services";
import uuid from "react-uuid";
import { ImPlay, ImQuotesLeft, ImQuotesRight } from "react-icons/im";
import { AiOutlineArrowLeft, AiOutlineArrowRight } from "react-icons/ai";

import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Card from "react-bootstrap/Card";
import TestimonialPlayerModal from "./TestimonialPlayerModal";

export default function Testimonials({ testimonials }) {
  const sliderRef = useRef(null);

  const handlePrev = useCallback(() => {
    if (!sliderRef.current) return;
    sliderRef.current.swiper.slidePrev();
  }, []);

  const handleNext = useCallback(() => {
    if (!sliderRef.current) return;
    sliderRef.current.swiper.slideNext();
  }, []);

  return (
    <Container className={`${styles.container}`} fluid>
      <Row>
        <Col lg={4} md={6}>
          <div className={`${styles.headingContainer} d-flex flex-column align-items-center justify-content-center h-100 mb-5 mb-md-0 mx-sm-4`}>
            <h2 className={`fs-1 text-center text-lg-left`}>
              What <span className="text-cvblue">Students Say</span> About Us
            </h2>
            <div className="d-flex align-items-center justify-content-center gap-2">
              <AiOutlineArrowLeft
                className="bg-white text-black shadow-sm p-2 rounded-3 border border-1 border-dark cursor-pointer"
                size={35}
                onClick={handlePrev}
              />
              <AiOutlineArrowRight
                className="bg-white text-black shadow-sm p-2 rounded-3 border border-1 border-dark cursor-pointer"
                size={35}
                onClick={handleNext}
              />
            </div>
          </div>
        </Col>
        <Col lg={8} md={6}>
          <Swiper
            ref={sliderRef}
            slidesPerView={1}
            loop={true}
            spaceBetween={30}
            centeredSlides={true}
            pagination={{
              clickable: true,
            }}
            modules={[Pagination]}
            breakpoints={{
              1000: {
                slidesPerView: 2,
                spaceBetween: 30,
              },
              1900: {
                slidesPerView: 3,
                spaceBetween: 30,
              },
            }}
            className="testimonialSlider"
          >
            {testimonials.map((testimonial) => (
              <SwiperSlide key={uuid()}>
                <TestimonialsTextCard testimonial={testimonial} />
              </SwiperSlide>
            ))}
          </Swiper>
        </Col>
      </Row>
    </Container>
  );
}

export function TestimonialsCard({ testimonial }) {
  return (
    <div className={`${styles.testimonialCard} rounded-4 overflow-hidden d-flex flex-column gap-3 mb-5 shadow-sm`}>
      <TestimonialPlayerModal testimonial={testimonial}>
        <div className={`${styles.testimonialContent} mb-3`}>
          <div className="position-relative rounded-4 overflow-hidden h-100">
            <BsPlayCircle
              size={50}
              className={`${styles.testimonialPlayBtn} position-absolute top-50 start-50 translate-middle text-white`}
              style={{
                zIndex: 1,
              }}
            />
            <Image
              src={getAWSImagePath(testimonial.testimonialVideoThumbnail)}
              alt={testimonial.name}
              className={styles.testimonialCardImg}
              objectFit="cover"
              layout="fill"
            />
          </div>
        </div>
        <div className="p-3 pt-0  ">
          <p className="fs-4 fw-bold m-0">{testimonial.name}</p>
          <p className="fs-6 text-truncate">{testimonial.shortDescription}</p>
        </div>
      </TestimonialPlayerModal>
    </div>
  );
}
export function TestimonialsTextCard({ testimonial }) {
  return (
    <div className={`${styles.testimonialCardText} rounded-4 d-flex flex-column align-items-center justify-content-center gap-3 my-5 shadow-sm`}>
      <div className={`${styles.testimonialUser} rounded-circle overflow-hidden `}>
        <Image
          src={getAWSImagePath(testimonial.testimonialVideoThumbnail)}
          alt={testimonial.name}
          className={styles.testimonialUserImg}
          objectFit="cover"
          width={150}
          height={150}
        />
      </div>
      <div className="d-flex flex-column align-items-center justify-content-center pt-5 mt-5 text-center px-3">
        <p className="fs-4 fw-bold m-0">{testimonial.name}</p>
        <p className="fs-md m-0 text-muted p-0 px-sm-4">{testimonial.shortDescription}</p>
      </div>
      {testimonial.isText ? (
        <div className={`${styles.testimonialContent} mb-3 `}>
          <div className="d-flex flex-column align-items-center justify-content-center h-100 position-relative">
            <div className={`${styles.testimonialQuotes} text-cvblue mb-3 `}>
              <ImQuotesLeft size={30} />
            </div>

            <p className={`fs-md h-max text-center px-5 mb-0  ${styles.testimonialText}`}>
              {testimonial.testimonialText?.length > 180 ? (
                <>
                  {testimonial.testimonialText.slice(0, 180)}...
                  <TestimonialPlayerModal testimonial={testimonial}>
                    <span className="text-cvblue cursor-pointer">Read More</span>
                  </TestimonialPlayerModal>
                </>
              ) : (
                testimonial.testimonialText
              )}
            </p>
          </div>
        </div>
      ) : (
        <div className={`${styles.testimonialContent} mb-3 `}>
          <div className="d-flex flex-column align-items-center justify-content-center h-100 position-relative">
            <TestimonialPlayerModal testimonial={testimonial}>
              <div className={`${styles.testimonialQuotes} text-cvblue mb-3 cursor-pointer `}>
                <BsPlayCircle size={50} />
              </div>
            </TestimonialPlayerModal>

            <p className={`fs-md h-max text-center px-5 mb-0  ${styles.testimonialText}`}>
              {testimonial.testimonialText?.length > 180 ? (
                <>
                  {testimonial.testimonialText.slice(0, 180)}...
                  <TestimonialPlayerModal testimonial={testimonial}>
                    <span className="text-cvblue cursor-pointer">Watch Full Video</span>
                  </TestimonialPlayerModal>
                </>
              ) : (
                testimonial.testimonialText
              )}
            </p>
          </div>
        </div>
      )}
    </div>
  );
}
