import React from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import { useState } from "react";
import { getAWSImagePath } from "../../../context/services";

export default function TestimonialPlayerModal({ children, testimonial }) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <span onClick={handleShow}>{children}</span>

      <Modal show={show} onHide={handleClose} centered>
        <Modal.Header closeButton>
          <Modal.Title>
            <div className="d-flex flex-column">
              <p className="m-0">{testimonial.name}</p>
              <small className="fs-md text-muted">{testimonial.shortDescription}</small>
            </div>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div
            style={{
              maxHeight: "70vh",
            }}
          >
            {testimonial.isText ? (
              <div
                dangerouslySetInnerHTML={{
                  __html: testimonial.testimonialText,
                }}
              ></div>
            ) : (
              <video
                controls
                muted
                autoPlay
                className="w-100 rounded-3"
                style={{
                  maxHeight: "70vh",
                }}
              >
                <source src={getAWSImagePath(testimonial.testimonialVideo)} type="video/mp4" />
              </video>
            )}
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
}
