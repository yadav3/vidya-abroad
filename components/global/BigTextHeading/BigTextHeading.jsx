import React from "react";
import styles from "./BigTextHeading.module.css";

export default function BigTextHeading({ text, highlight, end, bgText, bgPosition = "bottom right", bgUrl = "/bg/spiral_RHS.png", className }) {
  return (
    <div className={`${styles.bigTextContainer} ${className}`}>
      <p className={`${styles.smallText}`}>
        {text}
        <span>{highlight}</span>
        {end}
      </p>
      <p className={`${styles.bigText}`}>{bgText}</p>
    </div>
  );
}
