import React from "react";
import styles from "./ExpertBanner.module.css";
import Container from "react-bootstrap/Container";
import Marquee from "react-fast-marquee";
import Link from "next/link";
import Image from "next/image";
import SectionHeading from "../../SectionHeading/SectionHeading";

export default function ExpertBanner() {
  return (
    <>
      <SectionHeading
        heading={`<span>
          <span class="text-cvblue">Meet Our</span> Study Abroad Experts
      </span>`}
        subHeading={"Get your free personalized counseling session now!"}
      />
      <Container fluid className={`${styles.expertBanner}`}>
        <div className={`${styles.colRight}`}>
          <Marquee gradientColor="rgb(0, 21, 55)" gradientWidth="500" speed={20}>
            <Link href="experts">
              <a>
                <Image src="/bg/expert_strip.png" alt="Meet our study abroad experts" width={4096} height={350} />
              </a>
            </Link>
          </Marquee>
        </div>
        <Link href="suggest-me-a-university">
          <a className={styles.ctaBtn}>Connect with an expert</a>
        </Link>
      </Container>
    </>
  );
}
