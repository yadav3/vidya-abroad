import styles from "./ExpertHeroSlider.module.scss";
import Marquee from "react-fast-marquee";
import Image from "next/image";

export default function ExpertHeroSlider() {
  return (
    <div className={styles.expertHeroSlider}>
      <Marquee gradient={false} speed={5}>
        <div className={styles.stripTop}>
          <div
            className={styles.layer1}
            style={{
              marginRight: "1rem",
            }}
          >
            <Image
              width={150}
              height={150}
              src="/expert-slider/1.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/2.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/3.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/4.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/5.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/6.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/12.png"
              alt="Expert 1"
            />
          </div>
          <div
            className={styles.layer2}
            style={{
              marginRight: "1rem",
            }}
          >
            <Image
              width={150}
              height={150}
              src="/expert-slider/1.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/2.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/3.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/4.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/5.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/6.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/12.png"
              alt="Expert 1"
            />
          </div>
        </div>
      </Marquee>

      <Marquee gradient={false} direction="right" speed={5}>
        <div className={styles.stripBottom}>
          <div
            className={styles.layer2}
            style={{
              marginRight: "1rem",
            }}
          >
            <Image
              width={150}
              height={150}
              src="/expert-slider/7.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/8.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/9.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/10.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/11.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/1.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/13.png"
              alt="Expert 1"
            />
          </div>
          <div
            className={styles.layer2}
            style={{
              marginRight: "1rem",
            }}
          >
            <Image
              width={150}
              height={150}
              src="/expert-slider/7.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/8.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/9.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/10.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/11.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/1.png"
              alt="Expert 1"
            />
            <Image
              width={150}
              height={150}
              src="/expert-slider/13.png"
              alt="Expert 1"
            />
          </div>
        </div>
      </Marquee>
    </div>
  );
}
