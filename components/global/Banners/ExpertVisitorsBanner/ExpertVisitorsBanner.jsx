import React, { useState } from "react";
import { Row } from "react-bootstrap";
import styles from "./ExpertVisitorsBanner.module.scss";
import CountUp from "react-countup";
import VisibilitySensor from "react-visibility-sensor";

export default function ExpertVisitorsBanner() {
  const [didCountUp, setDidCountUp] = useState(false);

  return (
    <Row className={styles.expertVisitorsContainer}>
      <div className={styles.gradient}></div>
      <h2>
        {didCountUp ? (
          600000
        ) : (
          <CountUp end={600000} duration={2}>
            {({ countUpRef, start }) => (
              <VisibilitySensor
                onChange={() => {
                  start();
                  setTimeout(() => {
                    setDidCountUp(true);
                  }, 2000);
                }}
                offset={{
                  top: 10,
                }}
                delayedCall
              >
                <span ref={countUpRef} />
              </VisibilitySensor>
            )}
          </CountUp>
        )}
        +
      </h2>
      <p>Monthly Unique Visitors</p>
    </Row>
  );
}
