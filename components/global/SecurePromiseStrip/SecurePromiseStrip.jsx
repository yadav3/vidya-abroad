import React from "react";
import Badge from "react-bootstrap/Badge";
import { AiFillLock } from "react-icons/ai";
import styles from "./SecurePromiseStrip.module.scss";

export default function SecurePromiseStrip({
  text = "Your personal information is secure with us",
  m = 0,
  mb = 0,
  mt = 0,
  ml = 0,
  mr = 0,
  image = "/icons/certified_cv_icon.png",
}) {
  return (
    <div className={`text-center mb-${mb} mt-${mt} ml-${ml} mr-${mr} m-${m} `}>
      <Badge className={styles.badge} bg="success">
        <AiFillLock />
        {text}
      </Badge>
    </div>
  );
}
