import Image from "next/image";
import React, { useEffect, useState } from "react";
import styles from "./ToastyCharacter.module.scss";
import { useRef } from "react";
import { useRouter } from "next/router";
import { setCookie, getCookie } from "cookies-next";

export default function ToastyCharacter() {
  const router = useRouter();
  const [toggleCharacter, setToggleCharacter] = useState(false);
  const toastyContainerRef = useRef(null);
  const toastyRef = useRef(null);
  const toastyBubbleRef = useRef(null);

  const handleGoToSuggestMe = () => {
    window.location.href = `/suggest-me-a-university`;
    handleHideToasty();
  };

  const handleShowToasty = () => {
    if (getCookie("toastyCharacterShown")) return;
    toastyContainerRef.current.classList.remove("d-none");
    toastyRef.current.classList.add("toastyCharacterAnimation");
    toastyBubbleRef.current.classList.add("toastyCharacterBubbleAnim");
  };

  const handleHideToasty = () => {
    toastyContainerRef.current.classList.add("d-none");
    setCookie("toastyCharacterShown", true);
  };

  useEffect(() => {
    const toastTiming = setTimeout(() => {
      handleShowToasty();
    }, 1000 * 60 * 2);
    return () => {
      clearTimeout(toastTiming);
    };
  }, []);

  return (
    <div className={`${styles.toastyCharacterContainer} d-none`} ref={toastyContainerRef}>
      <div className={`${styles.toastyCharacter}`} ref={toastyRef}>
        <div>
          {toggleCharacter ? (
            <Image src={"/bg/girl-charater-2.png"} alt="Girl Character" width={500} height={500} objectFit="contain" />
          ) : (
            <Image src={"/bg/girl-charater-1.png"} alt="Girl Character" width={500} height={500} objectFit="contain" />
          )}{" "}
        </div>
      </div>
      <div className={`${styles.toastyCharacterBubble}`} ref={toastyBubbleRef}>
        <h2 className="fs-5 fw-semibold">Are you looking for something?</h2>
        <h3 className="fs-md">I can help you with your study abroad journey and answer all your questions.</h3>
        <div className="d-flex align-items-center gap-2">
          <button
            className="bg-cvblue text-white border-0 p-1 px-2 rounded-3 pointer-cursor fs-md"
            onMouseOver={() => {
              setToggleCharacter(true);
            }}
            onClick={handleGoToSuggestMe}
          >
            Connect with Expert
          </button>
          <button
            className="border-0 bg-light p-1 px-2 rounded-3 pointer-cursor fs-md"
            onMouseOver={() => {
              setToggleCharacter(false);
            }}
            onClick={handleHideToasty}
          >
            I&apos;ll Browse
          </button>
        </div>
      </div>
    </div>
  );
}
