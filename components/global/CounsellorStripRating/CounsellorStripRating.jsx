import React from "react";
import { Avatar } from "primereact/avatar";
import { AvatarGroup } from "primereact/avatargroup";
import { Badge } from "primereact/badge";
import { Rating } from "primereact/rating";

export default function CounsellorStripRating() {
  return (
    <div
      className="d-flex flex-row align-items-center justify-content-center gap-2 "
      style={{
        background: " white",
        padding: "5px",
        borderRadius: "9px",
        boxShadow: "#efefef 1px 2px 5px 0px",
      }}
    >
      <div className="d-flex flex-row align-items-center">
        <div>
          <AvatarGroup className="d-flex flex-row align-items-center justify-content-center">
            <Avatar image="/counsellors/ramiz.png" size="normal" shape="circle" />
            <Avatar image="/counsellors/akash.png" size="normal" shape="circle" />
            <Avatar image="/counsellors/manish.png" size="normal" shape="circle" />
          </AvatarGroup>
        </div>
      </div>
      <div
        className="d-flex flex-row align-items-center gap-2"
        style={{
          fontSize: "11px",
        }}
      >
        Connect with Top Study Abroad Experts ⭐⭐⭐⭐⭐
      </div>
    </div>
  );
}
