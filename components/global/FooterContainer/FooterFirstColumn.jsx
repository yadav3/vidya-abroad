import React from "react";
import Image from "next/image";
import { BsChatDots, BsLightningChargeFill, BsSearch } from "react-icons/bs";
import styles from "./FooterNew.module.scss";
import Link from "next/link";

export function FooterFirstColumn({}) {
  return (
    <div className="d-flex flex-column gap-3 align-items-center ">
      <div onClick={() => (window.location.href = "/")}>
        <Image src={"/logos/cv-abroad-white-logo.png"} alt="College Vidya Abroad Logo" width={200} height={80} objectFit="contain" />
      </div>
      <Link href="/suggest-me-a-university">
        <a
          className={`${styles.mainBtn} bg-orange  text-white border-0 py-3 rounded-5 fs-lg fw-semibold d-flex align-items-center justify-content-center gap-2 position-relative`}
        >
          Suggest University <BsSearch className={`${styles.mainBtnIcon}`} />
          <span className={`${styles.btnTag} myshine position-absolute bg-white text-black px-3 fs-xs rounded-5 `}>
            <BsLightningChargeFill /> A.I Powered
          </span>
        </a>
      </Link>

      <Link href="/ai-powered-faqs">
        <a
          className={`${styles.mainBtn} mt-2 bg-bright-green  text-white border-0 py-3 rounded-5 fs-lg fw-semibold d-flex align-items-center justify-content-center gap-2 position-relative`}
        >
          Chat with A.I <BsChatDots className={`${styles.mainBtnIcon}`} />
          <span className={`${styles.btnTag} myshine position-absolute bg-white text-black px-3 fs-xs rounded-5 `}>
            <BsLightningChargeFill /> Instant Answer
          </span>
        </a>
      </Link>
    </div>
  );
}
