import React from "react";

export function FooterDivider({ className }) {
  return (
    <div className={className}>
      <hr className="bg-white border" />
    </div>
  );
}
