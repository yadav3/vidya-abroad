import React from "react";
import Link from "next/link";
import uuid from "react-uuid";
import styles from "./FooterNew.module.scss";

export function FooterCountryColumn() {
  return (
    <div className="d-flex flex-column  gap-3">
      <p className="text-white text-semibold fs-5 mb-0">Explore Countries</p>
      {[
        {
          label: "Canada",
          href: "/countries/canada",
        },
        {
          label: "Australia",
          href: "/countries/australia",
        },
        {
          label: "New Zealand",
          href: "/countries/new-zealand",
        },
        {
          label: "USA",
          href: "/countries/us",
        },
        {
          label: "UK",
          href: "/countries/uk",
        },
        {
          label: "Germany",
          href: "/countries/germany",
        },
        {
          label: "Ireland",
          href: "/countries/ireland",
        },
      ].map(({ label, href }) => (
        <Link key={uuid()} href={href}>
          <a
            className={`text-white fw-light fs-6 
                   ${styles.hoverLink}`}
          >
            {label}
          </a>
        </Link>
      ))}
    </div>
  );
}
