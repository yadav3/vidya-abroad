import React from "react";
import Link from "next/link";
import uuid from "react-uuid";
import styles from "./FooterNew.module.scss";

export function FooterContactUs() {
  return (
    <div className="d-flex align-item-center justify-content-center flex-wrap gap-3">
      {[
        {
          label: "Toll Free: 1800 309 9018",
          href: "tel:18003099018",
          icon: "pi pi-phone",
        },
        {
          label: "Info@collegevidyaabroad.com",
          href: "mailto:Info@collegevidyaabroad.com",
          icon: "pi pi-envelope",
        },
      ].map(({ label, icon, href }) => (
        <Link key={uuid()} href={href}>
          <a className={`text-white fw-light fs-md d-flex align-items-center gap-2 ${styles.contactLink} `}>
            <i className={icon}></i>
            {label}
          </a>
        </Link>
      ))}
    </div>
  );
}
