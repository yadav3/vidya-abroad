import React from "react";
import Link from "next/link";
import uuid from "react-uuid";
import { BsLightningChargeFill } from "react-icons/bs";
import { AiFillCheckCircle } from "react-icons/ai";
import styles from "./FooterNew.module.scss";

export function FooterExploreColumn() {
  return (
    <div className="d-flex flex-column gap-3">
      <p className="text-white text-semibold fs-5 mb-0">Explore</p>
      {[
        {
          label: "Home",
          href: "/",
          badge: null,
        },
        {
          label: "Free Resources",
          href: "/resources",
          badge: null,
        },
        {
          label: "Video Library",
          href: "/video-library",
          badge: "New",
          badgeIcon: <AiFillCheckCircle />,
          badgeColor: "#fd4605",
        },
        {
          label: "Suggest University",
          href: "/suggest-me-a-university",
          // badge: "A.I Powered",
          // badgeIcon: <AiFillCheckCircle />,
          // badgeColor: "#fd4605",
        },
        {
          label: "Who we are?",
          href: "/experts",
          badge: null,
        },

        {
          label: "Explore with A.I",
          href: "/ai-powered-faqs",
          // badge: "Instant Answer",
          // badgeIcon: <BsLightningChargeFill />,
          // badgeColor: "#0e50d3",
        },
        {
          label: "Explore Universities",
          href: "/universities",
          badge: null,
        },
        {
          label: "Blogs",
          href: "/blog",
          badge: null,
        },

        {
          label: "Careers",
          href: "https://collegevidya.in/career/",
          badge: "We are hiring",
          badgeIcon: <AiFillCheckCircle />,
          badgeColor: "#00b894",
        },
      ].map(({ label, badge, badgeIcon, badgeColor, href }) => (
        <Link key={uuid()} href={href}>
          <a
            className={`text-white fw-light fs-6 d-flex align-items-center
                   ${styles.hoverLink}`}
          >
            {label}

            {badge ? (
              <span className="rounded-3 p-1 px-2 fs-xs ms-2 " style={{ backgroundColor: badgeColor }}>
                {badgeColor && <span className="me-1">{badgeIcon}</span>}
                {badge}
              </span>
            ) : null}
          </a>
        </Link>
      ))}
    </div>
  );
}
