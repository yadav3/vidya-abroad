import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { FooterDisclaimer } from "./FooterDisclaimer";
import { FooterDivider } from "./FooterDivider";
import { FooterUniversitiesColumn } from "./FooterUniversitiesColumn";
import { FooterCountryColumn } from "./FooterCountryColumn";
import { FooterExploreColumn } from "./FooterExploreColumn";
import { FooterFirstColumn } from "./FooterFirstColumn";
import { FooterCopyright } from "./FooterCopyright";
import { FooterSocialIcons } from "./FooterSocialIcons";
import styles from "./FooterNew.module.scss";
import uuid from "react-uuid";
import { FooterContactUs } from "./FooterContactUs";

export default function FooterNew() {
  return (
    <Container fluid className="m-0 px-0 ">
      <div
        className=" p-5 "
        style={{
          backgroundColor: "#011a3d",
        }}
      >
        <Row>
          <Col lg={3}>
            <FooterFirstColumn />
          </Col>
          <FooterDivider className="my-3 d-lg-none" />
          <Col lg={3}>
            <FooterExploreColumn />
          </Col>
          <FooterDivider className="my-3 d-lg-none" />
          <Col lg={3}>
            <FooterCountryColumn />
          </Col>
          <FooterDivider className="my-3 d-lg-none" />
          <Col lg={3}>
            <FooterUniversitiesColumn />
          </Col>
        </Row>
        <FooterDivider className="my-5" />
        <Row>
          <FooterDisclaimer />
        </Row>
        <FooterDivider className="my-5 mb-0" />
        <Row>
          <Col>
            <FooterContactUs  />
          </Col>
        </Row>
        <FooterDivider className="my-5 mt-0" />
        <Row>
          <Col>
            <div className="d-flex align-item-center justify-content-between gap-3">
              <FooterCopyright />
              <FooterSocialIcons />
            </div>
          </Col>
        </Row>
      </div>
    </Container>
  );
}
