import React from "react";

export function FooterCopyright({ }) {
  return <small className="text-white text-semibold fs-sm  mb-0">© {new Date().getFullYear()} CollegeVidya Inc. All Rights Reserved.</small>;
}
