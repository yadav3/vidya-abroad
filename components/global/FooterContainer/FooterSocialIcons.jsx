import React from "react";
import Link from "next/link";
import { BsFacebook, BsInstagram, BsLinkedin, BsYoutube } from "react-icons/bs";

export function FooterSocialIcons({ }) {
  return (
    <div className="d-flex align-items-center gap-3 text-white ">
      <Link href="https://www.facebook.com/profile.php?id=100084472042498" target="_blank" rel="noopener noreferrer">
        <a>
          <BsFacebook size={20} />
        </a>
      </Link>

      <Link href="https://instagram.com/collegevidyaabroad" target="_blank" rel="noopener noreferrer">
        <a>
          <BsInstagram size={20} />
        </a>
      </Link>

      <Link href="https://www.linkedin.com/company/collegevidyaabroad" target="_blank" rel="noopener noreferrer">
        <a>
          <BsLinkedin size={20} />
        </a>
      </Link>
      <Link href="https://www.youtube.com/channel/UC7nR8lMnH81juucjC8haqQw" target="_blank" rel="noopener noreferrer">
        <a>
          <BsYoutube size={20} />
        </a>
      </Link>
    </div>
  );
}
