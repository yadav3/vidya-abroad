import React from "react";

export function FooterDisclaimer({ }) {
  return (
    <div>
      <span
        className="  text-center text-white  "
        style={{
          fontSize: "10px",
          textDecoration: "underline",
        }}
      >
        <a>Disclaimer</a>
      </span>
      <p
        className="mt-1 text-white"
        style={{
          fontSize: "8px",
        }}
      >
        College Vidya Aboard is an Online Platform that provide the Admission Aspirants unbiased, precise information &amp; comparative guidance on
        world-class Universities and their Programs of Study. And this is backed by “AI” driven compare feature and Admissions Experts Counselling. We
        are neither a university nor we work for and on behalf of any university. We or any of our representatives never asks or accepts any fee
        payment from the student on behalf of any university for admissions or in any manner. The contents of the College vidya abroad
        site(www.collegevidyaabroad.com), such as Texts, Graphics, Images, Blogs, Videos, University Logos, etc. (collectively, “Content”) are for
        information purpose only. The content is not intended to be a substitute for in any form on offerings of its Academia Partner/ Country.
        Infringing on intellectual property or associated rights is not intended or deliberately acted upon. All information on the site and our
        mobile application is provided in good faith with accuracy and to the best of our knowledge, however, we make nor representation or warranty
        of any kind, express or implied, regarding the accuracy, adequacy, validity, reliability, completeness of any information on the site or our
        mobile application. Collegevidya &amp; its Fraternity will not be liable for any errors or omissions and damages or losses resultant if any
        from the usage of its information.
      </p>
    </div>
  );
}
