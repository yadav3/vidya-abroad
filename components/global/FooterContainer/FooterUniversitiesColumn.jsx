import React from "react";
import Link from "next/link";
import uuid from "react-uuid";
import styles from "./FooterNew.module.scss";

export function FooterUniversitiesColumn() {
  return (
    <div className="d-flex flex-column  gap-3">
      <p className="text-white text-semibold fs-5 mb-0">Top Universities</p>
      {[
        {
          label: "The University Of New South Wales",
          href: "/universities/the-university-of-new-south-wales",
        },
        {
          label: "Monash University",
          href: "/universities/monash-university-melbourne",
        },
        {
          label: "The University Of Queensland",
          href: "/universities/the-university-of-queensland",
        },
        {
          label: "University Of Birmingham",
          href: "/universities/university-of-birmingham",
        },
        {
          label: "Durham University",
          href: "/universities/durham-university",
        },
        {
          label: "Trinity College Dublin",
          href: "/universities/trinity-college-dublin",
        },
        {
          label: "The University Of Adelaide",
          href: "/universities/the-university-of-adelaide",
        },
      ].map(({ label, href }) => (
        <Link key={uuid()} href={href}>
          <a
            className={`text-white fw-light fs-6 
                   ${styles.hoverLink}`}
          >
            {label}
          </a>
        </Link>
      ))}
    </div>
  );
}
