import React from "react";
import styles from "./HomeFormContainer.module.css";
import { Row, Col } from "react-bootstrap";
import LeadForm from "../LeadForm/LeadForm";
import Image from "next/image";

export default function HomeFormContainer({ majors }) {
  return (
    <Row className={`${styles.homeFormContainer}`}>
      <Col sm={6} className={styles.homeFormLeftCol}>
        <div className={styles.homeFormLeft}>
          <div className={styles.formLeftArrow}>
            <Image src="/icons/form-arrow.png" alt="Form Arrow Icon" width={50} height={50} objectFit="contain" />
          </div>
          <h2>
            You are <span className="d-block ">just</span> one step
            <span className={styles.borderText}>away...</span>
          </h2>
          <Image alt="Man Illustration" width={900} height={700} objectFit="contain" src="/bg/leadform_man.png" className={styles.leftFormImage} />
        </div>
      </Col>
      <Col className={styles.homeFormRight} sm={6}>
        <LeadForm majors={majors} />
      </Col>
    </Row>
  );
}
