import { atom } from "recoil";

export const openLeadModalAtom = atom({
  key: "openLeadModalAtom",
  default: false,
});
