import React, { useEffect } from "react";
import styles from "./QuickCompareStrip.module.scss";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as Yup from "yup";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Dropdown } from "primereact/dropdown";
import { BsSearch } from "react-icons/bs";
import QuickCompareStripLeadModal from "./QuickCompareStripLeadModal";
import { useRecoilState } from "recoil";
import { useState } from "react";
import { openLeadModalAtom } from "./QuickCompare.store";

export default function QuickCompareStrip({ customStyles = styles, majors, countries, courseCategories, countryData }) {
  const [openModal, setOpenModal] = useRecoilState(openLeadModalAtom);
  const [filteredMajors, setFilteredMajors] = useState(majors);

  const schema = Yup.object().shape({
    country: Yup.string().required("Country is required"),
    category: Yup.number().required("Degree is required"),
    major: Yup.number().required("Major is required"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    getValues,
    setValue,
  } = useForm({
    resolver: yupResolver(schema),
    defaultValues: {
      country: countryData?.id,
    },
  });

  const onSubmit = (data) => {
    setOpenModal(true);
  };

  const handleOnChange = (e) => {
    setValue(e.target.name, e.value, { shouldValidate: true });

    if (getValues("category")) {
      setFilteredMajors(majors.filter((major) => parseInt(major.category_id) === parseInt(getValues("category"))));
    } else {
      setFilteredMajors(majors);
    }
  };

  useEffect(() => {
    if (countryData?.id) {
      setValue("country", countryData.id);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <Container className="position-relative">
        <Row>
          <Col>
            <form className={`${customStyles.quickCompareContainer} p-4 mt-4`} onSubmit={handleSubmit(onSubmit)}>
              <Row>
                <Col md={3}>
                  <Dropdown
                    name="country"
                    options={[
                      {
                        id: 0,
                        name: "Not Decided Yet",
                        fullName: "Not Decided Yet",
                        courseCount: "All Courses",
                      },
                      ...countries,
                    ]}
                    value={getValues("country")}
                    onChange={handleOnChange}
                    optionLabel="name"
                    itemTemplate={(item) => {
                      return (
                        <div className="d-flex flex-column">
                          <span>{item.name}</span>
                          <small className="fs-sm text-muted">{item.courseCount}+ Specializations</small>
                        </div>
                      );
                    }}
                    optionValue="id"
                    placeholder="Select a Country"
                    className="bg-white rounded-3 py-1 w-100 mb-3 mb-md-0"
                    style={{
                      border: errors?.country ? "1px solid red" : "1px solid gray",
                    }}
                  />
                </Col>
                <Col md={3}>
                  <Dropdown
                    name="category"
                    options={courseCategories}
                    value={getValues("category")}
                    onChange={handleOnChange}
                    optionLabel="name"
                    optionValue="id"
                    placeholder="Select a Degree"
                    className="bg-white rounded-3 py-1 w-100 mb-3 mb-md-0"
                    style={{
                      border: errors?.category ? "1px solid red" : "1px solid gray",
                    }}
                  />
                </Col>
                <Col md={3}>
                  <Dropdown
                    name="major"
                    options={filteredMajors.sort((a, b) => b._count.specializations_majors - a._count.specializations_majors)}
                    value={getValues("major")}
                    onChange={handleOnChange}
                    optionLabel="identifier"
                    itemTemplate={(item) => {
                      return (
                        <div className="d-flex flex-column">
                          <span>{item.name}</span>
                          <small className="fs-sm text-muted">{item._count.specializations_majors}+ Specializations</small>
                        </div>
                      );
                    }}
                    optionValue="id"
                    filter
                    filterBy="identifier"
                    placeholder="Select a Major"
                    className="bg-white rounded-3 py-1 w-100 mb-3 mb-md-0"
                    style={{
                      border: errors?.major ? "1px solid red" : "1px solid gray",
                    }}
                  />
                </Col>
                <Col md={3}>
                  <button type="submit" className={`${customStyles.searchBtn} h-100 w-100 text-white rounded-3 border-0 py-3 py-md-0`}>
                    <BsSearch className="me-2" />
                    <span>Search Now</span>
                  </button>
                </Col>
              </Row>
            </form>
          </Col>
        </Row>
      </Container>
      {openModal && (
        <QuickCompareStripLeadModal
          majorId={getValues("major")}
          countryId={getValues("country")}
          majors={majors}
          countries={countries}
          courseCategories={courseCategories}
        />
      )}
    </>
  );
}
