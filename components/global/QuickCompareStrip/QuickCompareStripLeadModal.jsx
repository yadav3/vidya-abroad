import React, { useEffect, useState, useRef } from "react";
import styles from "./QuickCompareStrip.module.scss";
import Modal from "react-bootstrap/Modal";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import moment from "moment";
import { InputText } from "primereact/inputtext";
import { Calendar } from "primereact/calendar";
import { useRecoilState } from "recoil";
import CounsellorStripRating from "../CounsellorStripRating/CounsellorStripRating";
import { getUniversitiesByCompareQueries } from "../../../services/universitiesService";
import { getCookie, hasCookie, setCookie } from "cookies-next";
import { COUNSELLOR_CLIENT_URL, COUNSELLOR_SERVER_URL, createNewLead, generatePassword, storeUserCompareResults } from "../../../context/services";
import { toast } from "react-hot-toast";
import { createLsqOpportunity, updatedLsqLeadDetails } from "../../../services/lead.service";
import { compareQueryParamsState } from "../../../store/compare/compareStore";
import { cvCounsellorDetailsAtom } from "../../../store/journeyStore";
import { useLocalStorage } from "../../../context/CustomHooks";
import axios from "axios";

import Lottie from "lottie-react";
import buildingLoader from "../../../public/gif/building_loader.json";
import { openLeadModalAtom } from "./QuickCompare.store";

export default function QuickCompareStripLeadModal({ children, majorId, countryId, majors, countries, courseCategories }) {
  const [show, setShow] = useRecoilState(openLeadModalAtom);
  const [userCompareQuery, setUserCompareQuery] = useRecoilState(compareQueryParamsState);
  const [cvCounsellorDetails, setCvCounsellorDetails] = useRecoilState(cvCounsellorDetailsAtom);

  const [leadData, setLeadData] = useLocalStorage("leadData", {});
  const [leadFilledUserID, setLeadFilledUserID] = useLocalStorage("LeadFilledUserID", null);

  const [resultsFound, setResultsFound] = useState(null);
  const [ctaText, setCtaText] = useState("Building your profile...");
  const [isLoading, setIsLoading] = useState(false);

  const gtmButtonRef = useRef(null);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const schema = yup.object().shape({
    name: yup.string().required("This field is required!").min(1, "Enter a valid name!"),
    email: yup
      .string()
      .email("Should be a valid email.")
      .matches(/^[a-zA-Z0-9_.]+@[^\s@]+\.[^\s@]{2,}$/, "Invalid email")
      .required("This field is required"),
    phone: yup.string().required("This field is required!").min(10, "Number should be 10 numbers long!").max(10, "Number should be 10 numbers long!"),
    dob: yup
      .string()
      .nullable()
      .required("Date of birth is required!")
      .test("DOB", "Please choose a valid date of birth.", (date) => moment().diff(moment(date), "years") >= 1),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
    getValues,
    getFieldState,
  } = useForm({
    // defaultValues: {
    //   major: majorId,
    //   country: countryId,
    // },
    resolver: yupResolver(schema),
  });

  const createCVCounsellorCustomer = (userDetails, leadData, cv_id) => {
    axios
      .post(
        `${COUNSELLOR_SERVER_URL}/collegevidyaabroad/customer/create`,
        {
          uuid: userDetails.user_id,
          name: leadData.name,
          phone: leadData.phone,
          email: leadData.email,
          cv_id: cv_id,
          dob: new Date(leadData.age).toISOString().split("T")[0],
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      )
      .then(() => {
        setCvCounsellorDetails({
          ...cvCounsellorDetails,
          cv_id: cv_id,
          redirectLink: `${COUNSELLOR_CLIENT_URL}/collegevidya/redirect/login?cv_id=${cv_id}&country=${leadData.country}`,
        });
      })
      .catch((err) => {
        if (err?.request?.status == "409") {
          setCvCounsellorDetails({
            ...cvCounsellorDetails,
            redirectLink: `${COUNSELLOR_CLIENT_URL}/collegevidya/redirect/login?exist=1`,
          });
        } else {
          console.error(err);
        }
      });
  };

  const onSubmit = (data) => {
    setIsLoading(true);
    setLeadData({
      ...data,
    });
    const cv_id = generatePassword(8);
    const user_id = cv_id.toString();
    const country =
      parseInt(countryId) === 0 ? { id: 0, name: "Not Decided Yet" } : countries.find((country) => parseInt(country.id) === parseInt(countryId));
    const source = "College Vidya";
    const sub_source =
      getCookie("sub_source")?.length > 0
        ? getCookie("sub_source")
        : getCookie("source_campaign") === "collegevidya_online_website"
        ? "Online Website"
        : "Abroad Website";
    const source_campaign = getCookie("source_campaign");
    const campaign_name = getCookie("campaign_name");
    const ad_group = getCookie("ad_group_name");
    const ad_name = getCookie("ads_name");

    const leadData = {
      userId: user_id,
      name: data.name,
      phone: data.phone,
      email: data.email,
      age: data.dob,
      country: country.name,
      majorId: majorId,
      source: source,
      sub_source: sub_source,
      source_campaign: source_campaign,
      campaign_name: campaign_name,
      ad_group: ad_group,
      ad_name: ad_name,
    };

    /* 
    
    Create New Lead with API
    
    */
    createNewLead(leadData)
      .then(async () => {
        if (!hasCookie("lead-filled")) {
          gtmButtonRef.current.click();
        }

        setCookie("lead-filled", "true");

        const major = majors.find((major) => major.id === parseInt(leadData.majorId));
        // const program = major.category_id === 1 ? "Bachelors" : major.category_id === 2 ? "Masters" : major.category_id === 3 ? "Diploma" : null;
        const program = courseCategories.find((category) => category.id === major.category_id).name;
        const budget = "Not Decided Yet";
        // console.log(courseCategories.find((category) => category.id === major.category_id).name);

        const opportunityRes = await createLsqOpportunity(
          leadData.name,
          leadData.email,
          leadData.phone,
          budget,
          country.name,
          program,
          major.name,
          source,
          sub_source,
          source_campaign,
          campaign_name,
          ad_group,
          ad_name
        );

        const lsqLeadData = {
          data: [
            {
              Attribute: "mx_Study_Destination",
              Value: country.name,
            },
            {
              Attribute: "mx_Programme",
              Value: program,
            },
            {
              Attribute: "mx_Specilazation",
              Value: major.name,
            },
          ],
        };

        storeUserCompareResults(
          user_id,
          major.category_id,
          0,
          0,
          major.id,
          0,
          country.id,
          null,
          opportunityRes.opportunityId,
          0,
          0,
          [country.id],
          opportunityRes.relatedProspectId
        ).then(async (userDetails) => {
          setUserCompareQuery({
            uuid: userDetails.user_id,
            categoryId: userDetails.category_id,
            courseId: userDetails.course_id,
            majorId: userDetails.majorId,
            specializationId: userDetails.specialization_id,
            countryId: userDetails.country_id,
            budgetLimit: userDetails.budget,
            ieltsScore: userDetails.ielts,
            opportunityId: opportunityRes.opportunityId,
            countries: userDetails?.countries ? [...userDetails.countries] : [],
            relatedProspectId: opportunityRes.relatedProspectId,
          });
          setLeadFilledUserID(user_id);
          createCVCounsellorCustomer(userDetails, leadData, user_id);
          await updatedLsqLeadDetails(userDetails.user_id, lsqLeadData);
          window.location.href = `/compare/universities?uuid=${userDetails.user_id}`;
        });
      })
      .catch((error) => {
        console.error(error);
        setIsLoading(false);
      });
  };

  useEffect(() => {
    Object.keys(leadData).forEach((key) => {
      if (key === "dob") setValue("dob", new Date(leadData["dob"]), { shouldValidate: true });
      else if (key === "major") setValue("major", parseInt(leadData[key]));
      else setValue(key, leadData[key], { shouldValidate: true });
    });
  }, []);

  useEffect(() => {
    if (countryId || (countryId === 0 && majorId && show)) {
      setIsLoading(true);
      getUniversitiesByCompareQueries(parseInt(majorId), parseInt(countryId), 0, 0, 0, 5000000000000, [countryId])
        .then((data) => {
          setCtaText(`Found ${data.data.universities.length}+ Matches? See Results!`);
          setResultsFound(data.data.universities.length);
        })
        .catch((error) => console.error(error))
        .finally(() => setIsLoading(false));
    }
  }, [countryId, majorId, show]);

  useEffect(() => {
    setCookie("sub_source", `Quick Compare Homepage`);
  }, []);

  return (
    <>
      <span onClick={handleShow}>{children}</span>

      <Modal show={show} onHide={handleClose} centered keyboard={false}>
        <Modal.Body>
          <div className="p-3">
            <div className="mb-4">
              <h2 className="fs-lg text-center ">You are just one step away </h2>
            </div>
            <form onSubmit={handleSubmit(onSubmit)} className="d-flex flex-column gap-3">
              <span className="p-input-icon-left w-100">
                <i className="pi pi-user" />
                <InputText placeholder="Enter your name" className={`w-100 ${errors?.name && "p-invalid"} `} {...register("name")} />
              </span>
              {errors?.name && <span className="text-danger fs-xs">{errors.name.message}</span>}
              <span className="p-input-icon-left w-100">
                <i className="pi pi-envelope" />
                <InputText type="email" placeholder="Enter your email" className={`w-100 ${errors?.email && "p-invalid"} `} {...register("email")} />
              </span>
              {errors?.email && <span className="text-danger fs-xs">{errors.email.message}</span>}
              <span className="p-input-icon-left w-100">
                <i className="pi pi-phone" />
                <InputText
                  type="tel"
                  placeholder="Enter your phone"
                  maxLength={10}
                  className={`w-100 ${errors?.phone && "p-invalid"} `}
                  {...register("phone")}
                />
              </span>
              {errors?.phone && <span className="text-danger fs-xs">{errors.phone.message}</span>}
              <span className="p-input-icon-left w-100">
                <i className="pi pi-user" />
                <Calendar
                  // className={styles.pCalendarInput}
                  showOnFocus={false}
                  value={getValues("dob")}
                  visible={false}
                  placeholder="DD/MM/YYYY"
                  inputMode={"numeric"}
                  name="dob"
                  onChange={(e) =>
                    setValue("dob", e.value, {
                      shouldValidate: true,
                    })
                  }
                  dateFormat="dd/mm/yy"
                  mask="99/99/9999"
                  className={`w-100 ${errors?.dob && "p-invalid"} `}
                />
              </span>
              {errors?.dob && <span className="text-danger fs-xs">{errors.dob.message}</span>}
              <button
                type="submit"
                className={`border-0 bg-cvblue text-white rounded-2 d-flex align-items-center justify-content-center ${styles.compareNowBtn}`}
                onClick={() => {
                  console.error(errors);
                }}
              >
                {isLoading ? (
                  <div className="d-flex align-items-center justify-content-center gap-2">
                    <div
                      style={{
                        width: "50px",
                        height: "50px",
                      }}
                    >
                      <Lottie
                        animationData={buildingLoader}
                        style={{
                          width: "100%",
                          height: "100%",
                        }}
                      />
                    </div>
                    <span>Compiling {resultsFound}+ results for you!</span>
                  </div>
                ) : (
                  ctaText
                )}
              </button>
              <CounsellorStripRating />
            </form>
            <input ref={gtmButtonRef} id="SUGGEST-ME" type={"hidden"} />
            {/* <OttaPointLeadSwiper /> */}
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
}
