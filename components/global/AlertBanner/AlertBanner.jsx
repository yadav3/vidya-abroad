import React from "react";
import { GrValidate } from "react-icons/gr";
import { AiFillInfoCircle, AiFillWarning, AiOutlineBulb } from "react-icons/ai";
import { MdDangerous } from "react-icons/md";
import { FaInfo } from "react-icons/fa";
import styles from "./AlertBanner.module.scss";

export default function AlertBanner({ showIcon, title, subTitle, variant = "info" }) {
  const renderLightColor = () => {
    switch (variant) {
      case "success":
        return "#f1f8f4";
      case "info":
        return "#e7effa";
      case "warning":
        return "#fef8eb";
      case "danger":
        return "#faefeb";
      default:
        break;
    }
  };
  const renderBrightColor = () => {
    switch (variant) {
      case "success":
        return "#60dc73";
      case "info":
        return "#3b88e7";
      case "warning":
        return "#fbbe33";
      case "danger":
        return "#f65357";
      default:
        break;
    }
  };

  const renderIcon = () => {
    if (!showIcon) return null;

    const iconProps = {
      className: "rounded-circle p-2",
      style: {
        backgroundColor: renderBrightColor(),
        color: "white",
        minHeight: "20px",
        minWidth: "20px",
      },
    };

    switch (variant) {
      case "success":
        return (
          <div className="d-flex align-items-center">
            <div {...iconProps}>
              <GrValidate size={25} color="white" />
            </div>
          </div>
        );
      case "info":
        return (
          <div className="d-flex align-items-center">
            <div {...iconProps}>
              <AiOutlineBulb size={25} color="white" className="" />
            </div>
          </div>
        );
      case "warning":
        return (
          <div className="d-flex align-items-center">
            <div {...iconProps}>
              <AiFillWarning size={25} color="white" />
            </div>
          </div>
        );
      case "danger":
        return (
          <div className="d-flex align-items-center">
            <div {...iconProps}>
              <MdDangerous size={25} color="white" />
            </div>
          </div>
        );
      default:
        return null;
    }
  };

  return (
    <div
      className="d-flex gap-2 p-2 py-3 ps-4 rounded-4 mb-4 mt-1 shadow-sm"
      style={{
        backgroundColor: renderLightColor(),
        border: `2px solid ${renderBrightColor()}`,
      }}
    >
      {renderIcon()}
      <div className="d-flex flex-column">
        <strong className="d-block m-0 ">{title}</strong>
        <small className="text-muted fs-sm">{subTitle}</small>
      </div>
    </div>
  );
}
