import React from "react";
import { Accordion } from "react-bootstrap";
import styles from "./DynamicAccordion.module.scss";


export default function DynamicAccordion({ heading, data, bg, className, showLeft = true }) {
  return (
    <div
      className={`${styles.faqContainer}
      ${className ? className : " "}
      `}
      style={{
        backgroundImage: `url(${bg})`,
      }}
    >
      {showLeft ? (
        <div className={`${styles.left}`}>
          <h2>{heading}</h2>
        </div>
      ) : null}
      <div className={`${styles.faqs}`}>
        <Accordion defaultActiveKey={[0]}>
          {data.map((data, i) => {
            return (
              <Accordion.Item key={i} eventKey={i} className={`${styles.accordionHeader}`}>
                <Accordion.Header>{data.question}</Accordion.Header>
                <Accordion.Body>
                  {data?.answer_html?.length > 0 ? (
                    <span
                      dangerouslySetInnerHTML={{
                        __html: data.answer_html,
                      }}
                    ></span>
                  ) : data?.answer?.length > 0 ? (
                    <span>{data.answer}</span>
                  ) : null}
                </Accordion.Body>
              </Accordion.Item>
            );
          })}
        </Accordion>
      </div>
    </div>
  );
}
