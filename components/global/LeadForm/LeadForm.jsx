import React, { useEffect, useState } from "react";
import styles from "./LeadForm.module.css";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Calendar } from "primereact/calendar";
import { Image, Spinner } from "react-bootstrap";
import { BiRightArrowAlt } from "react-icons/bi";
import { useStateContext } from "../../../context/StateContext";
import axios from "axios";
import { toast } from "react-hot-toast";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import moment from "moment";
import { useRouter } from "next/router";
import SecurePromiseStrip from "../SecurePromiseStrip/SecurePromiseStrip";
import uuid from "react-uuid";
import { getCookie } from "cookies-next";
import {
  COUNSELLOR_CLIENT_URL,
  COUNSELLOR_SERVER_URL,
  createNewLead,
  generatePassword,
  getCountrySource,
  storeUserCompareResults,
} from "../../../context/services";
import CounsellorStripRating from "../CounsellorStripRating/CounsellorStripRating";
import { useLocalStorage } from "../../../context/CustomHooks";
import { Dropdown } from "primereact/dropdown";
import { compareQueryParamsState } from "../../../store/compare/compareStore";
import { cvCounsellorDetailsAtom } from "../../../store/journeyStore";
import { useRecoilState } from "recoil";
import { useRef } from "react";
import { createLsqOpportunity, updatedLsqLeadDetails } from "../../../services/lead.service";

export default function LeadForm({ ctaText = "Get Expert Advice", selectedCountry, majors, onSubmitDone = () => {} }) {
  /*
  
  Context API State
  
  */
  const { formData, setFormData, isLoading, setIsLoading, ottaData, courseSpecializations } = useStateContext();

  const router = useRouter();

  /* 
  
  Local Storage State
  
  */
  const [leadData, setLeadData] = useLocalStorage("leadData", {});
  const [isLeadFilled, setIsLeadFilled] = useLocalStorage("isLeadFilled", false);
  const [leadFilledUserID, setLeadFilledUserID] = useLocalStorage("LeadFilledUserID", null);

  /* 
  
  Local State

  */
  const [selectedGender, setSelectedGender] = useState("");
  const [selectedDob, setSelectedDob] = useState("");

  /*  Compare States */
  const [userCompareQuery, setUserCompareQuery] = useRecoilState(compareQueryParamsState);
  const [cvCounsellorDetails, setCvCounsellorDetails] = useRecoilState(cvCounsellorDetailsAtom);
  const gtmButtonRef = useRef(null);

  /* 
  
  Form Validation Schema with Yup
  
  */

  const schema = yup.object().shape({
    name: yup.string().required("This field is required!").min(1, "Enter a valid name!").max(20, "Should not be maximum than 20 characters"),
    email: yup.string().email("Should be a valid email.").required("This field is required"),
    phone: yup.string().required("This field is required!").min(10, "Number should be 10 numbers long!").max(10, "Number should be 10 numbers long!"),
    dob: yup
      .string()
      .nullable()
      .required("Date of birth is required!")
      .test("DOB", "Please choose a valid date of birth.", (date) => moment().diff(moment(date), "years") >= 1),
    country: yup.string().required("This field is required!"),
    major: yup.string().required("This field is required!"),
  });

  const countries = [
    { id: 0, name: "Not Decided Yet", value: "Not Decided Yet" },
    { id: 1, name: "USA", value: "USA" },
    { id: 2, name: "Canada", value: "Canada" },
    { id: 3, name: "UK", value: "UK" },
    { id: 4, name: "Germany", value: "Germany" },
    { id: 5, name: "Australia", value: "Australia" },
    { id: 6, name: "New Zealand", value: "New Zealand" },
    { id: 7, name: "Ireland", value: "Ireland" },
  ];

  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
    getValues,
    getFieldState,
  } = useForm({
    defaultValues: {
      major: leadData?.major ? parseInt(leadData.major) : 1,
      dob: leadData?.dob ? new Date(leadData.dob) : null,
      country: leadData?.country ? leadData.country : null,
    },
    resolver: yupResolver(schema),
  });

  /* 
  
  Create CV Counsellor Customer
  
  */

  const createCVCounsellorCustomer = (userDetails, leadData, cv_id) => {
    axios
      .post(
        `${COUNSELLOR_SERVER_URL}/collegevidyaabroad/customer/create`,
        {
          uuid: userDetails.user_id,
          name: leadData.name,
          phone: leadData.phone,
          email: leadData.email,
          cv_id: cv_id,
          dob: new Date(leadData.age).toISOString().split("T")[0],
        },
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      )
      .then(() => {
        setCvCounsellorDetails({
          ...cvCounsellorDetails,
          cv_id: cv_id,
          redirectLink: `${COUNSELLOR_CLIENT_URL}/collegevidya/redirect/login?cv_id=${cv_id}&country=${leadData.country}`,
        });
      })
      .catch((err) => {
        if (err?.request?.status == "409") {
          setCvCounsellorDetails({
            ...cvCounsellorDetails,
            redirectLink: `${COUNSELLOR_CLIENT_URL}/collegevidya/redirect/login?exist=1`,
          });
        } else {
          console.error(err);
          toast.error(err?.message ? err?.message : "Something went wrong, please try again!", { duration: 2000 });
        }
      });
  };

  /* 


  Handle Form Submit


  */

  const onSubmit = (data) => {
    /* 
    
    Set Loading State

    */
    setIsLoading(true);

    setLeadData({
      ...data,
    });

    /* 
    
    Structure Lead Data
    
    */

    const cv_id = generatePassword(8);
    const user_id = cv_id.toString();
    const source = "College Vidya";
    const sub_source =
      getCookie("sub_source")?.length > 0
        ? getCookie("sub_source")
        : getCookie("source_campaign") === "collegevidya_online_website"
        ? "Online Website"
        : "Abroad Website";
    const source_campaign = getCookie("source_campaign");
    const campaign_name = getCookie("campaign_name");
    const ad_group = getCookie("ad_group_name");
    const ad_name = getCookie("ads_name");

    const leadData = {
      userId: user_id,
      gender: data.gender,
      name: data.name,
      phone: data.phone,
      email: data.email,
      age: data.dob,
      country: data.country,
      majorId: data.major,
      source: source,
      sub_source: sub_source,
      source_campaign: source_campaign,
      campaign_name: campaign_name,
      ad_group: ad_group,
      ad_name: ad_name,
    };

    /* 
    
    Create New Lead with API
    
    */
    createNewLead(leadData)
      .then(async () => {
        toast.success("Thank you for sharing your details. Our expert will get back to you shortly.");

        const major = majors.find((major) => major.id === parseInt(leadData.majorId));
        const country = countries.find((country) => country.name === leadData.country);
        const program = major.category_id === 1 ? "Bachelors" : major.category_id === 2 ? "Masters" : major.category_id === 3 ? "Diploma" : null;
        const budget = "Not Decided Yet";

        const opportunityRes = await createLsqOpportunity(
          leadData.name,
          leadData.email,
          leadData.phone,
          budget,
          country.name,
          program,
          major.name,
          source,
          sub_source,
          source_campaign,
          campaign_name,
          ad_group,
          ad_name
        );

        const lsqLeadData = {
          data: [
            {
              Attribute: "mx_Study_Destination",
              Value: country.name,
            },
            {
              Attribute: "mx_Programme",
              Value: program,
            },
            {
              Attribute: "mx_Specilazation",
              Value: major.name,
            },
          ],
        };

        storeUserCompareResults(
          user_id,
          major.category_id,
          0,
          0,
          major.id,
          0,
          country.id,
          null,
          opportunityRes.opportunityId,
          0,
          0,
          [country.id],
          opportunityRes.relatedProspectId
        )
          .then(async (userDetails) => {
            setUserCompareQuery({
              uuid: userDetails.user_id,
              categoryId: userDetails.category_id,
              courseId: userDetails.course_id,
              majorId: userDetails.majorId,
              specializationId: userDetails.specialization_id,
              countryId: userDetails.country_id,
              budgetLimit: userDetails.budget,
              ieltsScore: userDetails.ielts,
              opportunityId: opportunityRes.opportunityId,
              countries: userDetails?.countries ? [...userDetails.countries] : [],
              relatedProspectId: opportunityRes.relatedProspectId,
            });
            setLeadFilledUserID(user_id);
            const res = await updatedLsqLeadDetails(userDetails.user_id, lsqLeadData);
            createCVCounsellorCustomer(userDetails, leadData, user_id);
            window.location.href = `/compare/universities?uuid=${userDetails.user_id}`;
          })
          .finally(() => {
            onSubmitDone();
          });
      })
      .catch((error) => {
        console.error(error);
        setIsLoading(false);
        toast.dismiss();
        toast.error("That's strange! Something went wrong. Don't worry you can try again.");
      })
      .finally(() => {
        setIsLoading(false);
      });
  };

  /* 
  
  Handle Gender Select
  
  */
  const handleGenderSelect = (e) => {
    setSelectedGender(e.target.attributes[1].nodeValue.toLowerCase());
  };

  /* 
  
  Calculate Age from DOB

  */
  const calculateAge = (date) => {
    const age = moment().diff(moment(date), "years");
    return age;
  };

  /* 
  
  
  React Hook Form Set Values with Local Storage
  
  */
  useEffect(() => {
    Object.keys(leadData).forEach((key) => {
      if (key === "dob") setValue("dob", new Date(leadData["dob"]), { shouldValidate: true });
      else if (key === "major") setValue("major", parseInt(leadData[key]));
      else setValue(key, leadData[key], { shouldValidate: true });
    });
  }, []);

  /* 
  
  
   Gender Handler with Local Storage
  
  */
  useEffect(() => {
    setValue("gender", selectedGender);
  }, [selectedGender]);

  /* 
  
  Props Selected Country Handler  
  
  */
  useEffect(() => {
    switch (selectedCountry) {
      case "australia":
        setValue("country", "Australia");
        break;
      case "canada":
        setValue("country", "Canada");
        break;
      case "new-zealand":
        setValue("country", "New Zealand");
        break;
      case "uk":
        setValue("country", "United Kingdom");
        break;
      case "us":
        setValue("country", "USA");
        break;
      case "germany":
        setValue("country", "Germany");
        break;
      case "Ireland":
        setValue("country", "Ireland");
      default:
        setValue("country", "Not Decided Yet");
        break;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedCountry]);

  // Selected Major if major is only one

  useEffect(() => {
    if (majors.length === 1) {
      setValue("major", majors[0].id);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [majors]);

  return (
    <Form className={styles.form} onSubmit={handleSubmit(onSubmit)}>
      {/* 
      
      Gender 
      
      */}
      {/* <Form.Group className={`mb-3 d-flex justify-content-center gap-3 ${styles.genderGroup}`} controlId="leadFormFirstName">
        <div
          className={styles.genderSelect}
          name="male"
          onClick={handleGenderSelect}
          style={{
            backgroundColor: selectedGender === "male" ? "#0f56e5" : "white",
            color: selectedGender === "male" ? "white" : "black",
          }}
        >
          <Image src="/male.png" alt="Male Icon" loading="lazy" className={styles.genderImage} />
          Male
        </div>
        <div
          className={styles.genderSelect}
          name="female"
          onClick={handleGenderSelect}
          style={{
            backgroundColor: selectedGender === "female" ? "#0f56e5" : "white",
            color: selectedGender === "female" ? "white" : "black",
          }}
        >
          <Image src="/female.png" alt="Female Icon" loading="lazy" className={styles.genderImage} />
          Female
        </div>
      </Form.Group> */}

      {/* 
      
      Full Name

      */}
      <Form.Group className="mb-3" controlId="leadFormFirstName">
        <Form.Label>
          Full Name <span className="text-danger">*</span>
        </Form.Label>
        <Form.Control type="text" name="name" placeholder="What is your name?" {...register("name", { required: true })} />
        {errors.name && <Form.Text className="d-block text-danger mt-2">{errors.name?.message}</Form.Text>}
      </Form.Group>

      {/* 
      
      Email
      
      */}
      <Form.Group className="mb-3" controlId="leadFormEmail">
        <Form.Label>
          Email Address <span className="text-danger">*</span>
        </Form.Label>
        <Form.Control type="email" name="email" placeholder="What is your email?" {...register("email", { required: true })} />
        {errors.email && <Form.Text className="d-block text-danger mt-2">{errors.email?.message}</Form.Text>}
      </Form.Group>

      {/* 
      
      Phone Number
      
      */}
      <Form.Group className="mb-3" controlId="leadFormNumber">
        <Form.Label>
          Phone Number <span className="text-danger">*</span>
        </Form.Label>
        <Form.Control type="tel" name="phone" placeholder="What is your number?" maxLength="10" {...register("phone", { required: true })} />
        {errors.phone && <Form.Text className="d-block text-danger mt-2">{errors.phone?.message}</Form.Text>}
      </Form.Group>

      <Row className={styles.formRow}>
        <Col>
          {/*  
          
          Date of Birth
          
          */}
          <Form.Group controlId="leadFormDate">
            <Form.Label>
              Date of Birth <span className="text-danger">*</span>
            </Form.Label>
            <span className="position-relative">
              <span
                className="position-absolute end-0 bg-light-grey  p-1 rounded-2 fs-md me-3 mb-0"
                style={{
                  zIndex: 1,
                  bottom: "-5px",
                }}
              >
                {isNaN(calculateAge(getValues("dob"))) ? "Age" : `${calculateAge(getValues("dob"))} Years`}
              </span>
              <Calendar
                className={styles.pCalendarInput}
                showOnFocus={false}
                value={getValues("dob")}
                visible={false}
                placeholder="DD/MM/YYYY"
                inputMode={"numeric"}
                name="dob"
                onChange={(e) =>
                  setValue("dob", e.value, {
                    shouldValidate: true,
                  })
                }
                dateFormat="dd/mm/yy"
                mask="99/99/9999"
              />
            </span>
            {errors.dob && <Form.Text className="d-block text-danger mt-2">{errors.dob?.message}</Form.Text>}
          </Form.Group>
        </Col>
        <Col>
          {/* 
          
          Country
          
          */}
          <Form.Label>Country</Form.Label>
          <Dropdown
            value={getValues("country")}
            options={countries}
            onChange={(e) => {
              setValue("country", e.value, {
                shouldValidate: true,
              });
            }}
            optionLabel="name"
            optionValue="name"
            placeholder="Select a Country"
            className={`bg-white rounded-3 py-1 mb-3 w-100`}
          />

          {errors.country && <Form.Text className="d-block text-danger mt-2">{errors.country?.message}</Form.Text>}
        </Col>
      </Row>

      {/* 
      
      Major
      
      */}
      <Form.Group className="d-flex flex-column">
        <Form.Label>Major</Form.Label>
        <Dropdown
          value={getValues("major")}
          options={majors}
          onChange={(e) => {
            setValue("major", e.value, {
              shouldValidate: true,
            });
          }}
          optionLabel="identifier"
          optionValue="id"
          placeholder="Select a Major"
          className={`bg-white rounded-3 py-1 mb-3`}
          filter={true}
          filterBy="identifier"
        />

        <Form.Text className="text-danger">{errors.course?.message}</Form.Text>
      </Form.Group>
      {/* 
      
      Submit Button
      
      */}

      <div className="d-grid mb-3">
        <Button variant="primary" type="submit" className={styles.submitBtn}>
          {ctaText}{" "}
          {isLoading ? (
            <Spinner animation="border" role="status" size="sm">
              <span className="visually-hidden ">Loading...</span>
            </Spinner>
          ) : (
            <BiRightArrowAlt color="white" size={20} />
          )}
        </Button>
      </div>
      <CounsellorStripRating />
    </Form>
  );
}
