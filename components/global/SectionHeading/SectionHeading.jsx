import React from "react";

export default function SectionHeading({
  heading,
  subHeading,
  className = "d-flex flex-column text-center justify-content-center align-items-center my-5 ",
  headingClass = " fs-1 fw-semibold text-dark-gray",
  subHeadingClass = "m-0  mb-1 fs-lg w-md-75 ",
}) {
  return (
    <div className={`px-2 ${className} `}>
      <h2 className={`${headingClass}`} dangerouslySetInnerHTML={{ __html: heading }}></h2>
      <p className={`${subHeadingClass}`} dangerouslySetInnerHTML={{ __html: subHeading }}></p>
    </div>
  );
}
