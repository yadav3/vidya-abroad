import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import Image from "next/image";
import { BsFillTelephoneFill } from "react-icons/bs";
import Link from "next/link";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import styles from "./CvCounsellorPopup.module.scss";
import { useStateContext } from "../../../context/StateContext";

import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, EffectFlip, Pagination, Navigation } from "swiper";

// Import Swiper styles
import "swiper/css";
import "swiper/css/effect-flip";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { useRouter } from "next/router";
import { cvCounsellorDetailsAtom } from "../../../store/journeyStore";
import { useRecoilState, useRecoilValue } from "recoil";
import { JourneyRemainingQuestionModalAtom, JourneyRemainingQuestionModalStateAtom } from "../../../store/journeyFormStore";
import { compareQueryParamsState } from "../../../store/compare/compareStore";
import { toast } from "react-hot-toast";

export default function CvCounsellorPopup({ children }) {
  const [show, setShow] = useState(false);
  const router = useRouter();
  const { ottaData } = useStateContext();
  const [cvCounsellorDetails, setCvCounsellorDetails] = useRecoilState(cvCounsellorDetailsAtom);

  const userCompareQuery = useRecoilValue(compareQueryParamsState);
  const [showJourneyRemainingQuestionModal, setShowJourneyRemainingQuestionModal] = useRecoilState(JourneyRemainingQuestionModalAtom);
  const [questionsModal, setQuestionsModal] = useRecoilState(JourneyRemainingQuestionModalStateAtom);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const handleRedirectToCvCounsellor = () => {
    if (cvCounsellorDetails?.redirectLink?.length > 0) {
      router.push(cvCounsellorDetails.redirectLink);
    } else {
      toast.error("Something went wrong, please try again later.");
    }
    handleClose();
  };

  return (
    <div>
      <span onClick={handleShow}>{children}</span>

      <Modal show={show} onHide={handleClose} centered size="md">
        <Modal.Header closeButton></Modal.Header>
        <Modal.Body>
          <Row className="gap-2 gap-md-0">
            <Col md={12}>
              <Swiper
                effect={"flip"}
                grabCursor={true}
                // pagination={true}
                // navigation={true}
                autoplay={{
                  delay: 2500,
                  disableOnInteraction: false,
                }}
                modules={[EffectFlip, Autoplay, Pagination, Navigation]}
                className="cv-counsellor-redirect-swiper"
              >
                <SwiperSlide>
                  <Image src={"/counsellors/dhirendra.png"} width={300} height={300} alt="Dhirendra Counsellor" />
                </SwiperSlide>
                <SwiperSlide>
                  <Image src={"/counsellors/manish.png"} width={300} height={300} alt="Manish Counsellor" />
                </SwiperSlide>
                <SwiperSlide>
                  <Image src={"/counsellors/ramiz.png"} width={300} height={300} alt="Ramiz Counsellor" />
                </SwiperSlide>
                <SwiperSlide>
                  <Image src={"/counsellors/abhishek.png"} width={300} height={300} alt="Abhishek Counsellor" />
                </SwiperSlide>
              </Swiper>

              <p className="text-center text-cvblue mb-2">30,000+ Students were Happy After their calls!</p>
              <p className="text-center mb-4">
                {/* {ottaData.name.length > 0 ? (
                  <>
                    Hey, <span className="text-danger">{ottaData.name}</span>
                  </>
                ) : null}{" "} */}
                Make College decisions like a pro 😎
              </p>
            </Col>
            <Col md={6}>
              <button
                className={`${styles.btn}`}
                onClick={() => {
                  handleRedirectToCvCounsellor();
                }}
                // disabled={!cvCounsellorDetails.redirectLink}
              >
                <span className={`position-absolute top-0 start-50 fs-sm bg-cvblue text-white px-1 w-max absolute-center  myshine `}>
                  Limited Time Offer
                </span>
                Connect with Expert
              </button>
            </Col>
            <Col md={6}>
              <Link href="tel:18003099018">
                <a id="OUTBOUND-LINK" className={`${styles.btn} ${styles.btn2} d-flex align-items-center gap-2 `}>
                  <BsFillTelephoneFill /> Call Now <span className={`position-absolute top-0 end-0 fs-sm bg-bright-green px-1  `}>Toll Free</span>
                </a>
              </Link>
            </Col>
          </Row>
        </Modal.Body>
      </Modal>
    </div>
  );
}
