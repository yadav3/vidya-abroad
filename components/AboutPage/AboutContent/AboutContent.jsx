import React from "react";
import styles from "./AboutContent.module.scss";

export default function AboutContent() {
  return (
    <div className={`${styles.aboutContent}`}>
      <p className=" text-center">
        College Vidya is{" "}
        <strong>India&apos;s leading online education portal</strong>{" "}
        particularly focusing on{" "}
        <strong>online and distance learning programs</strong>. At the same
        time, we have assisted <strong>thousands of aspirants</strong> in
        finding suitable courses and the best universities. <br></br>
        <br></br>
        College Vidya enjoys its strong reputation with universities built over
        a period of time by generating a significant amount of business with
        unbeatable services to students. Simultaneously, we have earned
        substantial popularity and trust amongst the student community{" "}
        <strong>seeking distance and online learning programs.</strong>
        <br></br>
        <br></br>
        College Vidya Abroad supports prospect learners in their quest to study
        at the Best Global Universities. Our team of global study experts guides
        you at every step of the way - from choosing the right university &amp;
        right program of study to helping you with your application and
        financing counseling, facilitating visa services for you, and thus
        landing you to your highly sought-after study abroad destination.
        <br></br>
        <br></br>
        College Vidya&apos;s key aim is to bring aspirants and universities on a
        common platform to offer a win-win situation for both. In short, we
        provide <strong>360-degree solutions</strong> and other logistic support
        to both students and universities.
      </p>
    </div>
  );
}
