import React from "react";
import Image from "next/image";
import styles from "./AboutTeam.module.scss";
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Pagination, Navigation } from "swiper";

const data = [
  {
    name: "Mayank Gupta",
    image: "/team/mayank_sir.png",
    designation: "CEO",
  },
  {
    name: "Rohit Gupta",
    image: "/team/Rohit_sir.png",
    designation: "COO",
  },
  {
    name: "Ashok Joshi",
    image: "/team/ashok_sir.png",
    designation: "Sr. V.P Strategic Alliance",
  },
  {
    name: "Sarthak Garg",
    image: "/team/Sarthak_sir.png",
    designation: "Director",
  },
  {
    name: "Siddharth Stephen",
    image: "/team/Sidhartha_sir.png",
    designation: "Sr. Manager Corporate Strategy",
  },
  {
    name: "Shalini Sinha",
    image: "/team/shalini.png",
    designation: "DGM Human Resource",
  },
];

export default function AboutTeam() {
  return (
    <div className={`${styles.aboutTeamContainer}`}>
      <div className={`${styles.teamPattern}`}>
        <Image src="/team/bg.png" alt="Team Background Pattern" width={357} height={398} />
      </div>
      <div className={`${styles.teamPattern2}`}>
        <Image src="/team/bg1.png" alt="Team Background Pattern" width={357} height={398} className={`${styles.teamPattern2}`} />
      </div>
      <h2 className={`${styles.aboutTeamHeading}`}>Meet our core Team</h2>
      <div className={`${styles.teamGrid}`}>
        {data.map((team, i) => (
          <MemberCard key={`${team.name}`} {...team} />
        ))}
      </div>
      <div className={`${styles.teamSlider}`}>
        <Swiper
          slidesPerView={1}
          spaceBetween={30}
          loop={true}
          pagination={{
            clickable: true,
          }}
          centeredSlides={true}
          navigation={true}
          modules={[Pagination, Navigation]}
          className="aboutTeamSlider"
        >
          {data.map((team, i) => (
            <SwiperSlide key={`${team.name}`}>
              <MemberCard {...team} />
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </div>
  );
}

export function MemberCard({ image, name, designation }) {
  return (
    <div className={`${styles.memberCard}`}>
      <Image src={image} alt={name} width={300} height={300} />
      <div className={`${styles.memberDetails}`}>
        <h2>{name}</h2>
        <p>{designation}</p>
      </div>
    </div>
  );
}
