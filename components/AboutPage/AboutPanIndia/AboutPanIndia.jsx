import React from "react";
import styles from "./AboutPanIndia.module.scss";

export default function AboutPanIndia() {
  return (
    <div className={styles.container}>
      <div className={styles.left}>
        <h2>
          Assisting <span>Students</span> Across Pan India
        </h2>
      </div>
      <div className={styles.right}>
        <p className=" text-center">
          College Vidya is{" "}
          <strong>India&apos;s leading online education portal</strong>{" "}
          particularly focusing on{" "}
          <strong>online and distance learning programs</strong>. At the same
          time, we have assisted <strong>thousands of aspirants</strong> in
          finding suitable courses and the best universities. <br></br>
          <br></br>
          College Vidya Abroad supports prospect learners in their quest to
          study at the Best Global Universities. Our team of global study
          experts guides you at every step of the way - from choosing the right
          university &amp; right program of study to helping you with your
          application and financing counseling, facilitating visa services for
          you, and thus landing you to your highly sought-after study abroad
          destination.
        </p>
      </div>
    </div>
  );
}
