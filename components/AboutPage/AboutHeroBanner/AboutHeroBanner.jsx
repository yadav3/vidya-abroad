import React from "react";
import Image from "next/image";
import Marquee from "react-fast-marquee";
import styles from "./AboutHeroBanner.module.scss";
import ReactTypingEffect from "react-typing-effect";
import { useStateContext } from "../../../context/StateContext";
import ExpertHeroBannerImage from "../../../public/bg/expert-hero-banner3.png";

export default function AboutHeroBanner() {
  const { setShowForm } = useStateContext();
  return (
    <div className={styles.container}>
      <div className={styles.left}>
        <h2>
          Your
          <span>
            <ReactTypingEffect text={["Success", "Aspiration", "Dream"]} typingDelay={0} speed={200} />
          </span>
          is our <strong>Mission</strong>
        </h2>
        <button onClick={() => (window.location.href = "/suggest-me-a-university")}>Connect with Us</button>
      </div>
      <div className={styles.right}>
        <Marquee speed={20} gradient={false} direction="left">
          <Image
            alt="College Vidya Abroad Experts"
            className={styles.expertImage}
            src={ExpertHeroBannerImage}
            objectFit="contain"
            height={1000}
            width={1800}
          />
          <Image
            alt="College Vidya Abroad Experts"
            className={styles.expertImage}
            src={ExpertHeroBannerImage}
            objectFit="contain"
            height={1000}
            width={1800}
          />
        </Marquee>
      </div>
    </div>
  );
}
