import React from "react";
import styles from "./AboutStats.module.scss";
import { useState } from "react";
import { HiChevronDoubleDown } from "react-icons/hi";
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import { Doughnut } from "react-chartjs-2";
import { useRef } from "react";
import { useEffect } from "react";

ChartJS.register(ArcElement, Tooltip, Legend);

export default function AboutStats() {
  const [showLeft, setShowLeft] = useState(false);
  const [showRight, setShowRight] = useState(false);

  const boxMobile1Ref = useRef();
  const boxMobile2Ref = useRef();
  const containerRef = useRef();

  const handleBoxOpen = () => {
    setShowLeft((show) => !show);
    if (containerRef.current) containerRef.current.scrollIntoView({ behavior: "smooth" });
  };

  const handleBoxLeftMobile = () => {
    setShowLeft((show) => !show);
    if (boxMobile1Ref.current) boxMobile1Ref.current.scrollIntoView({ behavior: "smooth" });
  };

  const handleBoxRightMobile = () => {
    setShowRight((show) => !show);
    if (boxMobile2Ref.current) boxMobile2Ref.current.scrollIntoView({ behavior: "smooth" });
  };

  useEffect(() => {
    const donut1 = document.querySelector(".about-stats-donut1");
    const donut2 = document.querySelector(".about-stats-donut2");
    if (donut1) donut1.style.height = "200px ";
    if (donut2) donut2.style.height = "200px ";
  }, []);

  return (
    <>
      <div className={styles.container} ref={containerRef}>
        <button className={styles.boxOpenBtn} onClick={handleBoxOpen}>
          {showLeft ? "Show Less" : "Know More"}
          <HiChevronDoubleDown
            style={{
              transform: showLeft ? "rotate(180deg)" : "rotate(0)",
            }}
          />
        </button>
        <div className={styles.left}>
          <div className={styles.stats}>
            <h2 >What we do?</h2>
            <div
              className={styles.dynamicBox}
            >
              {showLeft && (
                <Doughnut
                  options={{
                    plugins: {
                      legend: {
                        labels: {
                          color: "white",
                        },
                      },
                    },
                  }}
                  className="about-stats-donut1"
                  data={{
                    labels: ["4200 Telephonic Counseling", "800 Video Counseling"],
                    datasets: [
                      {
                        label: "# of Votes",
                        data: [4200, 800],
                        backgroundColor: ["#b0e8ff", "#56cdff"],
                        borderColor: ["white", "white"],
                        borderWidth: 3,
                      },
                    ],
                  }}
                />
              )}

              <p className={styles.dynamicBoxPara}>
                <span>5000+</span> Students get Counseled everyday
              </p>

              <hr />
            </div>
          </div>
        </div>
        <div className={styles.right}>
          <div className={styles.stats}>
            <h2>How we do it?</h2>
            <div
              className={styles.dynamicBox}
            >
              {showLeft && (
                <Doughnut
                  options={{
                    plugins: {
                      legend: {
                        labels: {
                          color: "white",
                        },
                      },
                    },
                  }}
                  height="200px"
                  width="200px"
                  className="about-stats-donut2"
                  data={{
                    labels: ["300 Inhouse Counselors", "600 External Counselors"],
                    datasets: [
                      {
                        label: "# of Votes",
                        data: [300, 600],
                        backgroundColor: ["#56cdff", "#b0e8ff"],
                        borderColor: ["white", "white"],
                        borderWidth: 3,
                      },
                    ],
                  }}
                />
              )}

              <p className={styles.dynamicBoxPara}>
                <span>900+</span> Counsellors
              </p>

              <hr />
            </div>
          </div>
        </div>
      </div>
      <div className={styles.containerMobile}>
        <div className={styles.left} ref={boxMobile1Ref}>
          <div className={styles.stats}>
            <h2>What we do?</h2>
            <div
              className={styles.dynamicBox}
            >
              {showLeft && (
                <Doughnut
                  options={{
                    plugins: {
                      legend: {
                        labels: {
                          color: "white",
                        },
                      },
                    },
                  }}
                  data={{
                    labels: ["4200 Telephonic Counseling", "800 Video Counseling"],
                    datasets: [
                      {
                        label: "# of Votes",
                        data: [4200, 800],
                        backgroundColor: ["#b0e8ff", "#56cdff"],
                        borderColor: ["white", "white"],
                        borderWidth: 3,
                      },
                    ],
                  }}
                />
              )}

              <p className={styles.dynamicBoxPara}>
                <span>5000+</span> Students get Counseled everyday
              </p>

              <hr />

              <button onClick={handleBoxLeftMobile}>
                {showLeft ? "Show Less" : "Know More"}
                <HiChevronDoubleDown
                  style={{
                    transform: showLeft ? "rotate(180deg)" : "rotate(0)",
                  }}
                />
              </button>
            </div>
          </div>
        </div>
        <div className={styles.right} ref={boxMobile2Ref}>
          <div className={styles.stats}>
            <h2>How we do it?</h2>
            <div
              className={styles.dynamicBox}
            >
              {showRight && (
                <Doughnut
                  options={{
                    plugins: {
                      legend: {
                        labels: {
                          color: "white",
                        },
                      },
                    },
                  }}
                  height="200px"
                  width="200px"
                  data={{
                    labels: ["300 Inhouse Counselors", "600 External Counselors"],
                    datasets: [
                      {
                        label: "# of Votes",
                        data: [300, 600],
                        backgroundColor: ["#56cdff", "#b0e8ff"],
                        borderColor: ["white", "white"],
                        borderWidth: 3,
                      },
                    ],
                  }}
                />
              )}

              <p className={styles.dynamicBoxPara}>
                <span>900+</span> Counsellors
              </p>

              <hr />

              <button onClick={handleBoxRightMobile}>
                {showRight ? "Show Less" : "Know More"}
                <HiChevronDoubleDown
                  style={{
                    transform: showRight ? "rotate(180deg)" : "rotate(0)",
                  }}
                />
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
