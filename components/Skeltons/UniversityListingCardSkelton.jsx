import React from "react";
import styles from "./Skelton.module.scss";
import { Skeleton } from "primereact/skeleton";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
export default function UniversityListingCardSkelton() {
  return (
    <div className="w-100">
      <div>
        <Row>
          <Col className="col-2">
            <Skeleton width="100%" height="8rem" className="mb-2"></Skeleton>
          </Col>
          <Col>
            <Skeleton width="100%" height="8rem" className="mb-2"></Skeleton>
          </Col>
        </Row>
        <Skeleton width="100%" height="1rem" className="mb-2"></Skeleton>
        <Skeleton width="100%" height="1rem" className="mb-2"></Skeleton>
      </div>
      <div>
        <Row>
          <Col className="col-2">
            <Skeleton width="100%" height="8rem" className="mb-2"></Skeleton>
          </Col>
          <Col>
            <Skeleton width="100%" height="8rem" className="mb-2"></Skeleton>
          </Col>
        </Row>
        <Skeleton width="100%" height="1rem" className="mb-2"></Skeleton>
        <Skeleton width="100%" height="1rem" className="mb-2"></Skeleton>
      </div>
      <div>
        <Row>
          <Col className="col-2">
            <Skeleton width="100%" height="8rem" className="mb-2"></Skeleton>
          </Col>
          <Col>
            <Skeleton width="100%" height="8rem" className="mb-2"></Skeleton>
          </Col>
        </Row>
        <Skeleton width="100%" height="1rem" className="mb-2"></Skeleton>
        <Skeleton width="100%" height="1rem" className="mb-2"></Skeleton>
      </div>
      <div>
        <Row>
          <Col className="col-2">
            <Skeleton width="100%" height="8rem" className="mb-2"></Skeleton>
          </Col>
          <Col>
            <Skeleton width="100%" height="8rem" className="mb-2"></Skeleton>
          </Col>
        </Row>
        <Skeleton width="100%" height="1rem" className="mb-2"></Skeleton>
        <Skeleton width="100%" height="1rem" className="mb-2"></Skeleton>
      </div>
    </div>
  );
}
