import React from "react";
import Col from "react-bootstrap/Col";
import uuid from "react-uuid";
import { VideoCard } from "./VideoCard/VideoCard";
import NoResultFound from "../global/NoResultFound/NoResultFound";

export function AllVideos({ allVideos, filters, setFilters }) {
  return (
    <>
      {filters.authorId && allVideos.length > 0 ? (
        <span className="fs-lg mb-4 text-center text-sm-start">
          {allVideos.length === 1 ? "Video" : "Videos"} by{" "}
          <span className="text-cvblue">{allVideos.length > 0 ? allVideos[0].authors.name : null}</span>
        </span>
      ) : null}
      {allVideos?.length > 0 ? (
        allVideos.map((video) => {
          return (
            <Col sm={6} md={4} lg={3} key={uuid()}>
              <VideoCard video={video} filters={filters} setFilters={setFilters} />
            </Col>
          );
        })
      ) : (
        <NoResultFound heading="No Videos Found" showCtaText={false} />
      )}
    </>
  );
}
