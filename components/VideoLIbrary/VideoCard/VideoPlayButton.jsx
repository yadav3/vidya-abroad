import React from "react";
import styles from "./VideoCard.module.scss";
import { BsPlayCircle } from "react-icons/bs";
import VideoPlayerModal from "../VideoPlayerModal/VideoPlayerModal";

export function VideoPlayButton({ video }) {
  return (
    <div
      className={`${styles.videoCard__overlay__playButton} cursor-pointer position-absolute top-50 start-50 translate-middle text-white circle-pulse-ring `}
    >
      <BsPlayCircle size={40} />
    </div>
  );
}
