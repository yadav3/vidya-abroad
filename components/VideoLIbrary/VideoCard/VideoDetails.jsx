import React from "react";
import Image from "next/image";
import styles from "./VideoCard.module.scss";
import { getAWSImagePath } from "../../../context/services";
import { formateSecondsToMinutes } from "../../../utils/misc";
import { Tooltip } from "react-tippy";

export function VideoDetails({ video, filters, setFilters }) {
  const handleSelectCategory = (categoryId) => {
    setFilters((prev) => ({ ...prev, categoryId: categoryId, page: 1 }));
  };

  const handleSelectAuthor = (authorId) => {
    setFilters((prev) => ({ ...prev, authorId: authorId, page: 1 }));
    window.scrollTo(0, 0);
  };

  function highlightSearchTerm(text, searchTerm) {
    if (!searchTerm || searchTerm === "") {
      return text;
    }

    const regex = new RegExp(searchTerm, "gi");
    const matchIndices = [...text.matchAll(regex)].map((match) => match.index);

    if (matchIndices.length === 0) {
      return text;
    }

    let highlightedText = "";
    let currentIndex = 0;

    for (const index of matchIndices) {
      highlightedText += `${text.slice(currentIndex, index)}<span class="text-cvblue fw-semibold">${text.slice(
        index,
        index + searchTerm.length
      )}</span>`;
      currentIndex = index + searchTerm.length;
    }

    highlightedText += text.slice(currentIndex);

    return highlightedText;
  }

  return (
    <div className={`${styles.videoCard__overlay__details}  position-absolute bottom-0 start-0 end-0 text-white m-3`}>
      {/* <div className={`${styles.videoCard__overlay__length} fs-sm text-truncate fw-normal`}>
        <span>{formateSecondsToMinutes(video.videoLength)}</span>
      </div> */}
      <div className={`${styles.videoCard__overlay__title} fs-lg  text-truncate fw-normal`}>
        <Tooltip title={video.title} position="top" arrow={true} size="small">
          <span
            dangerouslySetInnerHTML={{
              __html: highlightSearchTerm(video.title, filters?.search),
            }}
          ></span>
        </Tooltip>
        <span>{video.videoLength}</span>
      </div>
      <div className={`${styles.videoCard__overlay__author} mt-1 d-flex align-items-center gap-2 fs-sm`}>
        <small className="text-white cursor-pointer">
          by{" "}
          <span
            onClick={() => handleSelectAuthor(video.authors.id)}
            dangerouslySetInnerHTML={{
              __html: highlightSearchTerm(video.authors.name, filters?.search),
            }}
          ></span>{" "}
          •{" "}
          <span
            onClick={() => handleSelectCategory(video.video_library_categories.id)}
            dangerouslySetInnerHTML={{
              __html: highlightSearchTerm(video.video_library_categories.title, filters?.search),
            }}
          ></span>
        </small>
      </div>
    </div>
  );
}
