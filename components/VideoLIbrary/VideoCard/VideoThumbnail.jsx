import React from "react";
import Image from "next/image";
import styles from "./VideoCard.module.scss";
import { getAWSImagePath } from "../../../context/services";

export function VideoThumbnail({ video }) {
  return (
    <div className={`${styles.videoCard__thumbnail} position-relative`}>
      <Image src={getAWSImagePath(video.thumbnailUrl)} alt="video thumbnail" width={400} height={600} layout="responsive" objectFit="cover" />
    </div>
  );
}
