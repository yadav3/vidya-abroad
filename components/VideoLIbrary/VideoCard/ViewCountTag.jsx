import React from "react";
import styles from "./VideoCard.module.scss";
import { BsEye } from "react-icons/bs";
import { formatNumber } from "../../../utils/misc";

export function ViewCountTag({ video }) {
  return (
    <div
      className={`${styles.videoCard__overlay__viewCount} 
                position-absolute top-0 start-0 text-white m-2  px-2 py-1 rounded-3 fs-sm
        `}
    >
      <BsEye size={15} className="me-2" />
      <span>{formatNumber(video.views)}</span>
    </div>
  );
}
