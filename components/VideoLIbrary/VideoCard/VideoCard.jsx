import React from "react";
import styles from "./VideoCard.module.scss";
import { BsEyeFill } from "react-icons/bs";
import { VideoThumbnail } from "./VideoThumbnail";
import { VideoOverlay } from "./VideoOverlay";

export function VideoCard({ video, filters, setFilters }) {
  return (
    <div className={`${styles.videoCard} position-relative mb-3 overflow-hidden shadow-sm`}>
      <VideoOverlay video={video} filters={filters} setFilters={setFilters} />
      <VideoThumbnail video={video} />
    </div>
  );
}
