import React from "react";
import styles from "./VideoCard.module.scss";
import { VideoPlayButton } from "./VideoPlayButton";
import { ViewCountTag } from "./ViewCountTag";
import { VideoDetails } from "./VideoDetails";
import { VideoCardActions } from "./VideoCardActions";
import VideoPlayerModal from "../VideoPlayerModal/VideoPlayerModal";

export function VideoOverlay({ video, filters, setFilters }) {
  return (
    <div
      className={`${styles.videoCard__overlay} position-absolute top-0 start-0 end-0 bottom-0 `}
      style={{
        zIndex: 1,
      }}
    >
      {/* gradient toward up black to fade */}
      <div className={`${styles.videoCard__overlay__gradient} position-absolute start-0 end-0 bottom-0`}></div>
      <ViewCountTag video={video} />
      <VideoCardActions video={video} />
      <VideoPlayerModal video={video}>
        <VideoPlayButton video={video} />
      </VideoPlayerModal>
      <VideoDetails video={video} filters={filters} setFilters={setFilters} />
    </div>
  );
}
