import React from "react";
import styles from "./VideoCard.module.scss";
import { BsShareFill } from "react-icons/bs";
import { Tooltip } from "react-tippy";
import { LinkedinIcon, LinkedinShareButton, WhatsappIcon, WhatsappShareButton } from "next-share";

export function VideoCardActions({ video }) {
  return (
    <div className={`${styles.videoCard__overlay__actions} position-absolute top-0 end-0 m-3`}>
      {/* share  */}
      <div className={`${styles.videoCard__overlay__actions__share} cursor-pointer text-white d-flex justify-content-center align-items-center`}>
        <Tooltip
          title="Share"
          interactive={true}
          html={<div className="d-flex  gap-2">
            <LinkedinShareButton url={video.url} title={video.title}>
              <LinkedinIcon size={32} round={true} />
            </LinkedinShareButton>
            <WhatsappShareButton url={video.url} title={video.title}>
              <WhatsappIcon size={32} round={true} />
            </WhatsappShareButton>
          </div>}
          position="bottom"
          trigger="mouseenter"
        >
          <div className={`${styles.videoCard__overlay__actions__share__icon} d-flex justify-content-center align-items-center`}>
            <BsShareFill size={15} />
          </div>
        </Tooltip>
      </div>
    </div>
  );
}
