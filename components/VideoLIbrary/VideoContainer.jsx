import React, { useRef } from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import VideoLIbraryFilters from "./Filters/VideoLIbraryFilters";
import { FooterDivider } from "../global/FooterContainer/FooterDivider";
import { useState } from "react";
import { getAllVideos } from "../../services/videoLibrary.service";
import { useEffect } from "react";
import { useOnScreen } from "../../context/CustomHooks";
import { AllVideos } from "./AllVideos";
import { ClearFilterFloatingButton } from "./Filters/ClearFilterFloatingButton";

export default function VideoContainer({ videosData, categories }) {
  const [filters, setFilters] = useState({
    page: 1,
    limit: 4,
    sort: "id",
    order: "asc",
    categoryId: "",
    authorId: "",
    search: "",
  });

  const [allVideos, setAllVideos] = useState(videosData.videos);

  const videoContainerRef = useRef(null);
  const isVideoContainerInView = useOnScreen(videoContainerRef);

  const handleLoadMore = () => {
    const lastPage = videosData.totalPages;
    if (filters.page < lastPage || filters.page === videosData.totalPages) {
      setFilters((prev) => ({ ...prev, page: prev.page + 1 }));
    }
  };

  const handleClearFilters = () => {
    setFilters({
      page: 1,
      limit: 4,
      sort: "id",
      order: "asc",
      categoryId: 0,
      authorId: "",
      search: "",
    });
    window.scrollTo(0, 0);
  };

  useEffect(() => {
    getAllVideos(filters.page, filters.limit, filters.sort, filters.order, filters.categoryId, filters.authorId, filters.search).then((data) => {
      setAllVideos((prev) => [...prev, ...data.videos]);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filters.page]);

  useEffect(() => {
    getAllVideos(filters.page, filters.limit, filters.sort, filters.order, filters.categoryId, filters.authorId, filters.search).then((data) => {
      setAllVideos((prev) => [...data.videos]);
    });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filters.authorId, filters.categoryId, filters.limit, filters.order, filters.search, filters.sort]);

  return (
    <Row ref={videoContainerRef}>
      <Col>
        <Row>
          <Col>
            <VideoLIbraryFilters filters={filters} setFilters={setFilters} categories={categories} allVideos={allVideos} />
          </Col>
        </Row>
        <Row>
          <AllVideos allVideos={allVideos} filters={filters} setFilters={setFilters} />
        </Row>
        {(filters.page < videosData.totalPages && allVideos.length > 0) ||
        (filters.limit < videosData.totalVideos && filters.page === videosData.totalPages) ? (
          <Row>
            <Col>
              <div className="d-flex align-items-center justify-content-center">
                <button onClick={handleLoadMore} className="cursor-pointer bg-cvblue border-0 text-white rounded-5 px-5 py-3 fs-lg mt-3">
                  Load More
                </button>
              </div>
            </Col>
          </Row>
        ) : null}
        {/* floating button to clear filters */}
        {filters.authorId !== "" || filters.categoryId !== "" || filters.search !== "" ? (
          <ClearFilterFloatingButton isVideoContainerInView={isVideoContainerInView} handleClearFilters={handleClearFilters} />
        ) : null}
        <FooterDivider />
      </Col>
    </Row>
  );
}
