import React from "react";
import styles from "./GetInTouchSection.module.scss";
import Link from "next/link";

export default function GetInTouchSection() {
  const contactCards = [
    {
      title: "Talk to an Expert",
      subTitle: "Call Us On: 18003099018",
      href: "tel:18003099018",
      background: "#ffe5da6e",
      color: "#ff9d73",
    },
    {
      title: "You can also Mail",
      subTitle: "Info@collegevidyaabroad.com",
      href: "mailto:Info@collegevidyaabroad.com",
      background: "#E3E5FF6e",
      color: "#727cfd",
    },
    {
      title: "Schedule a Call",
      subTitle: "Schedule in 2 Mins",
      href: "#video-library-form",
      background: "#E3FFFC6e",
      color: "#5bb9af",
    },
  ];

  return (
    <div className="d-flex align-items-center justify-content-center flex-wrap w-100 gap-3 mb-5">
      {contactCards.map((card, index) => (
        <ContactCard key={index} {...card} />
      ))}
    </div>
  );
}

function ContactCard({ title, subTitle, href, background, color }) {
  return (
    <div
      className={`${styles.card} d-flex flex-column align-items-center justify-content-center p-3 px-4 rounded-4 shadow-sm myshine overflow-hidden text-center`}
      style={{
        backgroundColor: background,
        color: color,
        border: `1px solid ${color}`,
      }}
    >
      <Link href={href}>
        <a>
          <h5 className="text-center m-0 ">{title}</h5>
          <span className="fs-sm mt-1">{subTitle}</span>
        </a>
      </Link>
    </div>
  );
}
