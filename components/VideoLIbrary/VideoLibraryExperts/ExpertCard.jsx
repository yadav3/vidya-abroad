import React from "react";
import styles from "./VideoLibraryExperts.module.scss";
import Image from "next/image";
import { AiFillStar } from "react-icons/ai";
import { getAWSImagePath } from "../../../context/services";
import { useRouter } from "next/router";

export function ExpertCard({ expert }) {
  const router = useRouter();
  return (
    <div className={`${styles.expertCard} bg-white d-flex gap-4 mb-3 align-items-center shadow-sm bg-white rounded-3 position-relative`}>
      <div
        className={`${styles.expertCard__image}  
           position-relative d-flex align-items-end justify-content-center ms-3 h-100
        `}
      >
        <Image src={getAWSImagePath(expert.photo)} width={148} height={150} alt={expert.name} objectFit="contain" />
      </div>
      <div className={`${styles.expertCard__content}  h-100 d-flex flex-column justify-content-center `}>
        <span className="d-flex flex-column">
          <h3 className={`m-0 fs-5 ${styles.expert_card_title}`}>
            {expert.name}{" "}
            <span className="text-gold ">
              <AiFillStar /> <span className="fs-sm">{expert.rating}/5</span>
            </span>
          </h3>
          <small className={`m-0 text-muted fs-md d-flex align-items-center gap-2 ${styles.expert_details}`}>
            <span>{expert.designation}</span>•
            <span className={`${styles.expertCardVideoCount}`}>
              {expert._count.video_library > 0 ? <p className="m-0 ">{expert._count.video_library}+ Videos</p> : <p className="m-0 ">New</p>}
            </span>
          </small>
        </span>
        <span className="d-flex gap-2">
          <p
            role="button"
            className="m-0 mt-2 text-cvblue fs-md w-max myshine overflow-hidden"
            onClick={() => router.push(`/suggest-me-a-university`)}
          >
            Connect
          </p>
        </span>
      </div>
    </div>
  );
}
