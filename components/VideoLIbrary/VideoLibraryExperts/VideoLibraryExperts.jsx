import React from "react";
import uuid from "react-uuid";
import { ExpertCard } from "./ExpertCard";
import { divideArrayIntoSubarray } from "../../../utils/misc";

import { Swiper, SwiperSlide } from "swiper/react";
import { FreeMode, Pagination } from "swiper";
import "swiper/css";
import "swiper/css/free-mode";
import "swiper/css/pagination";

export default function VideoLibraryExperts({ authors }) {
  return (
    <div className="d-flex pb-5 pb-md-0">
      <Swiper
        slidesPerView={1}
        spaceBetween={30}
        freeMode={true}
        pagination={{
          clickable: true,
        }}
        modules={[FreeMode, Pagination]}
        breakpoints={{
          500: {
            slidesPerView: 2,
          },
        }}
        className="videoLibraryExperts__swiper"
      >
        {divideArrayIntoSubarray(authors, 2).map((experts) => (
          <SwiperSlide key={uuid()}>
            <div className="d-flex flex-column gap-2 pb-5">
              {experts.map((expert) => {
                return <ExpertCard key={uuid()} expert={expert} />;
              })}
            </div>
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
}
