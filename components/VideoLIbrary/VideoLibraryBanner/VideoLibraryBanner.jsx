import React from "react";
import styles from "./VideoLibraryBanner.module.scss";
import Image from "next/image";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

export default function VideoLibraryBanner() {
  return (
    <div className={`${styles.bannerBg}`}>
      <Row className="my-3 mt-5 mt-lg-0">
        <Col md={7}>
          <div className="d-flex flex-column justify-content-center h-100 text text-dark">
            <h1 className="fs-1 fw-bold text-center text-md-start ">
              Learn in <span className="text-cvblue">60 Seconds</span>
            </h1>
            <p className="fs-lg fw-normal px-2">
              <span className="fw-semibold fs-lg">Short on time but eager to study abroad?</span> Expert-led videos in bite-sized portions cover everything from choosing the right
              program to navigating visa requirements. Start your global education journey today!
            </p>
          </div>
        </Col>
        <Col md={5}>
          <div className="my-3 position-relative">
            <Image
              src="/illustrations/video-library.png"
              alt="Video Library Banner"
              width={584}
              height={427}
              layout="responsive"
              objectFit="contain"
            />
          </div>
        </Col>
      </Row>
    </div>
  );
}
