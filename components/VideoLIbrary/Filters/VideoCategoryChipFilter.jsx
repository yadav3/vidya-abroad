import React, { useMemo } from "react";
import styles from "./VideoLIbraryFilters.module.scss";
import { AiOutlineCloseCircle } from "react-icons/ai";

export function VideoCategoryChipFilter({ categories, filters, setFilters, allVideos }) {
  const handleSelectCategory = (categoryId) => {
    setFilters((prev) => ({ ...prev, categoryId: parseInt(categoryId), page: 1 }));
  };

  const handleRemoveAuthor = () => {
    setFilters((prev) => ({ ...prev, authorId: "", page: 1 }));
  };

  const memoizedChips = useMemo(
    () => [
      {
        title: "All",
        id: 0,
      },
      ...categories,
    ],
    [categories]
  );

  return (
    <div className="d-flex align-items-center py-3 my-2 overflow-auto">
      {filters?.authorId > 0 && allVideos.length > 0 && allVideos[0]?.authors && (
        <ChipFilter title={allVideos[0].authors.name} removable={true} handleRemove={handleRemoveAuthor} />
      )}
      {memoizedChips.map((chip) => (
        <ChipFilter key={chip.id} title={chip.title} handleClick={() => handleSelectCategory(chip.id)} isActive={filters.categoryId === chip.id} />
      ))}
    </div>
  );
}

function ChipFilter({ handleClick, handleRemove, removable, isActive, title }) {
  const backgroundColor = isActive ? "#0f58e5" : "white";
  const color = isActive ? "#fff" : "#000";

  return (
    <span
      className="cursor-pointer rounded-5 px-3 py-2 px-4 fs-md me-3"
      style={{ backgroundColor, color, whiteSpace: "nowrap" }}
      role="button"
      onClick={handleClick}
    >
      {title}
      {removable && (
        <span className="ms-2" onClick={handleRemove}>
          <AiOutlineCloseCircle />
        </span>
      )}
    </span>
  );
}
