import React, { useState } from "react";
import styles from "./VideoLIbraryFilters.module.scss";
import { BsSearch } from "react-icons/bs";

export function VideoLibrarySearchbar({ filters, setFilters }) {
  const [search, setSearch] = useState(filters.search);

  const handleSearch = () => {
    setFilters((prev) => ({ ...prev, search: search, page: 1 }));
    window.scrollTo(0, 0);
  };

  return (
    <form
      className={`${styles.videoLibraryFilters__search} border-0 position-relative bg-white rounded-3 overflow-hidden`}
      onSubmit={(e) => {
        e.preventDefault();
        handleSearch();
      }}
    >
      <div
        className={`${styles.videoLibraryFilters__search__icon} position-absolute  top-50 end-0 translate-middle-y me-2 bg-cvblue text-white p-1 d-flex align-items-center justify-content-center rounded-3`}
        style={{
          cursor: "pointer",
          zIndex: 1,
          width: "35px",
          height: "35px",
        }}
        onClick={handleSearch}
      >
        <BsSearch size={15} />
      </div>

      <input
        type="text"
        placeholder="Search what you are looking for..."
        className="w-100 h-100 py-3 ps-3 "
        value={search}
        onChange={(e) => setSearch(e.target.value)}
      />
    </form>
  );
}
