import React from "react";
import { RiFilterOffFill } from "react-icons/ri";
import { Tooltip } from "react-tippy";
import styles from "./VideoLIbraryFilters.module.scss";

export function ClearFilterFloatingButton({ isVideoContainerInView, handleClearFilters }) {
  return (
    <>
      {isVideoContainerInView ? (
        <Tooltip
          title="Clear Filters"
          position="top"
          trigger="mouseenter"
          arrow={true}
          className=""
          style={{
            position: "fixed",
            bottom: "3rem",
            right: "3rem",
            zIndex: "999",
          }}
        >
          <button
            onClick={handleClearFilters}
            className={`bg-cvblue border-0 text-white rounded-circle p-1 position-fixed bottom-0 end-0 m-3 ${styles.clearFilterFloatingBtn}`}
            style={{
              zIndex: 1000,
              width: "50px",
              height: "50px",
            }}
          >
            <RiFilterOffFill />
          </button>
        </Tooltip>
      ) : null}
    </>
  );
}
