import React from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { InputText } from "primereact/inputtext";
import { FooterDivider } from "../../global/FooterContainer/FooterDivider";
import { VideoLibrarySearchbar } from "./VideoLibrarySearchbar";
import { VideoCategoryChipFilter } from "./VideoCategoryChipFilter";

export default function VideoLIbraryFilters({ filters, setFilters, categories, allVideos }) {
  return (
    <>
      <Row>
        <Col>
          <VideoLibrarySearchbar filters={filters} setFilters={setFilters} />
        </Col>
      </Row>
      <Row>
        <Col>
          <VideoCategoryChipFilter categories={categories} filters={filters} setFilters={setFilters} allVideos={allVideos} />
        </Col>
      </Row>
    </>
  );
}
