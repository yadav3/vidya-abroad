import React, { useCallback, useRef, useState } from "react";
import styles from "./VideoLibrarySwiper.module.scss";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import SectionHeading from "../../global/SectionHeading/SectionHeading";

import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import { Navigation } from "swiper";

import Image from "next/image";
import { getAWSImagePath } from "../../../context/services";
import { AiOutlineArrowLeft, AiOutlineArrowRight, AiOutlinePlayCircle } from "react-icons/ai";
import VideoPlayerModal from "../VideoPlayerModal/VideoPlayerModal";
import Link from "next/link";

export default function VideoLibrarySwiper({ youtubeVideos }) {
  const sliderRef = useRef(null);
  const [activeVideoIndex, setActiveVideoIndex] = useState(null);

  const handlePrev = useCallback(() => {
    if (!sliderRef.current) return;
    sliderRef.current.swiper.slidePrev();
  }, []);

  const handleNext = useCallback(() => {
    if (!sliderRef.current) return;
    sliderRef.current.swiper.slideNext();
  }, []);
  return (
    <Container className="my-5 mb-0 mb-md-5" fluid>
      <Row>
        <Col md={4}>
          <div
            className={`d-flex flex-row flex-md-column align-items-center align-items-md-start justify-content-between justify-content-md-center h-100 mb-4 px-3 mb-md-0 px-md-0 mx-auto ${styles.sectionHeadingContainer}`}
          >
            <div className={`${styles.sectionHeading}`}>
              <p className={`m-0 fs-3 ${styles.heading}`}>
                Learn in
                <span className={`text-cvblue fw-bold d-block ${styles.headingMain}`}>60 Seconds</span>
              </p>
            
            </div>
            <div className="d-flex align-items-center justify-content-center gap-2">
              <AiOutlineArrowLeft
                className="bg-white text-black shadow-sm p-2 rounded-3 border border-1 border-dark cursor-pointer"
                size={35}
                onClick={handlePrev}
              />
              <AiOutlineArrowRight
                className="bg-white text-black shadow-sm p-2 rounded-3 border border-1 border-dark cursor-pointer"
                size={35}
                onClick={handleNext}
              />
            </div>
          </div>
        </Col>
        <Col md={8}>
          <Swiper
            ref={sliderRef}
            loop={true}
            slidesPerView="auto"
            spaceBetween={20}
            centeredSlides={false}
            keyboard={false}
            slidesPerGroup={1}
            breakpoints={{
              "@0.00": {
                slidesPerView: 1,
              },
              500: {
                slidesPerView: 1,
              },
              1024: {
                slidesPerView: 2,
              },
              1336: {
                slidesPerView: 3,
              },
            }}
            modules={[Navigation]}
            className={`${styles.influ_slider} video-slider px-5 mt-3`}
          >
            {youtubeVideos.videos.map((video, i) => {
              return (
                <SwiperSlide key={video.id}>
                  <VideoPlayerModal video={video}>
                    <div className="position-relative">
                      <div
                        className="action_wrap position-absolute bottom-0 start-50 translate-middle rounded-circle bg-white text-cvblue p-2 circle-pulse-ring cursor-pointer"
                        style={{
                          zIndex: 10,
                        }}
                      >
                        <AiOutlinePlayCircle size={25} />
                      </div>

                      <div className="d-flex flex-column gap-1 align-items-center">
                        <InfluenceVideoComponent video={video} />
                      </div>
                    </div>
                  </VideoPlayerModal>
                </SwiperSlide>
              );
            })}
          </Swiper>
        </Col>
      </Row>
      <Row>
        <Col>
          <div className="w-100 d-flex align-items-center justify-content-center pt-4">
            <Link href="/video-library" role="button">
              <a className="mx-auto border-0  p-2 px-4 fs-5 bg-cvblue text-white rounded-5 myshine">Discover More Videos</a>
            </Link>
          </div>
        </Col>
      </Row>
    </Container>
  );
}

const InfluenceVideoComponent = ({ video }) => {
  return (
    <div
      className="position-relative"
      // style={{ borderRadius: "20px 20px 20px 20px" }}
    >
      <Image
        width={400}
        height={600}
        alt=""
        style={{ zIndex: 1, top: "0px", left: "0px", objectFit: "contain" }}
        objectFit="cover"
        src={getAWSImagePath(video.thumbnailUrl)}
      />
    </div>
  );
};
