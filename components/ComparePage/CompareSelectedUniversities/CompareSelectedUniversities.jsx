import React, { useEffect, useState } from "react";
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import styles from "./CompareSelectedUniversities.module.scss";

import { AiFillCloseCircle, AiOutlinePlusCircle } from "react-icons/ai";
import {
  allUniversityToCompareModalAtom,
  compareQueryParamsState,
  removeUniversityFromCompare,
  selectedSpecializationsState,
  selectedUniversitiesState,
  selectedUniversityModal,
} from "../../../store/compare/compareStore";
import Image from "next/image";
import { useRouter } from "next/router";
import { userCompareQueryAtom } from "../../../store/userStore";
import Offcanvas from "react-bootstrap/Offcanvas";
import { getAWSImagePathBeta } from "../../../context/services";
import { JourneyRemainingQuestionModalAtom, JourneyRemainingQuestionModalStateAtom } from "../../../store/journeyFormStore";

export default function CompareSelectedUniversities(props) {
  const router = useRouter();
  const userCompareQuery = useRecoilValue(compareQueryParamsState);
  const [showJourneyRemainingQuestionModal, setShowJourneyRemainingQuestionModal] = useRecoilState(JourneyRemainingQuestionModalAtom);
  const [show, setShow] = useRecoilState(allUniversityToCompareModalAtom);
  const [questionsModal, setQuestionsModal] = useRecoilState(JourneyRemainingQuestionModalStateAtom);
  const [compareLimit, setCompareLimit] = useState(3);
  const selectedUniversities = useRecoilValue(selectedUniversitiesState);

  const universities = selectedUniversities.slice(0, compareLimit);
  const specializations = universities.map((university) => university?.specializationId);

  const handleCompareNow = () => {
    if (universities.length === 0) {
      return;
    }

    const universityIds = universities.map((university) => university && `university=${university.id}`).join("&");
    const specializationIds = specializations
      .filter(Boolean)
      .map((id) => `specialization=${id}`)
      .join("&");

    window.location.href = `/compare/overview/?${universityIds}&${specializationIds}&uuid=${router.query.uuid}`;
  };

  const handleOpenAllUniversityToCompareModal = () => {
    setShow(true);
  };

  const handleResize = () => {
    if (window?.innerWidth < 768) {
      setCompareLimit(2);
    } else {
      setCompareLimit(3);
    }
  };

  useEffect(() => {
    handleResize();
    window?.addEventListener("resize", handleResize);
    return () => {
      window?.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <Offcanvas
      backdrop={false}
      scroll={true}
      show={true}
      placement="bottom"
      style={{
        height: "max-content",
      }}
    >
      <Offcanvas.Body className={styles.container}>
        <Container fluid>
          <Row>
            <Col>
              <div>
                <h6 className="mb-3 d-flex align-items-center justify-content-center  justify-content-lg-start  gap-2">
                  <span>
                    <span className="fw-bold">Compare</span> Universities{" "}
                  </span>
                  {selectedUniversities.length < compareLimit && (
                    <span className=" badge bg-primary">Add {compareLimit - selectedUniversities.length} more</span>
                  )}
                </h6>
              </div>
            </Col>
          </Row>
          <Row className="align-items-center justify-content-center h-100 ">
            {[...Array(compareLimit)].map((_, index) => {
              const university = selectedUniversities[index];
              return (
                <Col className="h-100" xs={6} md={3} key={index}>
                  {university ? (
                    <div className={styles.universityBox}>
                      <Image alt={university.name} src={getAWSImagePathBeta(university.logo_full)} width={150} height={50} objectFit="contain" />
                      <UniversityBoxCloseIcon university={university} />
                      <p className="m-0 fs-sm text-center mt-2">{university.name}</p>
                    </div>
                  ) : (
                    <div className={styles.universityBox} onClick={() => handleOpenAllUniversityToCompareModal()}>
                      <AiOutlinePlusCircle />
                    </div>
                  )}
                </Col>
              );
            })}
            <Col md={3}>
              <div className="d-flex align-items-center justify-content-center mt-3 p-md-0">
                <button
                  className={styles.compareNowBtn}
                  onClick={() => {
                    handleCompareNow();
                  }}
                >
                  Compare Now
                </button>
              </div>
            </Col>
          </Row>
        </Container>
      </Offcanvas.Body>
    </Offcanvas>
  );
}

function UniversityBoxCloseIcon({ university }) {
  const setSelectedUniversities = useSetRecoilState(selectedUniversitiesState);
  const selectedUniversities = useRecoilValue(selectedUniversitiesState);

  const removeUniversityFromCompare = (universityId) => {
    if (selectedUniversities.length === 0) return;
    setSelectedUniversities((universities) => [...universities.filter((university) => university.id != universityId)]);
  };
  return (
    <button className={styles.universityBoxCloseIcon} onClick={() => removeUniversityFromCompare(university.id)}>
      <AiFillCloseCircle size={20} />
    </button>
  );
}
