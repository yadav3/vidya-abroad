import React from "react";
import Lottie from "lottie-react";
import SearchingDocumentAnimation from "../../../public/animations/searching_doc_json.json";
import TextTransition, { presets } from "react-text-transition";

import styles from "./UniversityOverviewLoader.module.scss";

export default function UniversityOverviewLoader({ width = "500px" }) {
  const [transitionIndex, setTransitionIndex] = React.useState(0);

  const COMPARE_PARAMS = [
    "Details",
    "Ranking",
    "Programme",
    "Course",
    "Specialization",
    "IELTS Requirement",
    "Intakes",
    "Course Duration",
    "Course Fees",
  ];

  React.useEffect(() => {
    const intervalId = setInterval(
      () => setTransitionIndex((index) => index + 1),
      3000 // every 3 seconds
    );
    return () => clearTimeout(intervalId);
  }, []);

  return (
    <div className="d-flex flex-column align-items-center justify-content-center w-100">
      <div style={{}}>
        <Lottie
          animationData={SearchingDocumentAnimation}
          height={200}
          style={{
            width: "100%",
          }}
        />
      </div>
      <h4 className={`text-center d-flex align-items-center ${styles.loadingText}`}>
        <span className="me-1">Extracting University</span>
        <span className="text-cvblue fw-semibold">
          <TextTransition springConfig={presets.wobbly}>{COMPARE_PARAMS[transitionIndex % COMPARE_PARAMS.length]}</TextTransition>
        </span>
      </h4>
    </div>
  );
}
