import React, { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import Col from "react-bootstrap/Col";
import Row from "react-bootstrap/Row";
import uuid from "react-uuid";
import Image from "next/image";
import { getAWSImagePathBeta, getCompareQueryByUserId } from "../../../context/services";
import { Tooltip } from "react-tippy";
import { useRecoilState, useRecoilValue } from "recoil";
import { Skeleton } from "primereact/skeleton";
import {
  allCompareFilteredUniversitiesAtom,
  allUniversityToCompareModalAtom,
  compareQueryParamsState,
  selectedSpecializationsState,
  selectedUniversitiesState,
} from "../../../store/compare/compareStore";
import formatCurrencyAmount from "../../../context/CustomHooks";

import styles from "./AllUniversityToCompareModal.module.scss";
import { AiFillCloseCircle, AiFillPlusCircle } from "react-icons/ai";
import { toast } from "react-hot-toast";
import { useRouter } from "next/router";
import { flushSync } from "react-dom";
import { JourneyRemainingQuestionModalAtom, JourneyRemainingQuestionModalStateAtom } from "../../../store/journeyFormStore";
import { useQuery } from "react-query";
import { getUniversitiesByCompareQueries } from "../../../services/universitiesService";
import UniversityOverviewLoader from "../UniversityOverviewLoader/UniversityOverviewLoader";

export default function AllUniversityToCompareModal({ isLoading, children, university, onClose = () => {} }) {
  const [show, setShow] = useRecoilState(allUniversityToCompareModalAtom);
  const [filteredUniversities, setFilteredUniversities] = useRecoilState(allCompareFilteredUniversitiesAtom);
  const [selectedUniversities, setSelectedUniversities] = useRecoilState(selectedUniversitiesState);
  const [userCompareQuery, setUserCompareQuery] = useRecoilState(compareQueryParamsState);
  const [showJourneyRemainingQuestionModal, setShowJourneyRemainingQuestionModal] = useRecoilState(JourneyRemainingQuestionModalAtom);
  const [questionsModal, setQuestionsModal] = useRecoilState(JourneyRemainingQuestionModalStateAtom);
  const [compareLimit, setCompareLimit] = useState(3);

  const router = useRouter();

  const handleClose = () => {
    setShow(false);
  };

  const handleCompare = (universities) => {
    if (universities.length === 0) return;

    const university1 = universities?.[0];
    const university2 = universities?.[1];
    const university3 = universities?.[2];

    const specialization1 = university1?.specializationId;
    const specialization2 = university2?.specializationId;
    const specialization3 = university3?.specializationId;

    const url = `/compare/overview/?${university1 ? "university=" + university1.id : ""}${university2 ? "&university=" + university2.id : ""}${
      university3 ? "&university=" + university3.id : ""
    }&specialization=${specialization1 ? specialization1 : ""}${specialization2 ? "&specialization=" + specialization2 : ""}${
      specialization3 ? "&specialization=" + specialization3 : ""
    }&uuid=${router.query.uuid}`;

    window.open(url, "_self");
  };

  const isUniversityActive = (university) => {
    return selectedUniversities?.find((selectedUniversity) => selectedUniversity?.id === university?.id);
  };

  // Add to Compare
  const handleAddToCompare = (university) => {
    const compareLimit = window.innerWidth <= 767.98 ? 2 : 3;

    if (selectedUniversities.length === compareLimit) {
      toast.dismiss();
      toast(`You can only compare ${compareLimit} universities at a time`, {
        style: {
          borderRadius: "10px",
          background: "#333",
          color: "#fff",
        },
        icon: "🚫",
      });
      return;
    }

    const newSelectedUniversities = [...selectedUniversities, university];
    setSelectedUniversities(newSelectedUniversities);
    if (newSelectedUniversities.length === 1) {
      handleCompare(newSelectedUniversities);
    }
  };

  // Remove from Compare
  const removeUniversityFromCompare = (universityId) => {
    if (selectedUniversities.length === 0) {
      return;
    }
    setSelectedUniversities((universities) => [...universities.filter((university) => university.id != universityId)]);
  };

  useEffect(() => {
    window?.addEventListener("resize", () => {
      if (window?.innerWidth < 768) {
        setCompareLimit(2);
      } else {
        setCompareLimit(3);
      }
    });

    return () => {
      window?.removeEventListener("resize", () => {
        if (window?.innerWidth < 768) {
          setCompareLimit(2);
        } else {
          setCompareLimit(3);
        }
      });
    };
  }, []);

  return (
    <div>
      <Modal show={show} onHide={handleClose} centered size="xl">
        <Modal.Header closeButton>
          <Modal.Title className="w-100 ">
            <h6 className="m-0  gap-2">
              <span className="fw-bold">Compare</span> Universities{" "}
            </h6>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body
          className="position-relative"
          style={{
            maxHeight: "80vh",
            overflowY: "auto",
          }}
        >
          {isLoading? (
            <div className="w-100 h-100">
              <Skeleton height="100px" />
            </div>
          ) : (
            <Row className="align-items-center justify-content-center pt-2">
              {filteredUniversities?.map((university) => (
                <Col key={uuid()} xs={12} md={4} lg={3} className="mb-3">
                  <div
                    className={`${styles.box} cursor-pointer`}
                    onClick={() => {
                      if (isUniversityActive(university)) {
                        removeUniversityFromCompare(university.id);
                      } else {
                        handleAddToCompare(university);
                      }
                    }}
                  >
                    <div
                      className={`${styles.dot} ${
                        isUniversityActive(university) ? styles.dotActive : ""
                      } d-flex align-items-center justify-content-center`}
                    >
                      {isUniversityActive(university) ? (
                        <Tooltip
                          title="Remove from Compare"
                          position="bottom"
                          trigger="mouseenter"
                          arrow={true}
                          duration={200}
                          animation="shift"
                          theme="light"
                          distance={10}
                        >
                          <span className={`${styles.plusIcon}`}>
                            <AiFillCloseCircle size={20} />
                          </span>
                        </Tooltip>
                      ) : (
                        <Tooltip
                          title="Add to Compare"
                          position="bottom"
                          trigger="mouseenter"
                          arrow={true}
                          duration={200}
                          animation="shift"
                          theme="light"
                          distance={10}
                        >
                          <span className={`${styles.plusIcon}`}>
                            <AiFillPlusCircle size={20} />
                          </span>
                        </Tooltip>
                      )}
                    </div>
                    <Image src={getAWSImagePathBeta(university?.logo_full)} alt={university?.name} width={100} height={50} objectFit="contain" />
                    <div className="text-center mt-2">
                      <h6 className="fs-md">{university?.name}</h6>
                      <span className={`${styles.fees} fs-sm px-2 py-1 rounded`}>
                        Approx.{" "}
                        {university.feesNew
                          ? formatCurrencyAmount(university.feesNew, university.currencyNew)
                          : formatCurrencyAmount(university.fees, university.currency)}
                        /Annually
                      </span>
                    </div>
                  </div>
                </Col>
              ))}
            </Row>
          )}
        </Modal.Body>
        <Modal.Footer>
          <div className="ms-4 text-center d-flex  flex-md-row flex-column justify-content-center align-items-center gap-3 w-100">
            {selectedUniversities?.length >= 1 ? (
              <button
                className="
               fs-md border-0 bg-primary text-white px-3 py-2 rounded-pill
              "
                onClick={() => {
                  if (userCompareQuery.questionAnswered === 0) {
                    setShow(false);
                    setShowJourneyRemainingQuestionModal(true);
                    setQuestionsModal({
                      ...questionsModal,
                      onClose: () => {
                        handleCompare(selectedUniversities);
                      },
                      submitButtonText: "Compare Now",
                    });
                    return;
                  } else {
                    handleCompare(selectedUniversities);
                  }
                }}
              >
                Compare {selectedUniversities?.length > 0 ? selectedUniversities.length : null} Universities Now
              </button>
            ) : (
              <span className="fs-md bg-cvblue text-white p-1 px-3 rounded-pill">Add at least 1 universities</span>
            )}
          </div>
        </Modal.Footer>
      </Modal>
    </div>
  );
}
