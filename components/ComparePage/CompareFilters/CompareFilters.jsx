import React, { useEffect, useMemo } from "react";
import styles from "./CompareFilters.module.scss";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { SearchFilter } from "./SearchFilter";
import { useState } from "react";
import { AiOutlineArrowDown, AiOutlineArrowUp, AiOutlineDollar } from "react-icons/ai";
import { storeUserCompareResults } from "../../../context/services";
import { SpecializationDurationFilter } from "./SpecializationDurationFilter";
import { CountryFilter } from "./CountryFilter";
import { compareQueryParamsState, selectedUniversitiesState } from "../../../store/compare/compareStore";
import { useRecoilState } from "recoil";
import { ChangeMajorFilter } from "./ChangeMajorFilter";
import { journeyDetailsAtom } from "../../../store/journeyStore";
import { useRouter } from "next/router";
import { ConvertFeesToINRSwitch } from "./ConvertFeesToINRSwitch";
import { toast } from "react-hot-toast";
import { Dropdown } from "primereact/dropdown";
import { getBudgetByStringLimit } from "../../../utils/misc";
import CompareFilterModal from "./CompareFilterModal";

export default function CompareFilters({ universities, filteredUniversities, setFilteredUniversities, majors, exchangeRates }) {
  const router = useRouter();
  const [showFilters, setShowFilters] = useState(true);
  const [userCompareQuery, setUserCompareQuery] = useRecoilState(compareQueryParamsState);
  const [selectedUniversities, setSelectedUniversities] = useRecoilState(selectedUniversitiesState);
  const [journeyDetails, setJourneyDetails] = useRecoilState(journeyDetailsAtom);

  const handleToggleFilters = () => {
    setShowFilters((prev) => !prev);
  };

  const handleApplyFilters = () => {
    if (userCompareQuery.categoryId === 0 || userCompareQuery.categoryId === null) {
      toast.error("Please select a category");
      return;
    }

    if (userCompareQuery.majorId === 0 || userCompareQuery.majorId === null) {
      toast.error("Please select a major");
      return;
    }

    storeUserCompareResults(
      router.query.uuid,
      userCompareQuery.categoryId,
      userCompareQuery.courseId,
      userCompareQuery.specializationId,
      userCompareQuery.majorId,
      userCompareQuery.budgetLimit,
      journeyDetails.countryId,
      userCompareQuery.ieltsScore,
      userCompareQuery.opportunityId,
      userCompareQuery.budgetLow,
      userCompareQuery.budgetHigh,
      userCompareQuery.countries
    )
      .then((userDetails) => {
        window.location.href = `/compare/universities?uuid=${userDetails.user_id}`;
      })
      .catch((err) => {
        console.error(err);
        toast.error(`
        Something went wrong while applying filters! ${err.response.data.description}
        `);
      });
  };

  // when screen is 960px or less, show hide filters
  useEffect(() => {
    if (window.innerWidth <= 960) {
      setShowFilters(false);
    }
  }, []);

  return (
    <Container className={`pt-4 px-2 px-sm-0 position-relative`}>
      <Row>
        <Col xs={12}>
          <div className={`${styles.filters} mb-3`}>
            <ChangeMajorFilter majors={majors} />
            <CountryFilter
              universities={universities}
              filteredUniversities={filteredUniversities}
              setFilteredUniversities={setFilteredUniversities}
            />
            <BudgetFilter universities={universities} filteredUniversities={filteredUniversities} setFilteredUniversities={setFilteredUniversities} />
            <SpecializationDurationFilter
              universities={universities}
              filteredUniversities={filteredUniversities}
              setFilteredUniversities={setFilteredUniversities}
            />
            {exchangeRates && exchangeRates.length > 0 && (
              <ConvertFeesToINRSwitch
                exchangeRates={exchangeRates}
                universities={universities}
                filteredUniversities={filteredUniversities}
                setFilteredUniversities={setFilteredUniversities}
              />
            )}
            <button onClick={() => handleApplyFilters()} className={`${styles.applyBtn} w-100 p-1 rounded `}>
              Apply Filters
            </button>
          </div>
        </Col>
      </Row>
    </Container>
  );
}

const BudgetFilter = () => {
  const [userCompareQuery, setUserCompareQuery] = useRecoilState(compareQueryParamsState);

  const [selectedBudget, setSelectedBudget] = useState(null);

  const budgetOption = useMemo(() => {
    return [
      {
        id: 0,
        value: "Not Decided Yet",
        label: "Not Decided Yet",
        budgetLow: 0,
        budgetHigh: 0,
      },
      {
        id: 1,
        value: "10L - 20L",
        label: "10 Lakh to 20 Lakh",
        budgetLow: 1000000,
        budgetHigh: 2000000,
      },
      {
        id: 2,
        value: "20L - 30L",
        label: "20 Lakh to 30 Lakh",
        budgetLow: 2000000,
        budgetHigh: 3000000,
      },
      {
        id: 3,
        value: "30L - 40L",
        label: "30 Lakh to 40 Lakh",
        budgetLow: 3000000,
        budgetHigh: 4000000,
      },
      {
        id: 4,
        value: "40L - 50L",
        label: "40 Lakh to 50 Lakh",
        budgetLow: 4000000,
        budgetHigh: 5000000,
      },
      {
        id: 5,
        value: "More than 50L",
        label: "More than 50L",
        budgetLow: 5000000,
        budgetHigh: 50000000,
      },
    ];
  }, []);

  const handleBudgetChange = (e) => {
    setSelectedBudget(e.value);
    setUserCompareQuery({
      ...userCompareQuery,
      budgetLow: getBudgetByStringLimit(e.value)[0],
      budgetHigh: getBudgetByStringLimit(e.value)[1],
    });
  };

  useEffect(() => {
    if ((userCompareQuery.budgetLow && userCompareQuery.budgetHigh) || (userCompareQuery.budgetLow === 0 && userCompareQuery.budgetHigh === 0)) {
      const budget = budgetOption.find(
        (budget) => budget.budgetLow === userCompareQuery.budgetLow && budget.budgetHigh === userCompareQuery.budgetHigh
      );
      setSelectedBudget(budget.value);
    }
  }, [userCompareQuery.budgetLow, userCompareQuery.budgetHigh, budgetOption]);

  return (
    <div className="d-flex flex-column">
      <label htmlFor="budget" className="mb-2 fs-md d-flex align-items-center gap-2">
        <AiOutlineDollar />
        Budget
      </label>
      <Dropdown
        options={budgetOption}
        value={selectedBudget}
        onChange={handleBudgetChange}
        optionLabel="label"
        optionValue="value"
        placeholder="Select a your budget"
      />
    </div>
  );
};
