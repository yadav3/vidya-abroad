import React from "react";
import { useState } from "react";
import { Dropdown } from "primereact/dropdown";
import { compareQueryParamsState } from "../../../store/compare/compareStore";
import { useRecoilState } from "recoil";
import { useEffect } from "react";
import { journeyDetailsAtom } from "../../../store/journeyStore";
import { TbCertificate, TbBook } from "react-icons/tb";

export function ChangeMajorFilter({ universities, filteredUniversities, setFilteredUniversities, majors }) {
  const [selectedMajor, setSelectedMajor] = useState(0);
  const [selectedCategory, setSelectedCategory] = useState(0);
  const [userCompareQuery, setUserCompareQuery] = useRecoilState(compareQueryParamsState);
  const [journeyDetails, setJourneyDetails] = useRecoilState(journeyDetailsAtom);

  const [filteredMajors, setFilteredMajors] = useState(majors);

  const categories = [
    { id: 1, name: "Bachelors" },
    { id: 2, name: "Masters" },
    { id: 3, name: "Diploma" },
  ];

  useEffect(() => {
    if (userCompareQuery.categoryId && userCompareQuery.categoryId !== 0) {
      setSelectedCategory(userCompareQuery.categoryId);
      setJourneyDetails({
        ...journeyDetails,
        categoryId: userCompareQuery.categoryId,
      });
      setFilteredMajors(majors.filter((major) => parseInt(major.category_id) === parseInt(userCompareQuery.categoryId)));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userCompareQuery.categoryId]);

  useEffect(() => {
    if (userCompareQuery.majorId && userCompareQuery.majorId !== 0) {
      setSelectedMajor(userCompareQuery.majorId);
      setJourneyDetails({
        ...journeyDetails,
        majorId: userCompareQuery.majorId,
      });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userCompareQuery.majorId]);

  return (
    <>
      <div className="d-flex flex-column">
        <label htmlFor="city" className="mb-2 fs-md d-flex align-items-center gap-2">
          <TbCertificate />
          Programme (Degree)
        </label>
        <Dropdown
          value={selectedCategory}
          options={categories}
          onChange={(e) => {
            setSelectedCategory(e.value);
            setSelectedMajor(0);
            setUserCompareQuery({
              ...userCompareQuery,
              categoryId: e.value,
              majorId: 0,
            });
          }}
          optionLabel="name"
          optionValue="id"
          placeholder="Select a Programme"
        />
      </div>
      {selectedCategory && selectedCategory !== 0 ? (
        <div className="d-flex flex-column">
          <label htmlFor="city" className="mb-2 fs-md d-flex align-items-center gap-2 ">
            <TbBook />
            Major
          </label>
          <Dropdown
            value={selectedMajor}
            options={filteredMajors}
            onChange={(e) => {
              setSelectedMajor(e.value);
              setUserCompareQuery({
                ...userCompareQuery,
                majorId: e.value,
                categoryId: selectedCategory,
              });
            }}
            optionLabel="name"
            optionValue="id"
            placeholder="Select a Major"
            filter={true}
            filterBy="name"
          />
        </div>
      ) : null}
    </>
  );
}
