import React, { useEffect } from "react";
import axios from "axios";
import { Dropdown } from "primereact/dropdown";
import { useState } from "react";
import { GiDuration } from "react-icons/gi";
import { BACKEND_API_URL } from "../../../context/services";
import { useRecoilState } from "recoil";
import { compareQueryParamsState } from "../../../store/compare/compareStore";
export function SpecializationDurationFilter({ universities, filteredUniversities, setFilteredUniversities }) {
  const [selectedCity, setSelectedCity] = useState(1000);
  const [userCompareQuery, setUserCompareQuery] = useRecoilState(compareQueryParamsState);

  const [isFilterChange, setIsFilterChange] = useState(null);

  const options = [
    { name: "Not Decided", value: 1000 },
    { name: "1 Year and Below", value: 12 },
    { name: "2 Year and Below", value: 24 },
    { name: "3 Year and Below", value: 36 },
    { name: "4 Year and Below", value: 48 },
    { name: "5 Year and Below", value: 60 },
  ];

  const handleFilterDuration = (duration) => {
    if (duration === 1000) {
      setFilteredUniversities(universities);
    } else {
      const filtered = universities.filter((university) => university.duration <= duration);
      setFilteredUniversities(filtered);
    }
    setIsFilterChange((prev) => (prev === null ? true : !prev));
  };

  useEffect(() => {
    if (userCompareQuery.relatedProspectId && isFilterChange !== null) {
      // Specialization Duration Filter Activity
      const specializationDurationFilterActivityLsqData = {
        RelatedProspectId: userCompareQuery.relatedProspectId,
        ActivityEvent: 270,
        ActivityNote: "Student filtered universities by specializations duration",
        Fields: [
          {
            SchemaName: "mx_Custom_1",
            Value: selectedCity === 1000 ? "Not Decided" : options.filter((option) => option.value === selectedCity)[0].name,
          },
          {
            SchemaName: "mx_Custom_2",
            Value: `https://collegevidyaabroad.com/compare/universities/?&uuid=${userCompareQuery.uuid}`,
          },
        ],
      };
      axios.post(BACKEND_API_URL + "/leadsquared/lead/post-lead-activity", specializationDurationFilterActivityLsqData);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userCompareQuery.relatedProspectId, isFilterChange]);

  return (
    <div className="d-flex flex-column">
      <label htmlFor="city" className="mb-2 fs-md d-flex align-items-center gap-2">
        <GiDuration />
        Specializations Duration
      </label>
      <Dropdown
        value={selectedCity}
        options={options}
        onChange={(e) => {
          setSelectedCity(e.value);
          handleFilterDuration(e.value);
        }}
        optionLabel="name"
        placeholder="Select a Duration"
      />
    </div>
  );
}
