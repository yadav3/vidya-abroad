import React from "react";
import { Dropdown } from "primereact/dropdown";
import { useState } from "react";
import { compareQueryParamsState } from "../../../store/compare/compareStore";
import { useRecoilState } from "recoil";
import { useEffect } from "react";
import { journeyDetailsAtom } from "../../../store/journeyStore";
import { Checkbox } from "primereact/checkbox";
import uuid from "react-uuid";
import { useQuery } from "react-query";
import { getCountries, getCountriesNames } from "../../../services/country.service";

import { GiWorld } from "react-icons/gi";
import Image from "next/image";

export function CountryFilter({ universities, filteredUniversities, setFilteredUniversities }) {
  const [userCompareQuery, setUserCompareQuery] = useRecoilState(compareQueryParamsState);
  const [journeyDetails, setJourneyDetails] = useRecoilState(journeyDetailsAtom);

  const { data: countries, isLoading, error } = useQuery("countries", getCountriesNames);

  const handleCountryOnChange = (option) => {
    if (userCompareQuery.countries.includes(option.id)) {
      setUserCompareQuery({
        ...userCompareQuery,
        countries: userCompareQuery.countries.filter((country) => country !== option.id),
      });
    } else {
      setUserCompareQuery({
        ...userCompareQuery,
        countries: [...userCompareQuery.countries, option.id],
      });
    }
  };

  return (
    <>
      <div className="d-flex flex-column gap-2">
        <p className="mb-2 fs-md d-flex align-items-center justify-content gap-2">
          <GiWorld />
          Country
        </p>
        <div className="d-flex align-items-center gap-2 w-100 fs-md">
          <Checkbox
            inputId={"all-country"}
            name={"All"}
            value={0}
            onChange={() => handleCountryOnChange({ fullName: "All", id: 0 })}
            checked={userCompareQuery?.countries?.includes(0) || userCompareQuery?.countries?.length === 0}
          />
          <label htmlFor={"all-country"}>All (Not Decided Yet)</label>
        </div>
        {isLoading ? (
          <p>Loading...</p>
        ) : (
          countries.map((option) => (
            <div className="d-flex align-items-center gap-2 w-100 fs-md" key={uuid()}>
              <Checkbox
                inputId={option.fullName}
                name={option.fullName}
                value={option.id}
                onChange={() => handleCountryOnChange(option)}
                checked={userCompareQuery?.countries?.includes(option.id)}
              />
              <label htmlFor={option.fullName} className="d-flex align-items-center gap-2">
                <Image className="rounded-circle overflow-hidden" src={option.flag} alt={option.fullName} width={15} height={15} />
                {option.fullName}
              </label>
            </div>
          ))
        )}
      </div>
    </>
  );
}
