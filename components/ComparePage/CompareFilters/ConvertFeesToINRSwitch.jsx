import React from "react";
import { useState } from "react";
import { InputSwitch } from "primereact/inputswitch";
import { TbSwitchHorizontal } from "react-icons/tb";

export const ConvertFeesToINRSwitch = ({ exchangeRates, universities, filteredUniversities, setFilteredUniversities }) => {
  const [isFeesInINR, setIsFeesInINR] = useState(true);
  const handleSwitchChange = (e) => {
    setIsFeesInINR(e.value);
    if (e.value) {
      setFilteredUniversities((prev) => {
        return prev.map((university) => {
          const exchangeRate = exchangeRates.find((rate) => rate.target === university.currency);
          return {
            ...university,
            feesNew: Math.trunc(university.fees / exchangeRate.rate),
            currencyNew: "INR",
            currencySymbolNew: "₹",
          };
        });
      });
    } else {
      setFilteredUniversities(
        filteredUniversities.map((university) => {
          return {
            ...university,
            feesNew: null,
            currencyNew: null,
            currencySymbolNew: null,
          };
        })
      );
    }
  };

  return (
    <div className="d-flex align-items-center justify-content-between">
      <span className="fs-md d-flex align-items-center gap-2">
        <TbSwitchHorizontal />
        Fees in INR
      </span>
      <InputSwitch checked={isFeesInINR} onChange={handleSwitchChange} className="currencyToggleSwitch" />
    </div>
  );
};
