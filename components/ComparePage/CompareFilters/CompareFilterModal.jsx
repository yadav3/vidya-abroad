import React, { useState } from "react";
import Modal from "react-bootstrap/Modal";
import Offcanvas from "react-bootstrap/Offcanvas";
import CompareFilters from "./CompareFilters";

export default function CompareFilterModal({ children, universities, filteredUniversities, setFilteredUniversities, majors, exchangeRates }) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <div onClick={handleShow}>{children}</div>

      <Offcanvas show={show} onHide={handleClose}>
        <Offcanvas.Header closeButton>
        </Offcanvas.Header>
        <Offcanvas.Body className="mt-0 pt-0">
          <CompareFilters
            universities={universities}
            filteredUniversities={filteredUniversities}
            setFilteredUniversities={setFilteredUniversities}
            majors={majors}
            exchangeRates={exchangeRates}
          />
        </Offcanvas.Body>
      </Offcanvas>
    </>
  );
}
