import React from "react";
import { InputText } from "primereact/inputtext";
import { useState } from "react";
import { useRecoilState } from "recoil";

export function SearchFilter({ universities, filteredUniversities, setFilteredUniversities }) {



  const handleSearch = (search) => {
    const filteredUniversities = universities.filter((university) => {
      return university.name.toLowerCase().includes(search.toLowerCase());
    });

    if (search === "") {
      setFilteredUniversities(universities);
    } else {
      setFilteredUniversities(filteredUniversities);
    }

  };

  return (
    <div>
      <label htmlFor="search-filter" className="mb-2 fs-md">
        Search University
      </label>
      <span className="p-input-icon-left w-100 ">
        <i className="pi pi-search" />
        <InputText
          placeholder="Search"
          className="w-100"
          id="search-filter"
          name="search"
          type="text"
          onChange={(e) => {
            handleSearch(e.target.value);
          }}
        />
      </span>
    </div>
  );
}
