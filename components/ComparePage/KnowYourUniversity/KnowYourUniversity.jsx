import React, { useState } from "react";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import Accordion from "react-bootstrap/Accordion";
import uuid from "react-uuid";
import Image from "next/image";
import { getAWSImagePathBeta } from "../../../context/services";
import { Tooltip } from "react-tippy";
import { AiOutlineInfoCircle } from "react-icons/ai";

export default function KnowYourUniversity({ children, university }) {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <div>
      <span onClick={handleShow}>{children}</span>

      <Modal show={show} onHide={handleClose} centered size="lg">
        <Modal.Header closeButton>
          <Modal.Title>Know Your University in 2 Minutes</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Accordion defaultActiveKey="0">
            <Accordion.Item eventKey="0">
              <Accordion.Header>📃 Overview</Accordion.Header>
              <Accordion.Body>
                {university?.details?.universities_overview[0].value_html ? (
                  <span dangerouslySetInnerHTML={{ __html: university.details.universities_overview[0].value_html }} className="fs-md" />
                ) : university?.details?.universities_overview[0].value.length > 0 ? (
                  <span className="fs-md">{university.details.universities_overview[0].value}</span>
                ) : null}
              </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="1">
              <Accordion.Header>
                📝 Admission Requirements
                <Tooltip
                  className="m-2"
                  title={`Admission requirements are the minimum requirements that a student must meet to be considered for admission to a university.`}
                  position="top"
                  trigger='manual'
                  
                >
                  <AiOutlineInfoCircle />
                </Tooltip>
              </Accordion.Header>
              <Accordion.Body>
                <ul className="list-group list-group-flush">
                  {university?.details?.university_admission_req[0].items.map((item, index) => (
                    <li key={uuid()} className="list-group-item py-3 fs-md ">
                      📃 {item}
                    </li>
                  ))}
                </ul>
              </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="2">
              <Accordion.Header>📈 Rankings</Accordion.Header>
              <Accordion.Body>
                <ul className="list-group list-group-flush">
                  {university?.details?.universities_rankings.map((item, index) => (
                    <li key={uuid()} className="list-group-item py-3 fs-md">
                      🏆 {item.name}: {item.ranking}
                    </li>
                  ))}
                </ul>
              </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="3">
              <Accordion.Header>📚 Quick Facts</Accordion.Header>
              <Accordion.Body>
                <ul className="list-group list-group-flush">
                  {university?.details?.universities_quick_facts.map((item, index) => (
                    <li key={uuid()} className="list-group-item py-3 fs-md">
                      💡 {item.name}
                    </li>
                  ))}
                </ul>
              </Accordion.Body>
            </Accordion.Item>
            <Accordion.Item eventKey="4">
              <Accordion.Header>🏢 Placements</Accordion.Header>
              <Accordion.Body>
                <ul className="list-group list-group-flush">
                  {university?.details?.universities_famous_companies.map((item, index) => (
                    <li key={uuid()} className="list-group-item py-3 fs-md">
                      {item?.media_library?.image ? (
                        <span className="d-flex align-items-center gap-3 ">
                          <Image src={getAWSImagePathBeta(item.media_library.image)} alt={item.title} width="100" height="50" objectFit="contain" />
                          {item.title}
                        </span>
                      ) : item?.logo ? (
                        <span className="d-flex align-items-center gap-3 ">
                          <Image src={getAWSImagePathBeta(item.logo)} alt={item.title} width="100" height="50" objectFit="contain" />
                          {item.title}
                        </span>
                      ) : (
                        <span className="d-flex align-items-center gap-3 ">{item.title}</span>
                      )}
                    </li>
                  ))}
                </ul>
              </Accordion.Body>
            </Accordion.Item>
          </Accordion>
        </Modal.Body>
      </Modal>
    </div>
  );
}
