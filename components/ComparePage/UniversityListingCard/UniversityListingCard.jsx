import React from "react";
import Col from "react-bootstrap/Col";
import Image from "next/image";
import styles from "./UniversityListingCard.module.scss";
import { useRecoilState, useRecoilValue, useSetRecoilState } from "recoil";
import { selectedSpecializationsState, selectedUniversitiesState, selectedUniversityModal } from "../../../store/compare/compareStore";
import { AiOutlineArrowDown, AiOutlineCiCircle, AiOutlineInfo, AiOutlineInfoCircle, AiOutlineArrowRight } from "react-icons/ai";
import { TbDiscount2 } from "react-icons/tb";
import { getAWSImagePathBeta } from "../../../context/services";
import Link from "next/link";
import { toast } from "react-hot-toast";
import KnowYourUniversity from "../KnowYourUniversity/KnowYourUniversity";
import formatCurrencyAmount, { CurrencyInternationalization } from "../../../context/CustomHooks";
import { Avatar } from "primereact/avatar";
import { AvatarGroup } from "primereact/avatargroup";
import { Tooltip } from "react-tippy";
import CvCounsellorPopup from "../../global/CvCounsellorPopup/CvCounsellorPopup";

export default function UniversityListingCard({ university, course }) {
  const [showModal, setShowModal] = useRecoilState(selectedUniversityModal);
  const setSelectedUniversities = useSetRecoilState(selectedUniversitiesState);
  const selectedUniversities = useRecoilValue(selectedUniversitiesState);
  const [selectedSpecialization, setSelectedSpecialization] = useRecoilState(selectedSpecializationsState);

  const isUniversitySelected = selectedUniversities.includes(university) ? true : false;

  // Add to Compare
  const handleAddToCompare = (university) => {
    const compareLimit = window.innerWidth <= 767.98 ? 2 : 3;

    if (selectedUniversities.length === compareLimit) {
      toast.dismiss();
      toast(`You can only compare ${compareLimit} universities at a time`, {
        style: {
          borderRadius: "10px",
          background: "#333",
          color: "#fff",
        },
        icon: "🚫",
      });
      return;
    }

    setSelectedUniversities((selectedUniversities) => [...selectedUniversities, university]);
    setSelectedSpecialization((selectedSpecialization) => [...selectedSpecialization, university.specializationId]);
    if (selectedUniversities.length > 0) {
      setShowModal(true);
    }
  };

  // Remove from Compare
  const removeUniversityFromCompare = (universityId) => {
    if (selectedUniversities.length === 0) return;
    setSelectedUniversities((universities) => [...universities.filter((university) => university.id != universityId)]);
    setSelectedSpecialization((specializations) => [...specializations.filter((specialization) => specialization != university.specializationId)]);
  };

  const renderUniversityRankingTag = (universities_rankings) => {
    // check if QS ranking is available, if not check for Times ranking, if not check for country ranking
    // and ranking is not = "Not Ranked", if not return null

    if (!universities_rankings || universities_rankings.length === 0) return null;

    const qsRanking = universities_rankings.find((ranking) => ranking.name === "QS Ranking");
    if (qsRanking && qsRanking.ranking !== "Not Ranked") {
      return (
        <span className={`${styles.dynamicTag}`}>
          <Tooltip title="QS World University Rankings is an annual publication of university rankings by Quacquarelli Symonds. It is one of the five most widely used university rankings">
            <span className="me-2">
              <AiOutlineInfoCircle className="me-2" size={15} />
              QS Ranking:{" "}
            </span>
            {qsRanking.ranking}
          </Tooltip>{" "}
        </span>
      );
    }

    const timesRanking = universities_rankings.find((ranking) => ranking.name === "Times Ranking");
    if (timesRanking && timesRanking.ranking !== "Not Ranked") {
      return (
        <span className={`${styles.dynamicTag}`}>
          <Tooltip title="The Times Higher Education World University Rankings is an annual publication of university rankings by Times Higher Education magazine.">
            <span className="me-2">
              <AiOutlineInfoCircle className="me-2" size={15} />
              Times Ranking:{" "}
            </span>
            {timesRanking.ranking}
          </Tooltip>
        </span>
      );
    }

    const countryRanking = universities_rankings.find((ranking) => ranking.name === "Country Ranking");
    if (countryRanking && countryRanking.ranking !== "Not Ranked") {
      return (
        <span className={`${styles.dynamicTag}`}>
          <Tooltip title="The country ranking of a university is based on the overall performance of the university in the country.">
            <span className="me-2">
              <AiOutlineInfoCircle className="me-2" size={15} />
              Country Ranking:{" "}
            </span>
            {countryRanking.ranking}
          </Tooltip>
        </span>
      );
    }

    return null;
  };

  const renderAcceptanceRate = (university_details) => {
    if (university_details && university_details.length > 0 && parseInt(university_details[0].acceptance_ratio) > 0) {
      return (
        <span className={``}>
          <Tooltip
            title="
          The acceptance rate is the percentage of students who are accepted into a college or university.
          "
            position="bottom"
            trigger="mouseenter"
          >
            <AiOutlineInfoCircle className="me-2" size={15} />
          </Tooltip>
          <span className="me-2">👍 Acceptance Rate: </span>
          {university_details[0].acceptance_ratio}%
        </span>
      );
    }
    return null;
  };

  return (
    <div className={`${styles.universityCard}`}>
      <Col xs={12}>
        <ul>
          {university.scholarship_available && parseInt(university.scholarship_available) > 0 ? (
            <span className={`${styles.scholarshipTag} py-1`}>
              <Tooltip
                title={
                  university?.scholarship_detail?.length > 0 && university.scholarship_detail !== "NA" && university.scholarship_detail !== "0"
                    ? university.scholarship_detail
                    : "This university offers scholarship"
                }
              >
                <span className="d-flex align-items-center gap-1 ">
                  <TbDiscount2 size={15} />
                  Scholarship Available
                </span>
              </Tooltip>
            </span>
          ) : (
            renderUniversityRankingTag(university?.details?.universities_rankings)
          )}
          <li>
            <div className="d-flex flex-column gap-3">
              {" "}
              <Image
                alt={university.name}
                src={getAWSImagePathBeta(university.logo_full)}
                width={200}
                height={100}
                objectFit="contain"
                className="pt-2"
              />
              <span className="d-block d-md-none">
                <Link href={`/universities/${university.slug}`}>
                  <a>
                    <span
                      style={{
                        backgroundColor: "#c9fcf7",
                        color: "#15b5a5",
                      }}
                      className={`${styles.fees}  fs-sm p-1 px-2 text-nowrap `}
                    >
                      Visit University
                    </span>
                  </a>
                </Link>
              </span>
            </div>
          </li>
          <li>
            <span className="fs-md cursor-pointer">
              <Tooltip title={university.specializationFullName} position="bottom" trigger="mouseenter">
                <span>
                  {university.specializationFullName.length > 30
                    ? university.specializationFullName.substring(0, 30) + "..."
                    : university.specializationFullName}
                </span>
              </Tooltip>
            </span>
          </li>
          <li className="d-none d-lg-flex">
            <span className="fs-lg">{university.intake}</span>
            <span className="mt-2 text-gray fs-sm ">(yearly Intakes)</span>
          </li>
          <li className="d-none d-sm-flex">
            <span>
              <span className="fs-lg">{university.duration}</span>
              <span className={`${styles.subtitleTag}`}>(Months)</span>
            </span>
          </li>
          <li className="">
            <span className="slotMachine my-2">
              {/* Create a slot machine text effect */}
              <span className="d-block fs-sm">Approx.</span>
              <span className="d-block fs-lg">
                {university.feesNew
                  ? formatCurrencyAmount(university.feesNew, university.currencyNew)
                  : formatCurrencyAmount(university.fees, university.currency)}
              </span>
              {/* <span className="text-gray fs-xs ">(Annually)</span> */}
            </span>
            <span className="d-none d-md-block">
              <Link href={`/universities/${university.slug}`}>
                <a>
                  <span
                    style={{
                      backgroundColor: "#c9fcf7",
                      color: "#15b5a5",
                    }}
                    className={`${styles.fees}  fs-sm p-1 px-2 text-nowrap `}
                  >
                    Visit University
                  </span>
                </a>
              </Link>
            </span>
            <span className="d-block d-md-none my-1">
              <CvCounsellorPopup>
                <span className={`bg-orange text-white rounded-2 d-block fs-sm p-1 text-nowrap text-xs `}>Apply Now</span>
              </CvCounsellorPopup>
            </span>
          </li>
          <li className="d-none d-md-flex align-items-center justify-items-center">
            <AvatarGroup>
              <Avatar image="/counsellors/ramiz.png" size="small" shape="circle" />
              <Avatar image="/counsellors/dhirendra.png" size="small" shape="circle" />
              <Avatar image="/counsellors/manish.png" size="small" shape="circle" />
            </AvatarGroup>
            <span className="my-1 text-gray fs-sm">
              <CvCounsellorPopup>
                <span className={`${styles.fees} d-block fs-sm p-1 px-2 text-nowrap `}>Apply Now</span>
              </CvCounsellorPopup>
            </span>
          </li>
        </ul>
      </Col>
      <div
        className="w-100 text-center py-2 d-flex align-items-center justify-content-center gap-2"
        style={{
          backgroundColor: "#637588",
        }}
      >
        <p className="m-0 fs-sm text-white ">{university.name}</p>
        <span
          className={`d-none d-md-flex align-items-center gap-2 fs-xs px-2 rounded-5 bg-white`}
          style={{
            color: "black",
          }}
        >
          <Image alt="University Country Flag" src={getAWSImagePathBeta(university.flag)} width={10} height={10} objectFit="contain" />
          <span>{university.countryName}</span>
        </span>
      </div>
      {/* {university.entry_requirement ||
      (university.toefl_score && parseInt(university.toefl_score) > 0) ||
      (university.pte_score && parseInt(university.pte_score) > 0) ||
      (university.det_score && parseInt(university.det_score) > 0) ||
      (university.application_fee && parseInt(university.application_fee) > 0) ||
      (university.gre_score && parseInt(university.gre_score) > 0) ||
      (university.gmat_score && parseInt(university.gmat_score) > 0) ||
      (university.backlogs && parseInt(university.backlogs) > 0) ? (
        <div
          className="w-100 p-2 d-flex align-items-center gap-2"
          style={{
            backgroundColor: "#F5F8FD",
          }}
        >
          {university.entry_requirement && (
            <span className="bg-none fs-sm border border-1 p-1 px-2 rounded-3">Entry Requirement {university.entry_requirement}</span>
          )}
          {university.toefl_score && <span className="bg-none fs-sm border border-1 p-1 px-2 rounded-3">TOEFL Score: {university.toefl_score}</span>}
          {university.pte_score && <span className="bg-none fs-sm border border-1 p-1 px-2 rounded-3">PTE: {university.pte_score}</span>}
          {university.det_score && <span className="bg-none fs-sm border border-1 p-1 px-2 rounded-3">DET Score: {university.det_score}</span>}
          {university.application_fee && (
            <span className="bg-none fs-sm border border-1 p-1 px-2 rounded-3">
              Application Fee: {formatCurrencyAmount(university.application_fee, university.currency)}
            </span>
          )}
          {university.gre_score && <span className="bg-none fs-sm border border-1 p-1 px-2 rounded-3">GRE Score: {university.gre_score}</span>}
          {university.gmat_score && <span className="bg-none fs-sm border border-1 p-1 px-2 rounded-3">GMAT Score: {university.gmat_score}</span>}
          {university.backlogs && parseInt(university.backlogs) > 0 && (
            <span className="bg-none fs-sm border border-1 p-1 px-2 rounded-3">Backlogs: {university.backlogs}</span>
          )}
          <span className="bg-none fs-sm  p-1 px-2 rounded-3 text-cvblue cursor-pointer">
            View All Details <AiOutlineArrowRight />
          </span>
        </div>
      ) : null} */}

      <div className={`${styles.universityCardDescription}`}>
        {/* create a checkox, on check add to compare */}
        <div className="d-flex gap-2">
          <input
            type="checkbox"
            name="compare"
            id={`compare-check-${university.id}`}
            checked={isUniversitySelected}
            onChange={() => {
              if (isUniversitySelected) {
                removeUniversityFromCompare(university.id);
              } else {
                handleAddToCompare(university);
              }
            }}
          />
          <label htmlFor={`compare-check-${university.id}`} className="cursor-pointer fw-semibold text-cvblue">
            {isUniversitySelected ? <span>Remove from Compare</span> : <span>Add to Compare</span>}
          </label>
        </div>
        <span className="d-none d-md-flex">{renderAcceptanceRate(university?.details?.university_details)}</span>
        <div className="d-flex gap-2">
          <KnowYourUniversity university={university}>
            <span className="cursor-pointer d-flex">
              <span className="d-none d-sm-block">⌛</span> Know your university in 2 mins
              <AiOutlineArrowDown className="ms-1" />
            </span>
          </KnowYourUniversity>
        </div>
      </div>
    </div>
  );
}
