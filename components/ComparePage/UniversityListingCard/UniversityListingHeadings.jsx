import React from "react";
import styles from "./UniversityListingHeadings.module.scss";
import Col from "react-bootstrap/Col";

export default function UniversityListingHeadings() {
  return (
    <Col xs={12} className={styles.strip}>
      <ul>
        <li>
          <span>University</span>
        </li>
        <li>
          <span>Specialization</span>
        </li>
        <li className="d-none d-lg-flex">
          <span>Intakes</span>
        </li>
        <li className="d-none d-sm-flex">
          <span>Duration</span>
        </li>
        <li>
          <span>Annual Fees</span>
        </li>
        <li className="d-none d-md-flex">
          <span>Advice</span>
        </li>
      </ul>
    </Col>
  );
}
