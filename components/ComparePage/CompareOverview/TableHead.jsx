import React from "react";
import { useRouter } from "next/router";
import Image from "next/image";
import { useRecoilState } from "recoil";
import uuid from "react-uuid";
import { AiOutlineArrowLeft, AiOutlinePlusCircle } from "react-icons/ai";
import Link from "next/link";
import { toast } from "react-hot-toast";
import styles from "./CompareOverview.module.scss";
import { allUniversityToCompareModalAtom, selectedUniversitiesState } from "../../../store/compare/compareStore";
import { getAWSImagePathBeta } from "../../../context/services";

export default function TableHead({ universityToCompare }) {
  const router = useRouter();
  const [selectedUniversities, setSelectedUniversities] = useRecoilState(selectedUniversitiesState);
  const [show, setShow] = useRecoilState(allUniversityToCompareModalAtom);

  const renderUniversityComparisonColumns = (universityToCompare) => {
    return universityToCompare.map((university) => {
      if (university === 0) {
        return (
          <th key={uuid()} scope="col">
            <div
              className="d-flex align-items-center justify-content-center gap-2 cursor-pointer border py-3 m-2"
              onClick={() => handleOpenAllUniversityToCompareModal()}
            >
              <AiOutlinePlusCircle size={20} />
              <span>Add To Compare</span>
            </div>
          </th>
        );
      }
      return (
        <th key={university.slug} scope="col">
          <span className={`${styles.universityBox}`}>
            <span
              className="position-absolute top-0 end-0 translate-middle badge rounded-pill bg-danger cursor-pointer"
              onClick={() => removeUniversityFromCompare(university)}
            >
              X
            </span>
            <Image src={getAWSImagePathBeta(university.logo_full)} width={130} height={80} alt={university.name} objectFit="contain" />
            <p className="text-center fs-md mb-0 d-none d-md-block">{university.name}</p>
          </span>
          <Link href={`/universities/${university.slug}`}>
            <a className={`${styles.universityTag} px-2 py-1 bg-cvblue text-white rounded fs-sm`}>View Details</a>
          </Link>
        </th>
      );
    });
  };

  const handleGoBack = () => {
    window.location.href = `/compare/universities?uuid=${router.query.uuid}`;
  };

  const handleOpenAllUniversityToCompareModal = () => {
    setShow(true);
  };
  const removeUniversityFromCompare = (university) => {
    if (selectedUniversities.length === 1 || universityToCompare.length === 1) {
      toast.dismiss();
      toast("You can't remove all universities from compare", {
        icon: "🚫",
        style: {
          borderRadius: "10px",
          background: "#333",
          color: "#fff",
        },
      });
      return;
    }
    // remove the university from the selected universities list
    const newSelectedUniversities = selectedUniversities.filter((uni) => uni.id !== university.id);
    setSelectedUniversities(newSelectedUniversities);
    handleCompare(newSelectedUniversities);
  };
  const handleCompare = (selectedUniversities) => {
    if (selectedUniversities.length === 0)
      return;

    const universities = selectedUniversities.slice(0, 3);
    const specializations = universities.map((university) => university?.specializationId);

    const universityIds = universities.map((university) => university && `university=${university.id}`).join("&");
    const specializationIds = specializations
      .filter(Boolean)
      .map((id) => `specialization=${id}`)
      .join("&");

    window.location.href = `/compare/overview/?${universityIds}&${specializationIds}&uuid=${router.query.uuid}`;
  };

  return (
    <thead>
      <tr>
        <th scope="col">
          <span onClick={handleGoBack} className="cursor-pointer">
            <AiOutlineArrowLeft className="me-2" />
            Back
          </span>
        </th>
        {universityToCompare?.length > 0 && renderUniversityComparisonColumns(universityToCompare)}
      </tr>
    </thead>
  );
}
