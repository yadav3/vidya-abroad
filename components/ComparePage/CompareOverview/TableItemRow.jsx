import React from "react";
import uuid from "react-uuid";
import { Tooltip } from "react-tippy";
import { AiOutlineInfoCircle } from "react-icons/ai";

export default function TableItemRow({ universityToCompare, labelName, labelValue, labelTooltip, labelIcon, valueIcon, valueTooltip }) {
  const renderLabelIcon = () => {
    return labelIcon ? labelIcon : null;
  };

  const renderLabelTooltip = () => {
    return labelTooltip ? (
      <Tooltip className="m-2" title={labelTooltip} position="top" trigger="mouseenter">
        <AiOutlineInfoCircle />
      </Tooltip>
    ) : null;
  };

  const renderValueIcon = (university) => {
    return valueIcon ? valueIcon(university) : null;
  };

  const renderValueTooltip = () => {
    return valueTooltip ? (
      <Tooltip className="m-2" title={valueTooltip} position="top" trigger="mouseenter">
        <AiOutlineInfoCircle />
      </Tooltip>
    ) : null;
  };

  const renderUniversityValue = (university) => {
    return (
      <span className="d-flex flex-column flex-md-row align-items-center justify-content-center gap-2">
        {labelValue(university)}
        {renderValueIcon(university)}
        {renderValueTooltip()}
        
      </span>
    );
  };

  return (
    <tr>
      <th scope="row">
        <span className="d-flex align-items-center justify-content-start gap-2">
          {labelName}
          {renderLabelIcon()}
          {renderLabelTooltip()}
        </span>
      </th>
      {universityToCompare?.length > 0 &&
        universityToCompare.map((university) => university !== 0 ? <td key={uuid()}>{renderUniversityValue(university)}</td> : <td key={uuid()}></td>
        )}
    </tr>
  );
}
