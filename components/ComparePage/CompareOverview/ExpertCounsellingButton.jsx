import React from "react";
import { useRouter } from "next/router";
import { AiFillVideoCamera } from "react-icons/ai";
import { useRecoilValue } from "recoil";
import { cvCounsellorDetailsAtom } from "../../../store/journeyStore";

export default function ExpertCounsellingButton() {
  const router = useRouter();
  const cvCounsellorDetails = useRecoilValue(cvCounsellorDetailsAtom);

  const handleButtonClick = () => {
    window.location.href = cvCounsellorDetails?.redirectLink;
  };

  return (
    <div className="d-flex align-items-center justify-content-center gap-2">
      <button
        className="bg-cvblue text-white border-0 outline-none fs-lg p-2 p-3 px-5 pt-4 rounded-2 cursor-pointer position-relative"
        onClick={handleButtonClick}
      >
        <span
          className="d-flex align-items-center justify-content-center gap-2 position-absolute p-1 px-2 fs-sm rounded-2 bg-bright-green myshine w-75"
          style={{
            top: "-10px",
            left: "50%",
            transform: "translateX(-50%)",
          }}
        >
          <AiFillVideoCamera />
          <span>Free Video Call + Chat</span>
        </span>
        Connect with Study Abroad Experts
      </button>
    </div>
  );
}
