import React from "react";
import Image from "next/image";
import TableItemRow from "./TableItemRow";

export default function TableBody({
  universityToCompare,
  formatCurrencyAmount,
  convertCurrency,
  exchangeRates,
  isCurrencyChange,
  setIsCurrencyChange,
}) {
  return (
    <tbody>
      <TableItemRow
        universityToCompare={universityToCompare}
        labelName={"University Name"}
        labelValue={(university) => (
          <span className="d-flex flex-column flex-md-row align-items-center gap-2">
            <span>{university.name}</span>
          </span>
        )}
      />
      <TableItemRow
        universityToCompare={universityToCompare}
        labelName={"University Location"}
        labelValue={(university) => (
          <span className="d-flex flex-column flex-md-row align-items-center gap-2">
            {university.location ? university.location : university.countryName ? university.countryName : "NA"}
            {university.countryFlag && <Image src={university.countryFlag} width={20} height={20} alt={university.name} objectFit="contain" />}
          </span>
        )}
      />
      <TableItemRow
        universityToCompare={universityToCompare}
        labelName={"World Ranking"}
        labelValue={(university) => (
          <span className="d-flex flex-column flex-md-row align-items-center gap-2">
            {university.world_ranking ? university.world_ranking : "NA"}
            <span className="d-flex align-items-center gap-2">🌏</span>
          </span>
        )}
        labelTooltip={"World Ranking is based on the QS World University Ranking"}
      />
      <TableItemRow
        universityToCompare={universityToCompare}
        labelName={"Programme"}
        labelValue={(university) => (
          <span className="d-flex flex-column flex-md-row align-items-center gap-2">
            {university.courseCategory === 1 ? "UG" : university.courseCategory === 2 ? "PG" : university.courseCategory === 3 ? "Diploma" : "NA"}
          </span>
        )}
      />
      <TableItemRow
        universityToCompare={universityToCompare}
        labelName={"Course"}
        labelValue={(university) => (
          <span className="d-flex flex-column flex-md-row align-items-center gap-2">
            {university.courseFullName ? university.courseFullName : "NA"}
          </span>
        )}
      />
      <TableItemRow
        universityToCompare={universityToCompare}
        labelName={"Specialization"}
        labelValue={(university) => (
          <span className="d-flex flex-column flex-md-row align-items-center gap-2">
            {university.specializationName ? university.specializationName : "NA"}
          </span>
        )}
      />
      <TableItemRow
        universityToCompare={universityToCompare}
        labelName={"IELTS Required"}
        labelValue={(university) => (
          <span className="d-flex flex-column flex-md-row align-items-center gap-2">
            {university.specializationIelts
              ? university.specializationIelts
              : university.ielts_required
              ? university.ielts_required
              : "Not Available"}
          </span>
        )}
        labelTooltip={"IELTS is an international standardized test of English language proficiency for non-native English language speakers."}
      />
      <TableItemRow
        universityToCompare={universityToCompare}
        labelName={"Intakes"}
        labelValue={(university) => (
          <span className="d-flex flex-column flex-md-row align-items-center gap-2">
            {university.specializationIntakes ? university.specializationIntakes : "NA"}
          </span>
        )}
        labelTooltip={"Intakes are the dates when the university starts accepting applications for a particular course"}
      />
      <TableItemRow
        universityToCompare={universityToCompare}
        labelName={"Course Duration"}
        labelValue={(university) => (
          <span className="d-flex flex-column flex-md-row align-items-center gap-2">
            {university.specializationDuration ? university.specializationDuration : "NA"} months
          </span>
        )}
      />
      <TableItemRow
        universityToCompare={universityToCompare}
        labelName={"Course Fees"}
        labelValue={(university) => (
          <span className="d-flex flex-column align-items-center justify-content-center gap-2">
            {university.specializationFees ? (
              <span className="d-flex align-items-center gap-2">
                Approx.{" "}
                {formatCurrencyAmount(
                  convertCurrency(university.specializationFees, university.currency, exchangeRates, isCurrencyChange),
                  isCurrencyChange ? "INR" : university.currency
                )}
              </span>
            ) : (
              "NA"
            )}
            {university.specializationFees ? (
              <button className="bg-orange text-white p-1 px-2 rounded-4 border-0 shadow-sm fs-xs" onClick={setIsCurrencyChange}>
                Convert to {isCurrencyChange ? university.currency : "INR"}
              </button>
            ) : null}
          </span>
        )}
        labelTooltip={"Fees may vary depending on the exchange rate and the currency you choose to pay in at the time of payment"}
      />
    </tbody>
  );
}
