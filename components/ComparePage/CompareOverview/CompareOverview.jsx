// Import statements
import React, { useEffect } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useRecoilState } from "recoil";

// Style import
import styles from "./CompareOverview.module.scss";

// Recoil state import
import { universityToCompareState } from "../../../store/compare/compareStore";

// Custom hooks import
import formatCurrencyAmount, { useToggle } from "../../../context/CustomHooks";

// Utility function import
import { convertCurrency } from "../../../utils/currencyConvertor";

// Component import
import TableBody from "./TableBody";
import ExpertCounsellingButton from "./ExpertCounsellingButton";
import TableHead from "./TableHead";

// Constants
const MAX_UNIVERSITIES = 3;

// Define the CompareOverview component
export default function CompareOverview({ exchangeRates }) {
  // Get the state and value of various atoms and toggle states
  const [universityToCompare, setUniversityToCompare] = useRecoilState(universityToCompareState);
  const [isCurrencyChange, setIsCurrencyChange] = useToggle(true);

  // Use the useEffect hook to ensure that the length of the universityToCompare array never exceeds MAX_UNIVERSITIES
  useEffect(() => {
    if (universityToCompare?.length < MAX_UNIVERSITIES) {
      const newUniversityToCompare = [...universityToCompare];
      newUniversityToCompare.length = MAX_UNIVERSITIES;
      newUniversityToCompare.fill(0, universityToCompare.length);
      setUniversityToCompare(newUniversityToCompare);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [universityToCompare]);

  // Render the CompareOverview component
  return (
    <Container className={styles.container} fluid>
      <Row>
        <Col>
          <table className={`${styles.table} table  table-bordered`}>
            {/* Render the table header */}
            <TableHead universityToCompare={universityToCompare} />

            {/* Render the table body */}
            <TableBody
              universityToCompare={universityToCompare}
              formatCurrencyAmount={formatCurrencyAmount}
              convertCurrency={convertCurrency}
              exchangeRates={exchangeRates}
              isCurrencyChange={isCurrencyChange}
              setIsCurrencyChange={setIsCurrencyChange}
            />
          </table>
        </Col>
      </Row>

      {/* Render the expert counselling button */}
      <Row className="mt-4">
        <Col>
          <ExpertCounsellingButton />
        </Col>
      </Row>
    </Container>
  );
}
