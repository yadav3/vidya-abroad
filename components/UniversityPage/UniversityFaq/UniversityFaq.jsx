import React from "react";
import DynamicAccordion from "../../global/DynamicAccordion/DynamicAccordion";

export default function UniversityFaq({ university }) {
  return (
    <div className="overflow-hidden">
      <DynamicAccordion data={university.universities_faq.slice(0, 5)} heading="Frequently asked questions" bg="/bg/faq_bg.svg" />
    </div>
  );
}
