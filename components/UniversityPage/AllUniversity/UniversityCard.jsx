import React, { useState } from "react";
import Card from "react-bootstrap/Card";
import Image from "next/image";
import { FooterDivider } from "../../global/FooterContainer/FooterDivider";
import Link from "next/link";
import formatCurrencyAmount, { useLocalStorage } from "../../../context/CustomHooks";
import { useEffect } from "react";
import { LinkedinIcon, LinkedinShareButton } from "next-share";
import { UniversityCardImage } from "./UniversityCardImage";
import { UniversityCardShareButtons } from "./UniversityCardShareButtons";
import { Tooltip } from "react-tippy";
import ReactTypingEffect from "react-typing-effect";
import { convertCurrency } from "../../../utils/currencyConvertor";
import { HiChevronDoubleRight } from "react-icons/hi";
import UniversityDetailsModal from "./UniversityDetailsModal";
import styles from "./UniversityCard.module.scss";
export default function UniversityCard({ university, exchangeRates }) {
  const [userSavedUniversityIds, setUserSavedUniversityIds] = useLocalStorage("userSavedUniversityIds", []);
  const [isSaved, setIsSaved] = useState(userSavedUniversityIds.includes(university.id));

  const handleSaveUniversity = () => {
    if (isSaved) {
      const filteredUniversityIds = userSavedUniversityIds.filter((id) => id !== university.id);
      setUserSavedUniversityIds(filteredUniversityIds);
    } else {
      setUserSavedUniversityIds([...userSavedUniversityIds, university.id]);
    }
    setIsSaved(!isSaved);
  };

  const getUniversityRanking = (university) => {
    const validRanking = university.universities_rankings.find(
      (ranking) =>
        ranking.name?.length > 0 && String(ranking.ranking)?.length > 0 && ranking.name.length > 0 && ranking.ranking.trim() !== "Not Ranked"
    );

    if (validRanking) {
      return [validRanking.name, validRanking.ranking];
    } else {
      return null;
    }
  };

  const getUniversityDetails = (university) => {
    if (parseInt(university.university_details[0].acceptance_ratio) > 0) {
      return `${university.university_details[0].acceptance_ratio}% Acceptance Ratio`;
    } else if (parseInt(university.university_details[0].international_student_ratio) > 0) {
      return `${university.university_details[0].international_student_ratio}% International Student Ratio`;
    } else {
      return `Intakes: ${university.university_details[0].intakes}`;
    }
  };

  useEffect(() => {
    setIsSaved(userSavedUniversityIds.includes(university.id));
  }, [university.id, userSavedUniversityIds]);

  return (
    <Card className="mb-4 border-0 shadow-sm">
      <div className="position-relative">
        {getUniversityRanking(university) ? (
          <span
            className={`${styles.universityBadge} position-absolute fs-xs bg-orange text-white py-2 px-2 rounded-1  text-center text-truncate shadow-sm text-wrap `}
            style={{
              top: "-8px",
              right: "10px",
              zIndex: 1,
              width: "60px",
            }}
          >
            {/* <Tooltip title={getUniversityRanking(university)} position="top" trigger="mouseenter">
              {getUniversityRanking(university)}
            </Tooltip> */}
            <p className="m-0 ">{getUniversityRanking(university)[0]}</p>
            <h6 className="m-0">{getUniversityRanking(university)[1]}</h6>
          </span>
        ) : null}
        <UniversityCardImage university={university} />
        <Card.Body className="d-flex flex-column justify-content-between">
          <div>
            <Card.Title className="text-dark-gray text-truncate">
              <Tooltip title={university.name} position="top" trigger="mouseenter">
                {university.name}
              </Tooltip>
            </Card.Title>
            <Card.Subtitle className="mb-2 text-muted d-flex align-items-center">
              <Image src={university.Countries.flag} alt="country-flag" width={15} height={15} />
              <span className="ms-1 text-truncate">
                <Tooltip title={university.location ? university.location : university.Countries.name} position="top" trigger="mouseenter">
                  {university.location ? university.location : university.Countries.name}
                </Tooltip>
              </span>
            </Card.Subtitle>
            <div className="d-flex align-items-center gap-2 mb-1 w-100">
              {university.university_details[0].intakes ? (
                <span className="fs-sm mt-2 bg-light-blue py-2 px-1 rounded-2 w-100 text-center text-truncate">
                  {
                    <Tooltip title={`Available Intakes: ${university.university_details[0].intakes}`} position="top" trigger="mouseenter">
                      <span className="fw-semibold">Intakes:</span> {university.university_details[0].intakes}
                    </Tooltip>
                  }
                </span>
              ) : null}
            </div>
          </div>
          <div>
            <FooterDivider />
            <div className="d-flex align-items-center justify-content-center fs-lg">
              <span className="text-cvblue fw-semibold scrollText">
                Starting at{" "}
                <span>
                  {formatCurrencyAmount(
                    convertCurrency(university.UniversitySpecializations[0].fees, university.Countries.currency, exchangeRates, true),
                    "INR"
                  )}
                </span>
                /year
              </span>
            </div>
            <FooterDivider />
            <div className="d-flex align-items-center justify-content-between fs-md">
              <UniversityCardShareButtons university={university} />
              <Link href={`/universities/${university.slug}`}>
                <a className="m-0 bg-cvblue text-white p-2 rounded-2 fs-sm fw-semibold myshine overflow-hidden ">
                  View Details <HiChevronDoubleRight />
                </a>
              </Link>
            </div>
          </div>
        </Card.Body>
      </div>
    </Card>
  );
}
