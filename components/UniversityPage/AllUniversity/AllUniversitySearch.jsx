import React, { useState } from "react";
import { useEffect } from "react";
import { InputText } from "primereact/inputtext";
import styles from "./AllUniversityFilters.module.scss";

export const AllUniversitySearch = ({ filters, setFilters }) => {
  const [searchText, setSearchText] = useState("");

  const handleOnChange = (e) => {
    setSearchText(e.target.value);
  };

  useEffect(() => {
    const timer = setTimeout(() => {
      setFilters({
        ...filters,
        search: searchText,
      });
    }, 500);
    return () => clearTimeout(timer);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchText]);

  return (
    <span className={`p-input-icon-left w-100 border-0 ${styles.searchContainer}`}>
      <i className="pi pi-search" />
      <InputText placeholder="Search" className="w-100 border-0 shadow-sm rounded-2" value={searchText} onChange={handleOnChange} />
    </span>
  );
};
