import React from "react";
import Row from "react-bootstrap/Row";
import ReactPaginate from "react-paginate";
import styles from "./UniversitiesPagination.module.scss";
import { useBreakpoint } from "../../../context/CustomHooks";

export function UniversitiesPagination({ handlePageChange, universitiesData }) {
  const breakpoint = useBreakpoint();

  return (
    <Row className="mt-3 w-100 d-flex align-items-center justify-content-center">
      <ReactPaginate
        breakLabel="..."
        nextLabel=">"
        onPageChange={handlePageChange}
        pageRangeDisplayed={breakpoint < 321 ? 1 : breakpoint < 576 ? 2 : breakpoint < 768 ? 3 : breakpoint < 992 ? 4 : 5}
        marginPagesDisplayed={breakpoint < 321 ? 1 : breakpoint < 576 ? 1 : breakpoint < 768 ? 2 : breakpoint < 992 ? 3 : 4}
        pageCount={universitiesData.totalPages}
        previousLabel="<"
        renderOnZeroPageCount={null}
        className="d-flex align-items-center justify-content-center gap-3 "
        nextClassName={styles.nextBtn}
        previousClassName={styles.prevBtn}
        pageClassName={styles.pageBtn}
        activeClassName={styles.activeBtn}
        breakClassName={styles.breakBtn}
        breakLinkClassName={styles.breakLinkBtn}
      />
    </Row>
  );
}
