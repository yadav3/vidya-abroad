import React from "react";
import styles from "./AllUniversityFilters.module.scss";
import uuid from "react-uuid";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Image from "next/image";
import { Checkbox } from "primereact/checkbox";
import { GiWorld } from "react-icons/gi";
import { AiOutlineDollar } from "react-icons/ai";
import { BsBook } from "react-icons/bs";
import { Dropdown } from "primereact/dropdown";
import { InputNumber } from "primereact/inputnumber";
import { useEffect } from "react";
import { useState } from "react";
import { useMemo } from "react";
import { getBudgetByStringLimit } from "../../../utils/misc";

export default function AllUniversityFilters({ exchangeRates, countries, filters, setFilters }) {
  return (
    <Row>
      <Col>
        <div className={`${styles.filterContainer} bg-white shadow-sm rounded-3 p-3 d-flex flex-column gap-4 mb-4 position-sticky top-0`}>
          <CountryFilter countries={countries} filters={filters} setFilters={setFilters} />
          <BudgetFilter filters={filters} setFilters={setFilters} exchangeRates={exchangeRates} />
          {/* <IELTSFilter filters={filters} setFilters={setFilters} /> */}
        </div>
      </Col>
    </Row>
  );
}

const CountryFilter = ({ countries, filters, setFilters }) => {
  const [selectedCountries, setSelectedCountries] = useState([filters.countries]);

  useEffect(() => {
    setSelectedCountries(filters.countries);
  }, [filters.countries]);

  const handleCountryChange = (countryId) => {
    if (selectedCountries.includes(countryId)) {
      const newSelectedCountries = selectedCountries.filter((country) => country !== countryId);
      setSelectedCountries(newSelectedCountries);
      setFilters({ ...filters, countries: newSelectedCountries, sortBy: "country_id", orderBy: "asc" });
    } else {
      setSelectedCountries([...selectedCountries, parseInt(countryId)]);
      setFilters({ ...filters, countries: [...selectedCountries, countryId], sortBy: "country_id", orderBy: "asc" });
    }
  };

  return (
    <>
      <div className="d-flex flex-column gap-2">
        <p className="mb-2 fs-md d-flex align-items-center justify-content gap-2">
          <GiWorld />
          Filter Universities by Country
        </p>
        <div className="d-flex align-items-center gap-2 w-100 fs-md">
          <Checkbox
            inputId={"all-country"}
            name={"All"}
            value={0}
            checked={selectedCountries.length === 0}
            onChange={() => {
              setSelectedCountries([]);
              setFilters({ ...filters, countries: [] });
            }}
          />
          <label htmlFor={"all-country"}>All (Not Decided Yet)</label>
        </div>
        {countries.map((option) => (
          <div className="d-flex align-items-center gap-2 w-100 fs-md" key={uuid()}>
            <Checkbox
              inputId={option.fullName}
              name={option.fullName}
              value={option.id}
              checked={selectedCountries.includes(option.id)}
              onChange={() => {
                handleCountryChange(option.id);
              }}
            />
            <label htmlFor={option.fullName} className="d-flex align-items-center gap-2">
              <Image className="rounded-circle overflow-hidden" src={option.flag} alt={option.fullName} width={15} height={15} />
              {option.fullName}
            </label>
          </div>
        ))}
      </div>
    </>
  );
};

const BudgetFilter = ({ filters, setFilters }) => {
  const [selectedBudget, setSelectedBudget] = useState("Not Decided Yet");

  const budgetOption = useMemo(() => {
    return [
      {
        id: 0,
        value: "Not Decided Yet",
        label: "Not Decided Yet",
        budgetLow: 0,
        budgetHigh: 0,
      },
      {
        id: 1,
        value: "10L - 20L",
        label: "10 Lakh to 20 Lakh",
        budgetLow: 1000000,
        budgetHigh: 2000000,
      },
      {
        id: 2,
        value: "20L - 30L",
        label: "20 Lakh to 30 Lakh",
        budgetLow: 2000000,
        budgetHigh: 3000000,
      },
      {
        id: 3,
        value: "30L - 40L",
        label: "30 Lakh to 40 Lakh",
        budgetLow: 3000000,
        budgetHigh: 4000000,
      },
      {
        id: 4,
        value: "40L - 50L",
        label: "40 Lakh to 50 Lakh",
        budgetLow: 4000000,
        budgetHigh: 5000000,
      },
      {
        id: 5,
        value: "More than 50L",
        label: "More than 50L",
        budgetLow: 5000000,
        budgetHigh: 50000000,
      },
    ];
  }, []);

  const handleBudgetChange = (e) => {
    setSelectedBudget(e.value);
    const [budgetLow, budgetHigh] = getBudgetByStringLimit(e.value);
    setFilters({ ...filters, budgetLow, budgetHigh });
  };
  return (
    <div className="d-flex flex-column">
      <label htmlFor="budget" className="mb-2 fs-md d-flex align-items-center gap-2">
        <AiOutlineDollar />
        Budget
      </label>
      <Dropdown
        options={budgetOption}
        value={selectedBudget}
        onChange={handleBudgetChange}
        optionLabel="label"
        optionValue="value"
        placeholder="Select a your budget"
      />
    </div>
  );
};
const IELTSFilter = ({ filters, setFilters }) => {
  const [selectedIelts, setSelectedIelts] = useState(10);

  const handleIeltsChange = (e) => {
    setSelectedIelts(e.value);
    setFilters({ ...filters, ielts: e.value });
  };

  return (
    <div className="d-flex flex-column">
      <label htmlFor="budget" className="mb-2 fs-md d-flex align-items-center gap-2">
        <BsBook />
        IELTS Required Under
      </label>
      <InputNumber value={selectedIelts} onChange={handleIeltsChange} placeholder="IELTS score required" max={10} min={0} />
    </div>
  );
};
