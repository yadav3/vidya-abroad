import React from "react";
import Card from "react-bootstrap/Card";
import styles from "./UniversityCard.module.scss";
import { getAWSImagePath, getAWSImagePathBeta } from "../../../context/services";
import Image from "next/image";
import { Tooltip } from "react-tippy";
import { AiFillCheckCircle } from "react-icons/ai";
export function UniversityCardImage({ university }) {
  return (
    <div className="position-relative">
      <Card.Img
        variant="top"
        src={getAWSImagePathBeta(university.background_img)}
        className={`${styles.cardImage}`}
        style={{
          objectFit: "cover",
          height: "250px",
        }}
      />
      <div className={`${styles.cardImageOverlayTag}  rounded-3 position-absolute top-0 start-0 m-2 d-flex align-items-center p-2`}>
        <Image src={getAWSImagePath(university.logo_full)} alt={university.name} width={80} height={30} objectFit="contain" />
      </div>
      <span
        className={`position-absolute start-0 fs-md bg-bright-green text-white py-1 px-2 text-center text-truncate`}
        style={{
          bottom: "8px",
          zIndex: 1,
        }}
      >
        <Tooltip title={university.ielts_required} position="top" trigger="mouseenter">
          <AiFillCheckCircle className="me-1" />
          IELTS Required: {university.ielts_required}
        </Tooltip>
      </span>
    </div>
  );
}
