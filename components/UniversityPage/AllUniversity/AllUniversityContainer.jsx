import React, { useState } from "react";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import UniversityCard from "./UniversityCard";
import { getAllUniversities } from "../../../services/universitiesService";
import uuid from "react-uuid";
import { useEffect } from "react";
import { UniversitiesPagination } from "./UniversitiesPagination";
import NoResultFound from "../../global/NoResultFound/NoResultFound";
import { UniversitySortSearchFilters } from "./UniversitySortSearchFilters";

export default function AllUniversityContainer({ universitiesData, setUniversitiesData, filters, setFilters, exchangeRates }) {
  const [allUniversities, setAllUniversities] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);

  const handlePageChange = (data) => {
    setCurrentPage(data.selected + 1);
    window.scrollTo(0, 0);
  };

  useEffect(() => {
    setAllUniversities(universitiesData.universities);
  }, []);

  useEffect(() => {
    getAllUniversities(
      currentPage,
      18,
      filters.sortBy,
      filters.orderBy,
      JSON.stringify(filters.countries),
      filters.budgetLow,
      filters.budgetHigh,
      filters.ielts,
      filters.search
    )
      .then((res) => {
        setUniversitiesData(res);
        setAllUniversities(res.universities);
      })
      .catch((err) => {
        console.error(err);
      });
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentPage, filters.countries, filters.budgetLow, filters.budgetHigh, filters.ielts, filters.sortBy, filters.orderBy, filters.search]);

  useEffect(() => {
    setCurrentPage(1);
  }, [filters.countries, filters.budgetLow, filters.budgetHigh, filters.ielts, filters.sortBy, filters.orderBy, filters.search]);

  return (
    <>
      <Row>
        <Col>
          <UniversitySortSearchFilters filters={filters} setFilters={setFilters} />
        </Col>
      </Row>
      <Row>
        {allUniversities?.length === 0 ? (
          <Col>
            <NoResultFound
              heading="No University Found"
              subheading="We couldn't find any university matching your search criteria. Please try again with different filters."
              showCtaText={false}
            />
          </Col>
        ) : (
          allUniversities.map((university) => (
            <Col md={6} lg={4} key={uuid()}>
              <UniversityCard exchangeRates={exchangeRates} university={university} />
            </Col>
          ))
        )}
      </Row>
      {allUniversities?.length > 0 && <UniversitiesPagination handlePageChange={handlePageChange} universitiesData={universitiesData} />}
    </>
  );
}
