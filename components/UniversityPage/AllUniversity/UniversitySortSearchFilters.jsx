import React from "react";
import { AllUniversitySearch } from "./AllUniversitySearch";
import { SortFilter } from "./SortFilter";
import styles from "./AllUniversityFilters.module.scss";

export function UniversitySortSearchFilters({ filters, setFilters }) {
  return (
    <div className={`d-flex flex-column flex-lg-row align-items-center justify-content-center justify-content-lg-start gap-3 mb-4`}>
      <SortFilter filters={filters} setFilters={setFilters} />
      <AllUniversitySearch filters={filters} setFilters={setFilters} />
    </div>
  );
}
