import React, { useState } from "react";
import { Dropdown } from "primereact/dropdown";
import { useMemo } from "react";
import styles from "./AllUniversityFilters.module.scss";
export const SortFilter = ({ filters, setFilters }) => {
  const [selectedSort, setSelectedSort] = useState(1);

  const sortOptions = useMemo(() => {
    return [
      {
        id: 1,
        label: "IELTS Required (High to Low)",
        value: "ielts_required",
        order: "desc",
      },
      {
        id: 2,
        label: "IELTS Required (Low to High)",
        value: "ielts_required",
        order: "asc",
      },
      // {
      //   id: 3,
      //   label: "Ranking (High to Low)",
      //   value: "world_ranking",
      //   order: "desc",
      // },
      // {
      //   id: 4,
      //   label: "Ranking (Low to High)",
      //   value: "world_ranking",
      //   order: "asc",
      // },
    ];
  }, []);

  const handleSortChange = (e) => {
    const sort_id = e.value;
    const newSelectedSort = sortOptions.find((sort) => sort.id === sort_id);
    setSelectedSort(newSelectedSort.id);
    setFilters({
      ...filters,
      sortBy: newSelectedSort.value,
      orderBy: newSelectedSort.order,
    });
  };

  return (
    <div className={`d-flex align-items-center gap-2 ${styles.sortFilterContainer}`}>
      <Dropdown
        className={`border-0 rounded-2 shadow-sm ${styles.sortFilter}`}
        options={sortOptions}
        value={selectedSort}
        onChange={handleSortChange}
        optionLabel="label"
        optionValue="id"
        placeholder="Select an option"
      />
    </div>
  );
};
