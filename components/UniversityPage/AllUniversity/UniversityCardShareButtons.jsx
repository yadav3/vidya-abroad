import React from "react";
import {
  FacebookIcon,
  FacebookShareButton,
  LinkedinIcon,
  LinkedinShareButton,
  TwitterIcon,
  TwitterShareButton,
  WhatsappIcon,
  WhatsappShareButton,
} from "next-share";

export function UniversityCardShareButtons({ university }) {
  return (
    <div className="d-flex align-items-center gap-2">
      <FacebookShareButton
        url={`https://collegevidyaabroad.com/universities/${university.slug}`}
        quote={university.slug}
        hashtag={"#collegevidyaabroad"}
      >
        <FacebookIcon size={32} round />
      </FacebookShareButton>
      <WhatsappShareButton url={`https://collegevidyaabroad.com/universities/${university.slug}`} title={university.name} separator=":  ">
        <WhatsappIcon size={32} round />
      </WhatsappShareButton>
      <LinkedinShareButton url={`https://collegevidyaabroad.com/universities/${university.slug}`} title={university.name} summary={university.name}>
        <LinkedinIcon size={32} round />
      </LinkedinShareButton>
    </div>
  );
}
