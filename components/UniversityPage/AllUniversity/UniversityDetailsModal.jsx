import React from "react";
import Offcanvas from "react-bootstrap/Offcanvas";
import Button from "react-bootstrap/Button";
import { useState } from "react";
import Image from "next/image";
import { getAWSImagePath } from "../../../context/services";
import { useQuery } from "react-query";
import { getUniversityDetailsBySlug } from "../../../services/universitiesService";
import { Skeleton } from "primereact/skeleton";
import { FooterDivider } from "../../global/FooterContainer/FooterDivider";
import SectionHeading from "../../global/SectionHeading/SectionHeading";
import ListGroup from "react-bootstrap/ListGroup";
import uuid from "react-uuid";
import { Tree } from "primereact/tree";

export default function UniversityDetailsModal({
  children = (
    <Button variant="primary" className="d-lg-none">
      Launch
    </Button>
  ),
  university,
}) {
  const [show, setShow] = useState(false);

  const [showFullDescription, setShowFullDescription] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const {
    data: universityDetails,
    isLoading,
    isError,
  } = useQuery(["university", university.slug], async () => await getUniversityDetailsBySlug(university.slug), {
    enabled: show,
  });

  const renderUniversityCoursesAsTree = (courses) => {
    /*   {
                            key: "0",
                            label: "Documents",
                            data: "Documents Folder",
                            icon: "pi pi-fw pi-inbox",
                            children: [
                              {
                                key: "0-0",
                                label: "Work",
                                data: "Work Folder",
                                icon: "pi pi-fw pi-cog",
                                children: [
                                  { key: "0-0-0", label: "Expenses.doc", icon: "pi pi-fw pi-file", data: "Expenses Document" },
                                  { key: "0-0-1", label: "Resume.doc", icon: "pi pi-fw pi-file", data: "Resume Document" },
                                ],
                              },
                              {
                                key: "0-1",
                                label: "Home",
                                data: "Home Folder",
                                icon: "pi pi-fw pi-home",
                                children: [{ key: "0-1-0", label: "Invoices.txt", icon: "pi pi-fw pi-file", data: "Invoices for this month" }],
                              },
                            ],
                          }, */
    /* [
        {
            "Courses": {
                "id": 37,
                "name": "BSc",
                "fullName": "Bachelor of Science",
                "Specializations": [
                    {
                        "id": 1708,
                        "name": "Mechanical Engineering",
                        "full_name": "BE Mechanical Engineering",
                        "icon": null,
                        "duration": null
                    },
                    {
                        "id": 1709,
                        "name": "BE Civil Engineering",
                        "full_name": "BE Civil Engineering",
                        "icon": null,
                        "duration": null
                    }
                ]
            }
        },
        {
            "Courses": {
                "id": 53,
                "name": "BE",
                "fullName": "Bachelor of Engineering",
                "Specializations": [
                    {
                        "id": 1708,
                        "name": "Mechanical Engineering",
                        "full_name": "BE Mechanical Engineering",
                        "icon": null,
                        "duration": null
                    },
                    {
                        "id": 1709,
                        "name": "BE Civil Engineering",
                        "full_name": "BE Civil Engineering",
                        "icon": null,
                        "duration": null
                    }
                ]
            }
        },
    
    ] */

    const treeData = courses.map((course) => {
      return {
        key: course.Courses.id,
        label: course.Courses.fullName,
        data: course.Courses.fullName,
        children: course.Courses.Specializations.map((specialization) => {
          return {
            key: specialization.id,
            label: specialization.full_name,
            duration: specialization.duration,
            data: specialization.full_name,
          };
        }),
      };
    });
    return treeData;
  };

  return (
    <>
      {React.cloneElement(children, { onClick: handleShow })}

      <Offcanvas show={show} onHide={handleClose} placement="end" size="lg">
        <Offcanvas.Header closeButton>
          <div className="border border-black p-1 rounded-3 d-flex align-items-center justify-content-center shadow-sm">
            <Image src={getAWSImagePath(university.logo_full)} alt={university.name} width={80} height={40} objectFit="contain" />
          </div>
          <Offcanvas.Title className="ms-3">{university.name}</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body className="position-relative pb-0">
          {isLoading ? (
            <UniversityDetailsModalSkeleton />
          ) : (
            <div className="d-flex flex-column justify-content-center align-items-center">
              {/* University Overview */}
              <div className="w-100 mt-3">
                <p className="mb-2">
                  {showFullDescription
                    ? universityDetails?.universities_overview[0].value
                    : universityDetails?.universities_overview[0].value.slice(0, 200).concat("...")}
                  <span className="text-primary ms-2 cursor-pointer" onClick={() => setShowFullDescription((prev) => !prev)}>
                    {showFullDescription ? "Read less" : "Read more"}
                  </span>
                </p>
              </div>

              <FooterDivider className="w-100 my-2" />
              {/* University Courses */}
              {universityDetails?.UniversityCourses?.length > 0 ? (
                <div className="w-100 ">
                  <SectionHeading
                    heading={`<span class="text-primary">Courses </span>Offered`}
                    subHeading={`Here are some quick facts about ${university.name}`}
                    className="text-left"
                    headingClass="fs-lg"
                    subHeadingClass="fs-sm"
                  />
                  <div className="card flex justify-content-center">
                    <Tree
                      value={renderUniversityCoursesAsTree(universityDetails?.UniversityCourses)}
                      filter
                      filterPlaceholder="Search Course"
                      className="w-full md:w-30rem"
                    />
                  </div>
                </div>
              ) : null}

              <FooterDivider className="w-100 my-2" />
              {/* University Quick Facts */}
              <div className="w-100">
                <SectionHeading
                  heading={`<span class="text-primary">Quick </span>Facts`}
                  subHeading={`Here are some quick facts about ${university.name}`}
                  className="text-left"
                  headingClass="fs-lg"
                  subHeadingClass="fs-sm"
                />
                <ListGroup className="w-100" numbered>
                  {universityDetails?.universities_quick_facts.map((fact) => (
                    <ListGroup.Item key={uuid()} className="">
                      <span className="fs-md">{fact.name}</span>
                    </ListGroup.Item>
                  ))}
                </ListGroup>
              </div>

              <div className="w-100 position-sticky bottom-0 p-2 bg-white">
                <button
                  className="bg-cvblue myshine text-white rounded-3 px-3 py-2 mt-3 border-0 w-100"
                  onClick={() => {
                    handleClose();
                    window.location.href = `/universities/${university.slug}`;
                  }}
                >
                  Know More
                </button>
              </div>
            </div>
          )}
        </Offcanvas.Body>
      </Offcanvas>
    </>
  );
}

const UniversityDetailsModalSkeleton = () => {
  return (
    <div className="d-flex flex-column justify-content-center align-items-center">
      <div className="w-100">
        <Skeleton height={200} />
      </div>
      <div className="w-100 mt-3">
        <Skeleton height={30} count={3} className="mb-2" />
      </div>
    </div>
  );
};
