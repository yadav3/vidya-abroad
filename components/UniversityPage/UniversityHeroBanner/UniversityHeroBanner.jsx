import Image from "next/image";
import React from "react";
import styles from "./UniversityHeroBanner.module.scss";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { getAWSImagePath, getAWSImagePathBeta } from "../../../context/services";
import { useBreakpoint } from "../../../context/CustomHooks";

export default function UniversityHeroBanner({ university }) {
  const breakpoint = useBreakpoint();
  return (
    <Container fluid>
      <Row>
        <Col className={styles.heroBanner}>
          <div className={`${styles.universityImageContainer} position-relative`}>
            <Image src={getAWSImagePathBeta(university?.background_img)} alt={university.name} width={1900} height={700} layout="responsive" />
          </div>
          {breakpoint > 992 ? (
            <div className={`${styles.titleBanner} d-none d-lg-flex `}>
              <div className={`${styles.logo}`}>
                {university?.logo ? (
                  <Image src={getAWSImagePathBeta(university.logo)} alt="University Logo" width={100} height={100} objectFit="contain" />
                ) : university?.logo_full ? (
                  <Image src={getAWSImagePathBeta(university.logo_full)} alt="University Logo" width={150} height={70} objectFit="contain" />
                ) : null}
              </div>
              <div>
                <h1 className="text-center mt-3 mb-2">{university.name}</h1>
                <span className="text-center  gap-2 d-flex">
                  <p className="text-center text-white fs-4 mb-0">In {university?.Countries?.name}</p>
                  <span
                    className={styles.flag}
                    style={{
                      borderRadius: "50%",
                      backgroundColor: "#fff",
                      border: "2px solid #fff",
                    }}
                  >
                    <Image alt="Country Flag" src={getAWSImagePathBeta(university?.Countries?.flag)} width={30} height={30} objectFit="contain" />
                  </span>
                </span>
              </div>
            </div>
          ) : null}
        </Col>
      </Row>

      {breakpoint <= 992 ? (
        <Row>
          <Col>
            <div className={`d-flex flex-column d-lg-none align-items-center justify-content-center py-4 ${styles.titleBarMobile}`}>
              <div className={``}>
                <Image src={getAWSImagePathBeta(university.logo_full)} alt="University Logo" width={100} height={50} objectFit="contain" />
              </div>
              <small className="m-0 text-muted text-center">In {university.location}</small>
              <h1 className="m-0 fs-4 text-center">{university.name}</h1>
            </div>
          </Col>
        </Row>
      ) : null}
    </Container>
  );
}
