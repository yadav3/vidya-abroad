import React from "react";
import UniversityHeroBanner from "./UniversityHeroBanner/UniversityHeroBanner";
import UniversityOverview from "./UniversityOverview/UniversityOverview";
import UniversityQuickFacts from "./UniversityQuickFacts/UniversityQuickFacts";
import UniversityBenefitsRanking from "./UniversityBenefitsRanking/UniversityBenefitsRanking";
import UniversityCourseFilter from "./UniversityCourseFilter/UniversityCourseFilter";
import UniversityAdmissionReq from "./UniversityAdmissionReq/UniversityAdmissionReq";
import UniversityPlacementBanner from "./UniversityPlacementBanner/UniversityPlacementBanner";
import UniversityFaq from "./UniversityFaq/UniversityFaq";
import UniversityExploreMore from "./UniversityExploreMore/UniversityExploreMore";
import HomeFormContainer from "../global/HomeFormContainer/HomeFormContainer";
import { FooterDivider } from "../global/FooterContainer/FooterDivider";
import UniversityGoodToKnowSection from "./UniversityGoodToKnowSection/UniversityGoodToKnowSection";
import QuickCompareStrip from "../global/QuickCompareStrip/QuickCompareStrip";

export default function UniversityPage({ universitiesData, majors }) {
  return (
    <>
      <UniversityHeroBanner university={universitiesData} />
      {/* <QuickCompareStrip /> */}
      <UniversityOverview university={universitiesData} />
      <FooterDivider className="py-3" />
      <UniversityQuickFacts university={universitiesData} />
      <FooterDivider className="py-3" />
      <UniversityGoodToKnowSection university={universitiesData} />
      <FooterDivider className="py-3" />
      <UniversityBenefitsRanking university={universitiesData} />
      <FooterDivider className="py-3" />
      <UniversityCourseFilter university={universitiesData} />
      <FooterDivider className="py-3" />
      <UniversityAdmissionReq university={universitiesData} />
      {universitiesData.universities_famous_companies?.length > 0 ? (
        <>
          <FooterDivider className="py-5" />
          <UniversityPlacementBanner university={universitiesData} />
          <FooterDivider className="py-5" />
        </>
      ) : null}
      <UniversityFaq university={universitiesData} />
      <div className="overflow-hidden">
        <HomeFormContainer majors={majors} />
      </div>
      <UniversityExploreMore university={universitiesData} />
    </>
  );
}
