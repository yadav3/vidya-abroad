import Image from "next/image";
import React from "react";
import styles from "./UniversityBenefitsRanking.module.scss";
import uuid from "react-uuid";
import { AiFillStar, AiOutlineExclamation, AiOutlineExclamationCircle } from "react-icons/ai";
import BigTextHeading from "../../global/BigTextHeading/BigTextHeading";
import SectionHeading from "../../global/SectionHeading/SectionHeading";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Tooltip } from "react-bootstrap";

export default function UniversityBenefitsRanking({ university }) {
  return (
    <Container className="py-5">
      <Row>
        <Col>
          <SectionHeading
            heading={`<span>Global <span class="text-cvblue">Ranking</span></span>`}
            subHeading={`
        Find out how ${university.name} ranks in the world and in the country.
        `}
            className="text-center mb-4"
          />
        </Col>
      </Row>
      <Row>
        <Col>
          <UniversityRankings university={university} />
        </Col>
      </Row>
    </Container>
  );
}

function UniversityRankings({ university }) {
  return (
    <div className={`${styles.rankingContainer} d-flex align-items-center justify-content-center gap-3 flex-wrap`}>
      {university.universities_rankings.map((ranking, i) => (
        <RankingListItem ranking={ranking} key={uuid()} />
      ))}
    </div>
  );
}

function RankingListItem({ ranking }) {
  return (
    <div className={`${styles.listItem} d-flex align-items-center gap-1 p-2 px-4 rounded-4 myshine`}>
      <div>
        <AiFillStar size={30} />
      </div>
      <div className={`${styles.listItemContent} d-flex flex-column `}>
        <p className="fs-5 mb-0">{ranking.ranking}</p>
        <small className="m-0 text-muted">{ranking.name}</small>
      </div>
    </div>
  );
}

// function RankingListItem({ ranking }) {
//   return (
//     <div className={`${styles.listItem}`}>
//       <div className={`${styles.listItemLeft} d-flex align-items-center gap-2 `}>
//         <AiFillStar size={40} />
//         <p className="fs-3 mb-0">{ranking.ranking}</p>
//         <div className={`${styles.rankingBg}`}>
//           <Image src={`/bg/ranking_text_bg_filler${i + 1}.svg`} alt="Ranking Icon" width={100} height={100} objectFit="contain" />
//         </div>
//       </div>
//       <div className={`${styles.listItemRight} d-flex flex-column align-items-start justify-content-center px-4`}>
//         <h6 className="m-0 fs-4">{ranking.name}</h6>
//       </div>
//     </div>
//   );
// }
