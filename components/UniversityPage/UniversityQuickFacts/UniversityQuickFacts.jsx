import React from "react";
import styles from "./UniversityQuickFacts.module.scss";
import uuid from "react-uuid";
import BigTextHeading from "../../global/BigTextHeading/BigTextHeading";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import SectionHeading from "../../global/SectionHeading/SectionHeading";
import { AiOutlineBulb, AiOutlineQuestionCircle } from "react-icons/ai";
import { HiLightBulb } from "react-icons/hi";
import Link from "next/link";
import { useRouter } from "next/router";

export default function UniversityQuickFacts({ university }) {
  const router = useRouter();
  return (
    <Container>
      <Row>
        <Col className={`${styles.right} d-flex flex-column  justify-content-center px-4  px-sm-5`}>
          <SectionHeading
            heading={`<span>Quick <span class="text-cvblue">Facts</span></span>`}
            subHeading={`Here are some quick facts about ${university.name} that you should know:`}
            className="mb-4 text-center"
          />
          <Row>
            {university.universities_quick_facts.map((university, i) => (
              <Col className="mb-3" md={6} key={uuid()}>
                <div className={`${styles.quickFactCard} border border-grey  rounded-3 p-3 d-flex align-items-center gap-1 gap-sm-2 `}>
                  <div
                    className={`bg-white rounded-circle myshine bulbHoverAnimation ${styles.quickFactIcon} `}
                    style={{
                      color: "#FFC107",
                    }}
                  >
                    <HiLightBulb size={30} />
                  </div>
                  <div>
                    <p className="m-0 ">{university.name}</p>
                  </div>
                </div>
              </Col>
            ))}
          </Row>
          <button
            className={`w-max mx-auto border-0 rounded-pill px-4 py-3 mt-4 bg-cvblue text-white`}
            onClick={() => router.push("/suggest-me-a-university")}
          >
            Connect with an Expert
          </button>
        </Col>
      </Row>
    </Container>
  );
}
