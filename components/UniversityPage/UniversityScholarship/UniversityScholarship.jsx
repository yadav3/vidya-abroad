import React from "react";
import Image from "next/image";
import styles from "./UniversityScholarship.module.scss";

const scholarships = [
  "Boustany MBA Harvard Scholarship PG",
  "The Horace W. Goldsmith Fellowship PG",
  "HGSE Financial Aid Scholarship Open to all",
  "Rotary International Ambassadorial Scholarships",
  "The Robert S Kalpan Life Sciences Fellowship PG",
  "Need Based Fellowship PG",
];

export default function UniversityScholarship() {
  return (
    <div className={`${styles.container}`} id="scholarship">
      <div className={`${styles.scholarshipContainer}`}>
        <div className={`${styles.left}`}>
          <h2>Scholarships</h2>
          <div className={`${styles.leftContainer}`}>
            {scholarships.map((item, i) => {
              return (
                <div
                  key={`scholarshipitem-${i}`}
                  className={`${styles.listItem}`}
                >
                  <Image
                    alt="List Item Icon"
                    src="/icons/ranking.png"
                    width={30}
                    height={30}
                  />
                  <p>{item}</p>
                </div>
              );
            })}
          </div>
        </div>
        <div className={`${styles.right}`}>
          <Image
            src={"/country/countrypage/scholarship_girl.png"}
            alt="Fast Facts"
            width={295.73}
            height={300}
            objectFit="contain"
          />
        </div>
      </div>
    </div>
  );
}
