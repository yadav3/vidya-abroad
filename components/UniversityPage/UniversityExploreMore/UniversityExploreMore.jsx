import React from "react";
import Image from "next/image";
import styles from "./UniversityExploreMore.module.scss";
import Link from "next/link";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import "swiper/css/pagination";
import { Pagination } from "swiper";
import { useState } from "react";
import uuid from "react-uuid";
import { useRouter } from "next/router";
import SectionHeading from "../../global/SectionHeading/SectionHeading";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { getAWSImagePathBeta } from "../../../context/services";
import Card from "react-bootstrap/Card";
import { Button } from "react-bootstrap";

export default function UniversityExploreMore({ university }) {
  return (
    <Container className="mb-5">
      <Row>
        <Col>
          <SectionHeading
            heading={`<span>Explore <span class="text-cvblue">More</span></span>`}
            subHeading={`You might also like these universities`}
          />
        </Col>
        <Col>
          <Row className={`${styles.universityContainer}`}>
            <Swiper
              slidesPerView={1}
              spaceBetween={30}
              // pagination={{
              //   clickable: true,
              // }}
              modules={[Pagination]}
              breakpoints={{
                600: {
                  slidesPerView: 2,
                  spaceBetween: 30,
                },
                800: {
                  slidesPerView: 3,
                  spaceBetween: 30,
                },
                1000: {
                  slidesPerView: 4,
                  spaceBetween: 30,
                },
              }}
              className="topUniversitiesSlider"
            >
              {university.relatedUniversities?.length > 0
                ? university.relatedUniversities.map((university, i) => {
                    return (
                      <SwiperSlide key={uuid()}>
                        <UnivCard university={university} />
                      </SwiperSlide>
                    );
                  })
                : null}
            </Swiper>
          </Row>
        </Col>
      </Row>

      {/* 
      <div className="text-center mt-4">
        <button className={styles.viewBtn}>View All</button>
      </div> */}
    </Container>
  );
}

function UnivCard({ university }) {
  return (
    <Card className=" mx-1 mx-0 h-100">
      <Link href={`/universities/${university.slug}`}>
        <a className="h-100">
          <div className="position-relative ">
            <Card.Img variant="top" style={{ height: "200px" }} src={getAWSImagePathBeta(university.universities_images[0]?.image_path)} />
            <span className="position-absolute top-0 start-0  bg-cvblue px-2 py-1 m-2 text-white rounded-2 fs-md">
              Minimum IELTS: {university.ielts_required}
            </span>
          </div>
          <Card.Body className="mb-0 ">
            <Card.Title>{university.name}</Card.Title>
            <Card.Text>{university.location}</Card.Text>
            {/* badges */}
            {/* <div className="d-flex flex-wrap gap-2">
              {university?.universities_rankings?.length > 0
                ? university.universities_rankings.slice(0, 1).map((ranking, i) => {
                    return (
                      <span key={uuid()} className="w-max text-white bg-dark-grey rounded-2 px-2 py-1 fs-sm">
                        🏆 {ranking.name}: {ranking.ranking}
                      </span>
                    );
                  })
                : null}
            </div> */}
          </Card.Body>
        </a>
      </Link>
    </Card>
  );
}
