import Image from "next/image";
import React, { useState } from "react";
import styles from "./UniversityAdmissionReq.module.scss";
import uuid from "react-uuid";
import BigTextHeading from "../../global/BigTextHeading/BigTextHeading";
import SectionHeading from "../../global/SectionHeading/SectionHeading";
import Form from "react-bootstrap/Form";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import ProgressBar from "react-bootstrap/ProgressBar";
import { useEffect } from "react";
import { useRouter } from "next/router";

export default function UniversityAdmissionReq({ university }) {
  const [checkCount, setCheckCount] = useState(0);
  const [checkList, setCheckList] = useState({});
  const [isAllChecked, setIsAllChecked] = useState(false);
  const router = useRouter();

  useEffect(() => {
    if (university.university_admission_req[0].items?.length > 0) {
      if (checkCount === university.university_admission_req[0].items.length) {
        setIsAllChecked(true);
      } else {
        setIsAllChecked(false);
      }
    }
  }, [checkCount, university.university_admission_req]);

  return (
    <Container className={`${styles.container}`} fluid="xl">
      <Row>
        <Col>
          <SectionHeading
            heading={`<span>Admission <span class="text-cvblue">Requirements</span></span>`}
            subHeading={`Find out what you need to apply to ${university.name} and what documents you need to submit.`}
            className="text-center my-4"
          />
        </Col>
      </Row>
      <Row>
        <ProgressBar
          className={`${styles.progressBar} university-admission-requirements-progress rounded-5 mx-auto p-0`}
          now={
            university.university_admission_req[0].items?.length > 0 ? (checkCount / university.university_admission_req[0].items.length) * 100 : 0
          }
          label={`${
            university.university_admission_req[0].items?.length > 0
              ? university.university_admission_req[0].items?.length === checkCount
                ? `Yay! You have met all the requirements`
                : `${checkCount} of ${university.university_admission_req[0].items.length} requirements met`
              : "No requirements"
          }`}
        />
      </Row>
      <Row className={`align-items-center mt-3 px-3`}>
        <Col lg={8}>
          <Row>
            {university.university_admission_req[0].items?.length > 0
              ? university.university_admission_req[0].items.map((requirement) => (
                  <Col lg={6} key={uuid()}>
                    <div className="py-3">
                      <Form.Check
                        type="checkbox"
                        id={`check-${requirement}`}
                        label={requirement}
                        onChange={(e) => {
                          if (e.target.checked) {
                            setCheckCount(checkCount + 1);
                            setCheckList({ ...checkList, [requirement]: true });
                          } else {
                            setCheckCount(checkCount - 1);
                            setCheckList({ ...checkList, [requirement]: false });
                          }
                        }}
                        checked={checkList[requirement]}
                      />
                    </div>
                  </Col>
                ))
              : null}
          </Row>
        </Col>
        <Col lg={4}>
          <div className={`${styles.imageContainer} d-flex align-items-center justify-content-center`}>
            <Image alt="Document Illustration" src="/banners/documents_illustration.svg" width={400} height={300} objectFit="contain" />
          </div>
        </Col>
      </Row>
      <Row>
        <Col>
          {/* <SectionHeading
            heading={`<span>
            What now?
            </span>`}
            subHeading={`
            College Vidya Abroad offers free counselling by our expert counsellors to help you with your application process.
            `}
            className="text-center "
          /> */}
          {isAllChecked ? (
            <div className="d-flex align-items-center justify-content-center">
              <button
                className="bg-cvblue rounded-5 px-4 py-3 border-0   text-white d-flex align-items-center justify-content-center "
                onClick={() => router.push("/suggest-me-a-university/")}
              >
                Get Free Counselling
              </button>
            </div>
          ) : null}
        </Col>
      </Row>
    </Container>
  );
}
