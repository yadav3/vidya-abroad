import Image from "next/image";
import React from "react";
import styles from "./UniversityOverview.module.scss";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
// import "swiper/css/effect-coverflow";
import "swiper/css/effect-cards";
import "swiper/css/pagination";

// import required modules
import { EffectCards, Pagination } from "swiper";

import uuid from "react-uuid";
import { getAWSImagePath, getAWSImagePathBeta } from "../../../context/services";
import SectionHeading from "../../global/SectionHeading/SectionHeading";

export default function UniversityOverview({ university }) {
  const renderOverview = () => {
    const universityOverview = university.universities_overview[0];
    const overviewValue = universityOverview?.value_html || universityOverview?.value;

    return overviewValue ? <span dangerouslySetInnerHTML={{ __html: overviewValue }}></span> : null;
  };

  return (
    <Container className={`pt-5 px-5 overflow-hidden`} fluid>
      <Row>
        <Col lg={6} className={`d-flex align-items-center justify-content-center mb-4 lg:mb-0`}>
          <Swiper
            effect={"cards"}
            grabCursor={true}
            centeredSlides={true}
            slidesPerView={"auto"}
            pagination={true}
            // loop={true}
            modules={[EffectCards, Pagination]}
            className="universityCampusSlider"
          >
            {university?.universities_images.length > 0
              ? university?.universities_images.slice(1).map((image) => (
                  <SwiperSlide key={uuid()}>
                    <div className={`${styles.overviewImage} `}>
                      <Image src={getAWSImagePath(image.image_path)} alt={university.name} objectFit="cover" width={350} height={350} />
                    </div>
                  </SwiperSlide>
                ))
              : null}
          </Swiper>
        </Col>
        <Col lg={6} className={`d-flex flex-column  justify-content-center`}>
          <SectionHeading heading={`Why study at <span class="text-cvblue">${university.name}</span>?`} className="ps-0"
           headingClass="fs-3 fs-lg-4 fs-xxl-5 fw-semibold text-center text-lg-start ps-0"
          />
          {renderOverview()}
        </Col>
      </Row>
    </Container>
  );
}
