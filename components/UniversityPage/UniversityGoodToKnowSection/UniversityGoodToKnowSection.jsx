import Image from "next/image";
import React from "react";
import styles from "./UniversityGoodToKnowSection.module.scss";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import SectionHeading from "../../global/SectionHeading/SectionHeading";
import TextTransition, { presets } from "react-text-transition";
import { TiWeatherCloudy, TiWorld } from "react-icons/ti";
import { IoSchool } from "react-icons/io5";
import { AiOutlineExclamationCircle } from "react-icons/ai";
import { ImLeaf } from "react-icons/im";
import { BsSnow, BsCloudSunFill, BsFlower1 } from "react-icons/bs";
import { FaCanadianMapleLeaf } from "react-icons/fa";
import { Tooltip } from "react-tippy";
import uuid from "react-uuid";

export default function UniversityGoodToKnowSection({ university }) {
  const [transitionIndex, setTransitionIndex] = React.useState(0);

  const renderTooltip = (title) => (
    <Tooltip title={title} position="top" arrow>
      <div className={`${styles.cardTooltip} position-absolute top-0 start-0 m-3 cursor-pointer`}>
        <AiOutlineExclamationCircle size={20} />
      </div>
    </Tooltip>
  );

  const renderTransitionIcons = (intakes) => {
    const allIcons = [
      <BsSnow key={uuid()} size={30} />,
      <ImLeaf key={uuid()} size={30} />,
      <BsCloudSunFill key={uuid()} size={30} />,
      <TiWeatherCloudy key={uuid()} size={30} />,
    ];

    const winterIcon = <BsSnow key={uuid()} size={30} />;
    const springIcon = <BsFlower1 key={uuid()} size={30} />;
    const summerIcon = <BsCloudSunFill key={uuid()} size={30} />;
    const fallIcon = <FaCanadianMapleLeaf key={uuid()} size={30} />;
    const autumnIcon = <FaCanadianMapleLeaf key={uuid()} size={30} />;
    const other = <TiWorld key={uuid()} size={30} />;

    const icons = [];

    const intakesString = intakes.toLowerCase();

    // intakes is a string that can includes fall, spring, summer, winter, autumn or any combination of them

    if (intakesString.includes("fall")) {
      icons.push(fallIcon);
    }
    if (intakesString.includes("spring")) {
      icons.push(springIcon);
    }
    if (intakesString.includes("summer")) {
      icons.push(summerIcon);
    }
    if (intakesString.includes("winter")) {
      icons.push(winterIcon);
    }
    if (intakesString.includes("autumn")) {
      icons.push(autumnIcon);
    }
    if (icons.length === 0) {
      icons.push(other);
    }

    return icons;
  };

  const details = university?.university_details[0];
  const intakes = details?.intakes ?? "Not Available";
  const acceptanceRate = details?.acceptance_ratio ? parseRatio(details.acceptance_ratio) : "Not Available";
  const intlStudents = details?.international_student_ratio ? parseRatio(details.international_student_ratio) : "Not Available";

  function parseRatio(ratio) {
    const parsed = parseInt(ratio);
    if (!isNaN(parsed) && parsed > 0) {
      return `${parsed}%`;
    }
    return "Not Available";
  }

  const detailsData = [
    {
      tooltip: "Intakes are the time periods when universities accept applications for admission.",
      icon: <TiWeatherCloudy size={30} />,
      icons: [
        <BsSnow key={uuid()} size={30} />,
        <ImLeaf key={uuid()} size={30} />,
        <BsCloudSunFill key={uuid()} size={30} />,
        <TiWeatherCloudy key={uuid()} size={30} />,
      ],
      content: (
        <>
          <p className="mb-0 fs-6">{intakes}</p>
          <small className="text-muted">Intakes</small>
        </>
      ),
    },
    {
      tooltip: "Acceptance rate is the percentage of applicants who received an offer out of all those who applied for admission.",
      icon: <IoSchool size={30} />,
      icons: [],
      content: (
        <>
          <p className="mb-0 fs-6">{acceptanceRate}</p>
          <small className="text-muted">Acceptance Rate</small>
        </>
      ),
    },
    {
      tooltip: "International students are students who are not citizens or permanent residents of the country in which the university is located.",
      icon: <TiWorld size={30} />,
      icons: [],
      content: (
        <>
          <p className="mb-0 fs-6">{intlStudents}</p>
          <small className="text-muted">International Students</small>
        </>
      ),
    },
  ];

  React.useEffect(() => {
    const intervalId = setInterval(
      () => setTransitionIndex((index) => index + 1),
      3000 // every 3 seconds
    );
    return () => clearTimeout(intervalId);
  }, []);

  return (
    <Container>
      <Row>
        <SectionHeading
          heading={`<span>Good to <span class="text-cvblue">Know</span></span>`}
          subHeading={`Here are some important things about ${university.name} that you should know:`}
          className="text-center mb-4"
        />
      </Row>
      <Row>
        <Col>
          <div className="d-flex justify-content-center flex-wrap gap-3">
            {detailsData.map((item, index) => (
              <div
                key={index}
                className={`p-4 d-flex flex-column align-items-center justify-content-center text-center gap-2 ${styles.card} position-relative`}
              >
                {renderTooltip(item.tooltip)}
                <div
                  className={`${styles.cardIcon} myshine p-2 rounded-circle`}
                  style={{
                    backgroundColor: index === 0 ? "#ffe5da" : index === 1 ? "#E3E5FF" : "#E3FFFC",
                    color: index === 0 ? "#ff9d73" : index === 1 ? "#a2a8f7" : "#84cac3",
                  }}
                >
                  {/* {item.icon} */}
                  {item?.icons?.length > 0 ? (
                    <TextTransition springConfig={presets.wobbly}>
                      {renderTransitionIcons(intakes)[transitionIndex % renderTransitionIcons(intakes).length]}
                    </TextTransition>
                  ) : (
                    item.icon
                  )}
                </div>
                <div className={`${styles.cardContent}`}>{item.content}</div>
              </div>
            ))}
          </div>
        </Col>
      </Row>
    </Container>
  );
}
