import Image from "next/image";
import React from "react";
import Marquee from "react-fast-marquee";
import styles from "./UniversityPlacementBanner.module.scss";
import uuid from "react-uuid";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { getAWSImagePath, getAWSImagePathBeta } from "../../../context/services";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";

// import required modules
import { Autoplay, FreeMode, Pagination } from "swiper";
import SectionHeading from "../../global/SectionHeading/SectionHeading";

export default function UniversityPlacementBanner({ university }) {
  return (
    <Container >
      <Row>
        <SectionHeading
          heading={`<span>Placements <span class="text-cvblue">Companies</span></span>`}
          subHeading={`Find out what companies have recruited from ${university.name}.`}
          className="text-center mb-4 px-2"
        />
        <Col>
          <Swiper
            slidesPerView={"auto"}
            spaceBetween={30}
            loop={true}
            centeredSlides={true}
            autoplay={{
              delay: 2500,
              disableOnInteraction: false,
            }}
            pagination={{
              clickable: true,
            }}
            breakpoints={{
              500: {
                slidesPerView: 1,
                spaceBetween: 10,
              },
              700: {
                slidesPerView: 2,
                spaceBetween: 10,
              },
              1000: {
                slidesPerView: 3,
                spaceBetween: 10,
              },
              1300: {
                slidesPerView: 4,
                spaceBetween: 10,
              },
            }}
            modules={[Autoplay, Pagination]}
            className="universityPlacementBanner"
          >
            {university.universities_famous_companies.map((company) => (
              <SwiperSlide key={uuid()}>
                <Image
                  alt="Placement Company Logo"
                  src={getAWSImagePathBeta(company.media_library.image)}
                  width={150}
                  height={70}
                  objectFit="contain"
                />
              </SwiperSlide>
            ))}
          </Swiper>
        </Col>
      </Row>
    </Container>
  );
}
