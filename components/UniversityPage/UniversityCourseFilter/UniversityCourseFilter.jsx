import React, { useEffect, useState } from "react";
import styles from "./UniversityCourseFilter.module.scss";
import uuid from "react-uuid";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Link from "next/link";
import SectionHeading from "../../global/SectionHeading/SectionHeading";
import { CourseFinderFilters } from "./CourseFinderFilters";
import { CourseCard } from "./CourseCard";
import NoResultFound from "../../global/NoResultFound/NoResultFound";

export default function UniversityCourseFilter({ university }) {
  const [courses, setCourses] = useState([]);
  const [filteredCourses, setFilteredCourses] = useState([]);
  const [search, setSearch] = useState("");
  const [selectedCourse, setSelectedCourse] = useState(null);
  const [selectedDuration, setSelectedDuration] = useState(5);
  const [limit, setLimit] = useState(3);

  useEffect(() => {
    const universitySpecializations = university?.UniversitySpecializations || [];
    setCourses(universitySpecializations);
    setFilteredCourses(universitySpecializations);
    setSearch("");
  }, [university]);

  useEffect(() => {
    let filteredCourses = courses;

    if (search) {
      filteredCourses = filteredCourses.filter((course) => course.Specializations.full_name.toLowerCase().includes(search.toLowerCase()));
      setFilteredCourses(filteredCourses);
    } else if (search.length === 0) {
      setFilteredCourses(courses);
    }


    if (selectedCourse && selectedCourse !== 0) {
      filteredCourses = filteredCourses.filter((course) => parseInt(course.Specializations.Courses.category_id) === parseInt(selectedCourse));
      setFilteredCourses(filteredCourses);
    }

    if (selectedCourse === 0) {
      setSelectedCourse(null);
      setSelectedDuration(null);
      setFilteredCourses(courses);
    }

    if (selectedDuration) {
      filteredCourses = filteredCourses.filter((course) => course.duration <= selectedDuration * 12);
      setFilteredCourses(filteredCourses);
    }

    // setFilteredCourses(filteredCourses.slice(0, limit));
  }, [search, selectedCourse, selectedDuration, limit, courses]);

  const handleCourseChange = (e) => {
    setSelectedCourse(e.value);
  };

  const handleDurationChange = (e) => {
    setSelectedDuration(e.value);
  };

  const handleShowMore = () => {
    setLimit((prev) => prev + 3);
  };

  const handleAddToCompare = (majorId) => {
    window.location.href = `/suggest-me-a-university?majorId=${majorId}`;
  };

  return (
    <Container className={`${styles.container} d-flex flex-column align-items-center justify-content-center  mt-3`}>
      <Row>
        <Col>
          <SectionHeading
            heading={`<span>Popular <span class="text-cvblue">Courses</span></span>`}
            subHeading={`Find out the most popular courses offered by ${university.name}.`}
            className="text-center mb-5"
          />
        </Col>
      </Row>
      <CourseFinderFilters
        search={search}
        setSearch={setSearch}
        selectedCourse={selectedCourse}
        handleCourseChange={handleCourseChange}
        selectedDuration={selectedDuration}
        handleDurationChange={handleDurationChange}
      />

      {filteredCourses && filteredCourses.length > 0 ? (
        <div className={`${styles.courseContainer} `}>
          {filteredCourses.slice(0, limit).map((course) => {
            return <CourseCard course={course} handleAddToCompare={handleAddToCompare} university={university} key={uuid()} />;
          })}
        </div>
      ) : (
        <div className="w-100 d-flex align-items-center justify-content-center">
          <NoResultFound subheading="We couldn't find any courses matching your search criteria. Please try again." showCtaText={false} />
        </div>
      )}

      <button className="mt-4 bg-transparent text-cvblue border-0 " onClick={() => handleShowMore(3)}>
        {filteredCourses.length > limit ? <span>Show More</span> : null}
      </button>
    </Container>
  );
}
