import React from "react";
import Image from "next/image";
import styles from "./UniversityCourseFilter.module.scss";
import Card from "react-bootstrap/Card";
import { FooterDivider } from "../../global/FooterContainer/FooterDivider";
import { Tooltip } from "react-tippy";
import { MajorSelectorModal } from "./MajorSelectorModal";
import { BiPlusCircle } from "react-icons/bi";

export function CourseCard({ course, handleAddToCompare, university }) {
  return (
    <Card
      className="py-3 px-2"
      style={{
        backgroundColor: "#F5F5F5",
        borderRadius: "10px",
      }}
    >
      <Card.Body>
        <Card.Title className="text-center ">
          <Tooltip title={course.Specializations?.full_name} placement="top">
            {course.Specializations?.full_name ? course.Specializations.full_name : course.Specializations?.name ? course.Specializations.name : null}
          </Tooltip>
        </Card.Title>
        <FooterDivider />
        <Card.Text>
          <div className={`${styles.factContainer} `}>
            <div className={`${styles.factItem}`}>
              <Image alt="Course Duration Icon" src="/icons/clock_icon.png" width={30} height={30} objectFit="contain" />
              <div>
                <p className="m-0 fs-md fw-semibold">{course.duration} Months</p>
                <p className="m-0 fs-sm text-muted">Duration</p>
              </div>
            </div>
            <div className={`${styles.factItem}`}>
              <Image alt="Intake Icon" src="/icons/intake_icon.png" width={30} height={30} />
              <div>
                <p className="m-0 fs-md fw-semibold">{course.intake}</p>
                <p className="m-0 fs-sm text-muted">Intake</p>
              </div>
            </div>
            <div className={`${styles.factItem}`}>
              <Image alt="Fee Icon" src="/icons/fees_icon.png" width={30} height={30} />
              <div>
                <p className="m-0 fs-md fw-semibold">
                  {university.Countries.currency_symbol}
                  {course.fees}
                </p>
                <p className="m-0 fs-sm text-muted">Fees</p>
              </div>
            </div>
            <div className={`${styles.factItem}`}>
              <Image alt="Document Icon" src="/icons/documenticon.png" width={30} height={30} />
              <div>
                <p className="m-0 fs-md fw-semibold">{course.ielts_requirement}</p>
                <p className="m-0 fs-sm text-muted">IELTS</p>
              </div>
            </div>
          </div>
        </Card.Text>
      </Card.Body>
      <Card.Footer className=" text-center border-0 bg-transparent">
        <MajorSelectorModal majors={course.Specializations.specializations_majors} handleAddToCompare={handleAddToCompare}>
          <button className="bg-cvblue text-white border-0 py-2 w-100 rounded-3 ">
            <BiPlusCircle className="fs-lg me-2" />
            Add to Compare
          </button>
        </MajorSelectorModal>
      </Card.Footer>
    </Card>
  );
}
