import React from "react";
import styles from "./UniversityCourseFilter.module.scss";
import { Dropdown } from "primereact/dropdown";
import { InputText } from "primereact/inputtext";

export function CourseFinderFilters({ search, setSearch, selectedCourse, handleCourseChange, selectedDuration, handleDurationChange }) {
  return (<div className={`${styles.filterHeader} w-100 mb-4`}>
    <span className="p-input-icon-right">
      <i className="pi pi-search" />
      <InputText placeholder="Search Course" value={search} onChange={e => setSearch(e.target.value)} />
    </span>
    <span>
      <Dropdown value={selectedCourse} options={[{
        name: "All",
        value: 0
      }, {
        name: "Bachelors",
        value: 1
      }, {
        name: "Masters",
        value: 2
      }, {
        name: "Diploma",
        value: 3
      }]} onChange={handleCourseChange} optionLabel="name" optionValue="value" placeholder="Filter by course" />
      <i className="pi pi-filter" />
    </span>
    <span>
      <Dropdown value={selectedDuration} options={[{
        name: "Under 1 Years",
        value: 1
      }, {
        name: "Under 2 Years",
        value: 2
      }, {
        name: "Under 3 Years",
        value: 3
      }, {
        name: "Under 4 Years",
        value: 4
      }, {
        name: "Under 5 Years",
        value: 5
      }]} onChange={handleDurationChange} optionLabel="name" optionValue="value" placeholder="Filter by duration" />
      <i className="pi pi-sort" />
    </span>
  </div>);
}
