import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import AllUniversityContainer from "../../components/UniversityPage/AllUniversity/AllUniversityContainer";
import { getAllUniversities } from "../../services/universitiesService";
import AllUniversityFilters from "../../components/UniversityPage/AllUniversity/AllUniversityFilters";
import { getCountriesNames } from "../../services/country.service";
import { useState } from "react";
import { useEffect } from "react";
import { getAllExchangeRates } from "../../context/services";
import { useBreakpoint } from "../../context/CustomHooks";
import { AiFillCloseCircle } from "react-icons/ai";
import Head from "next/head";

export async function getStaticProps(context) {
    let universityData = await getAllUniversities(1, 18);
    let countries = await getCountriesNames();
    let exchangeRates = await getAllExchangeRates();

    if (!universityData || !countries || !exchangeRates) {
        return {
            notFound: true,
        };
    }

    return {
        props: {
            data: universityData,
            countries: countries,
            exchangeRates: exchangeRates,
        },
        revalidate: 60,
    };
}

export default function AllUniversityPage({ data, countries, exchangeRates }) {
    const [universitiesData, setUniversitiesData] = useState(data);
    const [showFilters, setShowFilters] = useState(false);

    const breakpoint = useBreakpoint()

    const [filters, setFilters] = useState({
        countries: [],
        ielts: 10,
        budgetLow: 0,
        budgetHigh: 0,
        sortBy: "world_ranking",
        orderBy: "asc",
        search: "",
    });

    const handleToggleFilters = () => {
        setShowFilters(!showFilters);
    };

    const handleCloseFilters = () => {
        setShowFilters(false);
    };

    useEffect(() => {
        if (breakpoint < 992) {
            setShowFilters(false);
        } else {
            setShowFilters(true);
        }

    }, [breakpoint, filters]);


    return (
        <>
            <Head>
                <title>
                    Explore Top-Notch Universities to Study Abroad
                </title>
                <meta
                    name="description"
                    content="Explore Top-ranked University to Study Abroad. Get information about the Acceptance rate, Ranking, Admission Requirements, and more at College Vidya Abroad."
                />
                <link rel="canonical" href="https://collegevidyaabroad.com/universities/" />
                <meta property="og:locale" content="en_US" />
                <meta property="og:type" content="website" />
                <meta
                    property="og:title"
                    content="Explore Top-Notch Universities to Study Abroad"
                />

                <meta property="og:url" content="https://collegevidyaabroad.com/universities/" />
                <meta
                    property="og:site_name"
                    content="Explore Top-Notch Universities to Study Abroad"
                />
                <meta name="twitter:card" content="summary_large_image" />
                {/* <meta property="og:image" content={cvTalkBlogs[0]?.blogs.image} /> */}
                {/* <meta property="twitter:image" content={cvTalkBlogs[0]?.blogs.image} /> */}

                {/* Schema One */}
                <script
                    type="application/ld+json"
                    dangerouslySetInnerHTML={{
                        __html: JSON.stringify({
                            "@context": "http://schema.org",
                            "@type": "webpage",
                            url: "https://collegevidyaabroad.com/universities/",
                            name: "Explore Top-Notch Universities to Study Abroad",
                            description:
                                "Explore Top-ranked University to Study Abroad. Get information about the Acceptance rate, Ranking, Admission Requirements, and more at College Vidya Abroad.",
                            speakable: {
                                "@type": "SpeakableSpecification",
                                cssSelector: [".seotag"],
                            },
                        }),
                    }}
                />

                {/* Schema Two */}
                <script
                    type="application/ld+json"
                    dangerouslySetInnerHTML={{
                        __html: JSON.stringify({
                            "@context": "http://schema.org",
                            "@type": "Organization",
                            name: "College Vidya Abroad",
                            url: "https://collegevidyaabroad.com/",
                            sameAs: [
                                "https://facebook.com/collegevidyaabroad",
                                "https://twitter.com/collegevidyaabroad",
                                "https://www.linkedin.com/company/collegevidyaabroad",
                                "https://www.instagram.com/collegevidyaabroad",
                            ],
                            logo: "https://collegevidyaabroad.com/logos/cv-abroad-logo.png",
                            legalName: "College Vidya Abroad",
                            address: {
                                "@type": "PostalAddress",
                                addressCountry: "India",
                                addressLocality: "",
                                addressRegion: "",
                                postalCode: "",
                                streetAddress: ",",
                            },
                            contactPoint: {
                                "@type": "ContactPoint",
                                telephone: "18003099018",
                                contactType: "Customer Service",
                                contactOption: "TollFree",
                                areaServed: ["IN"],
                            },
                        }),
                    }}
                />

                {/* Breadcrumb Schema */}
                <script
                    type="application/ld+json"
                    dangerouslySetInnerHTML={{
                        __html: JSON.stringify({
                            "@context": "https://schema.org",
                            "@type": "BreadcrumbList",
                            itemListElement: [
                                {
                                    "@type": "ListItem",
                                    position: 1,
                                    item: {
                                        "@id": "https://collegevidyaabroad.com/universities/",
                                        name: "Universities",
                                    },
                                },
                            ],
                        }),
                    }}
                />
            </Head>
            <Container
                fluid
                className="pt-4 pt-md-5 py-5"
                style={{
                    backgroundColor: "#f2f3f8",
                }}
            >
                <Container>
                    <Row>
                        <Col lg={3} >
                            <div className="position-sticky"
                                style={{
                                    top: "1rem",
                                }}
                            >
                                {showFilters ? (
                                    <>
                                        <h5 className="fs-3 mb-4 ">Filters
                                            {
                                                breakpoint < 992 && (
                                                    <AiFillCloseCircle className="ms-2" onClick={handleCloseFilters} />
                                                )
                                            }
                                        </h5>
                                        <AllUniversityFilters countries={countries} filters={filters} setFilters={setFilters} exchangeRates={exchangeRates} />
                                    </>
                                ) : (
                                    <button className="border-0 text-white bg-cvblue w-100 p-1 py-2 rounded-3 mb-4"
                                        onClick={handleToggleFilters}
                                    >Show Filters</button>
                                )}
                            </div>
                        </Col>
                        <Col lg={9}>
                            <h1 className="fs-3 mb-4 text-center text-lg-start">
                                Explore <span className="text-cvblue">Top-Notch</span> Universities to Study Abroad
                            </h1>
                            <AllUniversityContainer
                                exchangeRates={exchangeRates}
                                universitiesData={universitiesData}
                                setUniversitiesData={setUniversitiesData}
                                filters={filters}
                                setFilters={setFilters}
                            />
                        </Col>
                    </Row>
                </Container>
            </Container></>
    );
}
