import React from "react";
import axios from "axios";
import { useRouter } from "next/router";
import UniversityPage from "../../components/UniversityPage/UniversityPage";
import { BACKEND_API_URL, getAWSImagePath, getAWSImagePathBeta } from "../../context/services";
import { getMajorWithSpecialization, getMajors } from "../../services/specializations.service";
import Head from "next/head";
import { getFAQSchema } from "../../utils/misc";

export async function getStaticPaths() {
  const universitiesSlugs = await axios.get(`${BACKEND_API_URL}/universities/slugs`).then((res) => res.data);

  const paths = universitiesSlugs.map((slug) => {
    return {
      params: { university: slug.slug },
    };
  });

  return {
    paths,
    fallback: true, // can also be true or 'blocking'
  };
}

export async function getStaticProps(context) {
  const universitySlug = context.params.university;

  let universityData = await axios.get(`${BACKEND_API_URL}/universities/page/data/${universitySlug}`).then((res) => res.data);
  let majors = await getMajorWithSpecialization()

  if (!universityData || !majors || !universitySlug) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      universitySlug: universitySlug,
      universityData: universityData,
      majors: majors,
    },
    revalidate: 60,
  };
}

export default function Universities({ universityData, universitySlug, majors }) {
  const router = useRouter();

  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  const university_local_meta_title = `${universityData.name}: Ranking, Courses and Admission Requirements`;
  const university_local_meta_description = `Apply to top universities in the ${universityData.name} and compare from 200+ universities.`;

  return <>
    <Head>
      <title>
        {universityData.meta_title?.length > 0 ? universityData.meta_title : university_local_meta_title}
      </title>
      <meta
        name="description"
        content={
          universityData.meta_description?.length > 0 ? universityData.meta_description : university_local_meta_description
        }
      />
      <meta property="og:imag e" content={getAWSImagePathBeta(universityData.background_img)} />
      <meta property="twitter:image" content={getAWSImagePathBeta(universityData.background_img)} />
      <meta property="og:locale" content="en_US" />
      <meta property="og:type" content="website" />
      <meta
        property="og:title"
        content={`${universityData.name}: Ranking, Courses and Admission Requirements`}
      />
      <meta
        property="og:description"
        content={
          universityData.meta_description?.length > 0 ? universityData.meta_description : university_local_meta_description

        }
      />
      <meta property="og:url" content="https://collegevidyaabroad.com/" />
      <meta property="og:site_name" content="College Vidya Abroad" />
      <meta
        property="og:article:publisher"
        content="https://www.facebook.com/collegevidyaabroad"
      />
      <meta property="twitter:card" content="summary" />
      <meta property="twitter:site" content="@CollegeVidyaAbroad" />
      <meta
        property="twitter:title"
        content={`Study Abroad in the ${universityData.name}`}
      />
      <meta
        property="twitter:description"
        content={
          universityData.meta_description?.length > 0 ? universityData.meta_description : university_local_meta_description
        }
      />
      <meta property="twitter:url" content="https://collegevidyaabroad.com/" />
      <link href={`https://collegevidyaabroad.com/`} rel="canonical" />


      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify({
            "@context": "http://schema.org",
            "@type": "webpage",
            url: "https://collegevidyaabroad.com",
            name: `${universityData.name}: Ranking, Courses and Admission Requirements`,
            description:
              {}
            ,
            speakable: {
              "@type": "SpeakableSpecification",
              cssSelector: [".seotag"],
            },
          }),
        }}
      />

      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify({
            "@context": "http://schema.org",
            "@type": "Organization",
            name: "College Vidya Abroad",
            url: "https://collegevidyaabroad.com/",
            sameAs: [
              "https://facebook.com/collegevidyaabroad",
              "https://twitter.com/collegevidyaabroad",
              "https://www.linkedin.com/company/collegevidyaabroad",
              "https://www.instagram.com/collegevidyaabroad",
            ],
            logo: "https://collegevidyaabroad.com/logos/cv-abroad-logo.png",
            legalName: "College Vidya Abroad",
            address: {
              "@type": "PostalAddress",
              addressCountry: "India",
              addressLocality: "",
              addressRegion: "",
              postalCode: "",
              streetAddress: ",",
            },
            contactPoint: {
              "@type": "ContactPoint",
              telephone: "18003099018",
              contactType: "Customer Service",
              contactOption: "TollFree",
              areaServed: ["IN"],
            },
          }),
        }}
      />

      {/* FAQ Schema */}
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify(getFAQSchema(universityData.universities_faq.map((faq) => {
            return {
              question: faq.question,
              answer_html: faq.answer
            }
          }))),
        }}
      />
    </Head>
    <UniversityPage universitiesData={universityData} universitiesSlug={universitySlug} majors={majors} />
  </>
}

// export default function Universities() {
//   return <>University Page</>;
// }
