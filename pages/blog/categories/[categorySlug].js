import React, { useEffect, useState } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import BlogPostContainer from "../../../components/BlogPage/BlogPostContainer";
import Blog from "../../../services/blog.service";
import axios from "axios";
import { BACKEND_API_URL } from "../../../context/services";
import { Breadcrumb } from "react-bootstrap";
import { useRouter } from "next/router";
import BlogPagination from "../../../components/BlogPage/BlogPagination/BlogPagination";
import Head from "next/head";
import { getSearchSchema } from "../../../utils/misc";
import BlogFilters from "../../../components/BlogPage/BlogFilters/BlogFilters";

const blogService = new Blog();

export async function getStaticPaths() {
  const blogCategorySlugs = await blogService.getAllCategoriesSlugs();

  const paths = blogCategorySlugs?.length > 0 ? blogCategorySlugs.map((slug) => {
    return {
      params: { categorySlug: slug.category_slug },
    };
  }) : [];

  return {
    paths,
    fallback: true, // can also be true or 'blocking'
  };
}

export async function getStaticProps(context) {
  const categorySlug = context.params.categorySlug;
  let blogPosts = await blogService.getBlogPosts(categorySlug, 1);

  if (!blogPosts) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      posts: blogPosts,
      categorySlug: categorySlug,
    },
    revalidate: 60,
  };
}

export default function BlogCategoriesPage({ posts, categorySlug }) {
  const [allPosts, setAllPosts] = useState(posts);
  const [currentPage, setCurrentPage] = useState(1);

  const slugToTitle = (slug) => {
    if (!slug) return "";
    return slug
      .split("-")
      .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
      .join(" ");
  };

  useEffect(() => {
    blogService.getBlogPosts(categorySlug, currentPage).then((posts) => {
      setAllPosts(posts);
    });
  }, [currentPage, categorySlug]);

  return (
    <>
      {" "}
      <Head>
        <title>{slugToTitle(categorySlug)}</title>
        {/* <meta name="description" content={allCategories.filter((category) => category.slug === categorySlug)[0].meta_description} /> */}
        <link
          rel="canonical"
          href={`https://collegevidyaabroad.com/blog/categories/${categorySlug}/`}
        />

        <meta
          property="og:url"
          content={`https://collegevidyaabroad.com/blog/categories/${categorySlug}/`}
        />
        <meta property="og:site_name" content="College Vidya Abroad" />
        <meta property="og:article:publisher" content="https://www.facebook.com/collegevidyaabroad" />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:site" content="@CollegeVidyaAbroad" />
        <meta property="twitter:title" content={slugToTitle(categorySlug)} />
        {/* <meta property="twitter:description" content={allCategories.filter((category) => category.slug === categorySlug)[0].meta_description} /> */}
        <meta property="twitter:url" content="https://collegevidyaabroad.com/" />

        <meta property="og:image" content={"https://collegevidyaabroad.com/logos/cv-abroad-logo.png"} />
        <meta property="twitter:image" content={"https://collegevidyaabroad.com/logos/cv-abroad-logo.png"} />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: `https://collegevidyaabroad.com/blog/categories/${categorySlug}/`,
              name: slugToTitle(categorySlug),
              // description: allCategories.filter((category) => category.slug === categorySlug)[0].description,
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: [".seotag"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya Abroad",
              url: "https://collegevidyaabroad.com/",
              sameAs: [
                "https://facebook.com/collegevidyaabroad",
                "https://twitter.com/collegevidyaabroad",
                "https://www.linkedin.com/company/collegevidyaabroad",
                "https://www.instagram.com/collegevidyaabroad",
              ],
              logo: "https://collegevidyaabroad.com/logos/cv-abroad-logo.png",
              legalName: "College Vidya Abroad",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18003099018",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        {/* Breadcrumb Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "https://schema.org",
              "@type": "BreadcrumbList",
              itemListElement: [
                {
                  "@type": "ListItem",
                  position: 1,
                  item: {
                    "@id": "https://collegevidyaabroad.com/blog",
                    name: "Blog",
                  },
                },
                {
                  "@type": "ListItem",
                  position: 2,
                  item: {
                    "@id": `https://collegevidyaabroad.com/blog/categories/${categorySlug}/`,
                    name: slugToTitle(categorySlug),
                  },
                },
              ],
            }),
          }}
        />
      </Head>
      <Container className="py-5">
        <Row>
          <Col>
            <BlogFilters />
          </Col>
        </Row>
        <Row>
          <Col className="p-0">
            <Breadcrumb className="my-2 d-flex align-items-center justify-content-center ">
              {/* dynamic breadcrumb based on url */}
              <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
              <Breadcrumb.Item href="/blog">Blog</Breadcrumb.Item>
              <Breadcrumb.Item active>Categories</Breadcrumb.Item>
            </Breadcrumb>
            <h1 className="text-center fs-4 fs-3 px-2 my-3 mb-5">
              Showing more posts from
              <span className="text-primary "> {slugToTitle(categorySlug)}</span>
            </h1>
            {allPosts?.allPosts?.length > 0 ? (
              <BlogPostContainer posts={allPosts?.allPosts} />
            ) : (
              <h1 className="text-center fs-4 fs-3 px-2 my-3 mb-5">No posts found</h1>
            )}
          </Col>
        </Row>
        <Row>
          <Col>
            <BlogPagination
              page={currentPage}
              setPage={setCurrentPage}
              totalPage={allPosts?.totalPages}
              itemsPerPage={9}
              posts={allPosts?.allPosts}
            />
          </Col>
        </Row>
      </Container>
    </>
  );
}
