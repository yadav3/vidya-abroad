import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import BlogPostContainer from "../../components/BlogPage/BlogPostContainer";
import Blog from "../../services/blog.service";
import axios from "axios";
import { BACKEND_API_URL } from "../../context/services";
import { Breadcrumb } from "react-bootstrap";
import { useRouter } from "next/router";
import { QueryClient, QueryClientProvider, useQuery } from "react-query";
import NoResultFound from "../../components/global/NoResultFound/NoResultFound";
import BlogFilters from "../../components/BlogPage/BlogFilters/BlogFilters";
import Image from "next/image";
import Link from "next/link";
import BlogSkelton from "../../components/BlogPage/BlogSkelton/BlogSkelton";
import { useEffect } from "react";
import Head from "next/head";
import { getSearchSchema } from "../../utils/misc";

const blogService = new Blog();

export default function BlogSearchPage() {
  const router = useRouter();
  const { search } = router.query;

  const { isLoading, error, data } = useQuery(["posts", search], async () => await blogService.getPostBySearch(search, 10), {
    refetchOnMount: true,
  });

  const slugToTitle = (slug) => {
    if (!slug) {
      return;
    }
    return slug
      .split("-")
      .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
      .join(" ");
  };

  return (
    <>
      <Head>
        <title>
          Search results about {slugToTitle(search)}: Study Abroad Blogs
        </title>
        <link rel="canonical" href="https://collegevidyaabroad.com/blog/" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content={`Search results about ${slugToTitle(search)}: Study Abroad Blogs`}
        />
        <meta
          name="description"
          content="Visit College Vidya Abroad to get the Study Abroad Blogs about Top Universities, Courses, Exams, the Best Destination to Study Abroad, and the latest news."
        />
        <meta property="og:url" content="https://collegevidyaabroad.com/blog/" />
        <meta
          property="og:site_name"
          content={`Search results about ${slugToTitle(search)}: Study Abroad Blogs`}
        />
        <meta name="twitter:card" content="summary_large_image" />
        {/* <meta property="og:image" content={cvTalkBlogs[0]?.blogs.image} /> */}
        {/* <meta property="twitter:image" content={cvTalkBlogs[0]?.blogs.image} /> */}

        {/* Search Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />

        {/* Schema One */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: "https://collegevidyaabroad.com/blog/",
              name: `Search results about ${slugToTitle(search)}: Study Abroad Blogs`
              ,
              description:
                "Visit College Vidya Abroad to get the Study Abroad Blogs about Top Universities, Courses, Exams, the Best Destination to Study Abroad, and the latest news.",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: [".seotag"],
              },
            }),
          }}
        />

        {/* Schema Two */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya Abroad",
              url: "https://collegevidyaabroad.com/",
              sameAs: [
                "https://facebook.com/collegevidyaabroad",
                "https://twitter.com/collegevidyaabroad",
                "https://www.linkedin.com/company/collegevidyaabroad",
                "https://www.instagram.com/collegevidyaabroad",
              ],
              logo: "https://collegevidyaabroad.com/logos/cv-abroad-logo.png",
              legalName: "College Vidya Abroad",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18003099018",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        {/* Breadcrumb Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "https://schema.org",
              "@type": "BreadcrumbList",
              itemListElement: [
                {
                  "@type": "ListItem",
                  position: 1,
                  item: {
                    "@id": "https://collegevidyaabroad.com/blog/",
                    name: "Blog",
                  },
                },
              ],
            }),
          }}
        />
      </Head>
      <Container className="py-5">
        <Row>
          <Col>
            <BlogFilters />
          </Col>
        </Row>
        <Row>
          <Col className="p-0">
            <Breadcrumb className="my-2 d-flex align-items-center justify-content-center ">
              {/* dynamic breadcrumb based on url */}
              <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
              <Breadcrumb.Item href="/blog">Blog</Breadcrumb.Item>
              <Breadcrumb.Item active>Search</Breadcrumb.Item>
            </Breadcrumb>
            <h1 className="text-center fs-2 mx-2 my-4 mb-5">
              You searched for
              <span className="text-primary "> {slugToTitle(search)}</span>
            </h1>
            {isLoading ? (
              <>
                <Row>
                  <Col md={6} className="mb-4">
                    <BlogSkelton />
                  </Col>
                  <Col md={6} className="mb-4">
                    <BlogSkelton />
                  </Col>
                </Row>
              </>
            ) : data?.length > 0 ? (
              <BlogPostContainer posts={data} />
            ) : (
              <>
                <div
                  className="d-flex flex-column
                align-items-center justify-content-center"
                >
                  <Image src="/gif/not found.gif" alt="No Result Found Illustration" width={200} height={200} />

                  <p className="text-center">
                    No result found for <span className="text-primary">{slugToTitle(search)}</span> <br></br>
                    Please try again with different keywords.
                  </p>
                  <Link href="/blog">
                    <a className="bg-cvblue text-white px-4 py-2 rounded-3">Go to Blog</a>
                  </Link>

                  <span className="text-muted my-2">OR</span>


                  <Link href="/suggest-me-a-university">
                    <a className="bg-orange text-white px-4 py-2 rounded-3 border-0">Suggest Me University in 2 Mins</a>
                  </Link>


                </div>

              </>
            )}
          </Col>
        </Row>
      </Container></>
  );
}
