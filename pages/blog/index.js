import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import BlogBanner from "../../components/BlogPage/BlogBanner/BlogBanner";
import BlogFilters from "../../components/BlogPage/BlogFilters/BlogFilters";
import BlogPostContainer from "../../components/BlogPage/BlogPostContainer";
import { BACKEND_API_URL } from "../../context/services";
import Blog from "../../services/blog.service";
import Breadcrumb from "react-bootstrap/Breadcrumb";
import { useRouter } from "next/router";
import { useState } from "react";
import { useEffect } from "react";
import BlogPagination from "../../components/BlogPage/BlogPagination/BlogPagination";
import Head from "next/head";
import { getSearchSchema } from "../../utils/misc";

const blogService = new Blog();

// `getStaticPaths` requires using `getStaticProps`
export async function getStaticProps() {
  let blogPosts = await blogService.getBlogPosts();

  if (!blogPosts) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      posts: JSON.parse(JSON.stringify(blogPosts)),
    },
    revalidate: 60,
  };
}

export default function BlogPage({ posts }) {
  const router = useRouter();
  const [allPosts, setAllPosts] = useState(posts);
  const [currentPage, setCurrentPage] = useState(1);


  useEffect(() => {
    blogService.getBlogPosts("all", currentPage).then((posts) => {
      setAllPosts(posts);
    });
  }, [currentPage])



  return (
    <>
      <Head>
        <title>
          Study Abroad Blogs: Trending & Updated
        </title>
        <link rel="canonical" href="https://collegevidyaabroad.com/blog/" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content="Study Abroad Blogs: Trending & Updated"
        />
        <meta
          name="description"
          content="Visit College Vidya Abroad to get the Study Abroad Blogs about Top Universities, Courses, Exams, the Best Destination to Study Abroad, and the latest news."
        />
        <meta property="og:url" content="https://collegevidyaabroad.com/blog/" />
        <meta
          property="og:site_name"
          content="Study Abroad Blogs: Trending & Updated"
        />
        <meta name="twitter:card" content="summary_large_image" />
        {/* <meta property="og:image" content={cvTalkBlogs[0]?.blogs.image} /> */}
        {/* <meta property="twitter:image" content={cvTalkBlogs[0]?.blogs.image} /> */}

        {/* Search Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />

        {/* Schema One */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: "https://collegevidyaabroad.com/blog/",
              name: "Study Abroad Blogs: Trending & Updated",
              description:
                "Visit College Vidya Abroad to get the Study Abroad Blogs about Top Universities, Courses, Exams, the Best Destination to Study Abroad, and the latest news.",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: [".seotag"],
              },
            }),
          }}
        />

        {/* Schema Two */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya Abroad",
              url: "https://collegevidyaabroad.com/",
              sameAs: [
                "https://facebook.com/collegevidyaabroad",
                "https://twitter.com/collegevidyaabroad",
                "https://www.linkedin.com/company/collegevidyaabroad",
                "https://www.instagram.com/collegevidyaabroad",
              ],
              logo: "https://collegevidyaabroad.com/logos/cv-abroad-logo.png",
              legalName: "College Vidya Abroad",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18003099018",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        {/* Breadcrumb Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "https://schema.org",
              "@type": "BreadcrumbList",
              itemListElement: [
                {
                  "@type": "ListItem",
                  position: 1,
                  item: {
                    "@id": "https://collegevidyaabroad.com/blog/",
                    name: "Blog",
                  },
                },
              ],
            }),
          }}
        />
      </Head>
      <Container className="pb-5 overflow-hidden">
        <Row>
          <Col className="p-0">
            <BlogBanner />
            <BlogFilters />
            <Breadcrumb className="my-2 d-flex align-items-center justify-content-center ">
              <Breadcrumb.Item href="/">Home</Breadcrumb.Item>
              <Breadcrumb.Item active>Blog</Breadcrumb.Item>
            </Breadcrumb>
            <BlogPostContainer posts={allPosts.allPosts} />
          </Col>
        </Row>
        <Row>
          <Col>
            <BlogPagination
              page={currentPage}
              setPage={setCurrentPage}
              totalPage={allPosts.totalPages}
              itemsPerPage={9}
              posts={allPosts.allPosts}
            />
          </Col>
        </Row>
      </Container>
    </>
  );
}
