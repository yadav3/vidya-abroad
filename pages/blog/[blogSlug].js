import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useRouter } from "next/router";
import BlogPost from "../../components/BlogPage/BlogPost/BlogPost";
import BlogPostSmallCard from "../../components/BlogPage/BlogPostSmallCard/BlogPostSmallCard";
import uuid from "react-uuid";
import Blog from "../../services/blog.service";
import { Spinner } from "react-bootstrap";
import axios from "axios";
import { BACKEND_API_URL, getAWSImagePath } from "../../context/services";
import Image from "next/image";
import BlogProgressBar from "../../components/BlogPage/BlogProgressBar";
import Link from "next/link";
import Head from "next/head";
import { getFAQSchema, getSearchSchema } from "../../utils/misc";
import BlogLeadForm from "../../components/BlogPage/BlogLeadForm/BlogLeadForm";
import { getMajorWithSpecialization, getMajors } from "../../services/specializations.service";
import BlogFilters from "../../components/BlogPage/BlogFilters/BlogFilters";
import BlogRelatedPosts from "../../components/BlogPage/BlogRelatedPosts/BlogRelatedPosts";

const blogService = new Blog();

export async function getStaticPaths() {
  const blogSlugs = await blogService.getAllPostSlugs();

  const paths = blogSlugs?.length > 0 ? blogSlugs.map((slug) => {
    return {
      params: { blogSlug: slug?.slug },
    };
  }) : [];


  return {
    paths,
    fallback: true, // can also be true or 'blocking'
  };
}

// `getStaticPaths` requires using `getStaticProps`
export async function getStaticProps(context) {
  const blogSlug = context.params.blogSlug;
  let blogPost = await blogService.getPostBySlug(blogSlug)
  const allCategories = await blogService.getAllCategories()
  const relatedPosts = await blogService.getRelatedPosts(blogSlug, 5);
  const relatedLatestPosts = await blogService.getBlogPosts()
  const majors = await getMajorWithSpecialization()



  if (!blogPost) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      post: blogPost,
      relatedPosts: relatedPosts,
      allCategories: allCategories,
      majors: majors,
      relatedLatestPosts: relatedLatestPosts
    },
    revalidate: 60,
  };
}

export default function BlogPostPage({ post, relatedPosts, allCategories, majors, relatedLatestPosts }) {

  const getPostReadTime = (post) => {
    const wordsPerMinute = 200;
    const minutes = post?.contentLength / wordsPerMinute;
    const readTime = Math.ceil(minutes);
    return readTime;
  };


  return (
    <>
      <Head>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta
          name="robots"
          content="index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1"
        />
        <title>{post?.title}</title>
        <meta name="description" content={post?.short_description} />
        <link
          rel="canonical"
          href={`https://collegevidyaabroad.com/blog/${post?.slug}/`}
        />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="article" />
        <meta property="og:title" content={post?.title} />
        <meta property="og:description" content={post?.short_description} />
        <meta
          property="og:url"
          content={`https://collegevidyaabroad.com/blog/${post?.slug}/`}
        />
        <meta property="og:site_name" content="College Vidya Abroad Blog" />
        <meta
          property="article:published_time"
          content={post?.published_at}
        />
        <meta
          property="article:modified_time"
          content={post?.updated_at}
        />

        <meta property="og:image:width" content="1280" />
        <meta property="og:image:height" content="720" />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:label1" content="Written by" />
        <meta name="twitter:data1" content="compare" />
        <meta name="twitter:label2" content="Est. reading time" />
        <meta name="twitter:data2" content={`${getPostReadTime(post)} minutes`} />
        <meta
          property="og:image"
          content={post?.thumbnail ? getAWSImagePath(post?.thumbnail) : "https://collegevidyaabroad.s3.ap-south-1.amazonaws.com/placeholder-image.jpg"}
        />
        <meta
          property="twitter:image"
          content={post?.thumbnail ? getAWSImagePath(post?.thumbnail) : "https://collegevidyaabroad.s3.ap-south-1.amazonaws.com/placeholder-image.jpg"}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Article",
              mainEntityOfPage: {
                "@type": "WebPage",
                "@id": `https://collegevidyaabroad.com/blog/${post?.slug}/`,
              },
              headline: post?.title,
              image: {
                "@type": "ImageObject",
                url: post?.thumbnail ? post?.thumbnail : "https://collegevidyaabroad.s3.ap-south-1.amazonaws.com/placeholder-image.jpg",
                height: 1280,
                width: 720,
              },
              datePublished: post?.published_at,
              dateModified: post?.updated_at,
              author: {
                "@type": "Person",
                name: "College Vidya Abroad",
              },
              publisher: {
                "@type": "Organization",
                name: "College Vidya Abroad",
                url: "https://collegevidyaabroad.com/",
                sameAs: [
                  "https://www.instagram.com/collegevidyaabroad",
                  "https://facebook.com/collegevidyaabroad",
                  "https://twitter.com/collegevidyaabroad",
                  "https://www.linkedin.com/company/collegevidyaabroad",
                ],
                logo: {
                  "@type": "ImageObject",
                  url: "https://collegevidyaabroad.com/logos/cv-abroad-logo.png",
                  width: 256,
                  height: 85,
                },
              },
              description: post?.short_description,
            }),
          }}
        />

        {/* Search Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getSearchSchema()),
          }}
        />

        {/* Breadcrumb Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "https://schema.org",
              "@type": "BreadcrumbList",
              itemListElement: [
                {
                  "@type": "ListItem",
                  position: 1,
                  item: {
                    "@id": "https://collegevidyaabroad.com/blog",
                    name: "Blog",
                  },
                },
                {
                  "@type": "ListItem",
                  position: 2,
                  item: {
                    "@id": `https://collegevidyaabroad.com/blog/categories/${post?.category?.category_slug}/`,
                    name: post?.category?.name,
                  },
                },
                {
                  "@type": "ListItem",
                  position: 3,
                  item: {
                    "@id": `https://collegevidyaabroad.com/blog/${post?.slug}/`,
                    name: post?.title,
                  },
                },
              ],
            }),
          }}
        />

        {/* Faq Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getFAQSchema(post?.blog_faq)),
          }}
        />

      </Head>
      <Container fluid className="pb-5">
        <BlogProgressBar post={post} />
        <Row className=" mx-auto"
          style={{
            maxWidth: "1400px",
          }}>
          <Col>
            <BlogFilters />
          </Col>
        </Row>
        <Row
          className=" mx-auto"
          style={{
            maxWidth: "1400px",
          }}
        >
          <Col lg={9}>
            <BlogPost post={post} />
          </Col>
          <Col lg={3}>
            {
              relatedPosts?.length > 0 ? <div
              >
                <p className="fs-5 fw-medium">You may also like: </p>
                {
                  relatedPosts?.length > 0 ? relatedPosts.map((post) => (
                    <BlogPostSmallCard key={uuid()} post={post} />
                  )) : <Spinner animation="border" />
                }
              </div>
                : null
            }

            {
              allCategories?.length > 0 ? <div
                className="mt-2 p-3 rounded-3"
                style={{
                  boxShadow: "rgba(0, 0, 0, 0.05) 0px 6px 24px 0px, rgba(0, 0, 0, 0.08) 0px 0px 0px 1px",
                  maxHeight: "500px",
                  overflowY: "auto",

                }}

              >
                <p className="fs-lg fw-medium mb-3">Browse All Categories:</p>
                {
                  allCategories?.length > 0 ? allCategories.map((category) => (
                    <Link href={`/blog/categories/${category.category_slug}`} key={uuid()}>
                      <a className="text-decoration-none
                    d-block text-dark bg-light p-2 rounded-3 mb-2
                    ">
                        <p className="fs-md mb-0">
                          <span className="me-2">⭐</span>
                          {category.name}</p>
                      </a>
                    </Link>

                  )) : <Spinner animation="border" />
                }
              </div>
                : null
            }

            <BlogLeadForm
              majors={majors}
            />

          </Col>
        </Row>
        {/* <Row
          className=" mx-auto"
          style={{
            maxWidth: "1400px",
          }}
        >
          <Col>
            <BlogRelatedPosts relatedLatestPosts={relatedLatestPosts.allPosts} currentPost={post} />
          </Col>
        </Row> */}
      </Container></>
  );
}
