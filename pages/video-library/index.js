import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import VideoContainer from "../../components/VideoLIbrary/VideoContainer";
import { getAllAuthors, getAllCategories, getAllVideos } from "../../services/videoLibrary.service";
import VideoLibraryBanner from "../../components/VideoLIbrary/VideoLibraryBanner/VideoLibraryBanner";
import SectionHeading from "../../components/global/SectionHeading/SectionHeading";
import VideoLibraryExperts from "../../components/VideoLIbrary/VideoLibraryExperts/VideoLibraryExperts";
import GetInTouchSection from "../../components/VideoLIbrary/VideoLibraryExperts/GetInTouchSection";
import LeadForm from "../../components/global/LeadForm/LeadForm";
import Image from "next/image";
import Lottie from "lottie-react";
import LeadFormAnimation from '../../public/animations/study-abroad-animation.json'
import { getMajorWithSpecialization, getMajors } from "../../services/specializations.service"
import Head from "next/head";
import { useEffect } from "react";
import { setCookie } from "cookies-next";

export async function getStaticProps(context) {
  let videosData = await getAllVideos(1, 8);
  let categoriesData = await getAllCategories(5);
  let authorsData = await getAllAuthors(10);
  let majors = await getMajorWithSpecialization();

  if (!videosData || !categoriesData || !authorsData) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      data: videosData,
      categories: categoriesData,
      authors: authorsData,
      majors: majors
    },
    revalidate: 60,
  };
}

export default function VideoLibraryPage({ data, categories, authors, majors }) {
  useEffect(() => {
    setCookie("sub_source", `Video Library`)
  }, [])
  return (
    <>
      <Head>
        <title>Learn in 60 Seconds - Study Abroad Videos</title>
        <meta
          name="description"
          content={`Expert-led videos in bite-sized portions cover everything from choosing the right program to navigating visa requirements. Start your global education journey today!`}
        />
        <meta property="og:image" content={"/illustrations/video-library.png"} />
        <meta property="twitter:image" content={"/illustrations/video-library.png"} />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content={`Learn in 60 Seconds - Study Abroad Videos`}
        />
        <meta
          property="og:description"
          content="Expert-led videos in bite-sized portions cover everything from choosing the right program to navigating visa requirements. Start your global education journey today!"
        />
        <meta property="og:url" content="https://collegevidyaabroad.com/" />
        <meta property="og:site_name" content="College Vidya Abroad" />
        <meta
          property="og:article:publisher"
          content="https://www.facebook.com/collegevidyaabroad"
        />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:site" content="@CollegeVidyaAbroad" />
        <meta
          property="twitter:title"
          content={`Learn in 60 Seconds - Study Abroad Videos`}
        />
        <meta
          property="twitter:description"
          content="Expert-led videos in bite-sized portions cover everything from choosing the right program to navigating visa requirements. Start your global education journey today!"
        />
        <meta property="twitter:url" content="https://collegevidyaabroad.com/" />
        <link href={`https://collegevidyaabroad.com/`} rel="canonical" />


        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: "https://collegevidyaabroad.com",
              name: `Learn in 60 Seconds - Study Abroad Videos`,
              description:
                "Expert-led videos in bite-sized portions cover everything from choosing the right program to navigating visa requirements. Start your global education journey today!",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: [".seotag"],
              },
            }),
          }}
        />

        {/* Address Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya Abroad",
              url: "https://collegevidyaabroad.com/",
              sameAs: [
                "https://facebook.com/collegevidyaabroad",
                "https://twitter.com/collegevidyaabroad",
                "https://www.linkedin.com/company/collegevidyaabroad",
                "https://www.instagram.com/collegevidyaabroad",
              ],
              logo: "https://collegevidyaabroad.com/logos/cv-abroad-logo.png",
              legalName: "College Vidya Abroad",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18003099018",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />
      </Head>
      <Container

      >
        <VideoLibraryBanner />
        <Row>
          <Col>
            <VideoContainer videosData={data} categories={categories} />
          </Col>
        </Row>

        {/* <Row>
          <Col>
            <SectionHeading
              heading={`Our <span class="text-cvblue">Study Abroad</span> Experts`}
              subHeading="Our certified study abroad counsellors are available to guide you through the entire process of studying abroad."
            />
          </Col>
        </Row> */}
        {/* <Row>
          <Col>
            <VideoLibraryExperts authors={authors} />
          </Col>
        </Row> */}

        <Row>
          <Col>
            <SectionHeading
              heading={`
             Still having doubts? <span class="text-cvblue">Get In Touch</span>
            `}
              subHeading="
            Connect with our experts to get all your study abroad queries answered.
            "
            />
          </Col>
        </Row>
        <Row>
          <Col>
            <GetInTouchSection />
          </Col>
        </Row>
        <Row className="bg-white" id="video-library-form">
          <Col lg={6}>
            <div className="w-100 h-100 d-flex flex-column align-items-center justify-content-center">
              <SectionHeading heading={
                `10X your <span class="text-cvblue">Admission Process</span>`
              } subHeading="You are just one step away from your dream of studying abroad." />
              <Lottie
                animationData={LeadFormAnimation}
                height={200}
                style={{
                  width: "70%",
                }}
              />
            </div>
          </Col>
          <Col lg={6}>
            <LeadForm majors={majors} />
          </Col>

        </Row>
      </Container></>
  );
}
