import React from "react";
import ExpertVisitorsBanner from "../components/global/Banners/ExpertVisitorsBanner/ExpertVisitorsBanner";
import AboutTeam from "../components/AboutPage/AboutTeam/AboutTeam";
import HomeFormContainer from "../components/global/HomeFormContainer/HomeFormContainer";
import AboutHeroBanner from "../components/AboutPage/AboutHeroBanner/AboutHeroBanner";
import AboutStats from "../components/AboutPage/AboutStats/AboutStats";
import AboutPanIndia from "../components/AboutPage/AboutPanIndia/AboutPanIndia";
import { getMajorWithSpecialization, getMajors } from "../services/specializations.service";
import Head from "next/head";

export async function getStaticProps(context) {
  let majors = await getMajorWithSpecialization()
  return {
    props: {
      majors: majors,
    },
    revalidate: 60,
  };
}

export default function Expert({ majors }) {
  return (
    <>
      <Head>
        <title>One-Stop Solution to Study Abroad</title>
        <meta property="og:image" content={"https://collegevidyaabroad.com/banners/mobile-herobanner-2023.jpg"} />
        <meta property="twitter:image" content={"https://collegevidyaabroad.com/banners/mobile-herobanner-2023.jpg"} />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content={`One-Stop Solution to Study Abroad`}
        />
        <meta
          property="og:description"
          content="College Vidya Abroad supports students to Study Abroad at the Best Global Universities. Experts assist in selecting Universities, Counseling, loans, and VISA."
        />
        <meta property="og:url" content="https://collegevidyaabroad.com/" />
        <meta property="og:site_name" content="College Vidya Abroad" />
        <meta
          property="og:article:publisher"
          content="https://www.facebook.com/collegevidyaabroad"
        />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:site" content="@CollegeVidyaAbroad" />
        <meta
          property="twitter:title"
          content={`One-Stop Solution to Study Abroad`}
        />
        <meta
          property="twitter:description"
          content="College Vidya Abroad supports students to Study Abroad at the Best Global Universities. Experts assist in selecting Universities, Counseling, loans, and VISA."
        />
        <meta property="twitter:url" content="https://collegevidyaabroad.com/" />
        <link href={`https://collegevidyaabroad.com/`} rel="canonical" />


        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: "https://collegevidyaabroad.com",
              name: `One-Stop Solution to Study Abroad`,
              description:
                "College Vidya Abroad supports students to Study Abroad at the Best Global Universities. Experts assist in selecting Universities, Counseling, loans, and VISA.",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: [".seotag"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya Abroad",
              url: "https://collegevidyaabroad.com/",
              sameAs: [
                "https://facebook.com/collegevidyaabroad",
                "https://twitter.com/collegevidyaabroad",
                "https://www.linkedin.com/company/collegevidyaabroad",
                "https://www.instagram.com/collegevidyaabroad",
              ],
              logo: "https://collegevidyaabroad.com/logos/cv-abroad-logo.png",
              legalName: "College Vidya Abroad",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18003099018",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />
      </Head>
      <AboutHeroBanner />
      <ExpertVisitorsBanner />
      <AboutStats />
      <AboutPanIndia />
      <AboutTeam />
      <HomeFormContainer image="/about/form-bg-2.jpg" majors={majors} />
    </>
  );
}
