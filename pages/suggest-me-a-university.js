import React from 'react'
import JourneyForm from '../components/JourneyPage/JourneyForm/JourneyForm'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import JourneyHeader from '../components/JourneyPage/JourneyHeader/JourneyHeader'
import JourneyFooter from '../components/JourneyPage/JourneyFooter/JourneyFooter'
import { getMajorWithSpecialization, getMajors } from '../services/specializations.service'
import { getCountriesNames } from '../services/country.service'
import Head from 'next/head'

export async function getStaticProps(context) {
  let majors = await getMajorWithSpecialization();
  let countries = await getCountriesNames()

  if (!majors || !countries) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      majors: majors,
      countries: countries
    },
    revalidate: 60,
  };
}

export default function SuggestUniversityPage({ majors, countries }) {
  return (
    <>
      <Head>
        <title>Suggest Me the Best University: Where to Study Abroad?</title>
        <meta
          name="description"
          content="Find your best matching university in a few steps. Choosing where to study, and to which university you should go, is one of the most important decisions you will make."
        />
        <meta
          name="robots"
          content="index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1"
        />
        <link
          href="https://collegevidyaabroad.com/suggest-me-a-university/"
          rel="canonical"
        />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content="Suggest Me the Best University: Where to Study?"
        />
        <meta
          property="og:description"
          content="Find your best matching university in a few steps. Choosing where to study, and to which university you should go, is one of the most important decisions you will make."
        />
        <meta
          property="og:url"
          content="https://collegevidyaabroad.com/suggest-me-a-university/"
        />
        <meta property="og:site_name" content="College Vidya Abroad" />
        <meta
          property="og:article:publisher"
          content="https://www.facebook.com/collegevidyaabroad"
        />
        <meta
          property="og:image"
          content="https://collegevidyaabroad.com/logos/cv-abroad-logo.png"
        />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:site" content="@CollegeVidyaAbroad" />
        <meta
          property="twitter:title"
          content="Find your best matching university in a few steps. Choosing where to study, and to which university you should go, is one of the most important decisions you will make."
        />
        <meta
          property="twitter:url"
          content="https://collegevidyaabroad.com/suggest-me-a-university/"
        />
        <meta
          property="twitter:image"
          content="https://collegevidyaabroad.com/logos/cv-abroad-logo.png"
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: "https://collegevidyaabroad.com/suggest-me-a-university/",
              name: "Suggest Me the Best University: Where to Study?",
              description:
                "Find your best matching university in a few steps. Choosing where to study, and to which university you should go, is one of the most important decisions you will make.",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: [".seotag"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya",
              url: "https://collegevidyaabroad.com/",
              sameAs: [
                "https://facebook.com/collegevidyaabroad",
                "https://twitter.com/collegevidyaabroad",
                " https://www.youtube.com/channel/UCJ1YLouvJ9Cx28-j-Yu7jJw",
                "https://www.linkedin.com/company/college-vidya",
                "https://www.instagram.com/collegevidyaabroad",
              ],
              logo: "https://collegevidyaabroad.com/images/logo.svg",
              legalName: "College Vidya",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18003099018",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "CollegeOrUniversity",
              name: "Suggest Me the Best University: Where to Study?",
              description:
                "Find your best matching university in a few steps. Choosing where to study, and to which university you should go, is one of the most important decisions you will make.",
              url: "https://collegevidyaabroad.com/suggest-me-a-university/",
              email: "info@collegevidyaabroad.com",
              telephone: "18003099018",
              logo: "https://collegevidyaabroad.com/logos/cv-abroad-logo.png",
              address: "India",
            }),
          }}
        />
      </Head>
      <Container fluid className='p-0 bg-light position-relative'>
        <Row >
          <Col >
            <JourneyHeader />
          </Col>
        </Row>
        <Row>
          <Col>
            <JourneyForm majors={majors} countries={countries} />
          </Col>
        </Row>
      </Container>
    </>

  )
}
