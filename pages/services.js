import React from "react";
import OurServicesPage from "../components/Home/OurServices/OurServicesPage";

export default function ServicePage() {
  return <OurServicesPage />;
}
