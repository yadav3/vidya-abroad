import React from "react";

export default function HelperPages() {
  return (
    <div>
      <p>
        <a href="https://collegevidyaabroad.com/compare/a" target="_blank" rel="noopener noreferrer">
          Compare Page (https://collegevidyaabroad.com/compare/a)
        </a>
      </p>
      <p>
        <a href="https://collegevidyaabroad.com/universities/harvard" target="_blank" rel="noopener noreferrer">
          University Page (https://collegevidyaabroad.com/universities/harvard)
        </a>
      </p>
      <p>
        <a href="https://collegevidyaabroad.com/countries/Germany" target="_blank" rel="noopener noreferrer">
          Country Page (https://collegevidyaabroad.com/countries/Germany)
        </a>
      </p>
    </div>
  );
}
