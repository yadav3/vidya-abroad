import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import BookAFreeCallPageBanner from "../components/BookAFreeCallPage/BookAFreeCallPageBanner/BookAFreeCallPage";
import BookAFreeCallLeadContainer from "../components/BookAFreeCallPage/BookAFreeCallLeadContainer/BookAFreeCallLeadContainer";
import SectionHeading from "../components/global/SectionHeading/SectionHeading";
import { getMajorWithSpecialization } from "../services/specializations.service";
import Head from "next/head";
import { useEffect } from "react";
import { getCookie, setCookie } from "cookies-next";
import Image from 'next/image'

export async function getStaticProps() {
    let majors = await getMajorWithSpecialization()

    return {
        props: {
            majors: majors,
        },
        revalidate: 60,
    };
}

export default function BookAFreeCallPage({ majors }) {


    useEffect(() => {
        setCookie("sub_source", `Landing Page: Book a Free Call`)
    }, [])


    return (
        <>
            <Head>
                <title>Book a Free Call - College Vidya Abroad</title>
                <meta
                    name="description"
                    content={`Book a free call with our experts to discover how we can assist you in making your
            admissions 10x Faster`}
                />
                <meta property="og:image" content={"https://collegevidyaabroad.com/banners/mobile-herobanner-2023.jpg"} />
                <meta property="twitter:image" content={"https://collegevidyaabroad.com/banners/mobile-herobanner-2023.jpg"} />
                <meta property="og:locale" content="en_US" />
                <meta property="og:type" content="website" />
                <meta
                    property="og:title"
                    content={`Book a Free Call - College Vidya Abroad`}
                />
                <meta
                    property="og:description"
                    content="Book a free call with our experts to discover how we can assist you in making your
            admissions 10x Faster"
                />
                <meta property="og:url" content="https://collegevidyaabroad.com/" />
                <meta property="og:site_name" content="College Vidya Abroad" />
                <meta
                    property="og:article:publisher"
                    content="https://www.facebook.com/collegevidyaabroad"
                />
                <meta property="twitter:card" content="summary" />
                <meta property="twitter:site" content="@CollegeVidyaAbroad" />
                <meta
                    property="twitter:title"
                    content={`Book a Free Call - College Vidya Abroad`}
                />
                <meta
                    property="twitter:description"
                    content="Book a free call with our experts to discover how we can assist you in making your
            admissions 10x Faster"
                />
                <meta property="twitter:url" content="https://collegevidyaabroad.com/" />
                <link href={`https://collegevidyaabroad.com/`} rel="canonical" />


                <script
                    type="application/ld+json"
                    dangerouslySetInnerHTML={{
                        __html: JSON.stringify({
                            "@context": "http://schema.org",
                            "@type": "webpage",
                            url: "https://collegevidyaabroad.com",
                            name: `Book a Free Call - College Vidya Abroad`,
                            description:
                                "Book a free call with our experts to discover how we can assist you in making your admissions 10x Faster",
                            speakable: {
                                "@type": "SpeakableSpecification",
                                cssSelector: [".seotag"],
                            },
                        }),
                    }}
                />

                {/* Address Schema */}
                <script
                    type="application/ld+json"
                    dangerouslySetInnerHTML={{
                        __html: JSON.stringify({
                            "@context": "http://schema.org",
                            "@type": "Organization",
                            name: "College Vidya Abroad",
                            url: "https://collegevidyaabroad.com/",
                            sameAs: [
                                "https://facebook.com/collegevidyaabroad",
                                "https://twitter.com/collegevidyaabroad",
                                "https://www.linkedin.com/company/collegevidyaabroad",
                                "https://www.instagram.com/collegevidyaabroad",
                            ],
                            logo: "https://collegevidyaabroad.com/logos/cv-abroad-logo.png",
                            legalName: "College Vidya Abroad",
                            address: {
                                "@type": "PostalAddress",
                                addressCountry: "India",
                                addressLocality: "",
                                addressRegion: "",
                                postalCode: "",
                                streetAddress: ",",
                            },
                            contactPoint: {
                                "@type": "ContactPoint",
                                telephone: "18003099018",
                                contactType: "Customer Service",
                                contactOption: "TollFree",
                                areaServed: ["IN"],
                            },
                        }),
                    }}
                />


            </Head>
            <Container fluid>
                <Row>
                    <Col>
                        <div className="p-3 cursor-pointer ">
                            <Image alt="College Vidya Abroad Logo" src="/logos/cv-abroad-logo.png" height={50} width={150} objectFit="contain" 
                            onClick={() => window.location.href = "/book-a-free-call"}
                            />
                        </div>
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <BookAFreeCallPageBanner />
                    </Col>
                </Row>
            </Container>
            <Container>
                <Row>
                    <Col>
                        <BookAFreeCallLeadContainer majors={majors} />
                    </Col>
                </Row>
               
            </Container>
        </>
    );
}
