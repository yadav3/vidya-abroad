import React, { useEffect, useMemo, useState } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { useRouter } from "next/router";
import FreeResourceBanner from "../../components/FreeResourcesPage/FreeResourceBanner/FreeResourceBanner";
import SingleResourceBanner from "../../components/FreeResourcesPage/SingleResourceBanner/SingleResourceBanner";
import ResourcePageStrip from "../../components/FreeResourcesPage/ResourcePageStrip/ResourcePageStrip";
import FreeResourceProduct from "../../components/FreeResourcesPage/FreeResourceProduct/FreeResourceProduct";
import { useLocalStorage } from "../../context/CustomHooks";
import axios from "axios";
import { getMajorWithSpecialization } from "../../services/specializations.service";
import { FreeResourceLeadForm } from "../../components/FreeResourcesPage/FreeResourceLeadForm/FreeResourceLeadForm";
import { setCookie } from "cookies-next";
import { getSearchSchema } from "../../utils/misc";
import Head from "next/head";
import freebieService from "../../services/freebie.service";
import { getAWSImagePath } from "../../context/services";
import MoreFreebiesSection from "../../components/FreeResourcesPage/MoreFreebiesSection/MoreFreebiesSection";

export async function getStaticPaths() {
  const freebiesSlug = await freebieService.getAllFreebieSlugs();

  const paths = freebiesSlug.map((slug) => {
    return {
      params: { resourceSlug: slug.slug },
    };
  })

  return {
    paths,
    fallback: true, // can also be true or 'blocking'
  };
}

export async function getStaticProps(context) {
  const freebieSlug = context.params.resourceSlug;
  let majors = await getMajorWithSpecialization();
  const freebie = await freebieService.getFreebieDataBySlug(freebieSlug);
  const relatedFreebies = await freebieService.getRelatedFreebiesBySlug(freebieSlug);

  return {
    props: {
      majors: majors,
      freebie: freebie,
      relatedFreebies: relatedFreebies,
    },
    revalidate: 60,
  };
}

export default function FreeResourcePage({ majors, freebie, relatedFreebies }) {
  const router = useRouter();
  const handleDownloadFile = () => {
    const fileUrl = getAWSImagePath(freebie.fileUrl);
    const link = document.createElement("a");
    link.href = fileUrl;
    link.download = `${freebie.slug}.pdf`;
    link.click();
  };

  useEffect(() => {
    setCookie("sub_source", `Freebie Page: ${router.query.resourceSlug}`)
  }, [])

  return (
    <>

      {freebie?.title?.length > 0 ? (
        <>
          <Head>
            <title>{freebie.title} - Download for Free</title>
            <meta
              name="description"
              content={`Discover the power of free knowledge - explore our collection of eBooks and Study Abroad Guides right now!`}
            />
            <meta property="og:image" content={"https://collegevidyaabroad.com/banners/mobile-herobanner-2023.jpg"} />
            <meta property="twitter:image" content={"https://collegevidyaabroad.com/banners/mobile-herobanner-2023.jpg"} />
            <meta property="og:locale" content="en_US" />
            <meta property="og:type" content="website" />
            <meta
              property="og:title"
              content={`${freebie.title} - Download Now`}
            />
            <meta
              property="og:description"
              content="Discover the power of free knowledge - explore our collection of eBooks and Study Abroad Guides right now!"
            />
            <meta property="og:url" content="https://collegevidyaabroad.com/" />
            <meta property="og:site_name" content="College Vidya Abroad" />
            <meta
              property="og:article:publisher"
              content="https://www.facebook.com/collegevidyaabroad"
            />
            <meta property="twitter:card" content="summary" />
            <meta property="twitter:site" content="@CollegeVidyaAbroad" />
            <meta
              property="twitter:title"
              content={`${freebie.title} - Download Now`}
            />
            <meta
              property="twitter:description"
              content="Discover the power of free knowledge - explore our collection of eBooks and Study Abroad Guides right now!"
            />
            <meta property="twitter:url" content="https://collegevidyaabroad.com/" />
            <link href={`https://collegevidyaabroad.com/`} rel="canonical" />


            <script
              type="application/ld+json"
              dangerouslySetInnerHTML={{
                __html: JSON.stringify({
                  "@context": "http://schema.org",
                  "@type": "webpage",
                  url: "https://collegevidyaabroad.com",
                  name: `${freebie.title} - Download Now`,
                  description:
                    "Discover the power of free knowledge - explore our collection of eBooks and Study Abroad Guides right now!",
                  speakable: {
                    "@type": "SpeakableSpecification",
                    cssSelector: [".seotag"],
                  },
                }),
              }}
            />

            {/* Address Schema */}
            <script
              type="application/ld+json"
              dangerouslySetInnerHTML={{
                __html: JSON.stringify({
                  "@context": "http://schema.org",
                  "@type": "Organization",
                  name: "College Vidya Abroad",
                  url: "https://collegevidyaabroad.com/",
                  sameAs: [
                    "https://facebook.com/collegevidyaabroad",
                    "https://twitter.com/collegevidyaabroad",
                    "https://www.linkedin.com/company/collegevidyaabroad",
                    "https://www.instagram.com/collegevidyaabroad",
                  ],
                  logo: "https://collegevidyaabroad.com/logos/cv-abroad-logo.png",
                  legalName: "College Vidya Abroad",
                  address: {
                    "@type": "PostalAddress",
                    addressCountry: "India",
                    addressLocality: "",
                    addressRegion: "",
                    postalCode: "",
                    streetAddress: ",",
                  },
                  contactPoint: {
                    "@type": "ContactPoint",
                    telephone: "18003099018",
                    contactType: "Customer Service",
                    contactOption: "TollFree",
                    areaServed: ["IN"],
                  },
                }),
              }}
            />


          </Head>
          <Container fluid className="m-0 p-0">
            <Row>
              <Col>
                <SingleResourceBanner handleDownloadFile={() => handleDownloadFile()} freebie={freebie} />
              </Col>
            </Row>
            <Row>
              <Col>
                <ResourcePageStrip majors={majors} handleDownloadFile={() => handleDownloadFile()} freebie={freebie} />
              </Col>
            </Row>
          </Container>
          <Container>
            <Row>
              <Col>
                <FreeResourceProduct majors={majors} handleDownloadFile={() => handleDownloadFile()} freebie={freebie} />
              </Col>
            </Row>
            <Row>
              <Col>
                <MoreFreebiesSection relatedFreebies={relatedFreebies} />
              </Col>
            </Row>
          </Container>
        </>
      ) : null}
    </>
  );
}

// <Head>
//   <meta charSet="UTF-8" />
//   <meta name="viewport" content="width=device-width, initial-scale=1" />
//   <meta name="robots" content="index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1" />
//   <meta name="title" content={freebie.title} />
//   <meta name="description" content={freebie.description} />
//   <link rel="canonical" href={`https://collegevidyaabroad.com/resources/${freebie.slug}/`} />
//   <meta property="og:locale" content="en_US" />
//   <meta property="og:type" content="article" />
//   <meta property="og:title" content={freebie.title} />
//   <meta property="og:description" content={freebie.description} />
//   <meta property="og:url" content={`https://collegevidyaabroad.com/resources/${freebie.slug}/`} />
//   <meta property="og:site_name" content="College Vidya Abroad Blog" />

//   <meta property="og:image:width" content="1280" />
//   <meta property="og:image:height" content="720" />
//   <meta name="twitter:card" content="summary_large_image" />
//   <meta name="twitter:label1" content="Written by" />
//   <meta name="twitter:data1" content="compare" />
//   <meta name="twitter:label2" content="Est. reading time" />
//   <meta
//     property="og:image"
//     content={freebie?.thumbnail ? freebie.thumbnail : "https://collegevidyaabroad.s3.ap-south-1.amazonaws.com/placeholder-image.jpg"}
//   />
//   <meta
//     property="twitter:image"
//     content={freebie?.thumbnail ? freebie.thumbnail : "https://collegevidyaabroad.s3.ap-south-1.amazonaws.com/placeholder-image.jpg"}
//   />

//   <script
//     type="application/ld+json"
//     dangerouslySetInnerHTML={{
//       __html: JSON.stringify({
//         "@context": "http://schema.org",
//         "@type": "Article",
//         mainEntityOfPage: {
//           "@type": "WebPage",
//           "@id": `https://collegevidyaabroad.com/resources/${freebie.slug}/`,
//         },
//         headline: freebie.title,
//         image: {
//           "@type": "ImageObject",
//           url: freebie.thumbnail ? freebie.thumbnail : "https://collegevidyaabroad.s3.ap-south-1.amazonaws.com/placeholder-image.jpg",
//           height: 1280,
//           width: 720,
//         },
//         author: {
//           "@type": "Person",
//           name: "College Vidya Abroad",
//         },
//         publisher: {
//           "@type": "Organization",
//           name: "College Vidya Abroad",
//           url: "https://collegevidyaabroad.com/",
//           sameAs: [
//             "https://www.instagram.com/collegevidyaabroad",
//             "https://facebook.com/collegevidyaabroad",
//             "https://twitter.com/collegevidyaabroad",
//             "https://www.linkedin.com/company/collegevidyaabroad",
//           ],
//           logo: {
//             "@type": "ImageObject",
//             url: "https://collegevidyaabroad.com/logos/cv-abroad-logo.png",
//             width: 256,
//             height: 85,
//           },
//         },
//         description: freebie.description,
//       }),
//     }}
//   />

//   {/* Search Schema */}
//   <script
//     type="application/ld+json"
//     dangerouslySetInnerHTML={{
//       __html: JSON.stringify(getSearchSchema()),
//     }}
//   />

//   {/* Breadcrumb Schema */}
//   <script
//     type="application/ld+json"
//     dangerouslySetInnerHTML={{
//       __html: JSON.stringify({
//         "@context": "https://schema.org",
//         "@type": "BreadcrumbList",
//         itemListElement: [
//           {
//             "@type": "ListItem",
//             position: 1,
//             item: {
//               "@id": "https://collegevidyaabroad.com/blog",
//               name: "Resources",
//             },
//           },
//           {
//             "@type": "ListItem",
//             position: 2,
//             item: {
//               "@id": `https://collegevidyaabroad.com/resources/freebie.slug`,
//               name: freebie.slug,
//             },
//           },
//         ],
//       }),
//     }}
//   />
// </Head>
