import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Head from "next/head";
import FreeResourceBanner from "../../components/FreeResourcesPage/FreeResourceBanner/FreeResourceBanner";
import SectionHeading from "../../components/global/SectionHeading/SectionHeading";
import FreeResourcesContainer from "../../components/FreeResourcesPage/FreeResroucesContainer/FreeResourcesContainer";
import freebieService from "../../services/freebie.service";

export async function getStaticProps(context) {
    const freebies = await freebieService.getAllFreebies()

    return {
        props: {
            freebies: freebies
        },
        revalidate: 60,
    };
}

export default function FreeStudyAbroadResources({ freebies }) {
    return (
        <>
            <Head>
                <title>Download Free Study Abroad Guide and Resources</title>
                <link rel="canonical" href="https://collegevidyaabroad.com/blog/" />
                <meta property="og:locale" content="en_US" />
                <meta property="og:type" content="website" />
                <meta property="og:title" content="Download Free Study Abroad Guide and Resources" />
                <meta
                    name="description"
                    content="Visit College Vidya Abroad to get the Study Abroad Blogs about Top Universities, Courses, Exams, the Best Destination to Study Abroad, and the latest news."
                />
                <meta property="og:url" content="https://collegevidyaabroad.com/blog/" />
                <meta property="og:site_name" content="Download Free Study Abroad Guide and Resources" />
                <meta name="twitter:card" content="summary_large_image" />

                {/* Schema One */}
                <script
                    type="application/ld+json"
                    dangerouslySetInnerHTML={{
                        __html: JSON.stringify({
                            "@context": "http://schema.org",
                            "@type": "webpage",
                            url: "https://collegevidyaabroad.com/blog/",
                            name: "Download Free Study Abroad Guide and Resources",
                            description:
                                "Visit College Vidya Abroad to get the Study Abroad Blogs about Top Universities, Courses, Exams, the Best Destination to Study Abroad, and the latest news.",
                            speakable: {
                                "@type": "SpeakableSpecification",
                                cssSelector: [".seotag"],
                            },
                        }),
                    }}
                />

                {/* Schema Two */}
                <script
                    type="application/ld+json"
                    dangerouslySetInnerHTML={{
                        __html: JSON.stringify({
                            "@context": "http://schema.org",
                            "@type": "Organization",
                            name: "College Vidya Abroad",
                            url: "https://collegevidyaabroad.com/",
                            sameAs: [
                                "https://facebook.com/collegevidyaabroad",
                                "https://twitter.com/collegevidyaabroad",
                                "https://www.linkedin.com/company/collegevidyaabroad",
                                "https://www.instagram.com/collegevidyaabroad",
                            ],
                            logo: "https://collegevidyaabroad.com/logos/cv-abroad-logo.png",
                            legalName: "College Vidya Abroad",
                            address: {
                                "@type": "PostalAddress",
                                addressCountry: "India",
                                addressLocality: "",
                                addressRegion: "",
                                postalCode: "",
                                streetAddress: ",",
                            },
                            contactPoint: {
                                "@type": "ContactPoint",
                                telephone: "18003099018",
                                contactType: "Customer Service",
                                contactOption: "TollFree",
                                areaServed: ["IN"],
                            },
                        }),
                    }}
                />

                {/* Breadcrumb Schema */}
                <script
                    type="application/ld+json"
                    dangerouslySetInnerHTML={{
                        __html: JSON.stringify({
                            "@context": "https://schema.org",
                            "@type": "BreadcrumbList",
                            itemListElement: [
                                {
                                    "@type": "ListItem",
                                    position: 1,
                                    item: {
                                        "@id": "https://collegevidyaabroad.com/blog/",
                                        name: "Free Resources",
                                    },
                                },
                            ],
                        }),
                    }}
                />
            </Head>
            <Container className="pb-5 overflow-hidden">
                <Row>
                    <Col className="p-0">
                        <FreeResourceBanner />
                    </Col>
                </Row>
                <Row className="d-none d-sm-flex">
                    <Col>
                        <SectionHeading
                            heading={`<span class="fw-normal">Select your <span class="text-cvblue fw-bold">Freebie</span></span>`}
                        />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <FreeResourcesContainer freebies={freebies} />
                    </Col>
                </Row>
            </Container>
        </>
    );
}
