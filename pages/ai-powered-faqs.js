import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { FaqHeader } from "../components/FaqPage/FaqHeader";
import { FaqSearch } from "../components/FaqPage/FaqSearch";
import FaqAnswer from "../components/FaqPage/FaqAnswer";
import FaqSearchGuide from "../components/FaqPage/FaqSearchGuide";
import styles from '../components/FaqPage/FaqPage.module.scss'
import Head from "next/head";

// export async function getStaticProps(context) {
//     let country_faq_data = await getCountryFaqPageData();

//     if (!country_faq_data) {
//         return {
//             notFound: true,
//         };
//     }

//     return {
//         props: {
//             country_faq_data: country_faq_data,
//         },
//         revalidate: 60,
//     };
// }

export default function FaqPage() {



    return (
        <>
            <Head>
                <title>
                    Ask Anything You Want - Get Instant Answers to Your Study Abroad Queries
                </title>
                <meta
                    name="description"
                    content="College Vidya Abroad brings you an AI-powered tool with which you can get study abroad information on Universities, Scholarships, and visas in seconds!"
                />
                <link rel="canonical" href="https://collegevidyaabroad.com/ai-powered-faqs/" />
                <meta property="og:locale" content="en_US" />
                <meta property="og:type" content="website" />
                <meta
                    property="og:title"
                    content="Ask Anything You Want - Get Instant Answers to Your Study Abroad Queries"
                />

                <meta property="og:url" content="https://collegevidyaabroad.com/ai-powered-faqs/" />
                <meta
                    property="og:site_name"
                    content="Ask Anything You Want - Get Instant Answers to Your Study Abroad Queries"
                />
                <meta name="twitter:card" content="summary_large_image" />
                {/* <meta property="og:image" content={cvTalkBlogs[0]?.blogs.image} /> */}
                {/* <meta property="twitter:image" content={cvTalkBlogs[0]?.blogs.image} /> */}

                {/* Schema One */}
                <script
                    type="application/ld+json"
                    dangerouslySetInnerHTML={{
                        __html: JSON.stringify({
                            "@context": "http://schema.org",
                            "@type": "webpage",
                            url: "https://collegevidyaabroad.com/ai-powered-faqs/",
                            name: "Ask Anything You Want - Get Instant Answers to Your Study Abroad Queries",
                            description:
                                "College Vidya Abroad brings you an AI-powered tool with which you can get study abroad information on Universities, Scholarships, and visas in seconds!",
                            speakable: {
                                "@type": "SpeakableSpecification",
                                cssSelector: [".seotag"],
                            },
                        }),
                    }}
                />

                {/* Schema Two */}
                <script
                    type="application/ld+json"
                    dangerouslySetInnerHTML={{
                        __html: JSON.stringify({
                            "@context": "http://schema.org",
                            "@type": "Organization",
                            name: "College Vidya Abroad",
                            url: "https://collegevidyaabroad.com/",
                            sameAs: [
                                "https://facebook.com/collegevidyaabroad",
                                "https://twitter.com/collegevidyaabroad",
                                "https://www.linkedin.com/company/collegevidyaabroad",
                                "https://www.instagram.com/collegevidyaabroad",
                            ],
                            logo: "https://collegevidyaabroad.com/logos/cv-abroad-logo.png",
                            legalName: "College Vidya Abroad",
                            address: {
                                "@type": "PostalAddress",
                                addressCountry: "India",
                                addressLocality: "",
                                addressRegion: "",
                                postalCode: "",
                                streetAddress: ",",
                            },
                            contactPoint: {
                                "@type": "ContactPoint",
                                telephone: "18003099018",
                                contactType: "Customer Service",
                                contactOption: "TollFree",
                                areaServed: ["IN"],
                            },
                        }),
                    }}
                />

                {/* Breadcrumb Schema */}
                <script
                    type="application/ld+json"
                    dangerouslySetInnerHTML={{
                        __html: JSON.stringify({
                            "@context": "https://schema.org",
                            "@type": "BreadcrumbList",
                            itemListElement: [
                                {
                                    "@type": "ListItem",
                                    position: 1,
                                    item: {
                                        "@id": "https://collegevidyaabroad.com/ai-powered-faqs/",
                                        name: "Ask Anything You Want",
                                    },
                                },
                            ],
                        }),
                    }}
                />
            </Head>
            <Container className={`pb-4 d-flex flex-column pt-1 pt-md-5`}
                style={{ minHeight: "100vh" }}
            >
                <FaqHeader />
                <FaqSearch />
                <FaqAnswer />
                <FaqSearchGuide />
            </Container></>
    );
}
