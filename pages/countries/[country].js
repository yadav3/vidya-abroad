import axios from "axios";
import { useRouter } from "next/router";
import React from "react";
import { useEffect } from "react";
import CountriesPage from "../../components/CountriesPage/CountriesPage";
import { BACKEND_API_URL } from "../../context/services";
import { getMajorWithSpecialization, getMajors } from "../../services/specializations.service";
import Head from "next/head";
import { getFAQSchema } from "../../utils/misc";
import { getCountriesNames } from "../../services/country.service";
import { getAllCourseCategories } from "../../services/course.service";

// pages/posts/[id].js

export async function getStaticPaths() {
  const countrySlugs = await axios.get(`${BACKEND_API_URL}/countries/slugs`).then((res) => res.data);


  const paths = countrySlugs.map((slug) => {
    return {
      params: { country: slug.slug },
    };
  });

  return {
    paths,
    fallback: true, // can also be true or 'blocking'
  };
}

// `getStaticPaths` requires using `getStaticProps`
export async function getStaticProps(context) {
  const countrySlug = context.params.country;
  let majors = await getMajorWithSpecialization();
  let countryData = await axios.get(`${BACKEND_API_URL}/countries/data/${countrySlug}`).then((res) => res.data);
  let countries = await getCountriesNames()
  let courseCategories = await getAllCourseCategories()

  if (!countryData) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      countrySlug,
      countryData: countryData,
      majors: majors,
      countries: countries,
      courseCategories: courseCategories
    },
    revalidate: 60,
  };
}

export default function Country({ countryData, countrySlug, majors, countries, courseCategories }) {
  const router = useRouter();

  if (router.isFallback) {
    return <div>Loading...</div>;
  }

  return <>
    <Head>
      <title>Study Abroad in the {countryData.name}</title>
      <meta
        name="description"
        content={`Apply to top universities in the ${countryData.name} and select the best course. College Vidya Abroad Experts assist you to compare and choose from 200+ universities.`}
      />
      <meta property="og:image" content={"https://collegevidyaabroad.com/banners/mobile-herobanner-2023.jpg"} />
      <meta property="twitter:image" content={"https://collegevidyaabroad.com/banners/mobile-herobanner-2023.jpg"} />
      <meta property="og:locale" content="en_US" />
      <meta property="og:type" content="website" />
      <meta
        property="og:title"
        content={`Study Abroad in the ${countryData.name}`}
      />
      <meta
        property="og:description"
        content={`Apply to top universities in the ${countryData.name} and select the best course. College Vidya Abroad Experts assist you to compare and choose from 200+ universities.`}
      />
      <meta property="og:url" content="https://collegevidyaabroad.com/" />
      <meta property="og:site_name" content="College Vidya Abroad" />
      <meta
        property="og:article:publisher"
        content="https://www.facebook.com/collegevidyaabroad"
      />
      <meta property="twitter:card" content="summary" />
      <meta property="twitter:site" content="@CollegeVidyaAbroad" />
      <meta
        property="twitter:title"
        content={`Study Abroad in the ${countryData.name}`}
      />
      <meta
        property="twitter:description"
        content={`Apply to top universities in the ${countryData.name} and select the best course. College Vidya Abroad Experts assist you to compare and choose from 200+ universities.`}
      />
      <meta property="twitter:url" content="https://collegevidyaabroad.com/" />
      <link href={`https://collegevidyaabroad.com/`} rel="canonical" />


      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify({
            "@context": "http://schema.org",
            "@type": "webpage",
            url: "https://collegevidyaabroad.com",
            name: `Study Abroad in the ${countryData.name}`,
            description:
              `Apply to top universities in the ${countryData.name} and select the best course. College Vidya Abroad Experts assist you to compare and choose from 200+ universities.`,
            speakable: {
              "@type": "SpeakableSpecification",
              cssSelector: [".seotag"],
            },
          }),
        }}
      />

      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify({
            "@context": "http://schema.org",
            "@type": "Organization",
            name: "College Vidya Abroad",
            url: "https://collegevidyaabroad.com/",
            sameAs: [
              "https://facebook.com/collegevidyaabroad",
              "https://twitter.com/collegevidyaabroad",
              "https://www.linkedin.com/company/collegevidyaabroad",
              "https://www.instagram.com/collegevidyaabroad",
            ],
            logo: "https://collegevidyaabroad.com/logos/cv-abroad-logo.png",
            legalName: "College Vidya Abroad",
            address: {
              "@type": "PostalAddress",
              addressCountry: "India",
              addressLocality: "",
              addressRegion: "",
              postalCode: "",
              streetAddress: ",",
            },
            contactPoint: {
              "@type": "ContactPoint",
              telephone: "18003099018",
              contactType: "Customer Service",
              contactOption: "TollFree",
              areaServed: ["IN"],
            },
          }),
        }}
      />

      {/* FAQ Schema */}
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{
          __html: JSON.stringify(getFAQSchema(countryData.CountryFaq.map((faq) => {
            return {
              question: faq.question,
              answer_html: faq.answer
            }
          }))),
        }}
      />
    </Head>
    <CountriesPage countryData={countryData} countrySlug={countrySlug} majors={majors} countries={countries} courseCategories={courseCategories} />
  </>;
}
