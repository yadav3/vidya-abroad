import React from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import uuid from "react-uuid";
import UniversityListingCard from "../../components/ComparePage/UniversityListingCard/UniversityListingCard";
import CompareSelectedUniversities from "../../components/ComparePage/CompareSelectedUniversities/CompareSelectedUniversities";
import { useRouter } from "next/router";
import { COUNSELLOR_CLIENT_URL, getAllExchangeRates, getCompareQueryByUserId, getCounsellorCustomerByUuid } from "../../context/services";
import { useRecoilState, useRecoilValue } from "recoil";
import { userCompareQueryAtom } from "../../store/userStore";
import { useEffect } from "react";
import { useState } from "react";
import {
  allCompareFilteredUniversitiesAtom,
  allCompareUniversitiesAtom,
  compareQueryParamsState,
  selectedUniversitiesState,
} from "../../store/compare/compareStore";
import { getUniversitiesByCompareQueries, getUniversitiesByCourseId } from "../../services/universitiesService";
import CompareFilters from "../../components/ComparePage/CompareFilters/CompareFilters";
import { getMajorWithSpecialization, getMajors } from "../../services/specializations.service";
import NoResultFound from "../../components/global/NoResultFound/NoResultFound";
import UniversityListingCardSkelton from "../../components/Skeltons/UniversityListingCardSkelton";
import UniversityListingHeadings from "../../components/ComparePage/UniversityListingCard/UniversityListingHeadings";
import { cvCounsellorDetailsAtom, journeyDetailsAtom } from "../../store/journeyStore";
import AllUniversityToCompareModal from "../../components/ComparePage/AllUniversityToCompareModal/AllUniversityToCompareModal";
import JourneyRemainingQuestions from "../../components/JourneyPage/JourneyRemainingQuestions/JourneyRemainingQuestions";
import { JourneyRemainingQuestionModalAtom, JourneyRemainingQuestionModalStateAtom, journeyFormStepAtom } from "../../store/journeyFormStore";
import Head from "next/head";
import AlertBanner from "../../components/global/AlertBanner/AlertBanner";
import FreebieAlertAndModal from "../../components/FreeResourcesPage/FreebieAlertAndModal/FreebieAlertAndModal";
import freebieService from "../../services/freebie.service";
import CompareFilterModal from "../../components/ComparePage/CompareFilters/CompareFilterModal";

export async function getStaticProps(context) {
  let majors = await getMajorWithSpecialization();
  let exchangeRates = await getAllExchangeRates();
  let freebies = await freebieService.getAllFreebies()
  return {
    props: {
      majors: majors,
      exchangeRates: exchangeRates,
      freebies: freebies
    },
    revalidate: 60,
  };
}

export default function CompareUniversitiesPage({ majors, exchangeRates, freebies }) {
  const router = useRouter();
  const { uuid: userId } = router.query;
  const [loading, setLoading] = useState(true);
  const [compareUniversitiesMessage, setCompareUniversitiesMessage] = useState(null);
  const [journeyFormStep, setJourneyFormStep] = useRecoilState(journeyFormStepAtom);
  const [userCompareQuery, setUserCompareQuery] = useRecoilState(compareQueryParamsState);
  const [journeyDetails, setJourneyDetails] = useRecoilState(journeyDetailsAtom);
  const selectedUniversities = useRecoilValue(selectedUniversitiesState);
  const [universities, setUniversities] = useRecoilState(allCompareUniversitiesAtom);
  const [filteredUniversities, setFilteredUniversities] = useRecoilState(allCompareFilteredUniversitiesAtom);
  const [cvCounsellorDetails, setCvCounsellorDetails] = useRecoilState(cvCounsellorDetailsAtom);
  const [showJourneyRemainingQuestionModal, setShowJourneyRemainingQuestionModal] = useRecoilState(JourneyRemainingQuestionModalAtom);
  const [questionsModal, setQuestionsModal] = useRecoilState(JourneyRemainingQuestionModalStateAtom);

  // Define a function to update the user compare query
  const updateUserCompareQuery = (data) => {

    if (parseInt(data.answered_questions) !== 1) {
      setShowJourneyRemainingQuestionModal(true);
      setQuestionsModal({
        ...questionsModal,
        onClose: () => {
          () => { };
        },
        submitButtonText: universities?.length > 0 ? `Compare ${universities.length}+ Results!` : "Compare Universities",
      });
    }

    setUserCompareQuery({
      ...userCompareQuery,
      uuid: userId,
      categoryId: data.category_id,
      courseId: data.course_Id,
      majorId: data.majorId,
      specializationId: data.specialization_id,
      countryId: data.country_id,
      ieltsScore: data.ielts,
      budgetLimit: data.budget,
      questionAnswered: data.answered_questions,
      opportunityId: data.prospect_opportunity_id,
      budgetLow: data.budget_low,
      budgetHigh: data.budget_high,
      countries: data?.countries ? [...data.countries] : [],
      relatedProspectId: data.relatedProspectId
    });
  };

  // Define a function to handle errors
  const handleError = (err) => console.error(err);

  // Define a function to update the counselor details
  const updateCvCounsellorDetails = (data) => {
    setCvCounsellorDetails({
      ...cvCounsellorDetails,
      cv_id: data?.cv_id || userId,
      redirectLink: `${COUNSELLOR_CLIENT_URL}/collegevidya/redirect/login?cv_id=${data?.cv_id ? data?.cv_id : userId}`,
    });
  };

  // Define the useEffect hook
  useEffect(() => {
    // Check if the router is ready and if the UUID has changed
    if (router.isReady && userId && userCompareQuery?.uuid !== userId) {
      // Call the getCompareQueryByUserId function and update the user compare query
      getCompareQueryByUserId(userId).then(updateUserCompareQuery).catch(handleError)
      // Call the getCounsellorCustomerByUuid function and update the counselor details
      getCounsellorCustomerByUuid(userId).then(updateCvCounsellorDetails).catch(handleError);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router.isReady, userCompareQuery.uuid, userId]);

  // Define a function to update universities and filtered universities
  const updateUniversities = (data, exchangeRates) => {
    const universities = data.data.universities;
    setCompareUniversitiesMessage(data.data.message);
    const filteredUniversities = universities.map((university) => {
      const exchangeRate = exchangeRates.find((rate) => rate.target === university.currency);
      return {
        ...university,
        feesNew: Math.trunc(university.fees / exchangeRate.rate),
        currencyNew: "INR",
        currencySymbolNew: "₹",
      };
    });
    setUniversities(universities);
    setFilteredUniversities(filteredUniversities);
  };

  // Define the useEffect hook
  useEffect(() => {
    // Update the journey form step
    setJourneyFormStep(1);
    // Check if major ID or country ID exists in user compare query
    if (userCompareQuery.majorId || userCompareQuery.countryId) {

      // Set loading to true
      setLoading(true);
      // Call the getExchangeRates function to get exchange rates
      getUniversitiesByCompareQueries(
        userCompareQuery.majorId,
        userCompareQuery.countryId,
        0,
        0,
        userCompareQuery.budgetLow,
        userCompareQuery.budgetHigh,
        userCompareQuery.countries,
        userId
      )
        .then((data) => {
          updateUniversities(data, exchangeRates)
        })
        .catch(handleError)
        .finally(() => {
          // Set loading to false
          setLoading(false);
        });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [userCompareQuery.majorId, userCompareQuery.countryId, userCompareQuery.categoryId]);


  return (
    <>
      <Head>
        <title>Compare Popular Study Abroad Colleges and Universities</title>
        <meta
          name="description"
          content={`Compare study abroad colleges universities on their fees, courses, approvals, e-learning facility, emi option and other details only at College Vidya Abroad compare portal.`}
        />
        <meta property="og:image" content={"https://collegevidyaabroad.com/logos/cv-abroad-logo.png"} />
        <meta property="twitter:image" content={"https://collegevidyaabroad.com/logos/cv-abroad-logo.png"} />
        <meta property="og:locale" content="en_US" />
        <link href={`https://collegevidyaabroad.com/compare/universities`} rel="canonical" />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: "https://collegevidyaabroad.com",
              name: `Compare Popular Study Abroad Colleges and Universities`,
              description:
                "Compare study abroad colleges universities on their fees, courses, approvals, e-learning facility, emi option and other details only at College Vidya Abroad compare portal.",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: [".seotag"],
              },
            }),
          }}
        />

        {/* Address Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya Abroad",
              url: "https://collegevidyaabroad.com/",
              sameAs: [
                "https://facebook.com/collegevidyaabroad",
                "https://twitter.com/collegevidyaabroad",
                "https://www.linkedin.com/company/collegevidyaabroad",
                "https://www.instagram.com/collegevidyaabroad",
              ],
              logo: "https://collegevidyaabroad.com/logos/cv-abroad-logo.png",
              legalName: "College Vidya Abroad",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18003099018",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />
      </Head>
      <Container
        fluid
        style={{
          backgroundColor: "#f4f5f7",
        }}
        className="px-md-5 pb-5"
      >
        <Row>
          <Col lg={3}>
            <div className="position-sticky top-0 d-none d-lg-block">
              <CompareFilters
                universities={universities}
                filteredUniversities={filteredUniversities}
                setFilteredUniversities={setFilteredUniversities}
                majors={majors}
                exchangeRates={exchangeRates}
              />
            </div>
            <div className="position-sticky top-0 d-block d-lg-none">
              <CompareFilterModal universities={universities}
                filteredUniversities={filteredUniversities}
                setFilteredUniversities={setFilteredUniversities}
                majors={majors}
                exchangeRates={exchangeRates}>
                <button className="w-100 border-0 py-2 rounded-3 mt-4 bg-cvblue text-white" >
                  Show Filters 
                </button>
              </CompareFilterModal>
            </div>
          </Col>
          <Col lg={9}>
            <div className="p-0 py-sm-4 pt-4 d-flex flex-column h-100 ">
              <UniversityListingHeadings />
              {compareUniversitiesMessage &&
                <AlertBanner showIcon title={"Thats a Unique Choice!"} subTitle={`However, here are other ${compareUniversitiesMessage}`} variant="info" />
              }
              {loading ? (
                <div className="w-100 h-100 d-flex flex-column align-items-center justify-content-center gap-3 p-2 text-center">
                  <UniversityListingCardSkelton />
                </div>
              ) : filteredUniversities && filteredUniversities?.length > 0 ? (
                filteredUniversities?.length <= 2 ? (
                  <>
                    {filteredUniversities.map((university) => (
                      <UniversityListingCard key={uuid()} university={university} />
                    ))}
                    <NoResultFound />
                  </>
                ) : (
                  filteredUniversities.map((university) => <UniversityListingCard key={uuid()} university={university} />)
                )
              ) : (
                <NoResultFound />
              )}
            </div>
          </Col>
        </Row>
        {selectedUniversities.length > 0 ? <CompareSelectedUniversities /> : null}
        <AllUniversityToCompareModal />
        <JourneyRemainingQuestions />
        {/* <FreebieAlertAndModal freebies={freebies} /> */}
      </Container>
    </>
  );
}
