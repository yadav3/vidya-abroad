import React from "react";
import CompareOverview from "../../components/ComparePage/CompareOverview/CompareOverview";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import Image from 'next/image'
import {
  BACKEND_API_URL,
  COUNSELLOR_CLIENT_URL,
  getAllExchangeRates,
  getCompareQueryByUserId,
  getCounsellorCustomerByUuid,
  getToBeCompareUniversities,
} from "../../context/services";
import { useRecoilState } from "recoil";
import {
  allCompareFilteredUniversitiesAtom,
  allCompareUniversitiesAtom,
  compareQueryParamsState,
  selectedSpecializationsState,
  selectedUniversitiesState,
  universityToCompareState,
} from "../../store/compare/compareStore.js";
import AllUniversityToCompareModal from "../../components/ComparePage/AllUniversityToCompareModal/AllUniversityToCompareModal";
import { getUniversitiesByCompareQueries } from "../../services/universitiesService";
import { cvCounsellorDetailsAtom } from "../../store/journeyStore";
import Head from "next/head";
import { useQuery } from "react-query";
import axios from "axios";
import UniversityListingCardSkelton from "../../components/Skeltons/UniversityListingCardSkelton";

import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import UniversityOverviewLoader from "../../components/ComparePage/UniversityOverviewLoader/UniversityOverviewLoader";

export async function getStaticProps(context) {
  let exchangeRates = await getAllExchangeRates();

  return {
    props: {
      exchangeRates: JSON.parse(JSON.stringify(exchangeRates)),
    },
    revalidate: 60,
  };
}

export default function Overview({ exchangeRates }) {
  const router = useRouter();
  const universityIds = [router.query.university];
  const specializationIds = [router.query.specialization];
  const [universityToCompare, setUniversityToCompare] = useRecoilState(universityToCompareState);
  const [userCompareQuery, setUserCompareQuery] = useRecoilState(compareQueryParamsState);
  const [selectedSpecialization, setSelectedSpecialization] = useRecoilState(selectedSpecializationsState);
  const [selectedUniversities, setSelectedUniversities] = useRecoilState(selectedUniversitiesState);
  const [universities, setUniversities] = useRecoilState(allCompareUniversitiesAtom);
  const [filteredUniversities, setFilteredUniversities] = useRecoilState(allCompareFilteredUniversitiesAtom);
  const [cvCounsellorDetails, setCvCounsellorDetails] = useRecoilState(cvCounsellorDetailsAtom);

  const handleCompareNow = () => {
    if (selectedUniversities.length === 0) return;

    const university1 = selectedUniversities?.[0];
    const university2 = selectedUniversities?.[1];
    const university3 = selectedUniversities?.[2];

    const specialization1 = university1?.specializationId;
    const specialization2 = university2?.specializationId;
    const specialization3 = university3?.specializationId;

    const url = `/compare/overview/?${university1 ? "university=" + university1.id : ""}${university2 ? "&university=" + university2.id : ""}${university3 ? "&university=" + university3.id : ""
      }&specialization=${specialization1 ? specialization1 : ""}${specialization2 ? "&specialization=" + specialization2 : ""}${specialization3 ? "&specialization=" + specialization3 : ""
      }&uuid=${router.query.uuid}`;

    window.open(url, "_self");
  };

  const compareParamQuery = useQuery("compareParamQuery", () => getCompareQueryByUserId(router.query.uuid), {
    enabled: router.isReady && router.query.uuid?.length > 0,
    onSuccess: async (data) => {
      setUserCompareQuery({
        ...userCompareQuery,
        uuid: router.query.uuid,
        categoryId: data.category_id,
        courseId: data.course_Id,
        majorId: data.majorId,
        specializationId: data.specialization_id,
        countryId: data.country_id,
        ieltsScore: data.ielts,
        budgetLimit: data.budget,
        questionAnswered: data.answered_questions,
        opportunityId: data.prospect_opportunity_id,
        countries: data?.countries ? [...data.countries] : [],
        relatedProspectId: data.relatedProspectId,
      });
    },
    refetchOnWindowFocus: false,
    refetchOnMount: true,
  });

  const compareUniversitiesQuery = useQuery(
    ["compareUniversitiesQuery", compareParamQuery.data],
    async ({ queryKey }) => {
      const major_id = queryKey[1]?.majorId;
      const country_id = queryKey[1]?.country_id;
      const countries = queryKey[1]?.countries.length > 0 ? queryKey[1]?.countries : [];
      return await getUniversitiesByCompareQueries(major_id, country_id, 0, 0, 0, 0, countries, router.query.uuid);
    },
    {
      enabled: compareParamQuery.isSuccess,
      onSuccess: (data) => {
        setUniversities(data.data.universities);
        setFilteredUniversities(data.data.universities);
      },
      refetchOnMount: false,
      refetchOnWindowFocus: false,
    }
  );

  const counsellorCustomerQuery = useQuery(
    ["counsellorCustomerQuery", compareParamQuery.data],
    async ({ queryKey }) => {
      const uuid = queryKey[1]?.user_id;
      return await getCounsellorCustomerByUuid(uuid);
    },
    {
      enabled: compareParamQuery.isSuccess,
      refetchOnWindowFocus: false,
      onSuccess: (data) => {
        const cvId = data?.cv_id || router.query.uuid;
        setCvCounsellorDetails({
          ...cvCounsellorDetails,
          cv_id: cvId,
          redirectLink: `${COUNSELLOR_CLIENT_URL}/collegevidya/redirect/login?cv_id=${cvId}`,
        });
      },
    }
  );

  const compareUniversities = useQuery(
    ["compareUniversities", universityIds, specializationIds],
    async ({ queryKey }) => {
      const university_ids = queryKey[1].length > 0 ? queryKey[1] : [];
      const specialization_ids = queryKey[2].length > 0 ? queryKey[2] : [];
      return await getToBeCompareUniversities(university_ids, specialization_ids, router.query.uuid);
    },
    {
      enabled: router.isReady && universityIds.length > 0 && specializationIds.length > 0,
      refetchOnMount: false,
      refetchOnWindowFocus: false,
      onSuccess: (data) => {
        setUniversityToCompare(data);
        setSelectedUniversities(data);
        setSelectedSpecialization(specializationIds);
      },
    }
  );

  useEffect(() => {
    console.log(compareUniversitiesQuery, compareUniversities);
  }, []);

  return (
    <>
      <Head>
        <title>Top Study Abroad Universities - Compare Study Abroad Universities</title>
        <meta
          name="description"
          content={`Compare study abroad colleges universities on their fees, courses, approvals, e-learning facility, emi option and other details only at College Vidya Abroad compare portal.`}
        />
        <meta property="og:image" content={"https://collegevidyaabroad.com/logos/cv-abroad-logo.png"} />
        <meta property="twitter:image" content={"https://collegevidyaabroad.com/logos/cv-abroad-logo.png"} />
        <meta property="og:locale" content="en_US" />
        <link href={`https://collegevidyaabroad.com/compare/universities`} rel="canonical" />

        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: "https://collegevidyaabroad.com",
              name: `Compare Popular Study Abroad Colleges and Universities`,
              description:
                "Compare study abroad colleges universities on their fees, courses, approvals, e-learning facility, emi option and other details only at College Vidya Abroad compare portal.",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: [".seotag"],
              },
            }),
          }}
        />

        {/* Address Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya Abroad",
              url: "https://collegevidyaabroad.com/",
              sameAs: [
                "https://facebook.com/collegevidyaabroad",
                "https://twitter.com/collegevidyaabroad",
                "https://www.linkedin.com/company/collegevidyaabroad",
                "https://www.instagram.com/collegevidyaabroad",
              ],
              logo: "https://collegevidyaabroad.com/logos/cv-abroad-logo.png",
              legalName: "College Vidya Abroad",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18003099018",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />
      </Head>
      {compareUniversitiesQuery.isLoading || compareUniversities.isLoading ? <Container>
        <Row >
          <Col>
            <div className="d-flex flex-column align-items-center justify-content-center"
              style={{
              height: '80vh'
            }}
            >
              <UniversityOverviewLoader />
            </div>
          </Col>
        </Row>
      </Container> : <CompareOverview exchangeRates={exchangeRates} />}
      <AllUniversityToCompareModal isLoading={compareUniversitiesQuery.isLoading} onClose={handleCompareNow} />
    </>
  );
}
