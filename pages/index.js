import React from "react";
import HomeFormContainer from "../components/global/HomeFormContainer/HomeFormContainer";
import CountryAccordion from "../components/CountryAccordion/CountryAccordion";
import HomeHeroCards from "../components/Home/HomeHeroCards/HomeHeroCards";
import HomeFaq from "../components/Home/HomeFaq/HomeFaq";
import HomeUniversitiesSlider from "../components/Home/HomeUniversitiesSlider/HomeUniversitiesSlider";
import Testimonials from "../components/global/Testimonials/Testimonials";
import { getTestimonials } from "../context/services";
import HomeHeroVideo from "../components/Home/HomeHeroVideo/HomeHeroVideo";
import { getMajorWithSpecialization, getMajors } from "../services/specializations.service";
import Head from "next/head";
import { getFAQSchema } from "../utils/misc";
import SectionHeading from "../components/global/SectionHeading/SectionHeading";
import HomePlaneJourney from "../components/Home/HomePlaneJourney/HomePlaneJourney";
import { FooterDivider } from "../components/global/FooterContainer/FooterDivider";
import ToastyCharacter from "../components/global/ToastyCharacter/ToastyCharacter";
import { getPopularUniversitiesLogos } from "../services/universitiesService";
import HomeTopCTASection from "../components/Home/HomeTopCTASection/HomeTopCTASection";
import HomeMajorBrowser from "../components/Home/HomeMajorBrowser/HomeMajorBrowser";
import HomeAdVideoSection from "../components/Home/HomeAdVideoSection/HomeAdVideoSection";
import { getAllVideos } from "../services/videoLibrary.service";
import VideoLibrarySwiper from "../components/VideoLIbrary/VideoLibrarySwiper/VideoLibrarySwiper";
import { getCountriesNames } from "../services/country.service";
import { getAllCourseCategories } from "../services/course.service";
import QuickCompareStrip from "../components/global/QuickCompareStrip/QuickCompareStrip";


export async function getStaticProps(context) {
  let testimonials = await getTestimonials();
  let majors = await getMajorWithSpecialization()
  let universitiesLogos = await getPopularUniversitiesLogos(30)
  let youtubeVideos = await getAllVideos(1, 10)
  let countries = await getCountriesNames()
  let courseCategories = await getAllCourseCategories()

  return {
    props: {
      testimonials: testimonials,
      majors: majors,
      universitiesLogos: universitiesLogos,
      youtubeVideos: youtubeVideos,
      countries: countries,
      courseCategories: courseCategories
    },
    revalidate: 60,
  };
}

export default function Home({ majors, testimonials, universitiesLogos, youtubeVideos, countries, courseCategories }) {

  const faqData = [
    {
      question: "Why us?",
      answer_html: `<p>We are CollegeVidya (Study Abroad). And through our Interactive Platform,
     we provide you with a portal to compare universities of your choice and courses of 
     your interest on an international scale. </p>
     <div>Moreover, we let you analyze and compare the best return on investment for 
     the time, money, and efforts you are willing to put into your dream of studying 
     abroad. Likewise, our experts are here to help and guide you with all of your 
     doubts, just like a good friend would.</div>`,
    },
    {
      question: "Why Study Abroad?",
      answer_html: `<p>Your study abroad journey enables you to get a great deal of exposure as
     well as experience which also stimulates you to bloom in your career, now with
      your reasonable/better perception of the world.</p>
      <div>Furthermore, you can acquire new languages, admire other cultures, withstand
       the challenges of residency in another country and acquire a vast insight
        into the world. In today's world, almost all contemporary businesses look 
        for these aspects when hiring, and such qualities are only going to become 
        more significant in the future.</div>`,
    },
    {
      question: "What documents are required to study abroad?",
      answer_html: `
    <p>You have to be prepared with some important documents to fulfil your dream of studying abroad. And to make it easier for you we have prepared a list of all the documents required for the same, have a look.</p>
     <ul>
     <li><b>Application Form</b></li>
     <li><b>Statement of Purpose (SOP)</b></li>
     <li><b>Academic transcripts</b></li>
     <li><b>Letter of Recommendation (LOR)</b></li>
     <li><b>Curriculum Vitae (CV) or Resume</b></li>
     <li><b>Test Scores</b></li>
     <li><b>Essays</b></li>
   
     </ul>
    `,
    },
    {
      question: "How does studying abroad help your career?",
      answer_html: `<p>Studying abroad proposes an enormous exhibition of new alternatives by pushing you out of your comfort zone. It also improves your communication skills which is an important aspect in today's world and will help you in building other elements of your communication skills, incorporating public speaking and presenting, and reconciling academic writing, and non-verbal communication. Plus, you can also demonstrate to future and current employers that you have the open mind, resourcefulness, and momentum required to adapt to a distinct setting.</p>`,
    },
  ]

  return (
    <>
      <Head>
        <title>College Vidya Abroad - Study at your dream destination</title>
        <meta
          name="description"
          content={`Explore, choose, and compare the best study abroad universities, with AI-based tools at College Vidya Abroad and book your free personalized counseling session now.`}
        />
        <meta property="og:image" content={"https://collegevidyaabroad.com/banners/mobile-herobanner-2023.jpg"} />
        <meta property="twitter:image" content={"https://collegevidyaabroad.com/banners/mobile-herobanner-2023.jpg"} />
        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta
          property="og:title"
          content={`College Vidya Abroad - Study Abroad at your dream destination`}
        />
        <meta
          property="og:description"
          content="Explore, choose, and compare the best study abroad universities, with AI-based tools at College Vidya Abroad and book your free personalized counseling session now."
        />
        <meta property="og:url" content="https://collegevidyaabroad.com/" />
        <meta property="og:site_name" content="College Vidya Abroad" />
        <meta
          property="og:article:publisher"
          content="https://www.facebook.com/collegevidyaabroad"
        />
        <meta property="twitter:card" content="summary" />
        <meta property="twitter:site" content="@CollegeVidyaAbroad" />
        <meta
          property="twitter:title"
          content={`College Vidya Abroad - Study Abroad at your dream destination`}
        />
        <meta
          property="twitter:description"
          content="Explore, choose, and compare the best study abroad universities, with AI-based tools at College Vidya Abroad and book your free personalized counseling session now."
        />
        <meta property="twitter:url" content="https://collegevidyaabroad.com/" />
        <link href={`https://collegevidyaabroad.com/`} rel="canonical" />


        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "webpage",
              url: "https://collegevidyaabroad.com",
              name: `College Vidya Abroad - Study Abroad at your dream destination`,
              description:
                "Explore, choose, and compare the best study abroad universities, with AI-based tools at College Vidya Abroad and book your free personalized counseling session now.",
              speakable: {
                "@type": "SpeakableSpecification",
                cssSelector: [".seotag"],
              },
            }),
          }}
        />

        {/* Address Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify({
              "@context": "http://schema.org",
              "@type": "Organization",
              name: "College Vidya Abroad",
              url: "https://collegevidyaabroad.com/",
              sameAs: [
                "https://facebook.com/collegevidyaabroad",
                "https://twitter.com/collegevidyaabroad",
                "https://www.linkedin.com/company/collegevidyaabroad",
                "https://www.instagram.com/collegevidyaabroad",
              ],
              logo: "https://collegevidyaabroad.com/logos/cv-abroad-logo.png",
              legalName: "College Vidya Abroad",
              address: {
                "@type": "PostalAddress",
                addressCountry: "India",
                addressLocality: "",
                addressRegion: "",
                postalCode: "",
                streetAddress: ",",
              },
              contactPoint: {
                "@type": "ContactPoint",
                telephone: "18003099018",
                contactType: "Customer Service",
                contactOption: "TollFree",
                areaServed: ["IN"],
              },
            }),
          }}
        />

        {/* FAQ Schema */}
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{
            __html: JSON.stringify(getFAQSchema(faqData)),
          }}
        />
      </Head>
      <HomeHeroVideo />
      <QuickCompareStrip  majors={majors} countries={countries} courseCategories={courseCategories} />
      <HomeTopCTASection />
      {/* <FooterDivider className="my-4 w-75 mx-auto" /> */}
      {/* <HomeMajorBrowser majors={majors} /> */}
      <div className={`countryAccordionContainer`}>
        <SectionHeading heading={`<span>
          Unlock a <span class="text-cvblue">World of Options</span>
      </span>`}
          subHeading={"Discover, Compare, and Select Your Dream University Abroad!"}
        />
        <CountryAccordion />
      </div>
      <HomeHeroCards />
      <HomePlaneJourney />
      <FooterDivider className={"mt-5 pt-3"} />
      <HomeAdVideoSection />
      <FooterDivider className={""} />
      <VideoLibrarySwiper youtubeVideos={youtubeVideos} />
      <HomeFormContainer majors={majors} />
      <Testimonials testimonials={testimonials} />
      <HomeUniversitiesSlider universitiesLogos={universitiesLogos} />
      <HomeFaq />
      {/* <ToastyCharacter/> */}
    </>
  );
}
