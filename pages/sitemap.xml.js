import axios from "axios";
import Blog from "../services/blog.service";
import { BACKEND_API_URL } from "../context/services";


function generateSiteMap(blogSlugs, blogCategorySlugs, universitiesSlugs, countrySlugs) {
    return `<?xml version="1.0" encoding="UTF-8"?>
            <urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">
                <url>
                    <loc>https://collegevidyaabroad.com/blog/</loc>
                </url>
                <url>
                    <loc>https://collegevidyaabroad.com/universities/</loc>
                </url>
                <url>
                    <loc>https://collegevidyaabroad.com/experts/</loc>
                </url>
                <url>
                    <loc>https://collegevidyaabroad.com/suggest-me-a-university/</loc>
                </url>
                <url>
                    <loc>https://collegevidyaabroad.com/ai-powered-faqs/</loc>
                </url>
                ${blogSlugs
            .map((slug) => {
                return `<url>
                                <loc>https://collegevidyaabroad.com/blog/${slug.slug}/</loc>
                            </url>`;
            })
            .join("")}
                ${blogCategorySlugs
            .map((slug) => {
                return `<url>
                                <loc>https://collegevidyaabroad.com/blog/categories/${slug.slug}/</loc>
                            </url>`;
            })
            .join("")}
                ${universitiesSlugs
            .map((slug) => {
                return `<url>
                                <loc>https://collegevidyaabroad.com/universities/${slug.slug}/</loc>
                            </url>`;
            })
            .join("")}
                ${countrySlugs
            .map((slug) => {
                return `<url>
                                <loc>https://collegevidyaabroad.com/countries/${slug.slug}/</loc>
                            </url>`;
            })
            .join("")}
            </urlset>`;
}

function Sitemap() { }

const blogService = new Blog()

export async function getServerSideProps({ res }) {
    const blogSlugs = await blogService.getAllPostSlugs();
    const blogCategorySlugs = await blogService.getAllCategoriesSlugs();
    const universitiesSlugs = await axios.get(`${BACKEND_API_URL}/universities/slugs`).then((res) => res.data);
    const countrySlugs = await axios.get(`${BACKEND_API_URL}/countries/slugs`).then((res) => res.data);



    const sitemap = generateSiteMap(blogSlugs, blogCategorySlugs, universitiesSlugs, countrySlugs);

    res.setHeader("Content-Type", "text/xml");
    // we send the XML to the browser
    res.write(sitemap);
    res.end();

    return {
        props: {},
    };
}

export default Sitemap;
