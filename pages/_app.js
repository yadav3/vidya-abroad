import "../styles/globals.scss";
import "primereact/resources/themes/bootstrap4-light-blue/theme.css"; //theme
import "primereact/resources/primereact.min.css"; //core css
import "primeicons/primeicons.css";
import "bootstrap/dist/css/bootstrap.min.css";
import "react-tippy/dist/tippy.css";
import Layout from "../components/Layout/Layout";
import { StateContext } from "../context/StateContext";
import { Toaster } from "react-hot-toast";
import { useEffect, useLayoutEffect } from "react";
import Login from "../components/Auth/Login";
import { useRouter } from "next/router";
import BottomNav from "../components/global/BottomNav/BottomNav";
import { SSRProvider } from "react-bootstrap";
import { deleteCookie, setCookie } from "cookies-next";
import TagManager from "react-gtm-module-defer";
import { RecoilRoot } from "recoil";
import Script from "next/script";
import { QueryClient, QueryClientProvider } from "react-query";
import ToastyCharacter from "../components/global/ToastyCharacter/ToastyCharacter";
import FooterNew from "../components/global/FooterContainer/FooterNew";
import Header from "../components/HeaderNew/Header";
const queryClient = new QueryClient();

function MyApp({ Component, pageProps }) {
  useEffect(() => {
    TagManager.initialize({ gtmId: "GTM-KD5JDNF" });
  }, []);

  const router = useRouter();

  const excludedPathsHeader = ["home", "book-a-free-call", "suggest-me-a-university"];
  const excludedPathsFooter = ["ai-powered-faqs", "book-a-free-call", "suggest-me-a-university", "services"];

  const shouldRenderHeader = () => {

    if (!excludedPathsHeader.some(path => router.pathname.includes(path))) {
      return <Header />;
    }

  }


  useLayoutEffect(() => {
    if (router.isReady) {
      if (router.query.SourceCampaign) {
        const sourceCampaign = router.query.SourceCampaign;
        setCookie("source_campaign", sourceCampaign);
      }
      if (router.query.utm_campaign) {
        const campaign_name = router.query.utm_campaign;
        const ad_group = router.query.utm_medium;
        const ads = router.query.utm_content;
        const sourcecampaign = router.query.utm_source;

        setCookie("campaign_name", campaign_name);
        setCookie("ad_group_name", ad_group);
        setCookie("ads_name", ads);
        setCookie("source_campaign", sourcecampaign);
      } else if (router.query.campaign) {
        const campaign_name = router.query.campaign;
        const ad_group = router.query.adgroup;
        const ads = router.query.keyword;
        const sourcecampaign = router.query.medium;

        setCookie("campaign_name", campaign_name);
        setCookie("ad_group_name", ad_group);
        setCookie("ads_name", ads);
        setCookie("source_campaign", sourcecampaign);
      }
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [router.isReady]);

  useEffect(() => {
    if (router.isReady) {
      const allImages = document.querySelectorAll("img");
      const imagesWithoutAlt = [...allImages].filter((img) =>
        !img.hasAttribute('alt') || img.getAttribute('alt') === ''
      );
      if (imagesWithoutAlt.length > 0) {
        imagesWithoutAlt.forEach((image) => {
          let imageName = image.src.replace(/^.*[\\\/]/, "");
          if (imageName.includes(".")) {
            imageName = imageName.split(".")[0];
          }
          image.setAttribute("alt", imageName.replaceAll("-", " "));
        }
        );
      }
    }
  }, [router.isReady]);


  return (
    <SSRProvider>
      {/* <Script defer strategy="afterInteractive" src="https://www.googletagmanager.com/gtag/js?id=AW-10855552401" /> */}
      <Script
        id="gtm-tag"
        defer
        strategy="afterInteractive"
        dangerouslySetInnerHTML={{
          __html: `(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KD5JDNF')`,
        }}
      />
      <Script
        id="gtm-final"
        defer
        strategy="afterInteractive"
        dangerouslySetInnerHTML={{
          __html: `window.dataLayer = window.dataLayer || []; function gtag()
            {dataLayer.push(arguments)}
            gtag('js', new Date());`,
        }}
      />
      <QueryClientProvider client={queryClient}>
        <RecoilRoot>
          <StateContext>
            <div className="global-web-container">
              {!router.pathname.includes("home") && !router.pathname.includes("book-a-free-call") && !router.pathname.includes("suggest-me-a-university") && <Header />}
              <Layout>
                <Component {...pageProps} />
                {!router.pathname.includes("book-a-free-call") && !router.pathname.includes("services") && !router.pathname.includes("suggest-me-a-university")
                  && !router.pathname.includes("ai-powered-faqs")
                  && <FooterNew />}
                <Login />
                <Toaster />
              </Layout>
            </div>
            {/* <ToastyCharacter /> */}
            {!router.pathname.includes("compare") && !router.pathname.includes("suggest-me-a-university") && <BottomNav />}
          </StateContext>
        </RecoilRoot>
      </QueryClientProvider>
    </SSRProvider>
  );
}

export default MyApp;
