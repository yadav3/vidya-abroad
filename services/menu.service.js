import axios from "axios";
import { BACKEND_API_URL } from "../context/services";

export const getMenuData = async () => {
    try {
        const res = await axios(`${BACKEND_API_URL}/main_menu_data`);
        const data = await res.data
        return data;
    } catch (error) {
        console.error(error);
    }
}