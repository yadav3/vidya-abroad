import axios from "axios";
import { BACKEND_API_URL } from "../context/services";

let cancelToken;
export async function getFaqAutoComplete(search) {
    if (typeof cancelToken != typeof undefined) {
        cancelToken.cancel("Operation cancelled due to new request.");
    }

    cancelToken = axios.CancelToken.source();

    try {
        const response = await axios.get(`${BACKEND_API_URL}/faq/get_autocomplete?search=${search}`, { cancelToken: cancelToken.token })
        return response.data;
    } catch (error) {
        console.error(error);
    }
}
