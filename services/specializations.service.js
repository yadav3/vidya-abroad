import axios from "axios";
import { BACKEND_API_URL } from "../context/services";

export async function getSpecializations(courseId, categoryId) {
  try {
    if (courseId && categoryId) {
      return await axios.get(BACKEND_API_URL + `/specializations?courseId=${courseId}&categoryId=${categoryId}`);
    }

    if (courseId && !categoryId) {
      return await axios.get(BACKEND_API_URL + `/specializations?courseId=${courseId}`);
    }
    // if category id is not null, then get specializations for that category
    if (!courseId && categoryId) {
      return await axios.get(BACKEND_API_URL + `/specializations?categoryId=${categoryId}`);
    }
    // if both are null, then get all specializations
    return await axios.get(BACKEND_API_URL + `/specializations`);
  } catch (error) {
    console.error(error);
    // return error;
  }
}

export async function getMajors(categoryId) {
  try {
    let majors = [];

    if (categoryId) {
      const response = await axios.get(BACKEND_API_URL + `/majors?categoryId=${categoryId}`);
      majors = response.data;
    } else {
      const response = await axios.get(BACKEND_API_URL + `/majors`);
      majors = response.data;
    }

    return majors;
  } catch (error) {
    console.error(error);
    // return error;
  }
}
export async function getMajorDetailsById(majorId) {
  try {
    const response = await axios.get(BACKEND_API_URL + `/major/${majorId}`);
    return response.data;
  } catch (error) {
    console.error(error);
    // return error;
  }
}
export async function getMajorWithSpecialization() {
  try {
    const response = await axios.get(BACKEND_API_URL + `/majors/with/specialization`);
    return response.data;
  } catch (error) {
    console.error(error);
    // return error;
  }
}
