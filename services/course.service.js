import axios from "axios";
import { BACKEND_API_URL } from "../context/services";

export async function getCourses() {
  try {
    const courses = await axios.get(BACKEND_API_URL + "/courses");
    return courses.data;
  } catch (error) {
    console.error(error);
    // return error;
  }
}
export async function getAllCourseCategories() {
  try {
    const categories = await axios.get(BACKEND_API_URL + "/course-categories");
    return categories.data;
  } catch (error) {
    console.error(error);
    // return error;
  }
}
