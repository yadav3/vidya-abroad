import axios from "axios";
import { BACKEND_API_URL } from "../context/services";

export async function getCountriesNames() {
  try {
    const countries = await axios.get(BACKEND_API_URL + "/countries/names");
    return countries.data;
  } catch (error) { }
}


export async function getCountries() {
  try {
    const countries = await axios.get(BACKEND_API_URL + "/countries");
    return countries.data;
  } catch (error) { }
}

export async function getCountryFaqPageData() {
  try {
    const countries = await axios.get(BACKEND_API_URL + "/countries/faq_page_data");
    return countries.data;
  } catch (error) { }
}

