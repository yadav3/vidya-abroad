import axios from "axios";

import { BACKEND_API_URL } from "../context/services";

export async function updateLeadFields(uuid, fields) {
    try {
        await axios.patch(BACKEND_API_URL + `/lead/update/new?uuid=${uuid}`, fields);
    } catch (error) {
        console.error(error);
    }
}

export async function createLsqOpportunity(
    name,
    email,
    phone,
    budget,
    country,
    program,
    major,
    source,
    sub_source,
    source_campaign,
    campaign_name,
    ad_group,
    ad_name,
    userId
) {
    try {
        const data = await axios.post(BACKEND_API_URL + `/lsq/opportunity/create`, {
            userId: userId,
            firstName: name,
            lastName: "",
            email: email,
            phone: phone,
            budget: budget,
            country: country,
            program: program,
            major: major,
            source: source,
            sub_source: sub_source,
            source_campaign: source_campaign,
            campaign_name: campaign_name,
            ad_group: ad_group,
            ad_name: ad_name,
        });
        return data.data;
    } catch (error) {
        console.error(error);
    }
}

export async function updatedLsqOpportunity(
    currentlyWorking,
    languageTest,
    highestQualification,
    highestQualificationScore,
    studyFund,
    opportunityId
) {
    try {
        const data = await axios.post(BACKEND_API_URL + `/lsq/opportunity/update`, {
            currently_working: currentlyWorking,
            language_test: languageTest,
            highest_qualification: highestQualification,
            highest_qualification_score: highestQualificationScore,
            study_fund: studyFund,
            opportunityId: opportunityId,
        });
        return data.data;
    } catch (error) {
        console.error(error);
    }
}
export async function updatedLsqLeadDetails(uuid, payload) {
    try {
        const response = await axios.post(
            `${BACKEND_API_URL}/lsq/lead/update?uuid=${uuid}`,
            payload,
            {
                headers: {
                    "Content-Type": "application/json",
                },
            }
        );
        return response.data;
    } catch (error) {
        console.error(error);
    }
}
