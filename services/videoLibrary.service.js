import axios from "axios";
import { BACKEND_API_URL } from "../context/services";

export async function getAllVideos(page = 1, limit = 10, sort = "id", order = "asc", categoryId, authorId, search = "") {
  try {
    const videos = await axios.get(
      BACKEND_API_URL +
        `/videos/all?page=${page}&limit=${limit}&sort=${sort}&order=${order}&categoryId=${categoryId}&authorId=${authorId}&search=${search}`
    );

    return videos.data;
  } catch (error) {
    console.error(error);
  }
}

export async function getAllCategories(limit = 5, sort = "id", order = "asc") {
  try {
    const videos = await axios.get(BACKEND_API_URL + `/videos/categories?limit=${limit}&sort=${sort}&order=${order}`);

    return videos.data;
  } catch (error) {
    console.error(error);
  }
}

export async function getAllAuthors(limit = 10, sort = "id", order = "asc") {
  try {
    const authors = await axios.get(BACKEND_API_URL + `/videos/authors?limit=${limit}&sort=${sort}&order=${order}`);

    return authors.data;
  } catch (error) {
    console.error(error);
  }
}
