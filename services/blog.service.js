// create a blog service class to call the API with axios
import axios from "axios";
import { BACKEND_API_URL } from "../context/services";

class Blog {
  constructor() {
    this.service = axios.create({
      baseURL: BACKEND_API_URL,
    });
  }

  getBlogPosts = async (categorySlug = "all", page = 1, limit = 9) => {
    try {
      const response = await this.service.get(`/blog/posts?page=${page}&categorySlug=${categorySlug}&limit=${limit}`);
      return response.data;
    } catch (error) {
      console.error(error);
    }
  };

  getRelatedPosts = async (slug, limit) => {
    try {
      const response = await this.service.get(`blog/posts/get-related-by-slug?slug=${slug}&limit=${limit}`);
      return response.data;
    } catch (error) {
      console.error(error);
    }
  };

  getPostBySearch = async (searchTerm, limit = 9) => {
    try {
      const response = await this.service.get(`/blog/posts/get-posts-by-search?limit=${limit}&search=${searchTerm}`);
      return response.data;
    } catch (error) {
      console.error(error);
    }
  };

  getPostBySlug = async (slug) => {
    try {
      const response = await this.service.get(`/blog/post/${slug}`);
      return response.data;
    } catch (error) {
      console.error(error);
    }
  };

  getAllPostSlugs = async () => {
    try {
      const response = await this.service.get("/blog/posts/slugs");
      return response.data;
    } catch (error) {
      console.error(error);
    }
  };
  getLatestPosts = async () => {
    try {
      const response = await this.service.get("/blog/posts/latest");
      return response.data;
    } catch (error) {
      console.error(error);
    }
  };
  getAllCategoriesSlugs = async () => {
    try {
      const response = await this.service.get("/blog/categories/slugs");
      return response.data;
    } catch (error) {
      console.error(error);
    }
  };
  getAllCategories = async () => {
    try {
      const response = await this.service.get("/blog/categories");
      return response.data;
    } catch (error) {
      console.error(error);
    }
  };

  addBlogPostView = async (slug) => {
    try {
      const response = await this.service.post(`/blog/add_post_view/${slug}`);
      return response.data;
    } catch (error) {
      console.error(error);
    }
  };
}

export default Blog;
