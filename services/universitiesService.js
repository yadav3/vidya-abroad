import axios from "axios";
import { BACKEND_API_URL } from "../context/services";

export async function getUniversitiesByCourseId(courseId) {
  try {
    const universities = axios.get(BACKEND_API_URL + `/universities/${courseId}`);
    return universities;
  } catch (error) {
    console.error(error);
  }
}
export async function getUniversitiesByCompareQueries(majorId, countryId, ieltsScore, budgetLimit, budgetLow, budgetHigh, countries, userId = null) {
  try {
    const universities = axios.get(
      BACKEND_API_URL +
      `/universities/compare?majorId=${majorId}&countryId=${countryId}&ieltsScore=${ieltsScore}&budgetLimit=${budgetLimit}&budgetLow=${budgetLow}&budgetHigh=${budgetHigh}&countries=${countries}&userId=${userId}`
    );
    return universities;
  } catch (error) {
    console.error(error);
  }
}

export async function getAllUniversities(page = 1, limit = 10, sort = "country_id", order = "asc", countries = [], budgetLow = 0, budgetHigh = 0, ielts = 10, search = "") {
  try {
    const universities = await axios.get(
      BACKEND_API_URL +
      `/universities/all?page=${page}&limit=${limit}&sort=${sort}&order=${order}&countries=${countries}&budgetLow=${budgetLow}&budgetHigh=${budgetHigh}&ielts=${ielts}&search=${search}`
    );
    return universities.data;
  } catch (error) {
    console.error(error);
  }
}

export async function getUniversityDetailsBySlug(slug) {
  try {
    const universities = await axios.get(
      BACKEND_API_URL +
      `/universities/details/${slug}`
    );
    return universities.data;
  } catch (error) {
    console.error(error);
  }
}

export async function getPopularUniversitiesLogos(limit) {
  try {
    const universities = await axios.get(
      BACKEND_API_URL +
      `/universities/popular/logos?limit=${limit}`
    );
    return universities.data;
  } catch (error) {
    console.error(error);
  }
}
