import axios from "axios";
import { BACKEND_API_URL } from "../context/services";

export async function updateUserCompareQueryFields(uuid, fields) {
    try {
        await axios.patch(BACKEND_API_URL + `/user/compare/results?uuid=${uuid}`, {
            fields: fields,
        });
    } catch (error) {
        console.error(error);
    }
}