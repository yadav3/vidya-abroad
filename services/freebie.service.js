// create a blog service class to call the API with axios
import axios from "axios";
import { BACKEND_API_URL } from "../context/services";

class FreebieService {
  constructor() {
    this.service = axios.create({
      baseURL: BACKEND_API_URL,
    });
  }

  getAllFreebieSlugs = async () => {
    try {
      const response = await this.service.get(`/freebie/slugs`);
      return response.data;
    } catch (error) {
      console.error(error);
    }
  };
  getAllFreebies = async () => {
    try {
      const response = await this.service.get(`/freebie/all`);
      return response.data;
    } catch (error) {
      console.error(error);
    }
  };
  getFreebieDataBySlug = async (slug) => {
    try {
      const response = await this.service.get(`/freebie/data/${slug}`);
      return response.data;
    } catch (error) {
      console.error(error);
    }
  };
  getRelatedFreebiesBySlug = async (slug) => {
    try {
      const response = await this.service.get(`/freebie/related/by/${slug}`);
      return response.data;
    } catch (error) {
      console.error(error);
    }
  };
  addDownloadCountToFreebie = async (id) => {
    try {
      const response = await this.service.post(`/freebie/count/download?id=${id}`);
      return response.data;
    } catch (error) {
      console.error(error);
    }
  };
}


export default new FreebieService()