import { atom } from "recoil";

export const coursesAtom = atom({
  key: "coursesAtom",
  default: {
    bachelors: [],
    masters: [],
  },
});
export const specializationsAtom = atom({
  key: "specializationsAtom",
  default: {},
});
