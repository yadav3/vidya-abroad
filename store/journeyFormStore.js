import { atom } from "recoil";

export const journeyFormAtom = atom({
    key: "journeyFormAtom",
    default: {
        categoryId: null,
        majorId: null,
        countryId: null,
        budget: null,
    },
});
export const journeyFormStepAtom = atom({
    key: "journeyFormStepAtom",
    default: 1,
});


export const journeyFormStaticQuestionAtom = atom({
    key: "journeyFormStaticQuestionAtom",
    default: [
        {
            id: 1,
            key: "degree",
            question: "What degree are you interested in?",
            type: "radio",
            name: "categoryId",
            placeholder: "Select a degree",
            error: {
                required: true,
                message: "Please select a degree",
                pattern: "",
            },
            options: [
                {
                    id: 1,
                    key: "bachelors",
                    value: "Bachelors",
                    label: "Bachelors",
                },
                {
                    id: 2,
                    key: "masters",
                    value: "Masters",
                    label: "Masters",
                },
                {
                    id: 3,
                    key: "diploma",
                    value: "Diploma",
                    label: "Diploma",
                },
            ],
        },
        {
            id: 2,
            key: "major",
            question: "What major are you interested in?",
            type: "radio",
            name: "majorId",
            placeholder: "Select a major",
            error: {
                required: true,
                message: "Please select a major",
                pattern: "",
            },
            options: [
                {
                    id: 1,
                    key: "computer-science",
                    value: "Computer Science",
                    label: "Computer Science",
                },
                {
                    id: 2,
                    key: "business",
                    value: "Business",
                    label: "Business",
                },
            ],
        },
        {
            id: 3,
            key: "country",
            question: "What country are you interested in?",
            type: "radio",
            name: "countryId",
            placeholder: "Select a country",
            error: {
                required: true,
                message: "Please select a country",
                pattern: "",
            },
            options: [
                {
                    id: 1,
                    key: "usa",
                    value: "USA",
                    label: "USA",
                },
                {
                    id: 2,
                    key: "canada",
                    value: "Canada",
                    label: "Canada",
                },
            ],
        },
        // {
        //     id: 4,
        //     key: "budget",
        //     question: "What is your budget?",
        //     type: "radio",
        //     name: "budget",
        //     placeholder: "Select a budget",
        //     error: {
        //         required: true,
        //         message: "Please select a budget",
        //         pattern: "",
        //     },
        //     options: [
        //         {
        //             id: 1,
        //             key: "10L-20L",
        //             value: "10L - 20L",
        //             label: "10 Lakh to 20 Lakh",
        //         },
        //         {
        //             id: 2,
        //             key: "20L-30L",
        //             value: "20L - 30L",
        //             label: "20 Lakh to 30 Lakh",
        //         },
        //         {
        //             id: 3,
        //             key: "30L-40L",
        //             value: "30L - 40L",
        //             label: "30 Lakh to 40 Lakh",
        //         },
        //         {
        //             id: 4,
        //             key: "40L-50L",
        //             value: "40L - 50L",
        //             label: "40 Lakh to 50 Lakh",
        //         },
        //         {
        //             id: 5,
        //             key: "more-than-50l",
        //             value: "More than 50L",
        //             label: "More than 50L",
        //         },
        //         {
        //             id: 6,
        //             key: "not-decided-yet",
        //             value: "Not Decided Yet",
        //             label: "Not Decided Yet",
        //         },
        //     ],
        // },
        {
            id: 5,
            key: "lead-form",
            question: "Our A.I has curated perfect Universities and courses just for you!",
            type: "lead-form",
            name: "lead-form",
            placeholder: "",
            error: {
                required: true,
                message: "Please fill in your details",
                pattern: "",
            },
            options: [],
        }
    ],
});


export const JourneyRemainingQuestionModalAtom = atom({
    key: "JourneyRemainingQuestionModalAtom",
    default: false,
});

export const JourneyRemainingQuestionModalStateAtom = atom({
    key: "JourneyRemainingQuestionModalStateAtom",
    default: {
        source: null,
        onClose: () => { },
        submitButtonText: "Compare Now"
    },
});