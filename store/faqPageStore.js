import { atom } from "recoil";


export const faqSearchAtom = atom({
    key: "faqSearchAtom",
    default: ""
});
export const faqDebouncedSearchAtom = atom({
    key: "faqDebouncedSearchAtom",
    default: ""
});

export const faqSelectedFaqAtom = atom({
    key: "faqSelectedFaqAtom",
    default: null
});
