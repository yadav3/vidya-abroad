import { atom } from "recoil";

export const selectedUniversityModal = atom({
  key: "selectedUniversityModal",
  default: false,
});
export const allUniversitiesState = atom({
  key: "allUniversitiesState",
  default: [],
});

export const selectedUniversitiesState = atom({
  key: "selectedUniversitiesState",
  default: [],
});

export const universityToCompareState = atom({
  key: "universityToCompareState",
  default: [],
});

export const compareQueryParamsState = atom({
  key: "compareQueryParamsState",
  default: {
    uuid: null,
    categoryId: null,
    courseId: null,
    majorId: null,
    specializationId: null,
    countryId: null,
    ieltsScore: null,
    budgetLimit: null,
    questionAnswered: 0,
    opportunityId: null,
    budgetLow: null,
    budgetHigh: null,
    countries: [],
    relatedProspectId: null
  },
});

export const compareFiltersAtom = atom({
  key: "compareFiltersAtom",
  default: {
    showInINR: true,
    countryId: null,
    duration: null,
    ieltsScore: null,
    budgetLimit: null,
  },
});

export const selectedSpecializationsState = atom({
  key: "selectedSpecializationsState",
  default: [],
});

export const allUniversityToCompareModalAtom = atom({
  key: "allUniversityToCompareModalAtom",
  default: false,
});
export const allCompareUniversitiesAtom = atom({
  key: "allCompareUniversitiesAtom",
  default: [],
});
export const allCompareFilteredUniversitiesAtom = atom({
  key: "allCompareFilteredUniversitiesAtom",
  default: [],
});
