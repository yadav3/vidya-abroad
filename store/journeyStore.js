import { atom } from "recoil";

export const selectedCategoryAtom = atom({
  key: "selectedCategoryAtom",
  default: null,
});
export const selectedCourseAtom = atom({
  key: "selectedCourseAtom",
  default: null,
});
export const selectedSpecializationAtom = atom({
  key: "selectedSpecializationAtom",
  default: null,
});

export const journeyDetailsAtom = atom({
  key: "journeyDetailsAtom",
  default: {
    categoryId: null,
    courseId: null,
    specializationId: null,
    majorId: null,
    countryId: null,
    ielts: null,
    budget: null,
  },
});

export const cvCounsellorDetailsAtom = atom({
  key: "cvCounsellorDetailsAtom",
  default: {
    cv_id: null,
    redirectLink: null,
  },
});
