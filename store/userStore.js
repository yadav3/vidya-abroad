import { atom } from "recoil";

export const userCompareQueryAtom = atom({
  key: "userCompareQueryAtom",
  default: {},
});
