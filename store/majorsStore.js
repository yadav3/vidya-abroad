import { atom } from "recoil";

export const majorsAtom = atom({
  key: "majorsAtom",
  default: [],
});
export const selectedMajorsAtom = atom({
  key: "selectedMajorsAtom",
  default: [],
});
