/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  swcMinify: false,
  images: {
    domains: ["collegevidyaabroad.s3.ap-south-1.amazonaws.com", "d6wn6379hq47k.cloudfront.net"],
  },
  trailingSlash: true,
  async headers() {
    return [
      {
        source: "/:path*",
        locale: false,
        headers: [
          {
            key: "X-Frame-Options",
            value: "DENY",
          },
        ],
      },
    ];
  },
};
//

const withBundleAnalyzer = require("@next/bundle-analyzer")({
  enabled: process.env.ANALYZE === "true",
});

module.exports = withBundleAnalyzer(nextConfig);
